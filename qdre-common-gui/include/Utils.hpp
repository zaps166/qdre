/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <CommonGuiVisibility.hpp>

#include <QDrag>

class QAbstractButton;
class QWindow;
class QScreen;

namespace QDRE {
namespace Utils {

QDRE_COMMON_GUI_VISIBILITY QRect mapScreenGeometry(const QRect &geometry, const qreal scale);
QDRE_COMMON_GUI_VISIBILITY QRect mapGeometry(const QRectF &geometry, const qreal scale);
QDRE_COMMON_GUI_VISIBILITY QRect mapGeometry(const QRect &geometry, const qreal scale);

QDRE_COMMON_GUI_VISIBILITY QWidget *topLevelWidget(const QWidget *w);
QDRE_COMMON_GUI_VISIBILITY QWindow *topLevelWindow(const QWidget *w);

QDRE_COMMON_GUI_VISIBILITY QScreen *screenForWidget(const QWidget *w);

QDRE_COMMON_GUI_VISIBILITY QColor colorFromGSettingsTuple(const QVariant &variant);
QDRE_COMMON_GUI_VISIBILITY QVariant colorToGSettingsTuple(const QColor &color, const bool alpha);

QDRE_COMMON_GUI_VISIBILITY QSize sizeFromGSettingsTuple(const QVariant &variant);
QDRE_COMMON_GUI_VISIBILITY QVariant sizeToGSettingsTuple(const QSize &size);

// Returns cursor pos for screen which contains a widget
QDRE_COMMON_GUI_VISIBILITY QPoint cursorPos(const QWidget *w);
QDRE_COMMON_GUI_VISIBILITY QPoint localCursorPos(const QWidget *w);

}
}
