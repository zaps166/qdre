/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <CommonStyle.hpp>

#include <ThemeIcon.hpp>

#include <QStyleOptionViewItem>
#include <QLoggingCategory>
#include <QApplication>
#include <QPainter>
#include <QWidget>

namespace QDRE {

CommonStyle::CommonStyle()
    : QProxyStyle(QApplication::style())
{}
CommonStyle::~CommonStyle()
{}

void CommonStyle::drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
    const bool smooth = (widget && (element == CE_MenuItem || element == CE_PushButton || element == CE_ItemViewItem) && widget->devicePixelRatio() > 1);
    if (smooth)
    {
        painter->save();
        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        if (element == CE_ItemViewItem)
        {
            QStyleOptionViewItem *optionView = static_cast<QStyleOptionViewItem *>(const_cast<QStyleOption *>(option));
            QIcon::Mode mode = QIcon::Normal;
            if (!(optionView->state & QStyle::State_Enabled))
                mode = QIcon::Disabled;
            else if (optionView->state & QStyle::State_Selected)
                mode = QIcon::Selected;
            QIcon::State state = (optionView->state & QStyle::State_Open) ? QIcon::On : QIcon::Off;
            optionView->icon = optionView->icon.pixmap(optionView->decorationSize, widget->devicePixelRatio(), mode, state);
        }
    }
    QProxyStyle::drawControl(element, option, painter, widget);
    if (smooth)
        painter->restore();
}

int CommonStyle::styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData) const
{
    if (hint == SH_Menu_SubMenuDontStartSloppyOnLeave)
        return 1;
    return QProxyStyle::styleHint(hint, option, widget, returnData);
}

int CommonStyle::pixelMetric(PixelMetric metric, const QStyleOption *option, const QWidget *widget) const
{
    if (metric == QStyle::PM_MessageBoxIconSize)
        return 32;
    return QProxyStyle::pixelMetric(metric, option, widget);
}

}
