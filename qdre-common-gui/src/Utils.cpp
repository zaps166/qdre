/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <Utils.hpp>

#include <QPropertyAnimation>
#include <QAbstractButton>
#include <QApplication>
#include <QBitmap>
#include <QCursor>
#include <QScreen>
#include <QWidget>
#include <QWindow>
#include <QLabel>
#include <QDrag>

namespace QDRE {
namespace Utils {

using namespace std;

QRect mapScreenGeometry(const QRect &geometry, const qreal scale)
{
    return QRectF(QPointF(geometry.topLeft()), QSizeF(geometry.size()) * scale).toRect();
}
QRect mapGeometry(const QRectF &geometry, const qreal scale)
{
    return QTransform().scale(scale, scale).mapRect(geometry).toRect();
}
QRect mapGeometry(const QRect &geometry, const qreal scale)
{
    return QTransform().scale(scale, scale).mapRect(geometry);
}

QWidget *topLevelWidget(const QWidget *w)
{
    // QWidget::window() doesn't work for QMenu
    if (!w)
        return nullptr;
    QWidget *tlw = const_cast<QWidget *>(w);
    for (;;)
    {
        QWidget *parent = tlw->parentWidget();
        if (!parent)
            break;
        tlw = parent;
    }
    return tlw;
}
QWindow *topLevelWindow(const QWidget *w)
{
    if (QWidget *tlw = topLevelWidget(w))
    {
        return tlw->windowHandle();
    }
    return nullptr;
}

QScreen *screenForWidget(const QWidget *w)
{
    if (QWindow *win = topLevelWindow(w))
        return win->screen();
    return nullptr;
}

QColor colorFromGSettingsTuple(const QVariant &variant)
{
    const QVariantList colors = variant.toList();
    bool okR = false, okG = false, okB = false, okA = true;
    QColor color;
    if (colors.count() == 3 || colors.count() == 4)
    {
        color.setRed(colors[0].toInt(&okR));
        color.setGreen(colors[1].toInt(&okG));
        color.setBlue(colors[2].toInt(&okB));
    }
    if (colors.count() == 4)
    {
        color.setAlpha(colors[3].toInt(&okA));
    }
    if (!okR || !okG || !okB || !okA)
        return QColor();
    return color;
}
QVariant colorToGSettingsTuple(const QColor &color, const bool alpha)
{
    QVariantList colors {
        color.red(),
        color.green(),
        color.blue(),
    };
    if (alpha)
        colors.push_back(color.alpha());
    return colors;
}

QSize sizeFromGSettingsTuple(const QVariant &variant)
{
    const QVariantList sizeList = variant.toList();
    if (sizeList.count() == 2)
    {
        bool ok1 = false, ok2 = false;
        const QSize size(sizeList[0].toInt(&ok1), sizeList[1].toInt(&ok2));
        if (ok1 && ok2)
            return size;
    }
    return QSize();
}
QVariant sizeToGSettingsTuple(const QSize &size)
{
    return QVariantList {
        size.width(),
        size.height(),
    };
}

QPoint cursorPos(const QWidget *w)
{
    return QCursor::pos(screenForWidget(w));
}
QPoint localCursorPos(const QWidget *w)
{
    return w->mapFromGlobal(cursorPos(w));
}

}
}
