/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ThemeIcon.hpp>

#include <QCoreApplication>
#include <QLoggingCategory>
#include <QIcon>
#include <QDir>

namespace QDRE {
namespace ThemeIcon {

using NamePair = QPair<QString, QString>;
using IconCache = QHash<NamePair, QIcon>;

static IconCache *g_iconCache = nullptr;

#if QT_VERSION < QT_VERSION_CHECK(5, 11, 0)
static QStringList *g_fallbackSearchPaths = nullptr;

static QIcon fromTheme(const QString &name)
{
    QIcon icon = QIcon::fromTheme(name);
    if (icon.isNull())
    {
        const QString pngIconName = name + ".png";
        const QString xpmIconName = name + ".xpm";
        const QString svgIconName = name + ".svg";
        for (const QString &path : qAsConst(*g_fallbackSearchPaths))
        {
            const QDir currentDir(path);
            if (currentDir.exists(pngIconName))
            {
                icon = QIcon(currentDir.filePath(pngIconName));
                break;
            }
            else if (currentDir.exists(xpmIconName))
            {
                icon = QIcon(currentDir.filePath(xpmIconName));
                break;
            }
            else if (currentDir.exists(svgIconName))
            {
                icon = QIcon(currentDir.filePath(svgIconName));
                break;
            }
        }
    }
    return icon;
}
#else
static inline QIcon fromTheme(const QString &name)
{
    return QIcon::fromTheme(name);
}
#endif

/**/

void initialize()
{
    const QString localShareIcons = QDir::homePath() + "/.local/share/icons";
    QStringList iconSearchPaths = QIcon::themeSearchPaths();

    if (!iconSearchPaths.contains(localShareIcons) && QFileInfo(localShareIcons).isDir())
    {
        iconSearchPaths.prepend(localShareIcons);
        QIcon::setThemeSearchPaths(iconSearchPaths);
    }

    constexpr const char *pixmapsDir = "/usr/share/pixmaps";
    if (!iconSearchPaths.contains(pixmapsDir) && QFileInfo(pixmapsDir).isDir())
        iconSearchPaths.prepend(pixmapsDir);

#if QT_VERSION < QT_VERSION_CHECK(5, 11, 0)
    g_fallbackSearchPaths = new QStringList(iconSearchPaths);
    qAddPostRoutine([] {
        delete g_fallbackSearchPaths;
    });
#else
    QIcon::setFallbackSearchPaths(iconSearchPaths);
#endif

    g_iconCache = new IconCache;
    qAddPostRoutine([] {
        delete g_iconCache;
    });
}

QIcon get(const QString &name, const QString &fallbackName)
{
    const NamePair namePair {
        name,
        fallbackName,
    };

    const auto it = g_iconCache->constFind(namePair);
    if (it != g_iconCache->constEnd())
        return it.value();

    QIcon icon = fromTheme(name);
    if (icon.isNull())
    {
        if (!fallbackName.isEmpty())
        {
            icon = fromTheme(fallbackName);
            if (!icon.isNull())
                g_iconCache->insert(namePair, icon);
        }
    }
    else
    {
        g_iconCache->insert(namePair, icon);
    }
    return icon;
}
QIcon octetStream()
{
    return get("application-octet-stream", "text-x-generic");
}
QIcon executableScript()
{
    return get("application-x-executable-script", "application-x-executable");
}
QIcon executable()
{
    return get("application-x-executable");
}
QIcon url()
{
    return get("application-x-mswinurl", "text-html");
}

void clearCache()
{
    g_iconCache->clear();
}

}
}
