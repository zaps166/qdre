/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <DockWidgetTitleBar.hpp>

#include <ThemeIcon.hpp>

#include <QLoggingCategory>
#include <QHBoxLayout>
#include <QToolButton>
#include <qevent.h>
#include <QLabel>

Q_LOGGING_CATEGORY(docktitlebar, "qdre.dockwidgettitlebar")

namespace QDRE {

DockWidgetTitleBar::DockWidgetTitleBar(QDockWidget *parent)
    : QWidget(parent)
    , m_label(new QLabel(this))
    , m_restoreButton(new QToolButton(this))
    , m_closeButton(new QToolButton(this))
{
    m_label->setAttribute(Qt::WA_TransparentForMouseEvents);

    connect(m_restoreButton, &QToolButton::clicked,
            parent, [=] {
        parent->setFloating(!parent->isFloating());
    });
    m_restoreButton->setIcon(ThemeIcon::get("window-duplicate", "window-maximize-symbolic"));
    m_restoreButton->setToolTip(tr("Detach dock widget"));
    m_restoreButton->setIconSize({16, 16});
    m_restoreButton->setAutoRaise(true);

    connect(m_closeButton, &QToolButton::clicked,
            parent, &QDockWidget::close);
    m_closeButton->setIcon(ThemeIcon::get("window-close"));
    m_closeButton->setToolTip(tr("Close dock widget"));
    m_closeButton->setIconSize({16, 16});
    m_closeButton->setAutoRaise(true);

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(m_label);
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    layout->addWidget(m_restoreButton);
    layout->addWidget(m_closeButton);
    layout->setSpacing(2);
    layout->setContentsMargins(1, 1, 1, 1);

    connect(parent, &QDockWidget::featuresChanged,
            this, &DockWidgetTitleBar::dockFeaturesChanged);
    dockFeaturesChanged(parent->features());

    connect(parent, &QDockWidget::windowTitleChanged,
            this, &DockWidgetTitleBar::dockTitleChanged);
    dockTitleChanged(parent->windowTitle());
}
DockWidgetTitleBar::~DockWidgetTitleBar()
{}

void DockWidgetTitleBar::dockTitleChanged(const QString &title)
{
    m_label->setText("<b>" + title + "</b>");
}
void DockWidgetTitleBar::dockFeaturesChanged(const QDockWidget::DockWidgetFeatures features)
{
    m_restoreButton->setVisible(features.testFlag(QDockWidget::DockWidgetFloatable));
    m_closeButton->setVisible(features.testFlag(QDockWidget::DockWidgetClosable));

    if (features.testFlag(QDockWidget::DockWidgetVerticalTitleBar))
        qCWarning(docktitlebar, "Vertical title bar is not supported!");
}

}
