#include <KeySequence.hpp>

#include <IniSettings.hpp>

#include <QPointer>
#include <QAction>
#include <QHash>

namespace QDRE {

class KeySequence::Priv
{
public:
    inline QString getNameWithContext(const QString &name) const
    {
        return settings->group() + "/" + name;
    }

public:
    IniSettings *settings = nullptr;
    QStringList groupStack;
    QHash<QString, QPointer<QAction>> actions;
};

/**/

KeySequence::KeySequence(const QString &context, QObject *parent)
    : QObject(parent)
    , d(*(new Priv))
{
    d.settings = new IniSettings(IniSettings::Location::Config, "KeySequence", this, false);
    pushContext(context);
}
KeySequence::~KeySequence()
{
    delete &d;
}

void KeySequence::pushContext(const QString &context)
{
    const QString group = d.settings->group();
    if (!group.isEmpty())
    {
        d.groupStack.push_back(group);
        d.settings->endGroup();
    }
    d.settings->beginGroup(context);
}
void KeySequence::popContext()
{
    d.settings->endGroup();
    if (!d.groupStack.isEmpty())
        d.settings->beginGroup(d.groupStack.takeLast());
}

void KeySequence::bind(QAction *action, const QString &name, const QStringList &defStringList)
{
    Q_ASSERT(action);
    Q_ASSERT(!name.isEmpty());

    QList<QKeySequence> defList;
    for (const QString &str : defStringList)
        defList.push_back(str);
    init(name, defList);

    action->setShortcuts(get(name));

    d.actions[d.getNameWithContext(name)] = action;
}
void KeySequence::bind(QAction *action, const QString &name, const QString &def)
{
    bind(action, name, QStringList{def});
}

QList<QKeySequence> KeySequence::get(const QString &name)
{
    Q_ASSERT(!name.isEmpty());

    const QStringList keySequencesString = d.settings->value(name).toStringList();
    QList<QKeySequence> keySequences;
    for (const QString &str : keySequencesString)
        keySequences += str;
    return keySequences;
}

void KeySequence::set(const QString &name, const QList<QKeySequence> &seqList, const bool updateAction)
{
    Q_ASSERT(!name.isEmpty());

    QStringList stringSeqList;
    for (const QKeySequence &seq : seqList)
    {
        if (!seq.isEmpty())
            stringSeqList += seq.toString();
    }
    d.settings->setValue(name, stringSeqList);

    if (updateAction)
    {
        if (QAction *action = d.actions.value(d.getNameWithContext(name)))
            action->setShortcuts(seqList);
    }
}
void KeySequence::init(const QString &name, const QList<QKeySequence> &seqList)
{
    if (!d.settings->contains(name))
        set(name, seqList, false);
}

}
