/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ExifHelper.hpp>

#include <QImage>

namespace QDRE {
namespace ExifHelper {

QImage rotate(const QImage &input, const int rotation)
{
    switch (static_cast<ExifHelper::ImageOrientation>(rotation))
    {
        case ExifHelper::ImageOrientation::Hflip:
            return input.mirrored(true, false);
        case ExifHelper::ImageOrientation::Rot180:
            return input.mirrored(true, true);
        case ExifHelper::ImageOrientation::Vflip:
            return input.mirrored(false, true);
        case ExifHelper::ImageOrientation::Rot90:
        {
            QTransform matrix;
            matrix.rotate(90.0);
            return input.transformed(matrix);
        }
        case ExifHelper::ImageOrientation::Rot270:
        {
            QTransform matrix;
            matrix.rotate(270.0);
            return input.transformed(matrix);
        }
        default:
            break;
    }
    return input;
}

}
}
