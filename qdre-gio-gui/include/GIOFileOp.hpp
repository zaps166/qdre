/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>
#include <GUri.hpp>

#include <QPointer>
#include <QObject>

namespace QDRE {

class GIOFileOpThread;
class FileOpError;

struct GIOFileOpInfo
{
    GUri src;
    GUri dst;
    qint32 idx = 0;
};

class QDRE_GIO_GUI_VISIBILITY GIOFileOp : public QObject
{
    Q_OBJECT
    friend class GIOFileOpThread;

public:
    enum class Type
    {
        CreateFile,
        CreateDirectory,
        Copy,
        Move,
        Link,
        Trash,
        Rename,
        Unlink,
    };

private:
    GIOFileOp();
    ~GIOFileOp();

    static GIOFileOp *start(const GUriList &src,
                            const GUriList &dst,
                            const GIOFileOp::Type type,
                            const bool dialog,
                            const QByteArray &dataToWrite = QByteArray());

public:
    using Ptr = QPointer<const GIOFileOp>;

    static Ptr createFile(const GUri &file, const QByteArray &dataToWrite = QByteArray(), const bool dialog = true);
    static Ptr createDirectory(const GUri &dir, const bool dialog = true);

    static Ptr copy(const GUriList &src, const GUriList &dst, const bool dialog = true);
    static Ptr move(const GUriList &src, const GUriList &dst, const bool dialog = true);
    static Ptr link(const GUriList &src, const GUriList &dst, const bool dialog = true);
    static Ptr trash(const GUriList &uris, const bool dialog = true);
    static Ptr rename(const GUriList &src, const GUriList &dst, const bool dialog = true);
    static Ptr unlink(const GUriList &files, QWidget *questionParent = nullptr, const bool dialog = true);

    static Ptr copyTo(const GUriList &uris, const GUri &dir, const bool dialog = true);
    static Ptr moveTo(const GUriList &uris, const GUri &dir, const bool dialog = true);
    static Ptr linkTo(const GUriList &uris, const GUri &dir, const bool dialog = true);
    static Ptr renameTo(const GUriList &uris, const GUri &dir, const bool dialog = true);

    static void emptyTrash(QWidget *questionParent = nullptr, const bool dialog = true);

public:
    Type type() const;

    void cancel() const;

    GUriList srcUris() const;

Q_SIGNALS:
    void countingObjects(const qint32 num, const qint64 overalSize);
    void startedSingle(const GIOFileOpInfo &info);
    void progressChanged(const qint64 pos, const qint64 num, const qreal progress, const qreal speed);
    void finishedSingle(FileOpError *error, const GIOFileOpInfo &info); // Only one slot can handle error, the error must be handled!

private:
    GIOFileOpThread *d = nullptr;
};

}
