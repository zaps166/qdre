/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>
#include <GUriList.hpp>

#include <QAbstractItemModel>

class QItemSelection;

namespace QDRE {

class GIOModelPriv;
class GIOFileOp;

class QDRE_GIO_GUI_VISIBILITY GIOModel : public QAbstractItemModel
{
    Q_OBJECT
    friend class GIOModelPriv;

public:
    GIOModel(QObject *parent = nullptr);
    ~GIOModel();

public:
    void setSimpleReadMode();
    void setFileTypeMode();

    void setRootPath(const QString &path);
    GUri rootUri() const;

    void setDisplayHidden(const bool displayHidden);
    bool displayHidden() const;

    void reload();

    GUri getGUri(const QModelIndex &index) const;
    GUriList getGUris(const QModelIndexList &indexes) const;
    GUriList getGUris(const QList<QPersistentModelIndex> &indexes) const;

    bool isDirectoryWritable() const;

    GUri getUriForNewName(const QString &name) const;

    bool isHidden(const QModelIndex &index) const;
    bool isSymlink(const QModelIndex &index) const;
    bool isReadable(const QModelIndex &index) const;
    bool isWritable(const QModelIndex &index) const;
    bool isExecutable(const QModelIndex &index) const;
    bool isDirectory(const QModelIndex &index) const;

    bool canDelete(const QModelIndex &index) const;
    bool canTrash(const QModelIndex &index) const;
    bool canEmptyTrash(const QModelIndex &index) const;

    bool isDesktop(const QModelIndex &index) const;
    bool isTrashContainer(const QModelIndex &index) const;

    bool isCut(const QModelIndex &index) const;

    QString getMimeType(const QModelIndex &index) const;
    QStringList getMimeTypes(const QModelIndexList &indexes) const;
    QStringList getMimeTypes(const QList<QPersistentModelIndex> &indexes) const;

    void scheduleItemChangeWithSelection(const GIOFileOp *fileOp, const GUriList &uris, const bool rename = false);

    GUriList takeLastDropUris();

public:
    QModelIndex index(qint32 row, qint32 column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    bool setData(const QModelIndex &index, const QVariant &value, qint32 role = Qt::EditRole) override;
    QVariant data(const QModelIndex &index, qint32 role) const override;

    QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, qint32 row, qint32 column, const QModelIndex &parent) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, qint32 row, qint32 column, const QModelIndex &parent) override;
    Qt::DropActions supportedDropActions() const override;
    Qt::DropActions supportedDragActions() const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

private:
    inline bool inFilesBounds(const qint32 i) const;

    template<typename ModelIndexList>
    GUriList getGUrisInternal(const ModelIndexList &indexes) const;

    template<typename ModelIndexList>
    QStringList getMimeTypesInternal(const ModelIndexList &indexes) const;

Q_SIGNALS:
    void rootDirectoryError(const QString &error);
    void isRootDirectoryWritableChanged(const bool isDirectoryWritable);

    void selectionRequest(const QItemSelection &selection, const bool rename);
    void fileOpStartedSingle(const GUri &src, const GUri &dst);

    void changed();

private:
    GIOModelPriv &d;

};

}
