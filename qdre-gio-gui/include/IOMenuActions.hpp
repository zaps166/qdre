/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>

#include <QList>

class QPersistentModelIndex;
class QAbstractItemView;
class QModelIndex;
class QAction;
class QWidget;
class QMenu;

namespace QDRE {

class KeySequence;
class GIOModel;

namespace IOMenuActions {

QDRE_GIO_GUI_VISIBILITY QAction *cut();
QDRE_GIO_GUI_VISIBILITY QAction *copy();

QDRE_GIO_GUI_VISIBILITY QAction *symlink();
QDRE_GIO_GUI_VISIBILITY QAction *rename();

QDRE_GIO_GUI_VISIBILITY QAction *trash();
QDRE_GIO_GUI_VISIBILITY QAction *remove();

QDRE_GIO_GUI_VISIBILITY QAction *emptyTrash();

QDRE_GIO_GUI_VISIBILITY QAction *properties();

QDRE_GIO_GUI_VISIBILITY void bind(QAbstractItemView *view, KeySequence *keySeq);

QDRE_GIO_GUI_VISIBILITY void updateMenuActions(const GIOModel *model, const QModelIndex &index, const bool notFirstIndex);
QDRE_GIO_GUI_VISIBILITY void pasteActionEnableDisable(QAction *act);

QDRE_GIO_GUI_VISIBILITY QMenu *createContextMenu(QWidget *parent,
                                             const GIOModel *model,
                                             const QList<QPersistentModelIndex> &indexes,
                                             const QList<QAction *> &additionalActions = {});

}
}
