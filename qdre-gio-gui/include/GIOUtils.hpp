/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>
#include <GIOFileOp.hpp>

class QDropEvent;
class QMimeData;
class QMimeType;

struct _GDesktopAppInfo;
struct _GAppInfo;
struct _GIcon;

namespace QDRE {

class GIOModel;

namespace GIOUtils {

using UrisWithMimeType = QList<QPair<GUri, QString>>; // uri, mime-type

#ifdef GIO_LAUNCH
QDRE_GIO_GUI_VISIBILITY QString getDefaultTerminal();
#endif

QDRE_GIO_GUI_VISIBILITY QString getDefaultExecutableForType(const QString &mimeType);

// "getExecutableAbility()" work properly only when object is executable.
QDRE_GIO_GUI_VISIBILITY QPair<bool, bool> getExecutableAbility(const QMimeType &mimeType, const QString &baseName);

QDRE_GIO_GUI_VISIBILITY QVariant getIcon(_GIcon *icon);
QDRE_GIO_GUI_VISIBILITY QIcon getQIcon(_GIcon *icon, const bool octetStreamFallback = false, const bool executableFallback = false);

QDRE_GIO_GUI_VISIBILITY QString getMimeTypeForUri(const GUri &uri);
QDRE_GIO_GUI_VISIBILITY QString getDescriptionFromMimeType(const QString &mimeType);
QDRE_GIO_GUI_VISIBILITY QIcon getQIconFromMimeType(const QString &mimeType);

QDRE_GIO_GUI_VISIBILITY GUri getDesktopLinkToContainer(const QString &desktopFile, const bool allowTrash = false);

#ifdef GIO_LAUNCH
QDRE_GIO_GUI_VISIBILITY bool runProcess(const GUri &uri, const bool inTerminal, const QStringList &args = {}, const QString &workingDirectory = QString());

// "uriMimeType" is used mostly for non-local URIs; "uris" is used to open with desktop files or executable files
QDRE_GIO_GUI_VISIBILITY bool launch(const GUri &uriArg, const QString &uriMimeType = QString(), const bool isDesktop = false, const GUriList &uris = {}, const QString &desktopAction = QString());

// Dont't pass *.desktop files
QDRE_GIO_GUI_VISIBILITY void launchMulti(const UrisWithMimeType &urisWithMimeType);

QDRE_GIO_GUI_VISIBILITY bool launchWithMulti(const GUriList &uris, _GAppInfo *appInfo);
QDRE_GIO_GUI_VISIBILITY bool launchWith(const GUri &uri, _GAppInfo *appInfo);
#endif

QDRE_GIO_GUI_VISIBILITY _GAppInfo *createAppInfo(const QString &executable);

QDRE_GIO_GUI_VISIBILITY Qt::DropAction fileOpDropMenu(QWidget *parent, QDropEvent *event, const bool desktopLink = false);
QDRE_GIO_GUI_VISIBILITY GIOFileOp::Ptr fileOpAction(const GUriList &uris, const GUri &dir, const Qt::DropAction action);

QDRE_GIO_GUI_VISIBILITY bool isMimeDataCutSelection(const QMimeData *mimeData);
QDRE_GIO_GUI_VISIBILITY void setMimeDataCutSelection(QMimeData *mimeData);

QDRE_GIO_GUI_VISIBILITY GUriList paste(const GUri &destUri, GIOModel *model);

}
}
