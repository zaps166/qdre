/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>

#include <QPair>

struct _GDesktopAppInfo;
struct _GAppInfo;
struct _GIcon;
class QIcon;

namespace QDRE {

struct DesktopFileReaderPriv;

class QDRE_GIO_GUI_VISIBILITY DesktopFileReader
{
public:
    enum Read
    {
        App  = 1,
        Link = 2,
        All  = App | Link,
    };

    using Actions = QList<QPair<QString, QString>>;

public:
    DesktopFileReader(const QString &desktopFilePath, Read read = All);
    DesktopFileReader(_GDesktopAppInfo *desktopAppInfo, const bool move);
    DesktopFileReader(_GAppInfo *appInfo, const bool move);
    DesktopFileReader(const DesktopFileReader &other);
    DesktopFileReader(DesktopFileReader &&other);
    ~DesktopFileReader();

    bool isValid() const;
    bool isApp() const;
    bool isLink() const;

    void setLinkIconKey(const QByteArray &key);

    _GDesktopAppInfo *getDesktopAppInfoPtr() const;
    _GAppInfo *getAppInfoPtr() const;

    QString desktopFilePath() const;

    QString name() const;
    QString displayName() const; // Works only with "App" type
    QString description() const; // Works only with "App" type
    QString executable() const; // Works only with "App" type

    Actions actions() const; // Works only with "App" type

    QIcon icon(const bool loadLinkIconIfFilePath = true) const;
    _GIcon *appIcon() const; // Works only with "App" type
    QString linkIcon() const; // Works only with "Link" type

    QString linkUrl() const; // Works only with "Link" type
    bool isHttpLink() const; // Works only with "Link" type

    DesktopFileReader &operator =(const DesktopFileReader &other);
    DesktopFileReader &operator =(DesktopFileReader &&other);

private:
    DesktopFileReaderPriv *d = nullptr;
};

}
