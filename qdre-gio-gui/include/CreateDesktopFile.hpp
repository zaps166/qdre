/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOGuiVisibility.hpp>

#include <QDialog>

namespace Ui {
    class CreateDesktopFile;
}
class QIcon;

namespace QDRE {

class GUri;

class QDRE_GIO_GUI_VISIBILITY CreateDesktopFile : public QDialog
{
    Q_OBJECT

public:
    CreateDesktopFile(QWidget *parent = nullptr);
    ~CreateDesktopFile();

    void setUri(const GUri &uri);

    QString fileName() const;
    QByteArray data() const;

private:
    void setIconButtonIcon(const QString &icon, const bool fromTheme);
    void setIconButtonIcon(const QIcon &icon);

private Q_SLOTS:
    void on_typeComboBox_currentIndexChanged(qint32 index);
    void on_iconButton_clicked();
    void on_browseButton_clicked();

private:
    void accept() override;

private:
    Ui::CreateDesktopFile *ui;
};

}
