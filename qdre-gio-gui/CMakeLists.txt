cmake_minimum_required(VERSION 3.16)
project(qdre-gio-gui)

find_package(Qt6Widgets REQUIRED)
if(GIO_LAUNCH)
    find_package(KF6WindowSystem REQUIRED)
endif()
find_package(PkgConfig REQUIRED)

pkg_check_modules(EXIV2 REQUIRED exiv2)

set_source_files_properties("src/ThumbnailerPriv.cpp" PROPERTIES COMPILE_OPTIONS "-fexceptions")

add_definitions(
    -DQDRE_GIO_GUI_COMPILING_LIBRARY
    -DGMENU_I_KNOW_THIS_IS_UNSTABLE
)

file(GLOB PROJECT_FILES
    "src/*.cpp"
    "src/*.hpp"
    "src/*.ui"
    "include/*.hpp"
)

link_directories(${PROJECT_NAME}
    ${EXIV2_LIBRARY_DIRS}
)

add_library(${PROJECT_NAME}
    ${QDRE_LIB_TYPE}
    ${PROJECT_FILES}
)

target_link_libraries(${PROJECT_NAME}
    qdre-common-gui
    qdre-gio-core
    ${EXIV2_LIBRARIES}
)
if(GIO_LAUNCH)
    target_compile_definitions(${PROJECT_NAME}
        PUBLIC
        -DGIO_LAUNCH
    )
    target_link_libraries(${PROJECT_NAME}
        KF6::WindowSystem
    )
endif()
target_include_directories(${PROJECT_NAME}
    PUBLIC
    "include"
    PRIVATE
    "src"
    ${EXIV2_INCLUDE_DIRS}
)

install(TARGETS ${PROJECT_NAME}
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
)
install(DIRECTORY "include/"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/QDRE"
)

if(BUILD_TESTS)
    add_subdirectory(tests)
endif()
