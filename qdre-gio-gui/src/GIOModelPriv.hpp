/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <gio/gio.h>

#include <GObjectHolder.hpp>
#include <Thumbnailer.hpp>
#include <GUri.hpp>

#include <QLoggingCategory>
#include <QItemSelection>
#include <QIcon>

#include <memory>

Q_DECLARE_LOGGING_CATEGORY(giomodel)

namespace QDRE {

class DesktopFileReader;
class GIOModel;

class FileDescr : public GUri
{
public:
    bool isHidden() const;
    bool isSymlink() const;
    bool isReadable() const;
    bool isWritable() const;
    bool isExecutable() const;

    bool isDirectory() const;

    bool canDelete() const;
    bool canTrash() const;

    QString mimeType() const;

public:
    QString name;
    QString editableName;
    QString tmpName;

    QIcon themedIcon;

    QString fileIconPath;
    QImage fileIcon;

    bool isDesktop = false;
    bool isTrashContainer = false;
    bool isCut = false;
    bool shouldGenerateThumbnail = false;

    GObjectHolder<GFileInfo> info;
};

struct FileOpSelection
{
    qint32 count = 0;
    QItemSelection selection;
    bool rename = false;
};

class GIOModelPriv
{
    struct FileInfoAsync
    {
        GIOModelPriv *modelPriv = nullptr;
        quint64 seq = 0;
        GObjectHolder<GFile> oldFile;
        FileOpSelection *fileOpSelection = nullptr;
        FileDescr fileDescr;
    };

public:
    GIOModelPriv(GIOModel &gioModel);
    ~GIOModelPriv();

    void setSimpleReadMode();
    void setFileTypeMode();

    void setRootPath(const QString &path);
    void resetRootPath();

    void scheduleItemChange(GFile *newFile,
                            GFile *oldFile = nullptr,
                            FileOpSelection *fileOpSelection = nullptr);

private:
    inline bool isSimpleReadMode() const;
    inline bool isFileTypeMode() const;

    void initializeCutUrlsAndThumbnails();
    void initializeTrashMonitor();

    void unrefMonitor();
    void unrefCancellable();

    void resetModel();

    FileDescr getFileDescr(GFileInfo *info, const bool forceHidden = false);
    void updateFileDescrIcon(FileDescr &fileDescr, DesktopFileReader &desktopFileReader);

    void updateTrashState();

    static void loadIcon(FileDescr &file, GIcon *icon, const bool loadFallback);

    qint32 findFileInModel(GFile *file);

    void getCutUrlsFromClipboard();

    void processFileInfo(GFile *renamedFile, FileOpSelection *fileOpSelection, FileDescr &fileDescr);

    void removeRow(const qint32 row);

private:
    void gotDirInfo(GFile *file,
                    GAsyncResult *res,
                    GFileInfo *info,
                    GError *error);

    void dirMonitor(GFile *file,
                    GFile *otherFile,
                    GFileMonitorEvent eventType);
    void gotDirs(GFile *file,
                 GAsyncResult *res,
                 GFileEnumerator *direnum);

    void gotNextFiles(GFileEnumerator *direnum,
                      GAsyncResult *res,
                      GList *const listBegin);

    void gotFileInfo(GFile *file,
                     GAsyncResult *res,
                     FileInfoAsync *fileInfoAsync,
                     GFileInfo *info,
                     GError *error);

private:
    static void gotDirInfoStatic(GFile *file,
                                 GAsyncResult *res,
                                 GIOModelPriv &d);

    static void dirMonitorStatic(GFileMonitor *monitor,
                                 GFile *file,
                                 GFile *otherFile,
                                 GFileMonitorEvent eventType,
                                 GIOModelPriv &d);
    static void gotDirsStatic(GFile *file,
                              GAsyncResult *res,
                              GIOModelPriv &d);

    static void gotNextFilesStatic(GFileEnumerator *direnum,
                                   GAsyncResult *res,
                                   GIOModelPriv &d);

    static void gotFileInfoStatic(GFile *file,
                                  GAsyncResult *res,
                                  FileInfoAsync *fileInfoAsync);

private:
    GIOModel &q;

    const char *m_attributes;

    GUriList m_cutUris;

public:
    GUri m_uri;

    bool m_initializedCutUrlsAndThumbnailer = false;

    GCancellable *m_cancellable = nullptr;
    GFileMonitor *m_monitor = nullptr;

    bool m_dirWritable = false;
    bool m_displayHidden = false;

    std::vector<FileDescr> m_files;
    qint32 m_filesStartIdx = 0;

    // map is sorted by key, so the lowest sequence is available at "begin()"
    std::map<quint64, std::pair<std::unique_ptr<FileInfoAsync>, bool>> m_fileInfoAsyncMap;
    quint64 m_seq = 0;

    GUriList m_lastDropUris;

    std::unique_ptr<Thumbnailer> m_thumbnailer;

    GIOModel *m_trashModel = nullptr;
    bool m_trashFull = false;
};

}
