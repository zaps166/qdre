/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <AppsList.hpp>

#include <ThemeIcon.hpp>
#include <GIOUtils.hpp>

#include <QLoggingCategory>
#include <QButtonGroup>
#include <QRadioButton>
#include <QHeaderView>
#include <QToolButton>
#include <QMessageBox>

namespace QDRE {

using std::as_const;

AppsList::AppsList(QWidget *parent)
    : QTreeWidget(parent)
{
    setIndentation(0);
    setIconSize({24, 24});
    setRootIsDecorated(false);
    setUniformRowHeights(true);
    setExpandsOnDoubleClick(false);
    setVerticalScrollMode(ScrollPerPixel);
    setHorizontalScrollMode(ScrollPerPixel);

    setHeaderHidden(true);
    header()->setStretchLastSection(false);

    connect(this, &AppsList::itemActivated,
            this, [this](QTreeWidgetItem *item) {
        if (item && item == currentItem())
            emit appInfoActivated();
    });
    connect(this, &AppsList::currentItemChanged,
            this, [this](QTreeWidgetItem *current, QTreeWidgetItem *previous) {
        Q_UNUSED(previous)
        if (current == currentItem())
        {
            if (current)
                emit appInfoChanged(m_appsInfoList[currentIndex().row()]);
            else
                emit appInfoChanged(nullptr);
        }
    });
}
AppsList::~AppsList()
{}

bool AppsList::createList(const QByteArray &mimeType, const bool selectCurrent, const bool allApplications)
{
    clear();
    m_appsInfoList.clear();

    GList *appsInfo = allApplications
            ? g_app_info_get_all()
            : g_app_info_get_all_for_type(mimeType.constData());
    if (!appsInfo)
        return false;

    GObjectHolder<GAppInfo> defaultApp(g_app_info_get_default_for_type(mimeType.constData(), false));

    for (GList *it = appsInfo; it; it = it->next)
        m_appsInfoList.append(reinterpret_cast<GAppInfo *>(it->data));
    g_list_free(appsInfo);

    std::sort(m_appsInfoList.begin(), m_appsInfoList.end(), [](const auto &first, const auto &second) {
        const char *firstStr  = g_app_info_get_name(first);
        const char *secondStr = g_app_info_get_name(second);
        return (qstrcmp(firstStr, secondStr) < 0);
    });

    QTreeWidgetItem *currentItem = nullptr;

    const QIcon removeIcon = ThemeIcon::get("list-remove");
    QButtonGroup *buttonGroup = new QButtonGroup(this);
    for (const auto &info : as_const(m_appsInfoList))
    {
        const qint32 row = topLevelItemCount();
        QTreeWidgetItem *item = new QTreeWidgetItem(this);

        const QString name = g_app_info_get_name(info);

        QRadioButton *radioButton = nullptr;
        if (!allApplications)
        {
            radioButton = new QRadioButton;
            buttonGroup->addButton(radioButton, row);
            connect(radioButton, &QRadioButton::clicked,
                    this, [=](bool checked) {
                if (!checked)
                    return;
                if (g_app_info_set_as_default_for_type(m_appsInfoList.at(row), mimeType, nullptr))
                    m_checkedButton = radioButton;
                else if (m_checkedButton)
                    m_checkedButton->setChecked(true);
            });
        }

        QToolButton *removeButton = nullptr;
        if ((allApplications && g_app_info_can_delete(info)) || (!allApplications && g_app_info_can_remove_supports_type(info)))
        {
            removeButton = new QToolButton;
            if (removeButton->isEnabled())
                removeButton->setToolTip(tr("Remove \"%1\"").arg(name));
            removeButton->setFocusPolicy(Qt::ClickFocus);
            removeButton->setIconSize({16, 16});
            removeButton->setIcon(removeIcon);
            removeButton->setAutoRaise(true);
            if (allApplications)
            {
                connect(removeButton, &QToolButton::clicked,
                        this, [=] {
                    const qint32 choice = QMessageBox::question(
                                this,
                                tr("Removing user application"),
                                tr("Do you want to remove \"%1\"?").arg(name),
                                QMessageBox::Yes,
                                QMessageBox::No);
                    if (choice == QMessageBox::Yes && g_app_info_delete(m_appsInfoList.at(row)))
                        delete item;
                });
            }
            else
            {
                connect(removeButton, &QToolButton::clicked,
                        this, [=] {
                    const qint32 choice = QMessageBox::question(
                                this,
                                tr("Removing application for \"%1\"").arg(QString(mimeType)),
                                tr("Do you want to remove \"%1\"?").arg(name),
                                QMessageBox::Yes,
                                QMessageBox::No);
                    if (choice == QMessageBox::Yes && g_app_info_remove_supports_type(m_appsInfoList.at(row), mimeType.constData(), nullptr))
                        delete item;
                });
            }
        }

        qint32 column = 0;
        if (radioButton)
        {
            setItemWidget(item, column, radioButton);
            ++column;
        }

        item->setIcon(column, GIOUtils::getQIcon(g_app_info_get_icon(info), false, true));
        ++column;

        item->setText(column, name);
        item->setToolTip(column, g_app_info_get_description(info));
        ++column;

        if (removeButton)
            setItemWidget(item, column, removeButton);

        if (selectCurrent && !defaultApp.isNull() && g_app_info_equal(defaultApp, info))
        {
            currentItem = item;
            if (radioButton)
            {
                radioButton->setChecked(true);
                m_checkedButton = radioButton;
            }
        }
    }

    qint32 column = 0;
    if (allApplications)
    {
        setColumnCount(3);
    }
    else
    {
        setColumnCount(4);
        header()->setSectionResizeMode(column++, QHeaderView::ResizeToContents);
    }
    header()->setSectionResizeMode(column++, QHeaderView::ResizeToContents);
    header()->setSectionResizeMode(column++, QHeaderView::Stretch);
    header()->setSectionResizeMode(column++, QHeaderView::ResizeToContents);

    setCurrentItem(currentItem, 1);

    return true;
}

}
