/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <GIOFileOpThread.hpp>
#include <UnlinkQuestion.hpp>
#include <GIOFileOp.hpp>

#include <QLoggingCategory>
#include <QMessageBox>

Q_LOGGING_CATEGORY(fileop, "qdre.gio.fileop")

namespace QDRE {

static GUriList getTargets(const GUriList &src, const GUri &dstUri)
{
    GUriList dst;
    dst.reserve(src.count());

    for (const GUri &uri : src)
        dst.append(dstUri.pathAppended(uri.baseName()));

    return dst;
}

/**/

GIOFileOp::GIOFileOp()
{
    static auto id = qRegisterMetaType<GIOFileOpInfo>("GIOFileOpInfo");
    Q_UNUSED(id)
}
GIOFileOp::~GIOFileOp()
{
    delete d;
}

GIOFileOp *GIOFileOp::start(const GUriList &src,
                            const GUriList &dst,
                            const GIOFileOp::Type type,
                            const bool dialog,
                            const QByteArray &dataToWrite)
{
    const bool copyOrMove = (type == Type::Copy || type == Type::Move || type == Type::Link || type == Type::Rename);
    if (!copyOrMove && !dst.isEmpty())
    {
        qCWarning(fileop) << "Only copy and move can have destination";
        return nullptr;
    }

    if (copyOrMove && src.count() != dst.count())
    {
        qCWarning(fileop) << "Source and destination sizes doesn't match";
        return nullptr;
    }

    const qint32 count = src.count();
    if (count < 1)
    {
        qCWarning(fileop) << "Empty file list!";
        return nullptr;
    }

    GIOFileOp *gioFileOp = new GIOFileOp;
    gioFileOp->d = new GIOFileOpThread(*gioFileOp, src, dst, type, dataToWrite, dialog);

    return gioFileOp;
}

GIOFileOp::Ptr GIOFileOp::createFile(const GUri &file, const QByteArray &dataToWrite, const bool dialog)
{
    if (file.isNull())
        return nullptr;
    return start({file}, {}, Type::CreateFile, dialog, dataToWrite);
}
GIOFileOp::Ptr GIOFileOp::createDirectory(const GUri &dir, const bool dialog)
{
    if (dir.isNull())
        return nullptr;
    return start({dir}, {}, Type::CreateDirectory, dialog);
}

GIOFileOp::Ptr GIOFileOp::copy(const GUriList &src, const GUriList &dst, const bool dialog)
{
    if (src.isEmpty())
        return nullptr;
    return start(src, dst, Type::Copy, dialog);
}
GIOFileOp::Ptr GIOFileOp::move(const GUriList &src, const GUriList &dst, const bool dialog)
{
    if (src.isEmpty())
        return nullptr;
    return start(src, dst, Type::Move, dialog);
}
GIOFileOp::Ptr GIOFileOp::link(const GUriList &src, const GUriList &dst, const bool dialog)
{
    if (src.isEmpty())
        return nullptr;
    return start(src, dst, Type::Link, dialog);
}
GIOFileOp::Ptr GIOFileOp::trash(const GUriList &uris, const bool dialog)
{
    if (uris.isEmpty())
        return nullptr;
    return start(uris, {}, Type::Trash, dialog);
}
GIOFileOp::Ptr GIOFileOp::rename(const GUriList &src, const GUriList &dst, const bool dialog)
{
    if (src.isEmpty())
        return nullptr;
    return start(src, dst, Type::Rename, dialog);
}
GIOFileOp::Ptr GIOFileOp::unlink(const GUriList &files, QWidget *questionParent, const bool dialog)
{
    if (files.isEmpty())
        return nullptr;
    if (dialog && UnlinkQuestion(files, questionParent).exec() == QDialog::Rejected)
        return nullptr;
    return start(files, {}, Type::Unlink, dialog);
}

GIOFileOp::Ptr GIOFileOp::copyTo(const GUriList &uris, const GUri &dir, const bool dialog)
{
    return copy(uris, getTargets(uris, dir), dialog);
}
GIOFileOp::Ptr GIOFileOp::moveTo(const GUriList &uris, const GUri &dir, const bool dialog)
{
    return move(uris, getTargets(uris, dir), dialog);
}
GIOFileOp::Ptr GIOFileOp::linkTo(const GUriList &uris, const GUri &dir, const bool dialog)
{
    return link(uris, getTargets(uris, dir), dialog);
}
GIOFileOp::Ptr GIOFileOp::renameTo(const GUriList &uris, const GUri &dir, const bool dialog)
{
    return rename(uris, getTargets(uris, dir), dialog);
}

void GIOFileOp::emptyTrash(QWidget *questionParent, const bool dialog)
{
    if (dialog && QMessageBox::question(
                questionParent,
                tr("Empty trash"),
                tr("Do you want to empty the trash?"),
                QMessageBox::Yes,
                QMessageBox::No) == QMessageBox::No)
    {
        return;
    }
    start({GUri::trash()}, {}, Type::Unlink, dialog);
}

GIOFileOp::Type GIOFileOp::type() const
{
    return d->m_type;
}

void GIOFileOp::cancel() const
{
    d->cancel();
}

GUriList GIOFileOp::srcUris() const
{
    return d->m_src;
}

}
