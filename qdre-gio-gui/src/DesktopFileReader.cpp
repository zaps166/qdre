/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gdesktopappinfo.h>

#include <DesktopFileReader.hpp>

#include <GObjectHolder.hpp>
#include <ThemeIcon.hpp>
#include <GIOUtils.hpp>
#include <GKeyFile.hpp>
#include <GUri.hpp>

#include <QLoggingCategory>
#include <QFileInfo>
#include <QIcon>

namespace QDRE {

struct DesktopFileReaderPriv
{
    QString desktopFile;
    GObjectHolder<GAppInfo> appInfo;
    GKeyFile keyFile;
    QByteArray iconKey;
};

/**/

DesktopFileReader::DesktopFileReader(const QString &desktopFile, Read read)
    : d(new DesktopFileReaderPriv)
{
    if (read & App)
    {
        GDesktopAppInfo *desktopAppInfo = g_desktop_app_info_new_from_filename(desktopFile.toUtf8().constData());
        if (desktopAppInfo)
        {
            GAppInfo *appInfo = G_APP_INFO(desktopAppInfo);
            Q_ASSERT(appInfo);
            d->appInfo = std::move(appInfo);
        }
    }

    if ((read & Link) && d->appInfo.isNull())
    {
        if (d->keyFile.load(desktopFile))
        {
            d->keyFile.setGroupName("Desktop Entry");
            if (d->keyFile.getString("Type") != "Link")
                d->keyFile.unload();
        }
    }

    d->desktopFile = desktopFile;
}
DesktopFileReader::DesktopFileReader(GDesktopAppInfo *desktopAppInfo, const bool move)
    : d(new DesktopFileReaderPriv)
{
    if (!desktopAppInfo)
        return;

    GAppInfo *appInfo = G_APP_INFO(desktopAppInfo);
    Q_ASSERT(appInfo);

    if (move)
        d->appInfo = std::move(appInfo);
    else
        d->appInfo.ref(appInfo);

    d->desktopFile = g_desktop_app_info_get_filename(desktopAppInfo);
}
DesktopFileReader::DesktopFileReader(GAppInfo *appInfo, const bool move)
    : d(new DesktopFileReaderPriv)
{
    if (!appInfo)
        return;

    if (move)
        d->appInfo = std::move(appInfo);
    else
        d->appInfo.ref(appInfo);
}
DesktopFileReader::DesktopFileReader(const DesktopFileReader &other)
    : d(new DesktopFileReaderPriv)
{
    *this = other;
}
DesktopFileReader::DesktopFileReader(DesktopFileReader &&other)
{
    *this = std::move(other);
}
DesktopFileReader::~DesktopFileReader()
{
    delete d;
}

bool DesktopFileReader::isValid() const
{
    return (isApp() || isLink());
}
bool DesktopFileReader::isApp() const
{
    return !d->appInfo.isNull();
}
bool DesktopFileReader::isLink() const
{
    return d->keyFile.isLoaded();
}

void DesktopFileReader::setLinkIconKey(const QByteArray &key)
{
    if (d->keyFile.isLoaded())
        d->iconKey = key;
}

GDesktopAppInfo *DesktopFileReader::getDesktopAppInfoPtr() const
{
    return G_DESKTOP_APP_INFO(d->appInfo.ptr());
}
GAppInfo *DesktopFileReader::getAppInfoPtr() const
{
    return d->appInfo.ptr();
}

QString DesktopFileReader::desktopFilePath() const
{
    return d->desktopFile;
}

QString DesktopFileReader::name() const
{
    if (d->appInfo)
        return g_app_info_get_name(d->appInfo);
    if (d->keyFile.isLoaded())
        return d->keyFile.getLocaleString("Name");
    return QString();
}
QString DesktopFileReader::displayName() const
{
    if (d->appInfo)
        return g_app_info_get_display_name(d->appInfo);
    return QString();
}
QString DesktopFileReader::description() const
{
    if (d->appInfo)
        return g_app_info_get_description(d->appInfo);
    return QString();
}
QString DesktopFileReader::executable() const
{
    if (d->appInfo)
        return g_app_info_get_executable(d->appInfo);
    return QString();
}

DesktopFileReader::Actions DesktopFileReader::actions() const
{
    if (!d->appInfo)
        return {};

    auto desktopAppInfo = getDesktopAppInfoPtr();

    const gchar *const *actionsRaw = g_desktop_app_info_list_actions(desktopAppInfo);
    if (!actionsRaw)
        return {};

    Actions actions;

    while (*actionsRaw)
    {
        if (gchar *nameRaw = g_desktop_app_info_get_action_name(desktopAppInfo, *actionsRaw))
        {
            actions.push_back({*actionsRaw, nameRaw});
            g_free(nameRaw);
        }
        ++actionsRaw;
    }

    return actions;
}

QIcon DesktopFileReader::icon(const bool loadLinkIconIfFilePath) const
{
    if (d->appInfo)
        return GIOUtils::getQIcon(appIcon(), false, true);

    if (d->keyFile.isLoaded())
    {
        const QString iconStr = linkIcon();
        if (!iconStr.isEmpty())
        {
            const QFileInfo iconInfo(iconStr);
            if (iconInfo.isAbsolute())
            {
                if (iconInfo.isFile())
                    return loadLinkIconIfFilePath ? QIcon(iconStr) : QIcon();
            }
            else
            {
                const QIcon icon = ThemeIcon::get(iconStr);
                if (!icon.isNull())
                    return icon;
            }

            if (isHttpLink())
                return ThemeIcon::url();

            return ThemeIcon::executable();
        }
    }

    return QIcon();
}
GIcon *DesktopFileReader::appIcon() const
{
    if (d->appInfo)
        return g_app_info_get_icon(d->appInfo);
    return nullptr;
}
QString DesktopFileReader::linkIcon() const
{
    if (d->keyFile.isLoaded())
        return d->keyFile.getString(d->iconKey.isEmpty() ? "Icon" : d->iconKey);
    return nullptr;
}

QString DesktopFileReader::linkUrl() const
{
    if (d->keyFile.isLoaded())
        return d->keyFile.getString("URL");
    return QString();
}
bool DesktopFileReader::isHttpLink() const
{
    if (d->keyFile.isLoaded())
    {
        const QString url = linkUrl();
        return (url.startsWith("https://") || url.startsWith("http://"));
    }
    return false;
}

DesktopFileReader &DesktopFileReader::operator =(const DesktopFileReader &other)
{
    d->desktopFile = other.d->desktopFile;
    d->appInfo = other.d->appInfo;
    d->keyFile = other.d->keyFile;
    d->iconKey = other.d->iconKey;
    return *this;
}
DesktopFileReader &DesktopFileReader::operator =(DesktopFileReader &&other)
{
    std::swap(d, other.d);
    return *this;
}

}
