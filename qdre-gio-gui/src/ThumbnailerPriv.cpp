/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <glib.h>

#include <ThumbnailerPriv.hpp>
#include <Thumbnailer.hpp>

#include <ExifHelper.hpp>
#include <UtilsCore.hpp>
#include <GIOUtils.hpp>
#include <GKeyFile.hpp>

#include <QtConcurrent/QtConcurrentRun>
#include <QCryptographicHash>
#include <QCoreApplication>
#include <QLoggingCategory>
#include <QStandardPaths>
#include <QDateTime>
#include <QtEndian>
#include <QTimer>
#include <QImage>
#include <QFile>
#include <QDir>

#include <exiv2/exiv2.hpp>

Q_LOGGING_CATEGORY(thumbnailer, "qdre.thumbnailer")

namespace QDRE {

constexpr const char *g_uriTag = "Thumb::URI";
constexpr const char *g_mtimeTag = "Thumb::MTime";

ThumbnailerPriv::ThumbnailerPriv(Thumbnailer &thumbnailer)
    : q(thumbnailer)
    , m_thumbnailsDir(QStandardPaths::standardLocations(QStandardPaths::GenericCacheLocation).value(0) + "/thumbnails")
    , m_threads(new QThreadPool(&q))
{
    const gchar *userDataDir = g_get_user_data_dir();
    const gchar *const *systemDataDirs = g_get_system_data_dirs();
    if (userDataDir)
        loadThumbnailers(QString(userDataDir) + "/thumbnailers");
    while (*systemDataDirs)
    {
        loadThumbnailers(QString(*systemDataDirs) + "/thumbnailers");
        ++systemDataDirs;
    }

    QObject::connect(&m_processManager, &ProcessManager::stateChanged,
                     &q, [this](const quint64 id, const ProcessManager::State state, const int errorCode) {
        processStateChanged(id, state, errorCode);
    });
}
ThumbnailerPriv::~ThumbnailerPriv()
{
    stopAll();
}

void ThumbnailerPriv::scheduleGenerate(const GUri &uri,
                                       const QString &mimeTypeArg,
                                       const QString &outdatedThumbnailPath,
                                       const int sizeArg)
{
    if (!uri.isLocal())
        return;

    auto future = QtConcurrent::run(m_threads, [=] {
        const QString mimeType = mimeTypeArg.isEmpty()
                ? GIOUtils::getMimeTypeForUri(uri)
                : mimeTypeArg;
        if (mimeType.isEmpty())
            return;

        const auto it = m_thumbnailers.constFind(mimeType);

        const bool tryExif = mimeType.startsWith(QLatin1String("image/"));
        const bool useThumbnailer = (it != m_thumbnailers.constEnd());

        if (!tryExif && !useThumbnailer)
            return;

        const QString localPath = uri.toLocalPath();

        if (localPath.startsWith(m_thumbnailsDir))
            return; // Don't generate thumbnails for thumbnails

        QString dirName;
        if (!outdatedThumbnailPath.isEmpty())
        {
            if (!outdatedThumbnailPath.startsWith(m_thumbnailsDir))
            {
                qCCritical(thumbnailer) << "Outdated thumbnail path:" << outdatedThumbnailPath
                                        << "is not valid thumbnails dir:" << m_thumbnailsDir;
                return;
            }
            dirName = QFileInfo(QFileInfo(outdatedThumbnailPath).path()).fileName();
        }

        int size;
        if (dirName.isEmpty())
        {
            size = qBound(32, sizeArg, 512);
            dirName = (size <= 128 ? "normal" : "large");
        }
        else
        {
            size = (dirName == "large") ? 256 : 128;
        }

        const QString outputPath = m_thumbnailsDir + "/" + dirName;
        const QString thumbnailPath = outputPath
                + "/"
                + QCryptographicHash::hash(uri.toString().toUtf8(), QCryptographicHash::Md5).toHex()
                + ".png";

        if (!outdatedThumbnailPath.isEmpty() && outdatedThumbnailPath != thumbnailPath)
        {
            qCCritical(thumbnailer) << "Outdated thumbnail path:" << outdatedThumbnailPath
                                    << "differs from new thumbnail path:" << thumbnailPath;
            return;
        }

        if (checkFailed({uri, failedFromThumbnail(thumbnailPath)}))
            return;

        if (!QDir().mkpath(outputPath))
        {
            qCCritical(thumbnailer) << "Can't create thumbnails directory:"
                                    << outputPath;
           return;
        }

        QFile::remove(thumbnailPath); // Remove old thumbnail (if exists)

        if (tryExif) try
        {
            const auto exiv2 = Exiv2::ImageFactory::open(uri.toLocalPath().toStdString());
            Q_ASSERT(exiv2.get());

            exiv2->readMetadata();

            Exiv2::PreviewManager loader(*exiv2);
            const auto list = loader.getPreviewProperties();

            const Exiv2::PreviewProperties *previewProps = nullptr;
            int minD = 0;
            for (auto &&preview : list)
            {
                const int d = std::max(std::abs(static_cast<qint64>(preview.width_)  - size),
                                       std::abs(static_cast<qint64>(preview.height_) - size));
                if (!previewProps || minD > d)
                {
                    minD = d;
                    previewProps = &preview;
                }
            }

            if (previewProps)
            {
                const auto preview = loader.getPreviewImage(*previewProps);
                QImage img = QImage::fromData(preview.pData(), preview.size());
                if (!img.isNull())
                {
                    if (img.width() > size || img.height() > size)
                        img = img.scaled(size, size, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                    QString savePath = thumbnailPath;
                    if (size <= 128)
                    {
                        const QFileInfo thumbnailPathInfo(thumbnailPath);
                        const QString path = thumbnailPathInfo.path();
                        if (path.endsWith("/large")) // If we wants large thumbnail, but it doesn't exist in Exif metadata
                        {
                            savePath = QDir::cleanPath(path + "/../normal");
                            if (!QDir().mkpath(savePath))
                            {
                                qCCritical(thumbnailer) << "Can't create thumbnails directory:"
                                                        << savePath;
                                return;
                            }
                            savePath += "/" + thumbnailPathInfo.fileName();
                        }
                    }
                    const auto &tags = exiv2->exifData();
                    const auto orientationIt = tags.findKey(Exiv2::ExifKey("Exif.Image.Orientation"));
                    if (orientationIt != tags.end())
                        img = ExifHelper::rotate(img, orientationIt->value().toInt64());
                    if (img.save(savePath, "PNG"))
                    {
                        updateThumbnailMetadataAndEmit({uri, savePath});
                        return;
                    }
                }
            }
        } catch (...) {}

        if (useThumbnailer)
        {
            QString thumbnailer = it.value();
            thumbnailer.replace("%i", "\"" + localPath + "\"");
            thumbnailer.replace("%u", "\"" + uri.toString() + "\"");
            thumbnailer.replace("%o", "\"" + thumbnailPath + "\"");
            thumbnailer.replace("%s", QString::number(size));

            QTimer::singleShot(0, &q, [=] { // Run in main thread
                const quint64 id = m_processManager.startOrEnqueue(thumbnailer);
                m_idAndUri[id] = {uri, thumbnailPath};
            });
        }
    });
    Q_UNUSED(future)
}

void ThumbnailerPriv::stopAll()
{
    m_threads->clear();
    m_threads->waitForDone(); // Is it safe enough?
    m_processManager.stopAll();
}

void ThumbnailerPriv::loadThumbnailers(const QString &dir)
{
    const QFileInfoList thumbnailers = QDir(dir).entryInfoList({"*.thumbnailer"}, QDir::Files | QDir::Readable);
    GKeyFile keyFile;
    keyFile.setGroupName("Thumbnailer Entry");
    for (const QFileInfo &fInfo : thumbnailers)
    {
        if (!keyFile.load(fInfo.filePath()))
            continue;

        const QString name = keyFile.getString("Exec");
        if (name.isEmpty())
            continue;

        const QStringList mimeTypes = keyFile.getStringList("MimeType");
        for (const QString &mimeType : mimeTypes)
        {
            if (!m_thumbnailers.contains(mimeType))
                m_thumbnailers[mimeType] = name;
        }
    }
}

void ThumbnailerPriv::processStateChanged(const quint64 id, const ProcessManager::State state, const int errorCode)
{
    if (state == ProcessManager::Started)
        return;

    const OrigAndThumb origAndThumb = m_idAndUri.take(id);

    if (state == ProcessManager::Finished && errorCode == 0)
    {
        updateThumbnailMetadataAndEmit(origAndThumb);
    }
    else
    {
        if (state != ProcessManager::Cancelled)
        {
            const QString failFileName = failedFromThumbnail(origAndThumb.second);
            if (QDir().mkpath(QFileInfo(failFileName).path()))
            {
                QImage failImg(1, 1, QImage::Format_ARGB32);
                failImg.fill(Qt::transparent);
                if (failImg.save(failFileName, "PNG", 100))
                    updateThumbnailMetadata({origAndThumb.first, failFileName});
            }
        }
        QFile::remove(origAndThumb.second);
    }
}

bool ThumbnailerPriv::checkFailed(const OrigAndThumb &origAndThumb)
{
    QImage failedThumbnail(origAndThumb.second);
    if (!failedThumbnail.isNull())
    {
        bool ok = false;
        const qint64 mtime = failedThumbnail.text(g_mtimeTag).toLongLong(&ok);
        const QString uri = failedThumbnail.text(g_uriTag);
        if (ok && !uri.isEmpty() && QFileInfo(origAndThumb.first.toLocalPath()).lastModified().toSecsSinceEpoch() == mtime && uri == origAndThumb.first.toString())
            return true;
    }
    QFile::remove(origAndThumb.second);
    return false;
}

QByteArray ThumbnailerPriv::generatePNGtEXtChunk(const QByteArray &key, const QByteArray &value)
{
    Q_ASSERT(key.length() >= 1 && key.length() <= 79);

    const qint32 length = key.length() + 1 + value.length();
    const quint32 lengthBE = qToBigEndian(length);

    QByteArray chunk;
    chunk.reserve(sizeof(lengthBE) + 4 + length + sizeof(quint32));

    chunk.append((const char *)&lengthBE, sizeof(lengthBE));
    chunk.append("tEXt");

    chunk.append(key);
    chunk.append('\0');
    chunk.append(value);

    const quint32 crc32BE = qToBigEndian(UtilsCore::crc32((const quint8 *)chunk.constData() + sizeof(lengthBE), chunk.size() - sizeof(lengthBE)));
    chunk.append((const char *)&crc32BE, sizeof(crc32BE));

    return chunk;
}
void ThumbnailerPriv::updateThumbnailMetadata(const ThumbnailerPriv::OrigAndThumb &origAndThumb)
{
    const QByteArray uriChunk = generatePNGtEXtChunk(
                g_uriTag,
                origAndThumb.first.toString().toUtf8());
    const QByteArray mtimeChunk = generatePNGtEXtChunk(
                g_mtimeTag,
                QByteArray::number(QFileInfo(origAndThumb.first.toLocalPath()).lastModified().toSecsSinceEpoch()));

    QFile pngFile(origAndThumb.second);
    if (pngFile.open(QFile::ReadOnly))
    {
        union ChunkType
        {
            const char name[4];
            quint32 value;
        };

        const quint8 pngSignature[8] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};
        const ChunkType tEXt {{'t', 'E', 'X', 't'}};
        const ChunkType IDAT {{'I', 'D', 'A', 'T'}};
        const ChunkType IEND {{'I', 'E', 'N', 'D'}};

        QByteArray pngData;
        pngData.reserve(pngFile.size() + uriChunk.size() + mtimeChunk.size());

        pngData += pngFile.read(8);
        if (pngData.size() != 8 || memcmp(pngData.constData(), pngSignature, 8) != 0)
            return;

        bool stored = false;
        while (!pngFile.atEnd())
        {
            quint32 lengthBE;
            quint32 type;

            if (pngFile.read((char *)&lengthBE, sizeof(lengthBE)) != sizeof(lengthBE))
                return;
            if (pngFile.read((char *)&type, sizeof(type)) != sizeof(type))
                return;

            const quint32 length = qFromBigEndian(lengthBE);

            if (type == tEXt.value)
            {
                pngFile.seek(pngFile.pos() + length + 4);
                continue;
            }

            if (!stored && type == IDAT.value)
            {
                pngData.append(uriChunk);
                pngData.append(mtimeChunk);
                stored = true;
            }

            pngData.append((const char *)&lengthBE, sizeof(lengthBE));
            pngData.append((const char *)&type, sizeof(type));
            pngData.append(pngFile.read(length + 4));

            if (type == IEND.value)
                break;
        }

        pngFile.close();

        if (pngFile.open(QFile::WriteOnly))
            pngFile.write(pngData);
    }
}

void ThumbnailerPriv::updateThumbnailMetadataAndEmit(const OrigAndThumb &origAndThumb)
{
    updateThumbnailMetadata(origAndThumb);
    emit q.finished(origAndThumb.first);
}

QString ThumbnailerPriv::failedFromThumbnail(const QString &thumbnailPath)
{
    const QFileInfo thumbnailInfo(thumbnailPath);
    return QDir::cleanPath(thumbnailInfo.path() + "/../fail/qdre/" + thumbnailInfo.fileName());
}

}
