/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <ProcessManager.hpp>

#include <QString>
#include <QHash>

class QThreadPool;
class QProcess;

namespace QDRE {

class Thumbnailer;
class GUri;

class ThumbnailerPriv
{
    using OrigAndThumb = QPair<GUri, QString>;

public:
    ThumbnailerPriv(Thumbnailer &thumbnailer);
    ~ThumbnailerPriv();

    void scheduleGenerate(const GUri &uri,
                          const QString &mimeTypeArg,
                          const QString &outdatedThumbnailPath,
                          const int sizeArg);

    void stopAll();

private:
    void loadThumbnailers(const QString &dir);

    void processStateChanged(const quint64 id, const ProcessManager::State state, const int errorCode);

    bool checkFailed(const OrigAndThumb &origAndThumb);

    QByteArray generatePNGtEXtChunk(const QByteArray &key, const QByteArray &value);
    void updateThumbnailMetadata(const OrigAndThumb &origAndThumb);

    void updateThumbnailMetadataAndEmit(const OrigAndThumb &origAndThumb);

    static QString failedFromThumbnail(const QString &thumbnailPath);

private:
    Thumbnailer &q;
    const QString m_thumbnailsDir;
    QThreadPool *const m_threads;

    QHash<QString, QString> m_thumbnailers;
    QHash<quint64, OrigAndThumb> m_idAndUri;
    ProcessManager m_processManager;
};

}
