/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QDialog>

class QScrollArea;
class QVBoxLayout;

namespace QDRE {

class FileOpItem;
class GIOFileOp;

class FileOpWidget : public QDialog
{
    Q_OBJECT

public:
    static FileOpWidget *instance();

private:
    FileOpWidget();
    ~FileOpWidget();

    void setSize(const bool setToMin = false);
    void doSetSize();

    void itemDestroyed();
public:
    void addFileOp(const GIOFileOp *fileOp);

private:
    void resizeEvent(QResizeEvent *) override;

    void closeEvent(QCloseEvent *event) override;

    bool eventFilter(QObject *o, QEvent *e) override;

private:
    QScrollArea *m_scroll;
    QWidget *m_w;
    QVBoxLayout *m_layout;
    qint32 m_fileOps = 0;
    bool m_pendingResizeSetToMin = false;
};

}
