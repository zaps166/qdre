/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <IOMenuActions.hpp>

#include <DesktopFileReader.hpp>
#include <FileProperties.hpp>
#include <GObjectHolder.hpp>
#include <KeySequence.hpp>
#include <ThemeIcon.hpp>
#include <OpenWith.hpp>
#include <GUriList.hpp>
#include <GIOModel.hpp>
#include <GIOUtils.hpp>
#include <GUri.hpp>

#include <QAbstractItemView>
#include <QCoreApplication>
#include <QLoggingCategory>
#include <QGuiApplication>
#include <QMimeDatabase>
#include <QClipboard>
#include <QMimeData>
#include <QFileInfo>
#include <QAction>
#include <QMenu>
#include <QUrl>

#include <functional>
#include <algorithm>
#include <optional>

namespace QDRE {
namespace IOMenuActions {

enum class OpenType
{
    Unknown,
    Directory,
    Executable,
    ExecutableScript,
    DesktopApp,
    DesktopLink,
    Application,
};

struct NameAppInfo
{
    QString name;
    GObjectHolder<GAppInfo> appInfo;
};

struct OpenTypeNameIcon
{
    OpenType type;
    QString name;
    QIcon icon;
};

static bool operator ==(const NameAppInfo &first, const NameAppInfo &second)
{
    return (first.name == second.name);
}
static int qHash(const NameAppInfo &nameAppInfo)
{
    return qHash(nameAppInfo.name);
}

/**/

static inline QAction *createAction(const QIcon &icon, const QString &text)
{
    return new QAction(icon, text, QCoreApplication::instance());
}

static QMimeData *getUrlsMimeData(GIOModel *gioModel, QItemSelectionModel *selectionModel)
{
    const GUriList uris = gioModel->getGUris(selectionModel->selection().indexes());
    if (uris.isEmpty())
        return nullptr;

    QList<QUrl> urls;
    for (const GUri &uri : uris)
        urls += uri.toUrl();

    QMimeData *mimeData = new QMimeData;
    mimeData->setUrls(urls);

    return mimeData;
}

static OpenTypeNameIcon getOpenTypeNameIcon(
        const GUri &uri,
        const GIOModel *model,
        const QModelIndex &index,
        const QString &mimeType)
{
    if (model->isDirectory(index))
    {
        QString appName;
        if (GAppInfo *info = g_app_info_get_default_for_type(mimeType.toLatin1().constData(), false))
        {
            appName = g_app_info_get_name(info);
            g_object_unref(info);
        }
        return {OpenType::Directory, appName, QIcon()};
    }

    QPair<bool, bool> executableAbility;
    if (model->isExecutable(index) && uri.isNative())
    {
        const QFileInfo fileInfo(uri.toLocalPath());
        executableAbility = GIOUtils::getExecutableAbility(
                    QMimeDatabase().mimeTypeForFile(fileInfo.filePath()),
                    uri.baseName());
    }

    if (executableAbility.first || executableAbility.second)
    {
        return {
            executableAbility.first
                ? OpenType::Executable
                : OpenType::ExecutableScript,
            uri.baseName(),
            QIcon()
        };
    }

    if (model->isDesktop(index))
    {
        const DesktopFileReader desktopFileReader(uri.toLocalPath(), DesktopFileReader::App);
        if (desktopFileReader.isApp())
        {
            return {OpenType::DesktopApp, desktopFileReader.name(), desktopFileReader.icon()};
        }
        return {OpenType::DesktopLink, QString(), QIcon()}; // Possibly Desktop Link file
    }

    if (GAppInfo *info = g_app_info_get_default_for_type(mimeType.toLatin1().constData(), false))
    {
        const QString appName = g_app_info_get_name(info);
        const QIcon appIcon = GIOUtils::getQIcon(g_app_info_get_icon(info));
        g_object_unref(info);
        return {OpenType::Application, appName, appIcon};
    }

    return {OpenType::Unknown, QString(), QIcon()};
}
static QString createOpenMenuAction(const OpenTypeNameIcon &openTypeNameIcon, QMenu *menu, const std::function<void()> &openDefault)
{
    QString defaultName;
    switch (openTypeNameIcon.type)
    {
        case OpenType::Unknown:
            break;
        case OpenType::Directory:
            defaultName = openTypeNameIcon.name;
            menu->addAction(ThemeIcon::get("folder"),
                            QCoreApplication::translate("IOMenuActions", "&Open"),
                            openDefault);
            break;
        case OpenType::Executable:
            menu->addAction(ThemeIcon::executable(),
                            QCoreApplication::translate("IOMenuActions", "&Run %1").arg(openTypeNameIcon.name),
                            openDefault);
            break;
        case OpenType::ExecutableScript:
            menu->addAction(ThemeIcon::executableScript(),
                            QCoreApplication::translate("IOMenuActions", "&Run %1").arg(openTypeNameIcon.name),
                            openDefault);
            break;
        case OpenType::DesktopApp:
            menu->addAction(openTypeNameIcon.icon,
                            QCoreApplication::translate("IOMenuActions", "&Open %1").arg(openTypeNameIcon.name),
                            openDefault);
            break;
        case OpenType::DesktopLink:
            menu->addAction(ThemeIcon::get("document-open"),
                            QCoreApplication::translate("IOMenuActions", "&Open"),
                            openDefault);
            break;
        case OpenType::Application:
            defaultName = openTypeNameIcon.name;
            menu->addAction(openTypeNameIcon.icon,
                            QCoreApplication::translate("IOMenuActions", "&Open with %1").arg(defaultName),
                            openDefault);
            break;
    }
    return defaultName;
}

static QSet<NameAppInfo> getAppsToOpen(const QString &mimeType, const QString &defaultAppName)
{
    QSet<NameAppInfo> appList;
    if (GList *appsInfo = g_app_info_get_all_for_type(mimeType.toLatin1().constData()))
    {
        GList *appsInfoBegin = appsInfo;
        do
        {
            GObjectHolder<GAppInfo> info(reinterpret_cast<GAppInfo *>(appsInfo->data));
            const QString name = g_app_info_get_name(info);
            if (defaultAppName != name)
            {
                appList += {name, std::move(info)};
            }

            appsInfo = appsInfo->next;
        } while (appsInfo);
        g_list_free(appsInfoBegin);
    }
    return appList;
}
static QMenu *createOpenWithMenu(const QSet<NameAppInfo> &appsToOpen, QMenu *menu, const std::function<void(GAppInfo *)> &openWith)
{
    QMenu *openWithMenu = menu->addMenu(QCoreApplication::translate("IOMenuActions", "Open &with..."));

    QList<QAction *> actions;

    for (const auto &app : appsToOpen)
    {
        QAction *action = new QAction(
                    GIOUtils::getQIcon(g_app_info_get_icon(app.appInfo)),
                    app.name,
                    openWithMenu);
        QObject::connect(action, &QAction::triggered,
                         openWithMenu, [openWith, appInfo = app.appInfo] {
            openWith(appInfo);
        });
        actions += action;
    }

    std::sort(actions.begin(), actions.end(), [](const auto &first, const auto &second) {
        return (first->text() < second->text());
    });

    openWithMenu->addActions(actions);

    return openWithMenu;
}

/**/

QAction *cut()
{
    static QAction *action = createAction(
                ThemeIcon::get("edit-cut"),
                QCoreApplication::translate("IOMenuActions", "&Cut"));
    return action;
}
QAction *copy()
{
    static QAction *action = createAction(
                ThemeIcon::get("edit-copy"),
                QCoreApplication::translate("IOMenuActions", "&Copy"));
    return action;
}

QAction *symlink()
{
    static QAction *action = createAction(
                ThemeIcon::get("emblem-symbolic-link"),
                QCoreApplication::translate("IOMenuActions", "&Create symlink"));
    return action;
}
QAction *rename()
{
    static QAction *action = createAction(
                ThemeIcon::get("edit-rename"),
                QCoreApplication::translate("IOMenuActions", "&Rename"));
    return action;
}

QAction *trash()
{
    static QAction *action = createAction(
                ThemeIcon::get("user-trash-full"),
                QCoreApplication::translate("IOMenuActions", "&Move to trash"));
    return action;
}
QAction *remove()
{
    static QAction *action = createAction(
                ThemeIcon::get("edit-delete"),
                QCoreApplication::translate("IOMenuActions", "R&emove"));
    return action;
}

QAction *emptyTrash()
{
    static QAction *action = createAction(
                ThemeIcon::get("user-trash"),
                QCoreApplication::translate("IOMenuActions", "&Empty trash"));
    return action;
}

QAction *properties()
{
    static QAction *action = createAction(
                ThemeIcon::get("document-properties"),
                QCoreApplication::translate("IOMenuActions", "&Properties"));
    return action;
}

void bind(QAbstractItemView *view, KeySequence *keySeq)
{
    Q_ASSERT(view);
    Q_ASSERT(keySeq);

    keySeq->pushContext("IOMenuActions");
    keySeq->bind(cut(), "Cut", "Ctrl+X");
    keySeq->bind(copy(), "Copy", "Ctrl+C");
    keySeq->bind(rename(), "Rename", "F2");
    keySeq->bind(trash(), "Trash", "Delete");
    keySeq->bind(remove(), "Remove", "Shift+Delete");
    keySeq->bind(properties(), "Properties", {"Alt+Return", "Alt+Enter"});
    keySeq->popContext();

    GIOModel *gioModel = qobject_cast<GIOModel *>(view->model());
    QItemSelectionModel *selectionModel = view->selectionModel();

    view->addAction(cut());
    view->addAction(copy());
    view->addAction(symlink());
    view->addAction(rename());
    view->addAction(trash());
    view->addAction(remove());
    view->addAction(properties());

    QObject::connect(cut(), &QAction::triggered,
                     view, [=] {
        if (QMimeData *mimeData = getUrlsMimeData(gioModel, selectionModel))
        {
            GIOUtils::setMimeDataCutSelection(mimeData);
            QGuiApplication::clipboard()->setMimeData(mimeData);
        }
    });
    QObject::connect(copy(), &QAction::triggered,
                     view, [=] {
        if (QMimeData *mimeData = getUrlsMimeData(gioModel, selectionModel))
            QGuiApplication::clipboard()->setMimeData(mimeData);
    });

    QObject::connect(symlink(), &QAction::triggered,
                     view, [=] {
        const QModelIndexList selectedIndexes = selectionModel->selection().indexes();
        GUriList srcUris, dstUris;
        for (const QModelIndex &index : selectedIndexes)
        {
            const GUri srcUri = gioModel->getGUri(index);
            GUri dstUri = srcUri;
            if (dstUri.replaceBaseName(QCoreApplication::translate("IOMenuActions", "Symlink to %1").arg(srcUri.baseName())))
            {
                srcUris += srcUri;
                dstUris += dstUri;
            }
        }
        GIOFileOp::Ptr ptr = GIOFileOp::link(srcUris, dstUris);
        if (!ptr.isNull())
            gioModel->scheduleItemChangeWithSelection(ptr, srcUris);
    });
    QObject::connect(rename(), &QAction::triggered,
                     view, [=] {
        const QModelIndex index = selectionModel->currentIndex();
        view->edit(index);
    });

    QObject::connect(trash(), &QAction::triggered,
                     view, [=] {
        const QModelIndexList selectedIndexes = selectionModel->selection().indexes();
        const GUriList uris = gioModel->getGUris(selectedIndexes);
        GIOFileOp::Ptr ptr = GIOFileOp::trash(uris);
        if (!ptr.isNull())
            gioModel->scheduleItemChangeWithSelection(ptr, uris);
    });
    QObject::connect(remove(), &QAction::triggered,
                     view, [=] {
        const QModelIndexList selectedIndexes = selectionModel->selection().indexes();
        const GUriList uris = gioModel->getGUris(selectedIndexes);
        GIOFileOp::Ptr ptr = GIOFileOp::unlink(uris);
        if (!ptr.isNull())
            gioModel->scheduleItemChangeWithSelection(ptr, uris);
    });

    QObject::connect(emptyTrash(), &QAction::triggered,
                     view, [] {
        GIOFileOp::emptyTrash();
    });

    QObject::connect(properties(), &QAction::triggered,
                     view, [=] {
        const QModelIndexList selectedIndexes = selectionModel->selection().indexes();
        const GUriList uris = gioModel->getGUris(selectedIndexes);
        const QStringList mimeTypes = gioModel->getMimeTypes(selectedIndexes);
        (new FileProperties(uris, mimeTypes, view))->show();
    });
}

void updateMenuActions(const GIOModel *model, const QModelIndex &index, const bool notFirstIndex)
{
    const auto maybeSetEnabled = [=](QAction *action, const bool enabled) {
        if (!notFirstIndex || !enabled)
            action->setEnabled(enabled);
    };

    const bool isReadable = model->isReadable(index);

    maybeSetEnabled(cut(), isReadable && model->canDelete(index));
    maybeSetEnabled(copy(), isReadable);

    maybeSetEnabled(symlink(), model->isDirectoryWritable());
    maybeSetEnabled(rename(), !notFirstIndex && !model->data(index, Qt::EditRole).isNull());

    maybeSetEnabled(trash(), model->canTrash(index));
    maybeSetEnabled(remove(), model->canDelete(index));
}
void pasteActionEnableDisable(QAction *act)
{
    const QMimeData *mimeData = QGuiApplication::clipboard()->mimeData();
    act->setEnabled(mimeData && ((mimeData->hasUrls() && !mimeData->urls().isEmpty())
                                 || mimeData->hasText()
                                 || mimeData->hasImage()));
}

QMenu *createContextMenu(QWidget *parent,
                         const GIOModel *model,
                         const QList<QPersistentModelIndex> &indexes,
                         const QList<QAction *> &additionalActions)
{
    QMenu *menu = new QMenu(parent);

    const auto errorReturn = [&] {
        for (QAction *act : additionalActions)
            delete act;
        delete menu;
        return nullptr;
    };

    if (indexes.isEmpty())
        return errorReturn();

    if (indexes.count() == 1)
    {
        const QPersistentModelIndex index = indexes[0];
        const GUri uri = model->getGUri(index);

        const QString mimeType = model->getMimeType(index);
        if (mimeType.isEmpty())
            return errorReturn();

        const auto openDefault = [=] {
#ifdef GIO_LAUNCH
            GIOUtils::launch(uri, model->getMimeType(index), model->isDesktop(index));
#endif
        };
        const auto openWith = [=](GAppInfo *appInfo) {
#ifdef GIO_LAUNCH
            GIOUtils::launchWith(uri, appInfo);
#endif
        };

        const OpenTypeNameIcon openTypeNameIcon = getOpenTypeNameIcon(uri, model, index, mimeType);
        const QString defaultAppName = createOpenMenuAction(openTypeNameIcon, menu, openDefault);
        const QSet<NameAppInfo> appsToOpen = getAppsToOpen(mimeType, defaultAppName);

        QMenu *openWithMenu = createOpenWithMenu(appsToOpen, menu, openWith);
        openWithMenu->addSeparator();
        openWithMenu->addAction(QCoreApplication::translate("IOMenuActions", "&Other..."), [=] {
            (new OpenWith(mimeType, {model->getGUri(index)}, !model->isDesktop(index), parent))->show();
        });
    }
    else
    {
        const auto openDefaultAll = [=] {
#ifdef GIO_LAUNCH
            GIOUtils::UrisWithMimeType urisToRun;
            for (const QPersistentModelIndex &index : indexes)
            {
                if (!index.isValid())
                    continue;

                const GUri uri = model->getGUri(index);
                const QString mimeType = model->getMimeType(index);
                if (mimeType.isEmpty())
                    continue;

                if (model->isDesktop(index))
                    GIOUtils::launch(uri, mimeType, true);
                else
                    urisToRun += {uri, mimeType};
            }
            GIOUtils::launchMulti(urisToRun);
#endif
        };

        std::optional<OpenTypeNameIcon> lastOpenTypeNameIcon;
        bool lastOpenTypeNameIconNull = true;
        QString commonMimeType;
        bool allAreDesktop = false;
        for (const QPersistentModelIndex &index : indexes)
        {
            if (!index.isValid())
                continue;

            const GUri uri = model->getGUri(index);
            const QString mimeType = model->getMimeType(index);
            if (mimeType.isEmpty())
                continue;

            if (!allAreDesktop && model->isDesktop(index))
                allAreDesktop = true;

            if (commonMimeType.isNull())
            {
                commonMimeType = mimeType;
            }
            else if (!commonMimeType.isEmpty() && mimeType != commonMimeType)
            {
                commonMimeType = ""; // Empty, but not null
                Q_ASSERT(commonMimeType.isEmpty());
                Q_ASSERT(!commonMimeType.isNull());
            }

            OpenTypeNameIcon openTypeNameIcon = getOpenTypeNameIcon(uri, model, index, mimeType);
            if (lastOpenTypeNameIconNull)
            {
                lastOpenTypeNameIcon = std::move(openTypeNameIcon);
                lastOpenTypeNameIconNull = false;
            }
            else if (lastOpenTypeNameIcon.has_value() && (openTypeNameIcon.type != lastOpenTypeNameIcon->type ||
                                                          openTypeNameIcon.name != lastOpenTypeNameIcon->name))
            {
                lastOpenTypeNameIcon.reset();
            }
        }

        QString defaultAppName;
        if (lastOpenTypeNameIcon.has_value())
        {
            defaultAppName = createOpenMenuAction(lastOpenTypeNameIcon.value(),
                                                  menu,
                                                  openDefaultAll);
        }
        else
        {
            menu->addAction(ThemeIcon::get("document-open"),
                            QCoreApplication::translate("IOMenuActions", "&Open %1 elements").arg(indexes.count()),
                            openDefaultAll);
        }

        QSet<NameAppInfo> appsToOpenIntersected;
        bool appsToOpenIntersectedNull = true;
        for (const QPersistentModelIndex &index : indexes)
        {
            if (!index.isValid())
                continue;

            QSet<NameAppInfo> appsToOpen = getAppsToOpen(model->getMimeType(index), defaultAppName);
            if (appsToOpenIntersectedNull)
            {
                appsToOpenIntersected = std::move(appsToOpen);
                appsToOpenIntersectedNull = false;
            }
            else
            {
                appsToOpenIntersected &= appsToOpen;
            }
        }

        if (!appsToOpenIntersected.isEmpty() || !commonMimeType.isEmpty())
        {
            const auto openWithAll = [=](GAppInfo *appInfo) {
#ifdef GIO_LAUNCH
                GIOUtils::launchWithMulti(model->getGUris(indexes), appInfo);
#endif
            };
            QMenu *openWithMenu = createOpenWithMenu(appsToOpenIntersected, menu, openWithAll);
            if (!commonMimeType.isEmpty())
            {
                openWithMenu->addSeparator();
                openWithMenu->addAction(QCoreApplication::translate("IOMenuActions", "&Other..."), [=] {
                    (new OpenWith(commonMimeType, model->getGUris(indexes), !allAreDesktop, parent))->show();
                });
            }
        }
    }

    menu->addSeparator();

    menu->addAction(cut());
    menu->addAction(copy());

    menu->addSeparator();

    menu->addAction(symlink());
    menu->addAction(rename());

    menu->addSeparator();

    menu->addAction(trash());
    menu->addAction(remove());

    if (indexes.count() == 1 && model->isTrashContainer(indexes[0]))
    {
        QAction *emptyTrashAct = emptyTrash();
        emptyTrashAct->setEnabled(model->canEmptyTrash(indexes[0]));
        menu->addSeparator();
        menu->addAction(emptyTrashAct);
    }

    if (!additionalActions.isEmpty())
    {
        menu->addSeparator();
        for (QAction *act : additionalActions)
        {
            if (!act)
                continue;
            if (act->parent() == nullptr)
                act->setParent(menu);
            menu->addAction(act);
        }
    }

    menu->addSeparator();

    menu->addAction(properties());

    return menu;
}

}
}
