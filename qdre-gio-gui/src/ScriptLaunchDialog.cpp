/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ScriptLaunchDialog.hpp>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QStyle>
#include <QLabel>

namespace QDRE {

ScriptLaunchDialog::ScriptLaunchDialog(const QString &fileName, QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(tr("Launching a script"));

    QLabel *questionL = new QLabel(this);
    const int pixmapSize = style()->pixelMetric(QStyle::PM_MessageBoxIconSize, nullptr, this);
    questionL->setPixmap(style()->standardIcon(QStyle::SP_MessageBoxQuestion).pixmap({pixmapSize, pixmapSize}));

    QLabel *infoL = new QLabel(tr("Launch \"%1\" file or show its contents?").arg(fileName),
                               this);
    infoL->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    QPushButton *launchInTerminal = new QPushButton(tr("Launch in terminal"), this);
    QPushButton *display = new QPushButton(tr("Display"), this);
    QPushButton *cancel = new QPushButton(tr("Cancel"), this);
    QPushButton *launch = new QPushButton(tr("Launch"), this);

    connect(launchInTerminal, &QPushButton::clicked,
            this, [=] {
        m_type = Type::LaunchInTerminal;
        accept();
    });
    connect(display, &QPushButton::clicked,
            this, [=] {
        m_type = Type::Display;
        accept();
    });
    connect(cancel, &QPushButton::clicked,
            this, &ScriptLaunchDialog::reject);
    connect(launch, &QPushButton::clicked,
            this, [=] {
        m_type = Type::Launch;
        accept();
    });

    cancel->setFocus();

    QHBoxLayout *layout1 = new QHBoxLayout;
    layout1->addWidget(questionL);
    layout1->addWidget(infoL);
    layout1->setSpacing(10);
    layout1->setContentsMargins(0, 0, 0, 0);

    QHBoxLayout *layout2 = new QHBoxLayout;
    layout2->addWidget(launchInTerminal);
    layout2->addWidget(display);
    layout2->addWidget(cancel);
    layout2->addWidget(launch);
    layout2->setContentsMargins(0, 0, 0, 0);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(layout1);
    mainLayout->addLayout(layout2);
}
ScriptLaunchDialog::~ScriptLaunchDialog()
{}

}
