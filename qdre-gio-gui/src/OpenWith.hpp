/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GObjectHolder.hpp>

#include <QDialog>

#include <GUri.hpp>

class QDialogButtonBox;
struct _GAppInfo;
class QCheckBox;
class QGroupBox;
class QLineEdit;

namespace QDRE {

class OpenWith : public QDialog
{
    Q_OBJECT

public:
    OpenWith(const QString &mimeType, const GUriList &uris, const bool selectCurrent, QWidget *parent = nullptr);
    OpenWith(const QString &mimeType, QWidget *parent = nullptr);
    ~OpenWith();

    inline GObjectHolder<_GAppInfo> getAppInfo() const;

private:
    void maybeSetOpenButtonEnabled();

private Q_SLOTS:
    void accept() override;

private:
    const QByteArray m_mimeType;
    const GUriList m_uris;
    QString m_customOriginalCommand;
    GObjectHolder<_GAppInfo> m_appInfo;
    QGroupBox *m_customCommand;
    QLineEdit *m_customCommandEdit;
    QCheckBox *m_storeCheckBox = nullptr;
    QDialogButtonBox *m_buttons;
};

/* Inline implementation */

GObjectHolder<_GAppInfo> OpenWith::getAppInfo() const
{
    return m_appInfo;
}

}
