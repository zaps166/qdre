/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gdesktopappinfo.h>
#include <GObjectHolder.hpp>

#include <GIOUtils.hpp>

#include <ProcessEnvironment.hpp>
#include <ScriptLaunchDialog.hpp>
#include <CreateDesktopFile.hpp>
#include <DesktopFileReader.hpp>
#include <InitializeCore.hpp>
#include <GSettings.hpp>
#include <ThemeIcon.hpp>
#include <GIOFileOp.hpp>
#include <UtilsCore.hpp>
#include <GIOModel.hpp>
#include <Utils.hpp>

#include <QRegularExpression>
#include <QLoggingCategory>
#include <QMimeDatabase>
#include <QApplication>
#include <QInputDialog>
#include <QMessageBox>
#include <QClipboard>
#include <QFileInfo>
#include <QMimeData>
#include <QProcess>
#include <qevent.h>
#include <QBuffer>
#include <QMutex>
#include <QTimer>
#include <QIcon>
#include <QMenu>

#ifdef GIO_LAUNCH
# include <KWindowSystem>
# include <KStartupInfo>
# include <KWindowInfo>
# include <KX11Extras>
#endif

Q_LOGGING_CATEGORY(gioutils, "qdre.gio.utils")

namespace QDRE {
namespace GIOUtils {

constexpr const char *g_cutSelection = "application/x-kde-cutselection";

#ifdef GIO_LAUNCH
static ProcessEnvironment *g_processEnvironment = nullptr;
static QMutex g_notifyMutex;

static QString g_defaultTerminal;

QDRE_ADD_INIT_FUNCTION([] {
    g_processEnvironment = new ProcessEnvironment(QCoreApplication::instance());

    auto settings = new GSettings(QCoreApplication::instance(), "gio");
    settings->installSettingsChangedFunction("default-terminal", [=](const QString &key) {
        g_defaultTerminal = settings->getString(key);
    });
    settings->triggerInstalledValues();
})
#endif

/**/

#ifdef GIO_LAUNCH
static bool startupNotify(GDesktopAppInfo *desktopAppInfo, const bool maybeCheckProcesses = true)
{
    GAppInfo *appInfo = G_APP_INFO(desktopAppInfo);
    if (!appInfo)
        return false;

    if (!g_desktop_app_info_get_boolean(desktopAppInfo, "StartupNotify"))
        return false;

    const QByteArray wmClass = g_desktop_app_info_get_startup_wm_class(desktopAppInfo);
    const QString baseName = GUri(g_app_info_get_executable(appInfo)).baseName();

    if (!wmClass.isEmpty())
    {
        const QList<WId> windows = KX11Extras::windows();
        for (WId id : windows)
        {
            const KWindowInfo winInfo(id, NET::Properties(), NET::Property2::WM2WindowClass);
            const QByteArray wmClassTmp = winInfo.windowClassClass();
            if (wmClassTmp == wmClass)
                return false;
        }
    }
    else if (maybeCheckProcesses && UtilsCore::checkProcessExists(baseName))
    {
        return false;
    }

    g_notifyMutex.lock();

    KStartupInfoId id;
    id.initId(KStartupInfo::createNewStartupId());
    if (!id.setupStartupEnv())
    {
        g_notifyMutex.unlock();
        return false;
    }

    KStartupInfoData data;
    data.setHostname();
    data.setBin(baseName);
    data.setName(g_app_info_get_name(appInfo));
    data.setDescription(QCoreApplication::translate("GIOUtils", "Launching %1").arg(data.name()));
    if (!wmClass.isEmpty())
        data.setWMClass(wmClass);
    data.setDesktop(KX11Extras::currentDesktop());

    const QVariant vIcon = getIcon(g_app_info_get_icon(appInfo));
    switch (vIcon.typeId())
    {
        case QMetaType::QString:
            data.setIcon(vIcon.toString());
            break;
        case QMetaType::QIcon:
            data.setIcon(vIcon.value<QIcon>().name());
            break;
        default:
            data.setIcon(data.findIcon());
            break;
    }

    const bool ret = KStartupInfo::sendStartup(id, data);

    if (!ret)
        g_notifyMutex.unlock();

    return ret;
}
static void stopCurrentNotify()
{
    KStartupInfoId id;
    id.initId();
    KStartupInfo::sendFinish(id);
}

static void notifyFinish(const bool ret)
{
    if (!ret)
        stopCurrentNotify();
    KStartupInfo::resetStartupEnv();
    g_notifyMutex.unlock();
}

static void launchFinish(const GUri &uri, const bool ret, const bool hasStartupNotify)
{
    if (hasStartupNotify)
        notifyFinish(ret);
    if (!ret)
    {
        QTimer::singleShot(0, [=] {
            QMessageBox::critical(nullptr, QString(), QCoreApplication::translate("GIOUtils", "Unable to open: \"%1\"")
                                  .arg(uri.toString()));
        });
    }
}

static bool launchDefault(const GUri &uri, const QMimeType &mimeType = QMimeType())
{
    bool hasStartupNotify = false;
    if (mimeType.isValid())
    {
        if (GAppInfo *appInfo = g_app_info_get_default_for_type(mimeType.name().toLatin1().constData(), false))
        {
            hasStartupNotify = startupNotify(G_DESKTOP_APP_INFO(appInfo));
            g_object_unref(appInfo);
        }
    }

    const bool ret = g_app_info_launch_default_for_uri(uri.toString().toUtf8().constData(),
                                                       g_processEnvironment->getGAppLaunchContext(uri.baseName()),
                                                       nullptr);
    launchFinish(uri, ret, hasStartupNotify);

    return ret;
}

static bool launchApp(GAppInfo *appInfo, const GUriList &uris, QString *errorMsg = nullptr)
{
    GList *urisList = nullptr;
    for (const GUri &uri : uris)
        urisList = g_list_append(urisList, g_strdup(uri.toString().toUtf8().constData()));

    QString fileName;
    if (g_processEnvironment->hasApplicationsEnv())
        fileName = QFileInfo(g_app_info_get_executable(appInfo)).fileName();

    GError *error = nullptr;
    const bool ret = g_app_info_launch_uris(appInfo,
                                            urisList,
                                            g_processEnvironment->getGAppLaunchContext(fileName),
                                            errorMsg ? &error : nullptr);
    if (error)
    {
        *errorMsg = error->message;
        g_error_free(error);
    }

    if (urisList)
    {
        GList *urisListBegin = urisList;
        while (urisList)
        {
            g_free(urisList->data);
            urisList = urisList->next;
        }
        g_list_free(urisListBegin);
    }

    return ret;
}

static bool runProcess(const QFileInfo &fileInfo, const bool inTerminal, const QStringList &args, const QString &workingDirectory = QString())
{
    const QString absoluteFilePath = fileInfo.absoluteFilePath();

    QProcess process;
    process.setWorkingDirectory(QFileInfo(workingDirectory).isDir()
                                ? workingDirectory
                                : fileInfo.absolutePath());

    if (inTerminal)
    {
        QStringList terminalArgs {
            "-e",
            "\"" + absoluteFilePath + "\""
        };

        if (!args.isEmpty())
        {
            QString &cmdline = terminalArgs[1];
            for (const QString &arg : args)
                cmdline += " \"" + arg + "\"";
        }

        process.setProgram(g_defaultTerminal);
        process.setArguments(terminalArgs);
    }
    else
    {
        process.setProgram(absoluteFilePath);
        process.setArguments(args);
    }

    const auto processEnv = g_processEnvironment->getQProcessEnvironment(fileInfo.fileName());
    if (processEnv.has_value())
        process.setProcessEnvironment(processEnv.value());

    if (!process.startDetached())
    {
        qCCritical(gioutils) << "Can't launch process:" << process.program();
        return false;
    }
    return  true;
}
static bool runProcess(const QFileInfo &fileInfo, const bool inTerminal, const GUriList &uris)
{
    QStringList args;
    args.reserve(uris.count());
    for (const GUri &uri : uris)
        args += uri.toDecoded(); // XXX: Is it always OK?
    return runProcess(fileInfo, inTerminal, args);
}
#endif

/**/

#ifdef GIO_LAUNCH
QString getDefaultTerminal()
{
    return g_defaultTerminal;
}
#endif

QString getDefaultExecutableForType(const QString &mimeType)
{
    QString executable;
    if (GAppInfo *appInfo = g_app_info_get_default_for_type(mimeType.toLatin1().constData(), false))
    {
        executable = g_app_info_get_executable(appInfo);
        g_object_unref(appInfo);
    }
    return executable;
}

QPair<bool, bool> getExecutableAbility(const QMimeType &mimeType, const QString &baseName)
{
    const QStringList parentMimeTypes = mimeType.parentMimeTypes();
    const QString mimeTypeName = mimeType.name();

    const bool parentTextPlain = parentMimeTypes.contains("text/plain");
    const bool parentAppExec = parentMimeTypes.contains("application/x-executable");
    bool canRunProcess = (mimeTypeName == "application/x-executable");

    if (!canRunProcess && parentAppExec && !parentTextPlain)
    {
        for (const QString &mimeTypeName : parentMimeTypes)
        {
            if (mimeTypeName.startsWith("application/") && mimeTypeName != "application/x-executable")
            {
                canRunProcess = true;
                break;
            }
        }
    }

    return {canRunProcess, parentAppExec && parentTextPlain};
}

QVariant getIcon(_GIcon *icon)
{
    if (G_IS_THEMED_ICON(icon))
    {
        const char *const *names = g_themed_icon_get_names(reinterpret_cast<GThemedIcon *>(icon));
        for (qint32 i = 0; names && names[i]; ++i)
        {
            const QIcon themedIcon = ThemeIcon::get(names[i]);
            if (!themedIcon.isNull())
                return themedIcon;
        }
    }
    else if (G_IS_FILE_ICON(icon))
    {
        GFile *iconFile = g_file_icon_get_file(reinterpret_cast<GFileIcon *>(icon));
        if (char *iconFileRaw = g_file_get_path(iconFile))
        {
            const QString fileIconPath = iconFileRaw;
            g_free(iconFileRaw);
            return fileIconPath;
        }
    }
    return QVariant();
}
QIcon getQIcon(_GIcon *icon, const bool octetStreamFallback, const bool executableFallback)
{
    const QVariant vIcon = getIcon(icon);
    QIcon qIcon;

    if (vIcon.typeId() == QMetaType::QIcon)
        qIcon = vIcon.value<QIcon>();
    else if (vIcon.typeId() == QMetaType::QString)
        qIcon = QIcon(vIcon.toString());

    if (qIcon.isNull())
    {
        if (octetStreamFallback)
            qIcon = ThemeIcon::octetStream();
        else if (executableFallback)
            qIcon = ThemeIcon::executable();
    }

    return qIcon;
}

QString getMimeTypeForUri(const GUri &uri)
{
    if (GFileInfo *info = g_file_query_info(uri.gFile(),
                                            G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                            G_FILE_QUERY_INFO_NONE,
                                            nullptr,
                                            nullptr))
    {
        const QString mimeType = g_file_info_get_content_type(info);
        g_object_unref(info);
        return mimeType;
    }
    return QString();
}
QString getDescriptionFromMimeType(const QString &mimeType)
{
    if (gchar *description = g_content_type_get_description(mimeType.toLatin1().constData()))
    {
        const QString qDescription(description);
        g_free(description);
        return qDescription;
    }
    return QString();
}
QIcon getQIconFromMimeType(const QString &mimeType)
{
    GObjectHolder<GIcon> gIcon(g_content_type_get_icon(mimeType.toLatin1().constData()));
    return getQIcon(gIcon, true);
}

GUri getDesktopLinkToContainer(const QString &desktopFile, const bool allowTrash)
{
    const DesktopFileReader desktopFileReader(desktopFile, DesktopFileReader::Link);
    if (desktopFileReader.isLink())
    {
        const QString desktopUrl = desktopFileReader.linkUrl();

        if (allowTrash)
        {
            GUri uri(desktopUrl);
            if (uri.isTrash())
                return uri;
        }

        const QFileInfo fileInfo(desktopUrl);
        if (fileInfo.isDir())
            return GUri(fileInfo.absoluteFilePath());
    }
    return GUri();
}

#ifdef GIO_LAUNCH
bool runProcess(const GUri &uri, const bool inTerminal, const QStringList &args, const QString &workingDirectory)
{
    if (!uri.isLocal())
        return false;

    const QFileInfo fileInfo(uri.toLocalPath());
    if (!fileInfo.isExecutable() || !fileInfo.isFile())
        return false;

    return runProcess(fileInfo, inTerminal, args, workingDirectory);
}

bool launch(const GUri &uriArg, const QString &uriMimeType, const bool isDesktop, const GUriList &uris, const QString &desktopAction)
{
    const QMimeDatabase mimeDB;

    QMimeType mimeType = mimeDB.mimeTypeForName(uriMimeType);

    if (!uriArg.isLocal())
        return launchDefault(uriArg, mimeType);

    GUri uri = uriArg;
    if (!mimeType.isValid())
        mimeType = mimeDB.mimeTypeForName(getMimeTypeForUri(uri));

    if (isDesktop)
    {
        const DesktopFileReader desktopFileReader(uri.toLocalPath());
        if (desktopFileReader.isApp())
        {
            QString errorMsg;
            const bool hasStartupNotify = startupNotify(desktopFileReader.getDesktopAppInfoPtr(), !uris.isEmpty());
            bool ret = true;
            if (desktopAction.isEmpty())
                ret = launchApp(desktopFileReader.getAppInfoPtr(), uris, &errorMsg);
            else
                g_desktop_app_info_launch_action(desktopFileReader.getDesktopAppInfoPtr(), desktopAction.toUtf8().constData(), nullptr);
            if (hasStartupNotify)
                notifyFinish(ret);
            if (!errorMsg.isEmpty())
            {
                QTimer::singleShot(0, [=] {
                    QMessageBox::critical(nullptr, QString(), errorMsg);
                });
            }
            return ret;
        }
        else if (desktopFileReader.isLink())
        {
            const GUri linkUri(desktopFileReader.linkUrl());
            if (linkUri.isValid())
            {
                uri = linkUri;

                if (!uri.isLocal())
                {
                    mimeType = desktopFileReader.isHttpLink()
                            ? mimeDB.mimeTypeForName("text/html")
                            : QMimeType();
                    return launchDefault(uri, mimeType);
                }

                mimeType = mimeDB.mimeTypeForName(getMimeTypeForUri(linkUri));
            }
            else
            {
                QTimer::singleShot(0, [=] {
                    QMessageBox::critical(nullptr, QString(), QCoreApplication::translate("GIOUtils", "Desktop file didn't specify URL field"));
                });
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    const QFileInfo fileInfo(uri.toLocalPath());

    if (fileInfo.isExecutable())
    {
        const QPair<bool, bool> executableAbility = getExecutableAbility(mimeType, uri.baseName());
        if (executableAbility.first)
        {
            return runProcess(fileInfo, false, uris);
        }
        else if (executableAbility.second)
        {
            if (!qobject_cast<QApplication *>(QGuiApplication::instance()))
                return runProcess(fileInfo, false, uris);

            ScriptLaunchDialog sld(fileInfo.fileName());
            if (sld.exec() == QDialog::Rejected)
                return false;
            switch (sld.type())
            {
                case ScriptLaunchDialog::Type::Display:
                    break;
                case ScriptLaunchDialog::Type::Launch:
                    return runProcess(fileInfo, false, uris);
                case ScriptLaunchDialog::Type::LaunchInTerminal:
                    return runProcess(fileInfo, true, uris);
            }
        }
    }

    return launchDefault(uri, mimeType);
}

void launchMulti(const UrisWithMimeType &urisWithMimeType)
{
    QHash<QByteArray, QPair<GObjectHolder<GAppInfo>, GUriList>> appsToLaunch;
    const QMimeDatabase mimeDB;

    for (const auto &uriWithMimeType : urisWithMimeType)
    {
        const GUri &uri = uriWithMimeType.first;
        const QString &mimeType = uriWithMimeType.second;

        if (uri.isLocal())
        {
            const QFileInfo fileInfo(uri.toLocalPath());
            if (fileInfo.isExecutable())
            {
                const QPair<bool, bool> executableAbility = getExecutableAbility(
                            mimeDB.mimeTypeForName(getMimeTypeForUri(uri)),
                            uri.baseName());
                if (executableAbility.first || executableAbility.second)
                {
                    launch(uri);
                    continue;
                }
            }
        }

        if (GAppInfo *appInfo = g_app_info_get_default_for_type(mimeType.toLatin1().constData(), false))
        {
            const QByteArray commandLine = g_app_info_get_commandline(appInfo);
            if (commandLine.endsWith("%F") || commandLine.endsWith("%U"))
            {
                auto &pair = appsToLaunch[commandLine];
                if (pair.first.isNull())
                    pair.first.ref(appInfo);
                pair.second += uri;
            }
            else
            {
                launchDefault(uri, mimeDB.mimeTypeForName(mimeType));
            }
            g_object_unref(appInfo);
        }
    }

    for (const auto &appToLaunch : as_const(appsToLaunch))
    {
        const GObjectHolder<GAppInfo> &appInfo = appToLaunch.first;
        const GUriList &uris = appToLaunch.second;
        const bool hasStartupNotify = startupNotify(G_DESKTOP_APP_INFO(appInfo.ptr()));
        const bool ret = launchApp(appInfo, uris);
        if (hasStartupNotify)
            notifyFinish(ret);
    }
}

bool launchWithMulti(const GUriList &uris, GAppInfo *appInfo)
{
    if (!appInfo || uris.isEmpty())
        return false;

    const auto launch = [appInfo](const GUriList &uris) {
        const bool hasStartupNotify = startupNotify(G_DESKTOP_APP_INFO(appInfo));
        const bool ret = launchApp(appInfo, uris);
        if (uris.count() == 1)
            launchFinish(uris[0], ret, hasStartupNotify);
        else if (hasStartupNotify)
            notifyFinish(ret);
        return ret;
    };

    bool ret = true;

    const QByteArray commandLine = g_app_info_get_commandline(appInfo);
    if (commandLine.endsWith("%F") || commandLine.endsWith("%U"))
    {
        ret = launch(uris);
    }
    else for (const GUri &uri : uris)
    {
        ret &= launch({uri});
    }

    return ret;
}
bool launchWith(const GUri &uri, GAppInfo *appInfo)
{
    return launchWithMulti({uri}, appInfo);
}
#endif

GAppInfo *createAppInfo(const QString &executable)
{
    GAppInfo *appInfo = g_app_info_create_from_commandline(
                executable.toUtf8().constData(),
                nullptr,
                G_APP_INFO_CREATE_NONE,
                nullptr);
    return appInfo;
}

Qt::DropAction fileOpDropMenu(QWidget *parent, QDropEvent *event, const bool desktopLink)
{
    if (event->modifiers() & (Qt::ShiftModifier | Qt::ControlModifier | Qt::AltModifier))
        return event->dropAction();

    QVector<QAction *> actions;
    actions.reserve(3);

    QMenu *menu = new QMenu(parent);
    actions += menu->addAction(ThemeIcon::get("go-jump"),   QCoreApplication::translate("GIOUtils", "Move here"));
    actions += menu->addAction(ThemeIcon::get("edit-copy"), QCoreApplication::translate("GIOUtils", "Copy here"));
    actions += menu->addAction(ThemeIcon::get("edit-link"), QCoreApplication::translate("GIOUtils", "Link here"));
    if (desktopLink)
    {
        menu->addSeparator();
        actions += menu->addAction(ThemeIcon::get("preferences-desktop"), QCoreApplication::translate("GIOUtils", "Desktop link here"));
    }
    menu->addSeparator();
    menu->addAction(ThemeIcon::get("window-close"), QCoreApplication::translate("GIOUtils", "Cancel"));

    actions.at(0)->setEnabled(event->possibleActions() & Qt::MoveAction);
    actions.at(1)->setEnabled(event->possibleActions() & Qt::CopyAction);
    actions.at(2)->setEnabled(event->possibleActions() & Qt::LinkAction);
    if (desktopLink)
        actions.at(3)->setEnabled((event->mimeData()->urls().count() == 1));

    QAction *retAction = menu->exec(Utils::cursorPos(parent));
    switch (actions.indexOf(retAction))
    {
        case 0:
            return Qt::MoveAction;
        case 1:
            return Qt::CopyAction;
        case 2:
            return Qt::LinkAction;
        case 3:
            if (desktopLink)
            {
                const qint32 flags = (Qt::LinkAction | 0x100); // Hack for *.desktop link
                return static_cast<Qt::DropAction>(flags);
            }
    }

    menu->deleteLater();

    return Qt::IgnoreAction;
}
GIOFileOp::Ptr fileOpAction(const GUriList &uris, const GUri &dir, const Qt::DropAction action)
{
    switch (action & Qt::ActionMask)
    {
        case Qt::CopyAction:
            return GIOFileOp::copyTo(uris, dir);
        case Qt::MoveAction:
            return GIOFileOp::moveTo(uris, dir);
        case Qt::LinkAction:
        {
            if (action & 0x100) // Hack for *.desktop link
            {
                if (uris.count() != 1)
                    return nullptr;
                CreateDesktopFile createDesktopFile;
                createDesktopFile.setUri(uris.constFirst());
                if (createDesktopFile.exec() == QDialog::Accepted)
                {
                    const QString fileName = createDesktopFile.fileName();
                    if (!fileName.isEmpty())
                    {
                        return GIOFileOp::createFile(dir.pathAppended(fileName),
                                                     createDesktopFile.data());
                    }
                }
                return nullptr;
            }
            return GIOFileOp::linkTo(uris, dir);
        }
        default:
            break;
    }
    return nullptr;
}

bool isMimeDataCutSelection(const QMimeData *mimeData)
{
    return (mimeData && mimeData->data(g_cutSelection) == "1");
}
void setMimeDataCutSelection(QMimeData *mimeData)
{
    if (mimeData)
        mimeData->setData(g_cutSelection, "1");
}

GUriList paste(const GUri &destUri, GIOModel *model)
{
    if (destUri.isNull())
        return {};

    QClipboard *clipboard = QGuiApplication::clipboard();

    const QMimeData *mimeData = clipboard->mimeData();
    if (!mimeData)
        return {};

    if (mimeData->hasUrls())
    {
        const bool isCut = GIOUtils::isMimeDataCutSelection(mimeData);
        const GUriList uris = fromUrlList(mimeData->urls());

        GIOFileOp::Ptr ptr = GIOUtils::fileOpAction(uris, destUri, isCut ? Qt::MoveAction : Qt::CopyAction);
        if (!ptr.isNull())
        {
            model->scheduleItemChangeWithSelection(ptr, uris);
            if (isCut)
                clipboard->setMimeData(nullptr);
            return uris;
        }
    }
    else if (mimeData->hasText() || mimeData->hasImage())
    {
        bool ok = false;
        const QString fileName = QInputDialog::getText(
                    nullptr,
                    QCoreApplication::translate("GIOUtils", "Paste item"),
                    QCoreApplication::translate("GIOUtils", "File name for clipboard content"),
                    QLineEdit::Normal,
                    QString(),
                    &ok);
        if (!ok || fileName.isEmpty())
            return {};

        const GUri uri(destUri.pathAppended(fileName));

        QByteArray dataToWrite;
        if (mimeData->hasText())
        {
            dataToWrite = mimeData->text().toUtf8();
        }
        else if (mimeData->hasImage())
        {
            const QImage image = mimeData->imageData().value<QImage>();
            QBuffer buffer(&dataToWrite);
            if (buffer.open(QBuffer::WriteOnly))
                image.save(&buffer, "PNG");
        }
        if (!dataToWrite.isNull())
        {
            GIOFileOp::Ptr ptr = GIOFileOp::createFile(uri, dataToWrite);
            if (!ptr.isNull())
            {
                model->scheduleItemChangeWithSelection(ptr, {uri});
                return {uri};
            }
        }
    }

    return {};
}

}
}
