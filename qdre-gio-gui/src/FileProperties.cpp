/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <FileProperties.hpp>

#include <ThemeIcon.hpp>
#include <UtilsCore.hpp>
#include <AppsList.hpp>
#include <OpenWith.hpp>
#include <GUri.hpp>

#include <QDialogButtonBox>
#include <QLoggingCategory>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTabWidget>

namespace QDRE {

FileProperties::FileProperties(const GUriList &uris, const QStringList &mimeTypes, QWidget *parent)
    : QDialog(parent)
    , m_uris(uris)
    , m_mimeTypes(mimeTypes)
{
    if (Q_UNLIKELY(uris.isEmpty()))
    {
        deleteLater();
        return;
    }

    Q_ASSERT(uris.count() == mimeTypes.count());

    setWindowTitle(tr("Properties %1").arg((uris.count() == 1)
                                           ? uris.constFirst().baseName()
                                           : QString()));
    setWindowFlags(Qt::Window);
    setAttribute(Qt::WA_DeleteOnClose);

    QTabWidget *tabWidget = new QTabWidget(this);

    if (UtilsCore::listToSet(mimeTypes).count() == 1)
        tabWidget->addTab(createOpenWithWidget(), tr("Open with"));

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(tabWidget);
}
FileProperties::~FileProperties()
{}

QWidget *FileProperties::createOpenWithWidget()
{
    QWidget *w = new QWidget(this);

    const QByteArray mimeType = m_mimeTypes.constFirst().toLocal8Bit();

    m_appsList = new AppsList(w);
    m_appsList->createList(mimeType, true, false);

    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    QAbstractButton *resetButton = buttons->addButton(QDialogButtonBox::Reset);
    QAbstractButton *addButton = buttons->addButton(tr("Add"), QDialogButtonBox::ActionRole);
    addButton->setIcon(ThemeIcon::get("list-add"));

    QVBoxLayout *layout = new QVBoxLayout(w);
    layout->addWidget(m_appsList);
    layout->addWidget(buttons);

    connect(buttons, &QDialogButtonBox::clicked,
            this, [=](QAbstractButton *button) {
        if (button == resetButton)
        {
            g_app_info_reset_type_associations(mimeType.constData());
            m_appsList->createList(mimeType, true, false);
        }
        else if (button == addButton)
        {
            OpenWith openWith(mimeType, w);
            if (openWith.exec() == QDialog::Accepted)
            {
                GObjectHolder<GAppInfo> appInfo = openWith.getAppInfo();
                if (!appInfo.isNull() && g_app_info_add_supports_type(appInfo, mimeType.constData(), nullptr))
                {
                    g_app_info_set_as_default_for_type(appInfo, mimeType.constData(), nullptr);
                    m_appsList->createList(mimeType, true, false);
                }
            }
        }
    });

    return w;
}

}
