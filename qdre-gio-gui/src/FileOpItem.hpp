/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QElapsedTimer>
#include <QWidget>

#include <GIOFileOp.hpp>

class QProgressBar;
class QToolButton;
class QLabel;

namespace QDRE {

class FileOpError;

class FileOpItem : public QWidget
{
    Q_OBJECT

public:
    FileOpItem(const GIOFileOp *fileOp, QWidget *parent = nullptr);
    ~FileOpItem();

    void cancel();

private:
    void countingObjects(const qint32 num, const qint64 overalSize);
    void startedSingle(const GIOFileOpInfo &info);
    void progressChanged(const qint64 pos, const qint64 num, const qreal progress, const qreal speed);
    void finishedSingle(FileOpError *error);

private:
    const GIOFileOp *const m_fileOp;

    QLabel *m_titleLabel;
    QProgressBar *m_progress;
    QToolButton *m_cancelButton;
    QLabel *m_infoLabel;

    QElapsedTimer m_t;

    qint32 m_currentFile = -1;
    qint32 m_numFiles = 0;
    qint64 m_overalSize = 0;

    qint64 m_lastPos = 0;
    qint64 m_overalPos = 0;

    qreal m_avgSpeed = 0.0;
    qint64 m_speedValues = 0;
};

}
