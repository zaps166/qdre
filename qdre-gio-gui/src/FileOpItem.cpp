/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <FileOpItem.hpp>

#include <FileOpErrorDialog.hpp>
#include <FileOpError.hpp>
#include <ThemeIcon.hpp>
#include <UtilsCore.hpp>

#include <QLoggingCategory>
#include <QProgressBar>
#include <QToolButton>
#include <QGridLayout>
#include <QLabel>

namespace QDRE {

FileOpItem::FileOpItem(const GIOFileOp *fileOp, QWidget *parent)
    : QWidget(parent)
    , m_fileOp(fileOp)
    , m_titleLabel(new QLabel(this))
    , m_progress(new QProgressBar(this))
    , m_cancelButton(new QToolButton(this))
    , m_infoLabel(new QLabel(this))
{
    m_progress->setRange(0, 0);

    m_cancelButton->setIcon(ThemeIcon::get("dialog-cancel"));
    m_cancelButton->setIconSize({16, 16});
    m_cancelButton->setAutoRaise(true);
    connect(m_cancelButton, &QToolButton::clicked,
            fileOp, &GIOFileOp::cancel);

    QGridLayout *layout = new QGridLayout(this);
    layout->addWidget(m_titleLabel, 0, 0, 1, 2);
    layout->addWidget(m_progress, 1, 0, 1, 1);
    layout->addWidget(m_cancelButton, 1, 1, 1, 1);
    layout->addWidget(m_infoLabel, 2, 0, 1, 2);
    layout->setSpacing(4);
    layout->setContentsMargins(4, 4, 4, 4);

    connect(fileOp, &GIOFileOp::countingObjects,
            this, &FileOpItem::countingObjects);
    connect(fileOp, &GIOFileOp::startedSingle,
            this, &FileOpItem::startedSingle);
    connect(fileOp, &GIOFileOp::progressChanged,
            this, &FileOpItem::progressChanged);
    connect(fileOp, &GIOFileOp::finishedSingle,
            this, &FileOpItem::finishedSingle);
    connect(fileOp, &GIOFileOp::destroyed,
            this, &FileOpItem::deleteLater);

    m_t.start();
}
FileOpItem::~FileOpItem()
{}

void FileOpItem::cancel()
{
    m_fileOp->cancel();
}

void FileOpItem::countingObjects(const qint32 num, const qint64 overalSize)
{
    m_numFiles = num;
    m_overalSize = overalSize;
    if (m_t.elapsed() > 250)
    {
        m_t.start();
        m_titleLabel->setText(tr("Counting objects: %1 (%2)")
                              .arg(m_numFiles)
                              .arg(UtilsCore::bytesToString(m_overalSize)));
        m_progress->setRange(0, 0);
        m_currentFile = -1;
    }
}
void FileOpItem::startedSingle(const GIOFileOpInfo &info)
{
    if (m_currentFile < 0)
        m_t.start();

    m_currentFile = info.idx;
    m_lastPos = 0;

    QString text;
    switch (m_fileOp->type())
    {
        case GIOFileOp::Type::CreateFile:
            text = tr("Creating file");
            break;
        case GIOFileOp::Type::CreateDirectory:
            text = tr("Creating directory");
            break;
        case GIOFileOp::Type::Copy:
            text = tr("Copying");
            break;
        case GIOFileOp::Type::Move:
            text = tr("Moving");
            break;
        case GIOFileOp::Type::Link:
            text = tr("Linking");
            break;
        case GIOFileOp::Type::Trash:
            text = tr("Moving to trash");
            break;
        case GIOFileOp::Type::Rename:
            text = tr("Renaming");
            break;
        case GIOFileOp::Type::Unlink:
            text = tr("Removing");
            break;
    }
    text += " \"" + info.src.baseName() + "\"";
    switch (m_fileOp->type())
    {
        case GIOFileOp::Type::Copy:
        case GIOFileOp::Type::Move:
        case GIOFileOp::Type::Link:
        {
            const QString path = GUri(info.dst.toString(true)).baseName();
            text += " " + tr("to") + " \"" + path + "\"";
            break;
        }
        default:
            break;
    }
    m_titleLabel->setText(text);

    m_progress->setMaximum(m_numFiles * 100);
    m_progress->setValue(m_currentFile * 100);
}
void FileOpItem::progressChanged(const qint64 pos, const qint64 num, const qreal progress, const qreal speed)
{
    Q_UNUSED(pos)
    Q_UNUSED(num)

    if (progress >= 1.0)
        m_progress->setValue(m_currentFile * 100 + progress);

    m_avgSpeed += speed;
    ++m_speedValues;

    m_overalPos += (pos - m_lastPos);
    m_lastPos = pos;

    if (m_t.elapsed() >= 750)
    {
        m_t.start();
        const qreal avgSpeed = m_avgSpeed / m_speedValues;
        const qint64 remainingBytes = m_overalSize - m_overalPos;
        QString remainingTime;
        if (remainingBytes >= 0)
        {
            const qint64 secs = qRound((m_overalSize - m_overalPos) / avgSpeed);
            remainingTime = tr("remaining time ") + UtilsCore::timeToString(secs);
        }
        m_infoLabel->setText(QString("%1 z %2 - %3 (%4/s)")
                                .arg(UtilsCore::bytesToString(m_overalPos),
                                     UtilsCore::bytesToString(m_overalSize),
                                     remainingTime,
                                     UtilsCore::bytesToString(avgSpeed)));
    }
}
void FileOpItem::finishedSingle(FileOpError *error)
{
    bool canAdvanceProgress = true;

    if (error)
    {
        if (error->type() == FileOpError::Type::FastMoveImpossible)
        {
            error->response(FileOpError::Response::Retry);
            return;
        }

        window()->show();

        FileOpErrorDialog dialog(*error, m_fileOp->type(), (m_currentFile + 1 == m_numFiles), this);
        if (dialog.exec() == QDialog::Rejected)
            error->response(FileOpError::Response::Abort);
        canAdvanceProgress = dialog.canAdvanceProgress();

        m_t.start();
    }

    if (canAdvanceProgress)
        m_progress->setValue((m_currentFile + 1) * 100);
}

}
