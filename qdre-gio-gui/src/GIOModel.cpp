/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <GIOModelPriv.hpp>
#include <GIOModel.hpp>

#include <FileOpError.hpp>
#include <GIOFileOp.hpp>
#include <GIOUtils.hpp>

#include <QMimeDatabase>
#include <QMimeData>
#include <QUrl>

Q_LOGGING_CATEGORY(giomodel, "gio.model")

namespace QDRE {

using std::as_const;

GIOModel::GIOModel(QObject *parent)
    : QAbstractItemModel(parent)
    , d(*(new GIOModelPriv(*this)))
{
    connect(this, &GIOModel::modelReset,
            this, &GIOModel::changed);
    connect(this, &GIOModel::layoutChanged,
            this, &GIOModel::changed);
    connect(this, &GIOModel::rowsInserted,
            this, &GIOModel::changed);
    connect(this, &GIOModel::rowsRemoved,
            this, &GIOModel::changed);
}
GIOModel::~GIOModel()
{
    delete &d;
}

void GIOModel::setSimpleReadMode()
{
    d.setSimpleReadMode();
}
void GIOModel::setFileTypeMode()
{
    d.setFileTypeMode();
}

void GIOModel::setRootPath(const QString &path)
{
    d.setRootPath(path);
}
GUri GIOModel::rootUri() const
{
    return d.m_uri;
}

void GIOModel::setDisplayHidden(const bool displayHidden)
{
    if (d.m_displayHidden != displayHidden)
    {
        d.m_displayHidden = displayHidden;
        if (!d.m_uri.isNull())
            reload();
    }
}
bool GIOModel::displayHidden() const
{
    return d.m_displayHidden;
}

void GIOModel::reload()
{
    d.resetRootPath();
}

GUri GIOModel::getGUri(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return GUri();
    return d.m_files[index.row()];
}
GUriList GIOModel::getGUris(const QModelIndexList &indexes) const
{
    return getGUrisInternal(indexes);
}
GUriList GIOModel::getGUris(const QList<QPersistentModelIndex> &indexes) const
{
    return getGUrisInternal(indexes);
}

bool GIOModel::isDirectoryWritable() const
{
    return d.m_dirWritable;
}

GUri GIOModel::getUriForNewName(const QString &name) const
{
    QString suffix = QMimeDatabase().suffixForFileName(name);
    if (!suffix.isEmpty())
        suffix.prepend('.');

    const qint32 nameWithoutSuffixLen = (name.size() - suffix.size());

    QStringList baseNames;
    bool exists = false;

    for (const GUri &existingUri : as_const(d.m_files))
    {
        const QString existingBaseName = existingUri.baseName();
        if (existingBaseName.startsWith(QStringView(name).left(nameWithoutSuffixLen)))
        {
            baseNames += existingBaseName;
            if (existingBaseName == name)
                exists = true;
        }
    }

    if (!exists)
        return rootUri().pathAppended(name);

    QString newName = name.left(nameWithoutSuffixLen);
    qint32 number = 0;

    const qint32 idx = newName.lastIndexOf(' ');
    bool ok = false;
    if (idx > -1)
        number = QStringView(newName).mid(idx + 1).toInt(&ok);
    if (!ok)
        newName += " 0";

    for (;;)
    {
        if (++number < 0) // Break at integer overflow
            break;

        const qint32 idx = newName.lastIndexOf(' ');
        Q_ASSERT(idx > -1);
        newName.replace(idx + 1, newName.length() - idx - 1, QString::number(number));

        const QString fullNewName = newName + suffix;
        if (!baseNames.contains(fullNewName))
            return rootUri().pathAppended(fullNewName);
    }

    return GUri();
}

bool GIOModel::isHidden(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isHidden();
}
bool GIOModel::isSymlink(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isSymlink();
}
bool GIOModel::isReadable(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isReadable();
}
bool GIOModel::isWritable(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isWritable();
}
bool GIOModel::isExecutable(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isExecutable();
}
bool GIOModel::isDirectory(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isDirectory();
}

bool GIOModel::canDelete(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].canDelete();
}
bool GIOModel::canTrash(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].canTrash();
}
bool GIOModel::canEmptyTrash(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return (d.m_files[index.row()].isTrashContainer && d.m_trashFull);
}

bool GIOModel::isDesktop(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isDesktop;
}
bool GIOModel::isTrashContainer(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isTrashContainer;
}

bool GIOModel::isCut(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return false;
    return d.m_files[index.row()].isCut;
}

QString GIOModel::getMimeType(const QModelIndex &index) const
{
    if (!inFilesBounds(index.row()))
        return QString();
    return d.m_files[index.row()].mimeType();
}
QStringList GIOModel::getMimeTypes(const QModelIndexList &indexes) const
{
    return getMimeTypesInternal(indexes);
}
QStringList GIOModel::getMimeTypes(const QList<QPersistentModelIndex> &indexes) const
{
    return getMimeTypesInternal(indexes);
}

void GIOModel::scheduleItemChangeWithSelection(const GIOFileOp *fileOp, const GUriList &uris, const bool rename)
{
    FileOpSelection *fileOpSelection = new FileOpSelection;
    const GUriList *urisPtr = new GUriList(uris);
    QSet<GUri> *toSchedule = new QSet<GUri>;
    QPair<GUri, GUri> *lastStarted = new QPair<GUri, GUri>;
    fileOpSelection->rename = rename;
    connect(fileOp, &GIOFileOp::startedSingle,
            fileOp, [=](const GIOFileOpInfo &info) {
        if (urisPtr->contains(info.src))
        {
            // Notify the view about new name for current file operation. It can be called twice
            // for the same item when item already exists and it became renamed, so notify the
            // view again about rename.
            if (lastStarted->first != info.src)
            {
                lastStarted->first = info.src;
                lastStarted->second = info.src;
            }
            emit fileOpStartedSingle(lastStarted->second, info.dst);
            lastStarted->second = info.dst;
            // Add every started item to schedule list
            toSchedule->insert(info.dst.isNull() ? info.src : info.dst);
        }
    });
    connect(fileOp, &GIOFileOp::finishedSingle,
            fileOp, [=](FileOpError *error, const GIOFileOpInfo &info) {
        if (error && urisPtr->contains(info.src))
            toSchedule->remove(info.dst.isNull() ? info.src : info.dst);
    });
    connect(fileOp, &GIOFileOp::destroyed,
            fileOp , [=] {
        if (!toSchedule->isEmpty())
        {
            fileOpSelection->count = toSchedule->count();
            for (const GUri &url : as_const(*toSchedule))
            {
                d.scheduleItemChange(url.gFile(), nullptr, fileOpSelection);
            }
        }
        else
        {
            delete fileOpSelection;
        }
        delete toSchedule;
        delete urisPtr;
        delete lastStarted;
    });
}

GUriList GIOModel::takeLastDropUris()
{
    return std::move(d.m_lastDropUris);
}

QModelIndex GIOModel::index(qint32 row, qint32 column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return createIndex(row, column);
}
QModelIndex GIOModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}
int GIOModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return d.m_files.size();
}
int GIOModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}
bool GIOModel::setData(const QModelIndex &index, const QVariant &value, qint32 role)
{
    if (!index.isValid() || role != Qt::EditRole || d.m_uri.isNull())
        return false;

    const QString name = value.toString();
    if (name.isEmpty())
        return false;

    if (static_cast<qint32>(d.m_files.size()) <= index.row())
        return false;

    FileDescr &file = d.m_files[index.row()];
    if (file.editableName.isNull() || file.editableName == name)
        return false;

    const GUri newUri(d.m_uri.pathAppended(name));

    const GIOFileOp::Ptr ptr = GIOFileOp::rename({file}, {newUri});
    if (!ptr)
        return false;

    const bool hasMonitor = (d.m_monitor != nullptr);

    GUri oldUri;
    QString oldName;
    QString oldEditableName;

    if (hasMonitor)
    {
        // Real name will be obtained via directory monitor
        file.tmpName = name;
    }
    else
    {
        oldUri = file;
        oldName = file.name;
        oldEditableName = file.editableName;

        static_cast<GUri &>(file) = newUri;
        file.name = name;
        file.editableName = name;
    }

    const QVector<qint32> roles {
        Qt::DisplayRole,
        Qt::EditRole
    };

    const QPersistentModelIndex pIndex = index;
    connect(ptr, &GIOFileOp::finishedSingle,
            this, [=](FileOpError *error) {
        if (!error && !hasMonitor)
        {
            d.scheduleItemChange(newUri.gFile());
        }
        else if (error && pIndex.isValid() && static_cast<qint32>(d.m_files.size()) > pIndex.row())
        {
            // Rename error - revert old name
            FileDescr &file = d.m_files[pIndex.row()];
            if (hasMonitor)
            {
                file.tmpName.clear();
            }
            else
            {
                static_cast<GUri &>(file) = oldUri;
                file.name = oldName;
                file.editableName = oldEditableName;
            }
            emit dataChanged(pIndex, pIndex, roles);
            qCWarning(giomodel) << "Rename failed, reverted the name";
        }
    });

    emit dataChanged(index, index, roles);

    return true;
}
QVariant GIOModel::data(const QModelIndex &index, qint32 role) const
{
    if (!index.isValid())
        return QVariant();

    const qint32 row = index.row();
    if (row >= static_cast<qint32>(d.m_files.size()))
        return QVariant();

    FileDescr &file = d.m_files[row];

    const auto maybeLoadFileIcon = [&file] {
        if (file.fileIcon.isNull() && !file.fileIconPath.isEmpty())
            file.fileIcon = QImage(file.fileIconPath);
    };

    switch (role)
    {
        case Qt::DisplayRole: // This name is not unique in directory, e.g. for *.desktop files
            return file.tmpName.isNull() ? file.name : file.tmpName;
        case Qt::DecorationRole:
        {
            if (file.shouldGenerateThumbnail)
            {
                // TODO: Generate large thumbnails if necessary
                Q_ASSERT(d.m_thumbnailer);
                d.m_thumbnailer->scheduleGenerate(file, file.mimeType(), file.fileIconPath);
                file.fileIconPath.clear();
                file.shouldGenerateThumbnail = false;
            }

            maybeLoadFileIcon();
            if (!file.fileIcon.isNull())
                return file.fileIcon;

            return file.themedIcon;
        }
        case Qt::SizeHintRole:
            maybeLoadFileIcon();
            return file.fileIcon.size();
        case Qt::EditRole:
            return file.editableName;
    }

    return QVariant();
}

QStringList GIOModel::mimeTypes() const
{
    return {"text/uri-list"};
}
QMimeData *GIOModel::mimeData(const QModelIndexList &indexes) const
{
    QList<QUrl> urls;
    for (const QModelIndex &index : indexes)
    {
        if (inFilesBounds(index.row()))
            urls.append(d.m_files[index.row()].toUrl());
        else
            urls.append(QUrl());
    }
    if (!urls.isEmpty())
    {
        QMimeData *mimeData = new QMimeData;
        mimeData->setUrls(urls);
        return mimeData;
    }
    return nullptr;
}
bool GIOModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, qint32 row, qint32 column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if ((action & Qt::ActionMask) == Qt::IgnoreAction)
        return false;
    if (!data || row >= static_cast<qint32>(d.m_files.size()) || column > 0)
        return false;
    if (!data->hasUrls())
        return false;
    if (((action  & Qt::ActionMask) & supportedDropActions()) != (action & Qt::ActionMask))
        return false;
    if (row > -1)
    {
        const QModelIndex dropIndex = createIndex(row, column);
        const bool executable = isExecutable(dropIndex);
        const bool directory = isDirectory(dropIndex);
        return ((executable && !directory)
                || (executable && directory && isWritable(dropIndex))
                || (isDesktop(dropIndex) && isReadable(dropIndex)));
    }
    return true;
}
bool GIOModel::dropMimeData(const QMimeData *data, Qt::DropAction action, qint32 row, qint32 column, const QModelIndex &parent)
{
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    d.m_lastDropUris = fromUrlList(data->urls());
    if (row > -1 && column > -1)
    {
        const QModelIndex dropIndex = createIndex(row, column);
        GUri dst = getGUri(dropIndex);
        bool doFileOp = false;
        if (isDirectory(dropIndex))
        {
            doFileOp = true;
        }
        else if (isDesktop(dropIndex))
        {
            const GUri dir = GIOUtils::getDesktopLinkToContainer(dst.toLocalPath(), true);
            if (dir.isTrash())
                return GIOFileOp::trash(d.m_lastDropUris);
            if (dir.isValid())
            {
                dst = dir;
                doFileOp = true;
            }
        }
        if (doFileOp)
        {
            GIOFileOp::Ptr ptr = GIOUtils::fileOpAction(d.m_lastDropUris, dst, action);
            if (!ptr.isNull())
            {
                d.m_lastDropUris = ptr->srcUris();
                return true;
            }
            return false;
        }
#ifdef GIO_LAUNCH
        return GIOUtils::launch(dst, getMimeType(dropIndex), isDesktop(dropIndex), d.m_lastDropUris);
#else
        return false;
#endif
    }

    GIOFileOp::Ptr ptr = GIOUtils::fileOpAction(d.m_lastDropUris, d.m_uri, action);
    if (ptr.isNull())
        return false;

    d.m_lastDropUris = ptr->srcUris();
    scheduleItemChangeWithSelection(ptr, d.m_lastDropUris);

    return true;
}
Qt::DropActions GIOModel::supportedDropActions() const
{
    if (d.m_dirWritable)
        return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
    return Qt::IgnoreAction;
}
Qt::DropActions GIOModel::supportedDragActions() const
{
    Qt::DropActions dropActions = Qt::CopyAction | Qt::LinkAction;
    if (d.m_dirWritable)
        dropActions |= Qt::MoveAction;
    return dropActions;
}

Qt::ItemFlags GIOModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.isValid())
    {
        flags |= Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
        if (!index.data(Qt::EditRole).isNull())
            flags |= Qt::ItemIsEditable;
    }
    return flags;
}

inline bool GIOModel::inFilesBounds(const qint32 i) const
{
    return (static_cast<quint32>(i) < static_cast<quint32>(d.m_files.size()));
}

template<typename ModelIndexList>
GUriList GIOModel::getGUrisInternal(const ModelIndexList &indexes) const
{
    GUriList uris;
    for (const auto &index : indexes)
    {
        const GUri uri = getGUri(index);
        if (!uri.isNull())
            uris += uri;
    }
    return uris;
}

template<typename ModelIndexList>
QStringList GIOModel::getMimeTypesInternal(const ModelIndexList &indexes) const
{
    QStringList mimeTypes;
    for (const auto &index : indexes)
    {
        const QString mimeType = getMimeType(index);
        if (!mimeType.isEmpty())
            mimeTypes += mimeType;
    }
    return mimeTypes;
}

}
