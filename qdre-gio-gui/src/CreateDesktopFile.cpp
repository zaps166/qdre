/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <CreateDesktopFile.hpp>
#include <ui/ui_CreateDesktopFile.h>

#include <ThemeIcon.hpp>
#include <GIOUtils.hpp>
#include <GUri.hpp>

#include <QLoggingCategory>
#include <QMimeDatabase>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>

namespace QDRE {

CreateDesktopFile::CreateDesktopFile(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::CreateDesktopFile)
{
    ui->setupUi(this);
    ui->nameEdit->setFocus();
    setIconButtonIcon("preferences-desktop", true);

    on_typeComboBox_currentIndexChanged(ui->typeComboBox->currentIndex());

    resize(500, 1);
}
CreateDesktopFile::~CreateDesktopFile()
{}

void CreateDesktopFile::setUri(const GUri &uri)
{
    const QString localPath = uri.toLocalPath();
    const QMimeType mimeType = QMimeDatabase().mimeTypeForName(GIOUtils::getMimeTypeForUri(uri));
    bool isApplication = false;
    bool hasIcon = false;
    if (uri.isLocal() && QFileInfo(localPath).isExecutable())
    {
        const QPair<bool, bool> executableAbility = GIOUtils::getExecutableAbility(
                    mimeType,
                    uri.baseName());
        if (executableAbility.first && !executableAbility.second)
        {
            setIconButtonIcon(ThemeIcon::executable());
            hasIcon = true;
        }
        isApplication = (executableAbility.first || executableAbility.second);
    }
    ui->typeComboBox->setCurrentIndex(isApplication ? 0 : 2);
    if (!hasIcon)
        setIconButtonIcon(mimeType.iconName(), true);
    ui->nameEdit->setText(uri.baseName());
    ui->pathEdit->setText(isApplication ? localPath : uri.toString());
}

QString CreateDesktopFile::fileName() const
{
    const QString simplifiedName = ui->nameEdit->text().simplified().remove('/');
    if (!simplifiedName.isEmpty())
        return simplifiedName + ".desktop";
    return QString();
}
QByteArray CreateDesktopFile::data() const
{
    QByteArray data;
    QTextStream stream(&data, QIODevice::WriteOnly);
    stream << "[Desktop Entry]\n";
    stream << "Name=" << ui->nameEdit->text() << "\n";
    stream << "Icon=" << ui->iconButton->property("iconName").toString() << "\n";
    const qint32 index = ui->typeComboBox->currentIndex();
    switch (index)
    {
        case 0:
        case 1:
            stream << "Type=Application\n";
            stream << "Exec=" << ui->pathEdit->text() << "\n";
            stream << "Terminal=" << ((index == 1) ? "true" : "false") << "\n";
            stream << "StartupNotify=" << (ui->startupNotifyCheckBox->isChecked() ? "true" : "false") << "\n";
            break;
        case 2:
            stream << "Type=Link\n";
            stream << "URL=" << ui->pathEdit->text() << "\n";
            break;
    }
    stream << "Comment=" << ui->commentEdit->text() << "\n";
    return data;
}

void CreateDesktopFile::setIconButtonIcon(const QString &icon, const bool fromTheme)
{
    ui->iconButton->setProperty("iconName", icon);
    ui->iconButton->setIcon(fromTheme ? ThemeIcon::get(icon) : QIcon(icon));
}
void CreateDesktopFile::setIconButtonIcon(const QIcon &icon)
{
    ui->iconButton->setProperty("iconName", icon.name());
    ui->iconButton->setIcon(icon);
}

void CreateDesktopFile::on_typeComboBox_currentIndexChanged(qint32 index)
{
    switch (index)
    {
        case 0:
        case 1:
            ui->pathLabel->setText(tr("Command:"));
            ui->startupNotifyCheckBox->show();
            break;
        case 2:
            ui->pathLabel->setText(tr("Location:"));
            ui->startupNotifyCheckBox->hide();
            break;
    }
}
void CreateDesktopFile::on_iconButton_clicked()
{
    const QString filter = "Images (*.png *.xpm *.jpg *.jpeg *.svg *.svgz)";

    QString dir;

    const QFileInfo iconFile(ui->iconButton->property("iconName").toString());
    if (iconFile.isFile())
    {
        dir = iconFile.absolutePath();
    }

    const QString iconPath = QFileDialog::getOpenFileName(this, tr("Choose icon"), dir, filter);
    if (!iconPath.isEmpty())
    {
        const QStringList paths = QIcon::themeSearchPaths();
        const QString themeName = QIcon::themeName();
        for (const QString &path : paths)
        {
            const QString themePath = path + "/" + themeName;
            if (iconPath.startsWith(themePath))
            {
                const QString baseName = QFileInfo(iconPath).baseName();
                if (!ThemeIcon::get(baseName).isNull())
                {
                    setIconButtonIcon(baseName, true);
                    return;
                }
                break;
            }
        }
        setIconButtonIcon(iconPath, false);
    }
}
void CreateDesktopFile::on_browseButton_clicked()
{
    QString currentPath = ui->pathEdit->text();
    if (currentPath.startsWith("file://"))
        currentPath.remove(0, 7);
    QString path;
    switch (ui->typeComboBox->currentIndex())
    {
        case 0:
        case 1:
            path = QFileDialog::getOpenFileName(this, tr("Choose file to open"), currentPath);
            break;
        case 2:
            path = QFileDialog::getExistingDirectory(this, tr("Choose location"), currentPath);
            if (!path.isEmpty())
                path.prepend("file://");
            break;
    }
    if (!path.isEmpty())
        ui->pathEdit->setText(path);
}

void CreateDesktopFile::accept()
{
    if (fileName().isEmpty())
    {
        QMessageBox::critical(this, windowTitle(), tr("Incorrect name"));
        return;
    }
    QDialog::accept();
}

}
