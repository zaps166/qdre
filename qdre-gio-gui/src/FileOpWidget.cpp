/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <FileOpWidget.hpp>

#include <FileOpItem.hpp>

#include <QLoggingCategory>
#include <QApplication>
#include <QMessageBox>
#include <QScrollArea>
#include <QVBoxLayout>
#include <qevent.h>
#include <QTimer>

namespace QDRE {

static FileOpWidget *g_fileOpW = nullptr;

FileOpWidget *FileOpWidget::instance()
{
    if (!g_fileOpW)
    {
        g_fileOpW = new FileOpWidget;
        qAddPostRoutine([] {
            delete g_fileOpW;
            g_fileOpW = nullptr;
        });
    }
    return g_fileOpW;
}

FileOpWidget::FileOpWidget()
    : m_scroll(new QScrollArea(this))
    , m_w(new QWidget(m_scroll))
{
    setWindowTitle(tr("File operations"));

    m_layout = new QVBoxLayout(m_w);
    m_layout->setSpacing(0);
    m_layout->setContentsMargins(0, 0, 0, 0);

    m_scroll->setFrameShape(QFrame::NoFrame);
    m_scroll->setWidgetResizable(true);
    m_scroll->setWidget(m_w);

    m_w->installEventFilter(this);
}
FileOpWidget::~FileOpWidget()
{}

void FileOpWidget::setSize(const bool setToMin)
{
    m_pendingResizeSetToMin = setToMin;
    QTimer::singleShot(0, this, &FileOpWidget::doSetSize);
    if (!isVisible())
        doSetSize();
}
void FileOpWidget::doSetSize()
{
    qint32 w = 1, h = 1;
    qint32 c = 0;
    for (const QObject *obj : m_w->children())
    {
        if (const FileOpItem *item = qobject_cast<const FileOpItem *>(obj))
        {
            const QSize s = item->sizeHint();
            w = qMax(s.width(),  w);
            if (++c <= 5)
                h += s.height() + m_layout->spacing();
        }
    }
    setMinimumSize(w, h);
    if (m_pendingResizeSetToMin && c <= 5)
        resize(width(), h);
}

void FileOpWidget::itemDestroyed()
{
    if (--m_fileOps == 0)
        close();
    setSize(true);
    Q_ASSERT(m_fileOps >= 0);
}

void FileOpWidget::addFileOp(const GIOFileOp *fileOp)
{
    FileOpItem *item = new FileOpItem(fileOp, m_w);
    m_layout->addWidget(item);
    ++m_fileOps;

    connect(item, &FileOpItem::destroyed,
            this, &FileOpWidget::itemDestroyed);

    setSize(m_fileOps == 1);
}

void FileOpWidget::resizeEvent(QResizeEvent *event)
{
    m_scroll->setGeometry(QRect(QPoint(), event->size()));
}

void FileOpWidget::closeEvent(QCloseEvent *event)
{
    if (m_fileOps < 1)
        return;

    event->ignore();

    if (m_fileOps > 1)
    {
        const qint32 choice = QMessageBox::question(this, windowTitle(), tr("Do you want to abort all file operations?"), QMessageBox::Yes, QMessageBox::No);
        if (choice == QMessageBox::No)
            return;
    }

    for (QObject *obj : m_w->children())
    {
        if (FileOpItem *item = qobject_cast<FileOpItem *>(obj))
            item->cancel();
    }
}

bool FileOpWidget::eventFilter(QObject *o, QEvent *e)
{
    if (o == m_w && e->type() == QEvent::Resize)
        setSize();
    return QDialog::eventFilter(o, e);
}

}
