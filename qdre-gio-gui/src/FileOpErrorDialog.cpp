/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <FileOpErrorDialog.hpp>
#include <ui/ui_FileOpErrorDialog.h>

#include <FileOpError.hpp>
#include <ThemeIcon.hpp>

#include <QLoggingCategory>
#include <QPushButton>

namespace QDRE {

FileOpErrorDialog::FileOpErrorDialog(FileOpError &error, const GIOFileOp::Type type, const bool last, QWidget *parent)
    : QDialog(parent)
    , m_error(error)
    , ui(new Ui::FileOpErrorDialog)
{
    ui->setupUi(this);
    setWindowModality(Qt::WindowModal);

    ui->errorMessage->setText(m_error.message());

    QString overwriteText;

    QDialogButtonBox::StandardButtons buttons = ui->buttons->standardButtons();
    Q_ASSERT(buttons == (QDialogButtonBox::Abort | QDialogButtonBox::Ignore));
    if (last)
    {
        buttons &= ~QDialogButtonBox::Abort;
        ui->setToAllButton->hide();
    }
    switch (m_error.type())
    {
        case FileOpError::Type::AbortRetryIgnore:
            buttons |= QDialogButtonBox::Retry;
            break;
        case FileOpError::Type::DirectoryExists:
            if (type != GIOFileOp::Type::Rename)
                overwriteText = tr("Merge directories");
            break;
        case FileOpError::Type::FileExists:
            if (type != GIOFileOp::Type::Rename)
                overwriteText = tr("Overwrite");
            break;
        default:
            break;
    }
    if (!overwriteText.isEmpty())
        m_overwrite = ui->buttons->addButton(overwriteText, QDialogButtonBox::ActionRole);
    ui->buttons->setStandardButtons(buttons);

    if (m_overwrite)
    {
        const auto setItemName = [=] {
            ui->nameEdit->setText(m_error.destinationName());
        };
        const auto setRenameWidgets = [=](const bool e) {
            ui->nameEdit->setEnabled(e);
            ui->restoreButton->setEnabled(e);
            ui->setToAllButton->setDisabled(e);
            m_overwrite->setText(e ? tr("Rename") : overwriteText);
        };

        ui->restoreButton->setIcon(ThemeIcon::get("document-revert"));
        connect(ui->restoreButton, &QToolButton::clicked,
                this, setItemName);
        setItemName();

        connect(ui->renameCheckBox, &QCheckBox::toggled,
                this, setRenameWidgets);
        setRenameWidgets(false);

        if (!m_error.allowOverwrite())
        {
            ui->renameCheckBox->setChecked(true);
            ui->renameCheckBox->setDisabled(true);
        }
    }
    else
    {
        delete ui->renameWidget;
    }

    connect(ui->buttons, &QDialogButtonBox::clicked,
            this, &FileOpErrorDialog::clicked);
}
FileOpErrorDialog::~FileOpErrorDialog()
{
    delete ui;
}

void FileOpErrorDialog::clicked(QAbstractButton *button)
{
    switch (ui->buttons->standardButton(button))
    {
        case QDialogButtonBox::NoButton:
            if (m_overwrite && m_overwrite == button)
            {
                if (ui->renameCheckBox->isChecked())
                {
                    m_error.response(FileOpError::Response::Rename, false, ui->nameEdit->text());
                }
                else
                {
                    m_error.response(FileOpError::Response::Overwrite, ui->setToAllButton->isChecked());
                }
                accept();
            }
            break;
        case QDialogButtonBox::Abort:
            reject();
            break;
        case QDialogButtonBox::Retry:
            m_error.response(FileOpError::Response::Retry);
            accept();
            break;
        case QDialogButtonBox::Ignore:
            m_error.response(FileOpError::Response::Ignore, ui->setToAllButton->isChecked());
            m_canAdvanceProgress = true;
            accept();
            break;
        default:
            break;
    }
}

}
