/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <GIOFileOpThread.hpp>

#include <FileOpError.hpp>
#include <FileOpWidget.hpp>
#include <GIOFileOp.hpp>

#include <QLoggingCategory>
#include <QTimer>

Q_DECLARE_LOGGING_CATEGORY(fileop)

namespace QDRE {

GIOFileOpThread::GIOFileOpThread(GIOFileOp &fileOp,
                                 const GUriList &src,
                                 const GUriList &dst,
                                 const GIOFileOp::Type type,
                                 const QByteArray &dataToWrite,
                                 const bool dialog)
    : q(fileOp)
    , m_src(src)
    , m_type(type)
    , m_dataToWrite(dataToWrite)
    , m_cancellable(g_cancellable_new())
{
    const qint32 count = m_src.count();
    m_fileOpDescrs.reserve(count);
    for (qint32 i = 0; i < count; ++i)
        m_fileOpDescrs.append({m_src[i], dst.value(i), G_FILE_TYPE_UNKNOWN});

    if (dialog)
    {
        FileOpWidget *fileOpW = FileOpWidget::instance();
        fileOpW->addFileOp(&q);
        QTimer::singleShot(500, &q, [=] {
            fileOpW->show();
        });
    }

    connect(this, &GIOFileOpThread::finished,
            &q, &GIOFileOp::deleteLater);
    QTimer::singleShot(0, this, [this] {
        start();
    });
}
GIOFileOpThread::~GIOFileOpThread()
{
    g_object_unref(m_cancellable);
}

void GIOFileOpThread::cancel()
{
    g_cancellable_cancel(m_cancellable);
}

void GIOFileOpThread::progress(qint64 currentNumBytes, qint64 totalNumBytes, GIOFileOpThread &d)
{
    const qreal progress = currentNumBytes * 100.0 / totalNumBytes;
    const qreal elapsedT = d.m_t.nsecsElapsed() / 1e9;
    emit d.q.progressChanged(currentNumBytes, totalNumBytes, progress, currentNumBytes / elapsedT);
}

inline bool GIOFileOpThread::isCancelled() const
{
    return g_cancellable_is_cancelled(m_cancellable);
}

void GIOFileOpThread::run()
{
    if (m_type == GIOFileOp::Type::Copy
            || m_type == GIOFileOp::Type::Move
            || m_type == GIOFileOp::Type::Unlink)
    {
        qint64 overalSize = 0;

        fetchTypes(overalSize);
        if (isCancelled())
            return;

        if (m_type == GIOFileOp::Type::Move && !qEnvironmentVariableIsSet("QDRE_GIO_MOVE_ALWAYS_SLOW"))
        {
            // Move all files and dirs (if possible). Remaining dirs will
            // be scanned and moved with copy fallback (if possible).
            if (!doFileOp(true))
                return;
            if (m_fileOpDescrs.isEmpty())
                return;
        }

        scanDirs(overalSize);
        if (isCancelled())
            return;
    }
    else
    {
        emit q.countingObjects(m_fileOpDescrs.count(), -1);
    }
    doFileOp(false);
}

void GIOFileOpThread::fetchTypes(qint64 &overalSize)
{
    for (qint32 i = 0; i < m_fileOpDescrs.count(); ++i)
    {
        GFile *file = m_fileOpDescrs.at(i).srcPath.gFile();
        if (GFileInfo *info = g_file_query_info(
                    file,
                    G_FILE_ATTRIBUTE_STANDARD_TYPE ","
                    G_FILE_ATTRIBUTE_STANDARD_SIZE,
                    G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                    m_cancellable,
                    nullptr))
        {
            const quint32 type = g_file_info_get_attribute_uint32(info, G_FILE_ATTRIBUTE_STANDARD_TYPE);
            m_fileOpDescrs[i].srcType = type;
            if (type == G_FILE_TYPE_REGULAR)
                overalSize += qMax<qint64>(0, g_file_info_get_size(info));
            g_object_unref(info);
        }

        if (isCancelled())
            return;

        emit q.countingObjects(m_fileOpDescrs.count(), overalSize);
    }
}
void GIOFileOpThread::scanDirs(qint64 &overalSize)
{
    Q_ASSERT(m_type != GIOFileOp::Type::Rename && m_type != GIOFileOp::Type::Trash);

    FileOpDescrList dirsToCreate, dirsToRemove;

    for (qint32 i = 0; i < m_fileOpDescrs.count(); ++i)
    {
        if (m_fileOpDescrs.at(i).srcType != G_FILE_TYPE_DIRECTORY)
            continue;

        bool preserveCurrent = true;
        if (m_type == GIOFileOp::Type::Unlink && m_fileOpDescrs.at(i).srcPath.isTrash())
        {
            if (m_fileOpDescrs.at(i).srcPath.hasParent())
                continue; // Don't remove trash subdirectories (not possible)
            else
                preserveCurrent = false; // Don't try to remove a trash itself
        }

        FileOpDescr fileOpDescr = m_fileOpDescrs.takeAt(i);

        GFileEnumerator *direnum = g_file_enumerate_children(
                    fileOpDescr.srcPath.gFile(),
                    G_FILE_ATTRIBUTE_STANDARD_TYPE ","
                    G_FILE_ATTRIBUTE_STANDARD_NAME ","
                    G_FILE_ATTRIBUTE_STANDARD_SIZE,
                    G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                    m_cancellable,
                    nullptr);

        if (isCancelled())
            return;

        qint32 j = 0;

        if (direnum)
        {
            for (;; ++j)
            {
                GFileInfo *info = nullptr;
                if (!g_file_enumerator_iterate(direnum, &info, nullptr, m_cancellable, nullptr) || !info)
                    break;

                const QString name = g_file_info_get_name(info);
                const quint32 type = g_file_info_get_attribute_uint32(info, G_FILE_ATTRIBUTE_STANDARD_TYPE);

                if (type == G_FILE_TYPE_REGULAR)
                    overalSize += qMax<qint64>(0, g_file_info_get_size(info));

                GUri src(fileOpDescr.srcPath.pathAppended(name));
                GUri dst;
                if (!fileOpDescr.dstPath.isNull())
                    dst = fileOpDescr.dstPath.pathAppended(name);
                m_fileOpDescrs.insert(i + j, {src, dst, type});
            }
            g_object_unref(direnum);
        }

        if (isCancelled())
            return;

        if (preserveCurrent)
        {
            if (m_type == GIOFileOp::Type::Copy || m_type == GIOFileOp::Type::Move)
            {
                fileOpDescr.createDstDir = true;
                dirsToCreate.append(fileOpDescr);
                fileOpDescr.createDstDir = false;
            }
            if (m_type == GIOFileOp::Type::Unlink || m_type == GIOFileOp::Type::Move)
            {
                fileOpDescr.unlinkSrcDir = true;
                dirsToRemove.prepend(fileOpDescr);
                fileOpDescr.unlinkSrcDir = false;
            }
        }

        emit q.countingObjects(m_fileOpDescrs.count(), overalSize);

        --i;
    }

    if (!dirsToCreate.isEmpty() || !dirsToRemove.isEmpty())
    {
        m_fileOpDescrs = dirsToCreate + m_fileOpDescrs + dirsToRemove;
        emit q.countingObjects(m_fileOpDescrs.count(), overalSize);
    }
}
bool GIOFileOpThread::doFileOp(const bool fastMove)
{
    Q_ASSERT(!fastMove || m_type == GIOFileOp::Type::Move);

    const bool copyOrMove = (m_type == GIOFileOp::Type::Copy
                             || m_type == GIOFileOp::Type::Move
                             || m_type == GIOFileOp::Type::Link
                             || m_type == GIOFileOp::Type::Rename);
    const GFileCopyFlags moveFlag = ((m_type == GIOFileOp::Type::Rename)
                                     ? G_FILE_COPY_NO_FALLBACK_FOR_MOVE
                                     : G_FILE_COPY_NONE);
    GFileCopyFlags overwriteFlag = G_FILE_COPY_NONE;
    bool clearOverwriteFlag = false;
    bool overwriteDirs = false;
    bool ignoreAll = false;

    qint32 i = 0, j = 0;
    while (m_fileOpDescrs.count() > i)
    {
        const FileOpDescr fileOpDescr = m_fileOpDescrs.at(i);
        Q_ASSERT(fileOpDescr.dstPath.isNull() || copyOrMove);
        Q_ASSERT(!fileOpDescr.createDstDir || !fileOpDescr.unlinkSrcDir);

        const GIOFileOpInfo singleFileOpInfo {fileOpDescr.srcPath, fileOpDescr.dstPath, j};

        emit q.startedSingle(singleFileOpInfo);
        m_t.start();

        GFile *srcFile = fileOpDescr.srcPath.gFile();
        GFile *dstFile = copyOrMove ? fileOpDescr.dstPath.gFile() : nullptr;

        GError *error = nullptr;

        if (copyOrMove && !fileOpDescr.createDstDir && !fileOpDescr.unlinkSrcDir)
        {
            GFile *dstDirRaw = g_file_get_parent(dstFile);
            if (!g_file_query_exists(dstDirRaw, m_cancellable))
                g_file_make_directory_with_parents(dstDirRaw, m_cancellable, &error);
            g_object_unref(dstDirRaw);
        }

        if (!error)
        {
            const GFileCopyFlags maybeOverwrite = (fileOpDescr.srcType == G_FILE_TYPE_DIRECTORY)
                    ? G_FILE_COPY_NONE
                    : overwriteFlag;

            // FIXME: Better information about "createDstDir" and "unlinkSrcDir"
            if (fileOpDescr.createDstDir)
            {
                g_file_make_directory(dstFile,
                                      m_cancellable,
                                      (ignoreAll || overwriteDirs) ? nullptr : &error);
            }
            else if (fileOpDescr.unlinkSrcDir)
            {
                g_file_delete(srcFile,
                              m_cancellable,
                              ignoreAll ? nullptr : &error);
            }
            else switch (m_type)
            {
                case GIOFileOp::Type::CreateFile:
                    if (m_dataToWrite.isEmpty())
                    {
                        if (GFileOutputStream *f = g_file_create(srcFile, G_FILE_CREATE_NONE, m_cancellable, &error))
                        {
                            g_object_unref(f);
                        }
                    }
                    else if (GFileIOStream *f = g_file_create_readwrite(srcFile, G_FILE_CREATE_NONE, m_cancellable, &error))
                    {
                        g_output_stream_write(g_io_stream_get_output_stream(G_IO_STREAM(f)),
                                              m_dataToWrite.constData(),
                                              m_dataToWrite.size(),
                                              m_cancellable,
                                              nullptr);
                        g_object_unref(f);
                    }
                    break;
                case GIOFileOp::Type::CreateDirectory:
                {
                    g_file_make_directory(srcFile, m_cancellable, &error);
                    break;
                }
                case GIOFileOp::Type::Copy:
                    g_file_copy(srcFile,
                                dstFile,
                                static_cast<GFileCopyFlags>(G_FILE_COPY_NOFOLLOW_SYMLINKS | maybeOverwrite),
                                m_cancellable,
                                (GFileProgressCallback)progress,
                                this,
                                ignoreAll ? nullptr : &error);
                    break;
                case GIOFileOp::Type::Move:
                case GIOFileOp::Type::Rename:
                    g_file_move(srcFile,
                                dstFile,
                                static_cast<GFileCopyFlags>(G_FILE_COPY_NOFOLLOW_SYMLINKS | maybeOverwrite | moveFlag),
                                m_cancellable,
                                (GFileProgressCallback)progress,
                                this,
                                ignoreAll ? nullptr : &error);
                    break;
                case GIOFileOp::Type::Link:
                {
                    char *srcRaw = g_file_get_path(srcFile);
                    g_file_make_symbolic_link(dstFile,
                                              srcRaw,
                                              m_cancellable,
                                              ignoreAll ? nullptr : &error);
                    g_free(srcRaw);
                    break;
                }
                case GIOFileOp::Type::Trash:
                    g_file_trash(srcFile,
                                 m_cancellable,
                                 ignoreAll ? nullptr : &error);
                    break;
                case GIOFileOp::Type::Unlink:
                    g_file_delete(srcFile,
                                  m_cancellable,
                                  ignoreAll ? nullptr : &error);
                    break;
            }
        }

        if (clearOverwriteFlag)
        {
            overwriteFlag = G_FILE_COPY_NONE;
            clearOverwriteFlag = false;
        }

        if (isCancelled())
        {
            return false;
        }
        else if (error)
        {
            if (fastMove && fileOpDescr.srcType == G_FILE_TYPE_DIRECTORY && (error->code == G_IO_ERROR_EXISTS || error->code == G_IO_ERROR_WOULD_RECURSE))
            {
                // In second call of this function, dirs will be moved recursively
                FileOpError fileOpError(FileOpError::Type::FastMoveImpossible,
                                        error->message);
                g_error_free(error);
                emit q.finishedSingle(&fileOpError, singleFileOpInfo);
                const FileOpError::Response response = fileOpError.wait();
                switch (response)
                {
                    case FileOpError::Response::Ignore:
                        m_fileOpDescrs.removeAt(i);
                        ++j;
                        break;
                    case FileOpError::Response::Abort:
                        return false;
                    case FileOpError::Response::Retry:
                        ++i;
                        break;
                    default:
                        qCWarning(fileop) << "Incorrect response" << response << "for" << fileOpError.type();
                        break;
                }
                continue;
            }

            FileOpError::Type type = FileOpError::Type::AbortRetryIgnore;
            switch (error->code)
            {
                case G_IO_ERROR_EXISTS:
                    if (fileOpDescr.srcType == G_FILE_TYPE_DIRECTORY)
                        type = FileOpError::Type::DirectoryExists;
                    else
                        type = FileOpError::Type::FileExists;
                    break;
                case G_IO_ERROR_NOT_FOUND:
                case G_IO_ERROR_FILENAME_TOO_LONG:
                case G_IO_ERROR_NOT_SUPPORTED:
                    type = FileOpError::Type::AbortIgnore;
                    break;
            }

            const bool create = (m_type == GIOFileOp::Type::CreateFile || m_type == GIOFileOp::Type::CreateDirectory);

            QString baseName;
            if (copyOrMove)
                baseName = fileOpDescr.dstPath.baseName();
            else if (create)
                baseName = fileOpDescr.srcPath.baseName();

            FileOpError fileOpError(type, error->message, baseName, (!create && m_type != GIOFileOp::Type::Link));
            g_error_free(error);

            emit q.finishedSingle(&fileOpError, singleFileOpInfo);
            const FileOpError::Response response = fileOpError.wait();

            switch (response)
            {
                case FileOpError::Response::Ignore:
                    if (fileOpError.setToAll())
                        ignoreAll = true;
                    if (copyOrMove && type == FileOpError::Type::DirectoryExists)
                    {
                        // Remove old entries with ignored directory
                        for (auto it = m_fileOpDescrs.begin(); it != m_fileOpDescrs.end();)
                        {
                            if (fileOpDescr.srcPath == it->srcPath || fileOpDescr.srcPath.isParentOf(it->srcPath))
                            {
                                it = m_fileOpDescrs.erase(it);
                                ++j;
                            }
                            else
                            {
                                ++it;
                            }
                        }
                    }
                    else
                    {
                        m_fileOpDescrs.removeAt(i);
                        ++j;
                    }
                    break;
                case FileOpError::Response::Abort:
                    return false;
                case FileOpError::Response::Retry:
                    break;
                case FileOpError::Response::Overwrite:
                    if (copyOrMove)
                    {
                        if (type == FileOpError::Type::DirectoryExists)
                        {
                            emit q.startedSingle(singleFileOpInfo);
                            m_fileOpDescrs.removeAt(i);
                            if (fileOpError.setToAll())
                                overwriteDirs = true;
                            ++j;
                            emit q.finishedSingle(nullptr, singleFileOpInfo);
                        }
                        else
                        {
                            overwriteFlag = G_FILE_COPY_OVERWRITE;
                            if (!fileOpError.setToAll())
                                clearOverwriteFlag = true;
                        }
                    }
                    else
                    {
                        qCWarning(fileop) << "Incorrect response" << response << "for" << type;
                    }
                    break;
                case FileOpError::Response::Rename:
                    if (copyOrMove)
                    {
                        m_fileOpDescrs[i].dstPath.replaceBaseName(fileOpError.destinationName());
                        // TODO: Check for result
                    }
                    else if (create)
                    {
                        m_fileOpDescrs[i].srcPath.replaceBaseName(fileOpError.destinationName());
                        // TODO: Check for result
                    }
                    else
                    {
                        qCWarning(fileop) << "Incorrect response" << response << "for" << type;
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            emit q.finishedSingle(nullptr, singleFileOpInfo);
            m_fileOpDescrs.removeAt(i);
            ++j;
        }
    }

    return true;
}

}
