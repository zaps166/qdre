/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <OpenWith.hpp>

#include <AppsList.hpp>
#include <GIOUtils.hpp>

#include <QLoggingCategory>
#include <QDialogButtonBox>
#include <QGuiApplication>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>

namespace QDRE {

OpenWith::OpenWith(const QString &mimeType, const GUriList &uris, const bool selectCurrent, QWidget *parent)
    : QDialog(parent)
    , m_mimeType(mimeType.toLatin1())
    , m_uris(uris)
{
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);

    const QString mimeDescription = GIOUtils::getDescriptionFromMimeType(mimeType);

    QLabel *label = new QLabel(tr("Open <i>%1</i> with:").arg(mimeDescription),
                               this);

    AppsList *appsList = new AppsList(this);

    m_customCommand = new QGroupBox(tr("Custom command"), this);
    m_customCommand->setCheckable(true);
    m_customCommand->setChecked(false);

    m_customCommandEdit = new QLineEdit(m_customCommand);

    QGridLayout *customCommandLayout = new QGridLayout(m_customCommand);
    customCommandLayout->addWidget(m_customCommandEdit);
    customCommandLayout->setContentsMargins(2, 2, 2, 2);

    if (!m_uris.isEmpty())
    {
        m_storeCheckBox = new QCheckBox(tr("Store this software for \"%1\" files").arg(mimeDescription),
                                        this);
    }

    m_buttons = new QDialogButtonBox(Qt::Horizontal, this);
    m_buttons->setStandardButtons(QDialogButtonBox::Open | QDialogButtonBox::Cancel);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(label);
    layout->addWidget(appsList);
    layout->addWidget(m_customCommand);
    if (m_storeCheckBox)
        layout->addWidget(m_storeCheckBox);
    layout->addWidget(m_buttons);

    if (!m_uris.isEmpty())
        setWindowTitle(tr("Open with..."));
    else
        setWindowTitle(tr("Choose application..."));
    setWindowFlags(Qt::Window);
    if (!m_uris.isEmpty())
        setAttribute(Qt::WA_DeleteOnClose);

    connect(appsList, &AppsList::appInfoChanged,
            this, [=](const GObjectHolder<GAppInfo> &appInfo) {
        m_appInfo = appInfo;
        if (!m_appInfo.isNull())
        {
            m_customOriginalCommand = g_app_info_get_commandline(m_appInfo);
            m_customCommandEdit->setText(m_customOriginalCommand);
        }
        maybeSetOpenButtonEnabled();
    });
    connect(appsList, &AppsList::appInfoActivated,
            this, &OpenWith::accept);

    connect(m_customCommand, &QGroupBox::toggled,
            this, &OpenWith::maybeSetOpenButtonEnabled);
    connect(m_customCommandEdit, &QLineEdit::textEdited,
            this, [=] {
        appsList->setCurrentItem(nullptr);
        maybeSetOpenButtonEnabled();
    });

    connect(m_buttons, &QDialogButtonBox::accepted,
            this, &OpenWith::accept);
    connect(m_buttons, &QDialogButtonBox::rejected,
            this, &OpenWith::reject);

    maybeSetOpenButtonEnabled();

    appsList->createList(m_mimeType, selectCurrent, true);

    QGuiApplication::restoreOverrideCursor();
}
OpenWith::OpenWith(const QString &mimeType, QWidget *parent)
    : OpenWith(mimeType, {}, false, parent)
{}
OpenWith::~OpenWith()
{}

void OpenWith::maybeSetOpenButtonEnabled()
{
    m_buttons->button(QDialogButtonBox::Open)->setDisabled(
                m_appInfo.isNull()
                && (!m_customCommand->isChecked()
                    || m_customCommandEdit->text().isEmpty()));
}

void OpenWith::accept()
{
    if (m_customCommand->isChecked() && m_customCommandEdit->text() != m_customOriginalCommand)
    {
        m_appInfo = GIOUtils::createAppInfo(m_customCommandEdit->text());
    }
    if (!m_uris.isEmpty() && !m_appInfo.isNull())
    {
        if (m_storeCheckBox->isChecked())
            g_app_info_set_as_default_for_type(m_appInfo, m_mimeType.constData(), nullptr);
#ifdef GIO_LAUNCH
        if (!GIOUtils::launchWithMulti(m_uris, m_appInfo))
#endif
            return;
    }
    QDialog::accept();
}

}
