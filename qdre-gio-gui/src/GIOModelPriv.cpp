/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gdesktopappinfo.h>

#include <GIOModelPriv.hpp>
#include <GIOModel.hpp>

#include <DesktopFileReader.hpp>
#include <ThemeIcon.hpp>
#include <GIOUtils.hpp>

#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QUrl>

using namespace std;

namespace QDRE {

constexpr const char *g_attributes =
        G_FILE_ATTRIBUTE_STANDARD_TYPE ","
        G_FILE_ATTRIBUTE_STANDARD_IS_HIDDEN ","
        G_FILE_ATTRIBUTE_STANDARD_IS_SYMLINK ","
        G_FILE_ATTRIBUTE_STANDARD_NAME ","
        G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME ","
        G_FILE_ATTRIBUTE_STANDARD_EDIT_NAME ","
        G_FILE_ATTRIBUTE_STANDARD_ICON ","
        G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_READ ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_EXECUTE ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_DELETE ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_TRASH ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_RENAME ","
        G_FILE_ATTRIBUTE_THUMBNAIL_PATH ","
        G_FILE_ATTRIBUTE_THUMBNAIL_IS_VALID;

constexpr const char *g_attributesSimpleReadable =
        G_FILE_ATTRIBUTE_STANDARD_TYPE ","
        G_FILE_ATTRIBUTE_STANDARD_IS_HIDDEN ","
        G_FILE_ATTRIBUTE_STANDARD_NAME ","
        G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME ","
        G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE ","
        G_FILE_ATTRIBUTE_ACCESS_CAN_READ;

constexpr const char *g_attributesFileType =
        G_FILE_ATTRIBUTE_STANDARD_TYPE ","
        G_FILE_ATTRIBUTE_STANDARD_IS_HIDDEN ","
        G_FILE_ATTRIBUTE_STANDARD_NAME ",";

/**/

bool FileDescr::isHidden() const
{
    return g_file_info_get_is_hidden(info);
}
bool FileDescr::isSymlink() const
{
    return g_file_info_get_is_symlink(info);
}
bool FileDescr::isReadable() const
{
    if (isLocal())
        return g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_READ);
    return true;
}
bool FileDescr::isWritable() const
{
    if (isLocal())
        return g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE);
    return true;
}
bool FileDescr::isExecutable() const
{
    if (isLocal())
        return g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_EXECUTE);
    return false;
}
bool FileDescr::isDirectory() const
{
    return (g_file_info_get_file_type(info) == G_FILE_TYPE_DIRECTORY);
}

bool FileDescr::canDelete() const
{
    if (isLocal())
        return g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_DELETE);
    return true;
}
bool FileDescr::canTrash() const
{
    return g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_TRASH);
}

QString FileDescr::mimeType() const
{
    return g_file_info_get_attribute_string(info, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE);
}

/**/

GIOModelPriv::GIOModelPriv(GIOModel &gioModel)
    : q(gioModel)
    , m_attributes(g_attributes)
{}
GIOModelPriv::~GIOModelPriv()
{
    unrefMonitor();
    unrefCancellable();
}

void GIOModelPriv::setSimpleReadMode()
{
    m_attributes = g_attributesSimpleReadable;
}
void GIOModelPriv::setFileTypeMode()
{
    m_attributes = g_attributesFileType;
}

void GIOModelPriv::setRootPath(const QString &path)
{
    GUri newUri = GUri(path);
    if (newUri != m_uri)
    {
        m_uri = qMove(newUri);
        delete m_trashModel;
        m_trashModel = nullptr;
        resetRootPath();
    }
}
void GIOModelPriv::resetRootPath()
{
    if (m_thumbnailer)
        m_thumbnailer->stopAll();

    if (!m_initializedCutUrlsAndThumbnailer && !isSimpleReadMode() && !isFileTypeMode())
    {
        initializeCutUrlsAndThumbnails();
        m_initializedCutUrlsAndThumbnailer = true;
    }

    unrefMonitor();
    unrefCancellable();
    m_dirWritable = false;
    m_fileInfoAsyncMap.clear();
    resetModel();

    if (m_uri.isNull())
        return;

    m_cancellable = g_cancellable_new();

    GError *error = nullptr;

    g_file_query_info_async(
                m_uri.gFile(),
                G_FILE_ATTRIBUTE_ACCESS_CAN_READ ","
                G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE,
                G_FILE_QUERY_INFO_NONE,
                G_PRIORITY_DEFAULT,
                m_cancellable,
                (GAsyncReadyCallback)gotDirInfoStatic,
                this);

    m_monitor = g_file_monitor_directory(
                m_uri.gFile(),
                G_FILE_MONITOR_WATCH_MOVES,
                nullptr,
                &error);
    if (m_monitor)
    {
        g_signal_connect(
                    m_monitor,
                    "changed",
                    G_CALLBACK(dirMonitorStatic),
                    this);
    }
    if (error)
    {
        qCWarning(giomodel) << "Cannot create dir monitor:" << error->message;
        g_error_free(error);
    }

    g_file_enumerate_children_async(
                m_uri.gFile(),
                m_attributes,
                G_FILE_QUERY_INFO_NONE,
                G_PRIORITY_DEFAULT,
                m_cancellable,
                (GAsyncReadyCallback)gotDirsStatic,
                this);
}

void GIOModelPriv::scheduleItemChange(GFile *newFile,
                                      GFile *oldFile,
                                      FileOpSelection *fileOpSelection)
{
    ++m_seq;
    Q_ASSERT(m_fileInfoAsyncMap.count(m_seq) == 0);
    FileInfoAsync *fileInfoAsync = new FileInfoAsync {
        this,
        m_seq,
        GObjectHolder<GFile>::makeRef(oldFile),
        fileOpSelection,
    };
    m_fileInfoAsyncMap[m_seq] = {unique_ptr<FileInfoAsync>(fileInfoAsync), false};
    g_file_query_info_async(
                newFile,
                m_attributes,
                G_FILE_QUERY_INFO_NONE,
                G_PRIORITY_DEFAULT,
                m_cancellable,
                (GAsyncReadyCallback)gotFileInfoStatic,
                fileInfoAsync);
}

inline bool GIOModelPriv::isSimpleReadMode() const
{
    return (m_attributes == g_attributesSimpleReadable);
}
bool GIOModelPriv::isFileTypeMode() const
{
    return (m_attributes == g_attributesFileType);
}

void GIOModelPriv::initializeCutUrlsAndThumbnails()
{
    getCutUrlsFromClipboard();
    QObject::connect(QGuiApplication::clipboard(), &QClipboard::dataChanged,
                     &q, [this] {
        getCutUrlsFromClipboard();
    });

    m_thumbnailer = make_unique<Thumbnailer>();
    QObject::connect(m_thumbnailer.get(), &Thumbnailer::finished,
                     &q, [this](const GUri &uri) {
        if (m_cancellable)
            scheduleItemChange(uri.gFile());
    });
}
void GIOModelPriv::initializeTrashMonitor()
{
    Q_ASSERT(!m_trashModel);
    m_trashModel = new GIOModel(&q);
    m_trashModel->setFileTypeMode();
    m_trashModel->setDisplayHidden(true);
    QObject::connect(m_trashModel, &GIOModel::changed,
                     &q, bind(&GIOModelPriv::updateTrashState, this));
    m_trashModel->setRootPath(GUri::trash().toString());
}

void GIOModelPriv::unrefMonitor()
{
    if (m_monitor)
    {
        g_object_unref(m_monitor);
        m_monitor = nullptr;
    }
}
void GIOModelPriv::unrefCancellable()
{
    if (m_cancellable)
    {
        g_cancellable_cancel(m_cancellable);
        g_object_unref(m_cancellable);
        m_cancellable = nullptr;
    }
}

void GIOModelPriv::resetModel()
{
    if (!m_files.empty())
    {
        q.beginResetModel();

        m_files.clear();
        m_filesStartIdx = 0;

        q.endResetModel();
    }
    else
    {
        Q_ASSERT(m_filesStartIdx == 0);
    }
}

FileDescr GIOModelPriv::getFileDescr(GFileInfo *info, const bool forceHidden)
{
    FileDescr fileDescr;

    if (!(forceHidden || m_displayHidden || !g_file_info_get_is_hidden(info)))
        return fileDescr;

    static_cast<GUri &>(fileDescr) = m_uri.pathAppended(g_file_info_get_name(info));
    fileDescr.name = g_file_info_get_display_name(info);
    fileDescr.info.ref(info);

    if (isFileTypeMode())
        return fileDescr;

    const bool isDesktop = (fileDescr.mimeType() == "application/x-desktop");
    fileDescr.isDesktop = isDesktop && fileDescr.isLocal();

    if (isSimpleReadMode())
        return fileDescr;

    const bool canRename = (!fileDescr.isLocal() || g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_RENAME));
    if (canRename && fileDescr.editableName.isEmpty())
        fileDescr.editableName = g_file_info_get_edit_name(info);

    fileDescr.isCut = m_cutUris.contains(fileDescr);

    if (fileDescr.isDesktop)
    {
        DesktopFileReader desktopFileReader(fileDescr.toLocalPath());
        if (desktopFileReader.isValid())
            fileDescr.name = desktopFileReader.name();
        if (desktopFileReader.isApp())
        {
            loadIcon(fileDescr, desktopFileReader.appIcon(), false);
        }
        else if (desktopFileReader.isLink())
        {
            if (GUri(desktopFileReader.linkUrl()).isTrash())
            {
                fileDescr.isTrashContainer = true;
                if (!m_trashModel)
                    initializeTrashMonitor();
            }
            updateFileDescrIcon(fileDescr, desktopFileReader);
        }
    }

    if (fileDescr.themedIcon.isNull())
    {
        loadIcon(fileDescr, isDesktop ? nullptr : g_file_info_get_icon(info), true);

        if (!isDesktop)
        {
            if (char *thumbnail = g_file_info_get_attribute_as_string(info, G_FILE_ATTRIBUTE_THUMBNAIL_PATH))
            {
                fileDescr.fileIconPath = thumbnail;
                g_free(thumbnail);
            }
            if (!g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_THUMBNAIL_IS_VALID))
            {
                fileDescr.shouldGenerateThumbnail = true;
            }
        }
    }

    return fileDescr;
}
void GIOModelPriv::updateFileDescrIcon(FileDescr &fileDescr, DesktopFileReader &desktopFileReader)
{
    if (fileDescr.isTrashContainer && !m_trashFull)
        desktopFileReader.setLinkIconKey("EmptyIcon");
    fileDescr.themedIcon = desktopFileReader.icon(false);
    if (fileDescr.themedIcon.isNull())
        fileDescr.fileIconPath = desktopFileReader.linkIcon();
}

void GIOModelPriv::updateTrashState()
{
    const bool trashFull = (m_trashModel->rowCount(QModelIndex()) > 0);
    if (m_trashFull == trashFull)
        return;

    m_trashFull = trashFull;

    QModelIndexList trashIndexes;

    for (size_t i = 0; i < m_files.size(); ++i)
    {
        FileDescr &fileDescr = m_files[i];
        if (!fileDescr.isTrashContainer)
            continue;

        DesktopFileReader desktopFileReader(fileDescr.toLocalPath(), DesktopFileReader::Link);
        if (!desktopFileReader.isLink())
            continue;

        updateFileDescrIcon(fileDescr, desktopFileReader);
        trashIndexes.push_back(q.index(i, 0, QModelIndex()));
    }

    if (trashIndexes.isEmpty())
    {
        m_trashModel->deleteLater(); // Must be "deleteLater()"
        m_trashModel = nullptr;
    }
    else for (const QModelIndex &index : as_const(trashIndexes))
    {
        emit q.dataChanged(index, index, {Qt::DecorationRole});
    }
}

void GIOModelPriv::loadIcon(FileDescr &file, GIcon *icon, const bool loadFallback)
{
    const QVariant vIcon = GIOUtils::getIcon(icon);

    if (vIcon.typeId() == QMetaType::QIcon)
        file.themedIcon = vIcon.value<QIcon>();
    else if (vIcon.typeId() == QMetaType::QString)
        file.fileIconPath = vIcon.toString();

    if (loadFallback && file.themedIcon.isNull())
    {
        if (file.isDesktop)
            file.themedIcon = ThemeIcon::executable();
        else
            file.themedIcon = ThemeIcon::octetStream();
    }
}

qint32 GIOModelPriv::findFileInModel(GFile *file)
{
    if (file)
    {
        auto it = find(m_files.begin(), m_files.end(), file);
        if (it != m_files.end())
            return distance(m_files.begin(), it);
    }
    return -1;
}

void GIOModelPriv::getCutUrlsFromClipboard()
{
    const QMimeData *mimeData = QGuiApplication::clipboard()->mimeData();

    const bool prevHasCutUris = !m_cutUris.isEmpty();
    bool cutUrisChanged = false;

    if (!GIOUtils::isMimeDataCutSelection(mimeData))
    {
        m_cutUris.clear();
        if (prevHasCutUris)
            cutUrisChanged = true;
    }
    else
    {
        m_cutUris = fromUrlList(mimeData->urls());
        if (prevHasCutUris || !m_cutUris.isEmpty())
            cutUrisChanged = true;
    }

    if (!cutUrisChanged)
        return;

    const QVector<int> roles {
        Qt::DecorationRole
    };

    for (size_t i = 0; i < m_files.size(); ++i)
    {
        FileDescr &fileDescr = m_files[i];

        const bool isCut = m_cutUris.contains(fileDescr);
        if (isCut != fileDescr.isCut)
        {
            fileDescr.isCut = isCut;

            const QModelIndex index = q.createIndex(i, 0);
            emit q.dataChanged(index, index, roles);
        }
    }
}

void GIOModelPriv::processFileInfo(GFile *renamedFile, FileOpSelection *fileOpSelection, FileDescr &fileDescr)
{
    const auto findSortedIndexToInsert = [&]()->qint32 {
        if (fileDescr.isDirectory())
        {
            for (qint32 i = 0; i < m_filesStartIdx; ++i)
            {
                if (m_files[i].name > fileDescr.name)
                    return i;
            }
            return m_filesStartIdx;
        }

        for (qint32 i = m_filesStartIdx; i < static_cast<qint32>(m_files.size()); ++i)
        {
            if (m_files[i].name > fileDescr.name)
                return i;
        }
        return m_files.size();
    };

    const auto changeModelData = [&](const qint32 idx, const bool rename) {
        const auto processIndex = [&](const qint32 idx, const bool rename) {
            qint32 newIdx = rename ? findSortedIndexToInsert() : -1;

            m_files[idx] = qMove(fileDescr);

            const QModelIndex index = q.createIndex(idx, 0);
            emit q.dataChanged(index, index);
            if (fileOpSelection)
                fileOpSelection->selection.select(index, index);

            if (rename && idx != newIdx)
            {
                // Always keep files sorted in model

                const bool insertBelow = (newIdx > idx);

                if (insertBelow)
                    --newIdx;

                emit q.layoutAboutToBeChanged();

                FileDescr fileDescrToMove = qMove(m_files[idx]);
                m_files.erase(m_files.begin() + idx);
                m_files.insert(m_files.begin() + newIdx, qMove(fileDescrToMove));

                q.changePersistentIndex(q.createIndex(idx, 0), q.createIndex(m_files.size(), 0));
                for (qint32 row = idx + 1; row < static_cast<qint32>(m_files.size()); ++row)
                {
                    q.changePersistentIndex(q.createIndex(row, 0), q.createIndex(row - 1, 0));
                }
                for (qint32 row = newIdx; row < static_cast<qint32>(m_files.size()) - 1; ++row)
                {
                    q.changePersistentIndex(q.createIndex(row, 0), q.createIndex(row + 1, 0));
                }
                q.changePersistentIndex(q.createIndex(m_files.size(), 0), q.createIndex(newIdx, 0));

                emit q.layoutChanged();
            }
        };

        if (rename)
        {
            // Handle rename operation which replaces existing file
            auto it = find(m_files.begin(), m_files.end(), fileDescr);
            if (it != m_files.end())
            {
                processIndex(distance(m_files.begin(), it), true);
                removeRow(idx);
                return;
            }
        }

        processIndex(idx, rename);
    };

    qint32 idx = findFileInModel(renamedFile);
    if (!fileDescr.info)
    {
        if (idx > -1)
        {
            // Remove item from model if we don't have "GFileInfo" pointer.
            removeRow(idx);
        }
        return;
    }

    if (idx > -1)
    {
        // File renamed - update existing file in model and remove the old one.
        changeModelData(idx, true);
        return;
    }

    auto it = find(m_files.begin(), m_files.end(), fileDescr);
    if (it != m_files.end())
    {
        // Found existing file in model - update it.
        changeModelData(distance(m_files.begin(), it), false);
        return;
    }

    if (!m_displayHidden && fileDescr.isHidden())
        return;

    idx = findSortedIndexToInsert();

    q.beginInsertRows(QModelIndex(), idx, idx);
    if (fileDescr.isDirectory()) // Directory inserted
        ++m_filesStartIdx;
    m_files.insert(m_files.begin() + idx, qMove(fileDescr));
    q.endInsertRows();

    if (fileOpSelection)
    {
        const QModelIndex index = q.createIndex(idx, 0);
        fileOpSelection->selection.select(index, index);
    }
}

void GIOModelPriv::removeRow(const qint32 row)
{
    Q_ASSERT(row >= 0);
    Q_ASSERT(row < static_cast<qint32>(m_files.size()));
    q.beginRemoveRows(QModelIndex(), row, row);
    m_files.erase(m_files.begin() + row);
    if (row < m_filesStartIdx) // Directory removed
        --m_filesStartIdx;
    q.endRemoveRows();
}

void GIOModelPriv::gotDirInfo(GFile *file,
                              GAsyncResult *res,
                              GFileInfo *info,
                              GError *error)
{
    if (error)
    {
        Q_ASSERT(error->code != G_IO_ERROR_CANCELLED);
        emit q.rootDirectoryError(error->message);
        emit q.isRootDirectoryWritableChanged(false);
        g_error_free(error);
        return;
    }

    m_dirWritable = g_file_info_get_attribute_boolean(info, G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE);
    emit q.isRootDirectoryWritableChanged(m_dirWritable);

    g_object_unref(info);
}

void GIOModelPriv::dirMonitor(GFile *file,
                              GFile *otherFile,
                              GFileMonitorEvent eventType)
{
    Q_ASSERT(file);

    if (m_uri == file)
    {
        if (m_uri.isTrash())
            return;
        qCInfo(giomodel) << "Root directory changed with a code:" << eventType;
        resetRootPath();
        return;
    }

    switch (eventType)
    {
        case G_FILE_MONITOR_EVENT_DELETED:
        case G_FILE_MONITOR_EVENT_MOVED_OUT:
            scheduleItemChange(file);
            break;

        case G_FILE_MONITOR_EVENT_CHANGED:
        case G_FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED:
            scheduleItemChange(file);
            break;

        case G_FILE_MONITOR_EVENT_RENAMED:
            Q_ASSERT(otherFile);
            scheduleItemChange(otherFile, file);
            break;

        case G_FILE_MONITOR_EVENT_CREATED:
        case G_FILE_MONITOR_EVENT_MOVED_IN:
            scheduleItemChange(file);
            break;

        default:
            break;
    }
}

void GIOModelPriv::gotDirs(GFile *file,
                           GAsyncResult *res,
                           GFileEnumerator *direnum)
{
    g_file_enumerator_next_files_async(
                direnum,
                INT_MAX, // FIXME: load small chunks (?)
                G_PRIORITY_DEFAULT,
                m_cancellable,
                (GAsyncReadyCallback)gotNextFilesStatic,
                this);

    g_object_unref(direnum);
}
void GIOModelPriv::gotNextFiles(GFileEnumerator *direnum,
                                GAsyncResult *res,
                                GList *const listBegin)
{
    GList *list = listBegin;

    q.beginResetModel();

    Q_ASSERT(m_files.empty());
    Q_ASSERT(m_filesStartIdx == 0);

    bool hasDirs = false;
    bool hasFiles = false;

    while (list)
    {
        GObjectHolder info(static_cast<GFileInfo *>(list->data));

        FileDescr fileDescr = getFileDescr(info);
        if (fileDescr.info)
            m_files.push_back(qMove(fileDescr));

        if (g_file_info_get_file_type(info) == G_FILE_TYPE_DIRECTORY)
            hasDirs = true;
        else
            hasFiles = true;

        list = list->next;
    }

    if (hasDirs && hasFiles)
    {
        stable_sort(m_files.begin(), m_files.end(), [](const auto &first, const auto &second) {
            Q_UNUSED(second)
            return first.isDirectory();
        });

        for (size_t i = 0; i < m_files.size(); ++i)
        {
            if (m_files[i].isDirectory())
                continue;

            m_filesStartIdx = i;

            sort(m_files.begin(), m_files.begin() + i, [](const auto &first, const auto &second) {
                return (first.name < second.name);
            });
            sort(m_files.begin() + i, m_files.end(), [](const auto &first, const auto &second) {
                return (first.name < second.name);
            });

            break;
        }
    }
    else
    {
        sort(m_files.begin(), m_files.end(), [](const auto &first, const auto &second) {
            return (first.name < second.name);
        });
    }

    q.endResetModel();

    g_list_free(listBegin);
}

void GIOModelPriv::gotFileInfo(GFile *file,
                               GAsyncResult *res,
                               FileInfoAsync *fileInfoAsync,
                               GFileInfo *info,
                               GError *error)
{
    const auto fileOpSelectionDeref = [this](FileOpSelection *&fileOpSelection, const bool doEmit) {
        if (fileOpSelection && --fileOpSelection->count <= 0)
        {
            if (doEmit)
                emit q.selectionRequest(fileOpSelection->selection, fileOpSelection->rename);
            delete fileOpSelection;
            fileOpSelection = nullptr;
        }
    };

    if (error)
    {
        Q_ASSERT(error->code != G_IO_ERROR_CANCELLED);
        FileOpSelection *fileOpSelection = fileInfoAsync->fileOpSelection;
        const size_t nErased = m_fileInfoAsyncMap.erase(fileInfoAsync->seq);
        if (nErased > 0)
        {
            fileInfoAsync = nullptr;
            // Use cached value, because "fileInfoAsync" is already removed
            fileOpSelectionDeref(fileOpSelection, false);
        }

        const qint32 idx = findFileInModel(file);
        if (idx > -1)
        {
            // File removed
            removeRow(idx);
        }
        else if (error->code != G_IO_ERROR_NOT_FOUND)
        {
            qCWarning(giomodel) << error->message;
        }
        g_error_free(error);
        return;
    }

    Q_ASSERT(!m_fileInfoAsyncMap.empty()); // This should never happen - error (canceled) should occur instead
    Q_ASSERT(m_fileInfoAsyncMap.count(fileInfoAsync->seq) == 1); // We have exactly one occurence

    fileInfoAsync->fileDescr = getFileDescr(info, true);
    g_object_unref(info);

    m_fileInfoAsyncMap[fileInfoAsync->seq].second = true; // We have a file info for this sequence

    for (auto it = m_fileInfoAsyncMap.begin(); it != m_fileInfoAsyncMap.end();)
    {
        const bool ready = it->second.second;
        if (!ready)
        {
            return;
        }

        FileInfoAsync *fileInfoAsyncIt = it->second.first.get();
        processFileInfo(fileInfoAsyncIt->oldFile,
                        fileInfoAsyncIt->fileOpSelection,
                        fileInfoAsyncIt->fileDescr);
        fileOpSelectionDeref(fileInfoAsyncIt->fileOpSelection, true);

        it = m_fileInfoAsyncMap.erase(it);
    }
    Q_ASSERT(m_fileInfoAsyncMap.empty());
}

void GIOModelPriv::gotDirInfoStatic(GFile *file,
                                    GAsyncResult *res,
                                    GIOModelPriv &d)
{
    GError *error = nullptr;
    GFileInfo *info = g_file_query_info_finish(file, res, &error);
    if (error && error->code == G_IO_ERROR_CANCELLED)
    {
        g_error_free(error);
        return;
    }
    d.gotDirInfo(file, res, info, error);
}

void GIOModelPriv::dirMonitorStatic(GFileMonitor *monitor,
                                    GFile *file,
                                    GFile *otherFile,
                                    GFileMonitorEvent eventType,
                                    GIOModelPriv &d)
{
    if (!g_file_monitor_is_cancelled(monitor))
        d.dirMonitor(file, otherFile, eventType);
}
void GIOModelPriv::gotDirsStatic(GFile *file,
                                 GAsyncResult *res,
                                 GIOModelPriv &d)
{
    if (GFileEnumerator *direnum = g_file_enumerate_children_finish(file, res, nullptr))
        d.gotDirs(file, res, direnum);
}

void GIOModelPriv::gotNextFilesStatic(GFileEnumerator *direnum,
                                      GAsyncResult *res,
                                      GIOModelPriv &d)
{
    if (GList *list = g_file_enumerator_next_files_finish(direnum, res, nullptr))
        d.gotNextFiles(direnum, res, list);
}

void GIOModelPriv::gotFileInfoStatic(GFile *file,
                                     GAsyncResult *res,
                                     FileInfoAsync *fileInfoAsync)
{
    GError *error = nullptr;
    GFileInfo *info = g_file_query_info_finish(file, res, &error);
    if (error && error->code == G_IO_ERROR_CANCELLED)
    {
        g_error_free(error);
        return;
    }
    fileInfoAsync->modelPriv->gotFileInfo(file, res, fileInfoAsync, info, error);
}

}
