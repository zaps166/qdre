cmake_minimum_required(VERSION 3.16)
project(qdre-gio-tests)

find_package(Qt6Test REQUIRED)

file(GLOB PROJECT_FILES
    "*.cpp"
)

add_executable(${PROJECT_NAME}
    ${PROJECT_FILES}
)
add_test(
    NAME ${PROJECT_NAME}
    COMMAND ${PROJECT_NAME}
)

target_link_libraries(${PROJECT_NAME}
    qdre-gio-gui
    Qt::Test
)
