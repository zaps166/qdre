/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <FileOpError.hpp>
#include <GIOFileOp.hpp>

#include <QSignalSpy>
#include <QTest>

#include <QFileInfo>
#include <QDir>

#include <functional>
#include <ctime>

using namespace QDRE;
using namespace std;
using namespace placeholders;

class GIOTest : public QObject
{
    Q_OBJECT

    using FileOpFunc = const function<GIOFileOp::Ptr(const GUriList &)>;
    using CopyOrMoveFunc = const function<void(const QStringList &, const QStringList &)>;

private:
    void init();
    void cleanUp();

    void prepareFiles(const QStringList &files);
    void prepareDirs(const QStringList &dirs);

    QStringList createComplex(const QString &dir, const bool emptyDirs);

    void commonFileOp(const QStringList &files, const FileOpFunc &fileOpFunc);
    void commonUnlinkMove(const QStringList &files, const FileOpFunc &fileOpFunc);

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    /* GUri tests */
private Q_SLOTS:
    void trailingSlash();
    void removeBaseName();
    void replaceBaseName();
    void parents();

    /* Unlink tests */
private Q_SLOTS:
    void unlinkSingleFile();
    void unlinkFiles();
    void unlinkDir();
    void unlinkDirSimple();
    void unlinkDirComplex();
    void unlinkDirComplexWithEmptyDirs();
private:
    void unlinkCommon(const QStringList &files);

private:
    void copyOrMoveFiles(const CopyOrMoveFunc &copyOrMoveFunc);
    void copyOrMoveDirComplex(const CopyOrMoveFunc &copyOrMoveFunc);
    void copyOrMoveDirComplexWithEmptyDirs(const CopyOrMoveFunc &copyOrMoveFunc);

    /* Move tests */
private Q_SLOTS:
    void moveFiles();
    void moveDirComplex();
    void moveDirComplexWithEmptyDirs();
private:
    void moveCommon(const QStringList &src, const QStringList &dst);

    /* Rename tests */
private Q_SLOTS:
    void renameFiles();
    void renameDirComplex();
    void renameDirComplexWithEmptyDirs();
private:
    void renameCommon(const QStringList &src, const QStringList &dst);

    /* Copy tests */
private Q_SLOTS:
    void copyFiles();
    void copyDirComplex();
    void copyDirComplexWithEmptyDirs();
private:
    void copyCommon(const QStringList &src, const QStringList &dst);

private:
    QString m_testPath;
};

static GUriList toGUris(const QStringList &files)
{
    GUriList uris;
    for (const QString &file : files)
        uris += GUri(file);
    return uris;
}

void GIOTest::init()
{
    cleanUp();
    QVERIFY(QDir().mkpath(m_testPath));
}
void GIOTest::cleanUp()
{
    QVERIFY(m_testPath.startsWith(QDir::tempPath()));
    QVERIFY(QDir(m_testPath).removeRecursively());
    QVERIFY2(!QFileInfo(m_testPath).exists(),
             "Test environment is not clean!");
}

void GIOTest::prepareFiles(const QStringList &files)
{
    for (const QString &file : files)
    {
        QVERIFY(QDir().mkpath(QFileInfo(file).absolutePath()));
        QVERIFY(QFile(file).open(QFile::WriteOnly | QFile::Truncate));
        QVERIFY(QFileInfo(file).isFile());
    }
}
void GIOTest::prepareDirs(const QStringList &dirs)
{
    for (const QString &dir : dirs)
    {
        QVERIFY(QDir().mkpath(dir));
        QVERIFY(QFileInfo(dir).isDir());
    }
}

QStringList GIOTest::createComplex(const QString &dir, const bool emptyDirs)
{
    QStringList files;
    for (qint32 i = 1; i <= 4; ++i)
    {
        files += QString("%1/test-%2.dat")
                .arg(dir)
                .arg(i);
        for (qint32 j = 1; j <= 4; ++j)
        {
            files += QString("%1/test-%2-dir/test-%3.dat")
                    .arg(dir)
                    .arg(i)
                    .arg(j);
            for (qint32 k = 1; k <= 4; ++k)
            {
                files += QString("%1/test-%2-dir/test-%3-dir/test-%4.dat")
                        .arg(dir)
                        .arg(i)
                        .arg(j)
                        .arg(k);
            }
        }
    }
    prepareFiles(files);

    if (emptyDirs)
    {
        const QStringList dirs {
            dir + "/test-empty-dir",
            dir + "/test-dir/test-empty-dir"
        };
        prepareDirs(dirs);
        files += dirs;
    }

    return files;
}

void GIOTest::commonFileOp(const QStringList &files, const GIOTest::FileOpFunc &fileOpFunc)
{
    bool errorOccured = false;

    GIOFileOp::Ptr ptr = fileOpFunc(toGUris(files));
    QVERIFY(ptr);

    connect(ptr, &GIOFileOp::finishedSingle,
            this, [&](FileOpError *error) {
        if (error)
        {
            qWarning() << error->message();
            error->response(FileOpError::Response::Abort);
            errorOccured = true;
        }
    });

    QVERIFY(QSignalSpy(ptr, SIGNAL(destroyed())).wait());
    QVERIFY(!ptr);
    QVERIFY(!errorOccured);
}
void GIOTest::commonUnlinkMove(const QStringList &files, const FileOpFunc &fileOpFunc)
{
    commonFileOp(files, fileOpFunc);
    for (const QString &file : files)
        QVERIFY(!QFileInfo(file).exists());
}

void GIOTest::initTestCase()
{
    qputenv("QDRE_GIO_MOVE_ALWAYS_SLOW", QByteArray());

    QString testPath;

    srand(time(nullptr));

    testPath = QDir::tempPath();
    QVERIFY(testPath.startsWith('/'));

    testPath += QString("/QDRE-%1-%2")
            .arg(QString::number(rand(), 0x10))
            .arg(metaObject()->className());

    m_testPath = testPath;
    qDebug() << "Test path:" << m_testPath;
}
void GIOTest::cleanupTestCase()
{
    if (m_testPath.isEmpty())
    {
        QSKIP("Test initialization failed, nothing to cleanup");
        return;
    }
    cleanUp();
}

/* GUri tests */

void GIOTest::trailingSlash()
{
    GUri uri("/");

    QVERIFY(uri.toString() == "file:///");

    uri = uri.pathAppended("test1");
    QVERIFY(uri.toString() == "file:///test1");

    uri = uri.pathAppended("test2");
    QVERIFY(uri.toString() == "file:///test1/test2");

    uri = uri.pathAppended("/test3");
    QVERIFY(uri.toString() == "file:///test1/test2/test3");

    uri = uri.pathAppended("/test4");
    QVERIFY(uri.toString() == "file:///test1/test2/test3/test4");

    uri = uri.pathAppended("/test5/");
    QVERIFY(uri.toString() == "file:///test1/test2/test3/test4/test5");
}
void GIOTest::removeBaseName()
{
    GUri uri("/");

    QVERIFY(uri.toString(true) == "file:///");

    uri = GUri("/a/s/d");
    QVERIFY(uri.toString(true) == "file:///a/s");
}
void GIOTest::replaceBaseName()
{
    GUri uri("/");

    QVERIFY(!uri.replaceBaseName("test"));

    uri = GUri("/a/s/d");
    QVERIFY(uri.replaceBaseName("e"));
    QVERIFY(uri.toString() == "file:///a/s/e");
}
void GIOTest::parents()
{
    GUri uriParent("/a/s");
    GUri uri("/a/s/d");
    QVERIFY(uriParent.isParentOf(uri));
}

/* Unlink tests */

void GIOTest::unlinkSingleFile()
{
    init();

    const QString file = m_testPath + "/test-single.dat";
    prepareFiles({file});
    unlinkCommon({file});
}
void GIOTest::unlinkFiles()
{
    init();

    QStringList files;
    for (qint32 i = 1; i <= 100; ++i)
    {
        files += QString("%1/test-%2.dat")
                .arg(m_testPath)
                .arg(i);
    }
    prepareFiles(files);
    QBENCHMARK_ONCE {
        unlinkCommon(files);
    }
}
void GIOTest::unlinkDir()
{
    init();

    const QString dir = m_testPath + "/test-single-dir";
    prepareDirs({dir});
    unlinkCommon({dir});
}
void GIOTest::unlinkDirSimple()
{
    init();

    const QString dir = m_testPath + "/test-single-dir";

    QStringList files;
    for (qint32 i = 1; i <= 100; ++i)
    {
        files += QString("%1/test-%2.dat")
                .arg(dir)
                .arg(i);
    }
    prepareFiles(files);

    QVERIFY(QFileInfo(dir).exists());

    QBENCHMARK_ONCE {
        unlinkCommon({dir});
    }

    QVERIFY(!QFileInfo(dir).exists());
}
void GIOTest::unlinkDirComplex()
{
    init();

    const QString dir = m_testPath + "/test-multi-dir";
    createComplex(dir, false);

    QBENCHMARK_ONCE {
        unlinkCommon({dir});
    }
}
void GIOTest::unlinkDirComplexWithEmptyDirs()
{
    init();

    const QString dir = m_testPath + "/test-multi-dir";
    createComplex(dir, true);

    QVERIFY(QFileInfo(dir).exists());
    QBENCHMARK_ONCE {
        unlinkCommon({dir});
    }
    QVERIFY(!QFileInfo(dir).exists());
}

void GIOTest::unlinkCommon(const QStringList &files)
{
    commonUnlinkMove(files, bind(&GIOFileOp::unlink, _1, nullptr, false));
}

void GIOTest::copyOrMoveFiles(const CopyOrMoveFunc &copyOrMoveFunc)
{
    init();

    const QString srcDir = m_testPath + "/test-files-from";
    const QString dstDir = m_testPath + "/test-files-to";

    QStringList src, dst;
    for (qint32 i = 1; i <= 10; ++i)
    {
        src += QString("%1/test-%2-from.dat")
                .arg(srcDir)
                .arg(i);
        dst += QString("%1/test-%2-to.dat")
                .arg(dstDir)
                .arg(i);
    }
    prepareFiles(src);

    copyOrMoveFunc(src, dst);
}
void GIOTest::copyOrMoveDirComplex(const CopyOrMoveFunc &copyOrMoveFunc)
{
    init();

    const QString srcDir = m_testPath + "/test-multi-dir-from";
    const QString dstDir = m_testPath + "/test-multi-dir-to";
    const QStringList files = createComplex(srcDir, false);

    QBENCHMARK_ONCE {
        copyOrMoveFunc({srcDir}, {dstDir});
    }

    for (const QString &srcFile : files)
    {
        const QString dstFile = QString(srcFile).replace(srcDir, dstDir);
        QVERIFY2(QFileInfo(dstFile).exists(), dstFile.toUtf8().constData());
    }
}
void GIOTest::copyOrMoveDirComplexWithEmptyDirs(const CopyOrMoveFunc &copyOrMoveFunc)
{
    init();

    const QString srcDir = m_testPath + "/test-multi-dir-from";
    const QString dstDir = m_testPath + "/test-multi-dir-to";
    const QStringList files = createComplex(srcDir, true);

    QBENCHMARK_ONCE {
        copyOrMoveFunc({srcDir}, {dstDir});
    }

    for (const QString &srcFile : files)
    {
        const QString dstFile = QString(srcFile).replace(srcDir, dstDir);
        QVERIFY2(QFileInfo(dstFile).exists(), dstFile.toUtf8().constData());
    }
}

/* Move tests */

void GIOTest::moveFiles()
{
    copyOrMoveFiles(bind(&GIOTest::moveCommon, this, _1, _2));
}
void GIOTest::moveDirComplex()
{
    copyOrMoveDirComplex(bind(&GIOTest::moveCommon, this, _1, _2));
}
void GIOTest::moveDirComplexWithEmptyDirs()
{
    copyOrMoveDirComplexWithEmptyDirs(bind(&GIOTest::moveCommon, this, _1, _2));
}

void GIOTest::moveCommon(const QStringList &src, const QStringList &dst)
{
    for (const QString &file : dst)
        QVERIFY(!QFileInfo(file).exists());
    commonUnlinkMove(src, bind(&GIOFileOp::move, _1, toGUris(dst), false));
    for (const QString &file : dst)
        QVERIFY(QFileInfo(file).exists());
}

/* Rename tests */

void GIOTest::renameFiles()
{
    copyOrMoveFiles(bind(&GIOTest::renameCommon, this, _1, _2));
}
void GIOTest::renameDirComplex()
{
    copyOrMoveDirComplex(bind(&GIOTest::renameCommon, this, _1, _2));
}
void GIOTest::renameDirComplexWithEmptyDirs()
{
    copyOrMoveDirComplexWithEmptyDirs(bind(&GIOTest::renameCommon, this, _1, _2));
}

void GIOTest::renameCommon(const QStringList &src, const QStringList &dst)
{
    for (const QString &file : dst)
        QVERIFY(!QFileInfo(file).exists());
    commonUnlinkMove(src, bind(&GIOFileOp::rename, _1, toGUris(dst), false));
    for (const QString &file : dst)
        QVERIFY(QFileInfo(file).exists());
}

/* Copy tests */

void GIOTest::copyFiles()
{
    copyOrMoveFiles(bind(&GIOTest::copyCommon, this, _1, _2));
}
void GIOTest::copyDirComplex()
{
    copyOrMoveDirComplex(bind(&GIOTest::copyCommon, this, _1, _2));
}
void GIOTest::copyDirComplexWithEmptyDirs()
{
    copyOrMoveDirComplexWithEmptyDirs(bind(&GIOTest::copyCommon, this, _1, _2));
}

void GIOTest::copyCommon(const QStringList &src, const QStringList &dst)
{
    for (const QString &file : src)
        QVERIFY(QFileInfo(file).exists());
    for (const QString &file : dst)
        QVERIFY(!QFileInfo(file).exists());
    commonFileOp(src, bind(GIOFileOp::copy, _1, toGUris(dst), false));
    for (const QString &file : src)
        QVERIFY(QFileInfo(file).exists());
    for (const QString &file : dst)
        QVERIFY(QFileInfo(file).exists());
}

QTEST_MAIN(GIOTest)

#include <GIOTest.moc>
