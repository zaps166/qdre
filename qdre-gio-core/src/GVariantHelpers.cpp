/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <GVariantHelpers.hpp>

namespace QDRE::GVariantHelpers {

GVariant *qVariantToGVariant(const QVariant &value)
{
    switch (value.typeId())
    {
        case QMetaType::Bool:
            return g_variant_new_boolean(value.toBool());
        case QMetaType::Int:
            return g_variant_new_int32(value.toInt());
        case QMetaType::UInt:
            return g_variant_new_uint32(value.toUInt());
        case QMetaType::LongLong:
            return g_variant_new_int64(value.toLongLong());
        case QMetaType::ULongLong:
            return g_variant_new_uint64(value.toULongLong());
        case QMetaType::Double:
            return g_variant_new_double(value.toDouble());
        case QMetaType::Char:
            return g_variant_new_byte(value.toChar().toLatin1());
        case QMetaType::QString:
            return g_variant_new_string(value.toString().toUtf8().constData());
        case QMetaType::QByteArray:
            return g_variant_new_string(value.toByteArray().toBase64().constData());
        default:
            break;
    }
    return nullptr;
}
QVariant gVariantToQVariant(GVariant *value)
{
    switch (g_variant_classify(value))
    {
        case G_VARIANT_CLASS_BOOLEAN:
            return QVariant::fromValue((bool)g_variant_get_boolean(value));
        case G_VARIANT_CLASS_BYTE:
            return QVariant::fromValue(g_variant_get_byte(value));
        case G_VARIANT_CLASS_INT32:
            return QVariant::fromValue(g_variant_get_int32(value));
        case G_VARIANT_CLASS_UINT32:
            return QVariant::fromValue(g_variant_get_uint32(value));
        case G_VARIANT_CLASS_INT64:
            return QVariant::fromValue(g_variant_get_int64(value));
        case G_VARIANT_CLASS_UINT64:
            return QVariant::fromValue(g_variant_get_uint64(value));
        case G_VARIANT_CLASS_DOUBLE:
            return QVariant::fromValue(g_variant_get_double(value));
        case G_VARIANT_CLASS_STRING:
        case G_VARIANT_CLASS_OBJECT_PATH:
        case G_VARIANT_CLASS_SIGNATURE:
            return QVariant::fromValue(QString(g_variant_get_string(value, nullptr)));
        case G_VARIANT_CLASS_VARIANT:
            return gVariantToQVariant(g_variant_get_variant(value));
        case G_VARIANT_CLASS_ARRAY:
        case G_VARIANT_CLASS_TUPLE:
        {
            const auto fillList = [&](auto &&list, auto &&callback) {
                const int length = g_variant_n_children(value);
                if (length > 0)
                {
                    list.reserve(length);
                    for (int i = 0; i < length; ++i)
                    {
                        GVariant *child = g_variant_get_child_value(value, i);
                        callback(list, child);
                        g_variant_unref(child);
                    }
                }
            };

            if (qstrncmp((const char *)g_variant_get_type(value), "as", 2) == 0)
            {
                QStringList list;
                fillList(list, [](auto &&list, GVariant *child) {
                    list += g_variant_get_string(child, nullptr);
                });
                return list;
            }

            QVariantList list;
            fillList(list, [](auto &&list, GVariant *child) {
                list += gVariantToQVariant(child);
            });
            return list;
        }
        case G_VARIANT_CLASS_DICT_ENTRY:
        {
            QHash<QString, QVariant> dict;

            GVariantIter iter;
            g_variant_iter_init(&iter, value);

            QString key;
            while (GVariant *child = g_variant_iter_next_value(&iter))
            {
                QVariant v = gVariantToQVariant(child);
                if (key.isNull())
                {
                    key = v.toString();
                    if (key.isEmpty())
                        return QVariant(); // Something goes wrong (?)
                }
                else
                {
                    dict[key] = std::move(v);
                    key.clear();
                }
            }

            return dict;
        }
        default:
            break;
    }
    return QVariant();
}

}
