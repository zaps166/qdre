/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <GIODevice.hpp>

namespace QDRE {

struct GIODevicePriv
{
    QString fileName;

    GFile *file = nullptr;

    union
    {
        GIOStream *ioStream = nullptr;
        GInputStream *iStream;
        GOutputStream *oStream;

        GFileIOStream *fIoStream;
        GFileInputStream *fIStream;
        GFileOutputStream *fOStream;

        GSeekable *seekable;
    };

    qint64 size = 0;
};

/**/

GIODevice::GIODevice(const QString &fileName, QObject *parent)
    : QIODevice(parent)
    , d(*(new GIODevicePriv))
{
    d.fileName = fileName;
    d.file = g_file_parse_name(d.fileName.toUtf8().constData());
}
GIODevice::~GIODevice()
{
    close();
    g_object_unref(d.file);
    delete &d;
}

QString GIODevice::fileName() const
{
    return d.fileName;
}

bool GIODevice::isSequential() const
{
    if (!isOpen())
        return false;
    return !g_seekable_can_seek(d.seekable);
}

bool GIODevice::open(QIODevice::OpenMode mode)
{
    OpenMode openMode = NotOpen;

    if ((mode == NotOpen) || (mode & Unbuffered) || ((mode & Append) && (mode & ReadOnly)))
        return false;

    if (mode & Append)
    {
        if ((d.fOStream = g_file_append_to(d.file, G_FILE_CREATE_NONE, nullptr, nullptr)))
            openMode = mode | QIODevice::WriteOnly;
    }
    else if ((mode & ReadOnly) && (mode & WriteOnly))
    {
        if ((d.fIoStream = g_file_open_readwrite(d.file, nullptr, nullptr)))
            openMode = mode;
    }
    else if (mode & ReadOnly)
    {
        if ((d.fIStream = g_file_read(d.file, nullptr, nullptr)))
            openMode = mode;
    }
    else if ((mode & WriteOnly) || (mode & Truncate))
    {
        if ((d.fOStream = g_file_replace(d.file, nullptr, false, G_FILE_CREATE_NONE, nullptr, nullptr)))
            openMode = mode;
    }

    if (openMode == NotOpen)
        return false;

    if (GFileInfo *info = g_file_query_info(d.file, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_FILE_QUERY_INFO_NONE, nullptr, nullptr))
    {
        d.size = qMax<qint64>(0, g_file_info_get_size(info));
        g_object_unref(info);
    }

    return QIODevice::open(openMode);
}

void GIODevice::close()
{
    if (d.ioStream)
    {
        g_object_unref(d.ioStream);
        d.ioStream = nullptr;
    }
}

qint64 GIODevice::size() const
{
    Q_ASSERT(!isSequential()); // TODO: Sequential (?)
    return d.size;
}

bool GIODevice::seek(qint64 pos)
{
    if (g_seekable_seek(d.seekable, pos, G_SEEK_SET, nullptr, nullptr))
        return QIODevice::seek(pos);
    return false;
}

bool GIODevice::atEnd() const
{
    return (pos() + 1 >= size());
}

qint64 GIODevice::readData(char *data, qint64 maxlen)
{
    return g_input_stream_read(d.iStream, data, maxlen, nullptr, nullptr);
}

qint64 GIODevice::writeData(const char *data, qint64 len)
{
    const gssize s = g_output_stream_write(d.oStream, data, len, nullptr, nullptr);
    const goffset o = g_seekable_tell(d.seekable);
    d.size = qMax<qint64>(d.size, o);
    return s;
}

}
