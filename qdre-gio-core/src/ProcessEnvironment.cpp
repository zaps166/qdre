#include <gio/gio.h>

#include <ProcessEnvironment.hpp>

#include <QProcessEnvironment>
#include <QFileSystemWatcher>
#include <QLoggingCategory>
#include <QCoreApplication>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QFileInfo>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <QDir>

Q_LOGGING_CATEGORY(processenv, "qdre.gio.processenvironment")

namespace QDRE {

using EnvironmentList = QHash<QString, QString>;

class ProcessEnvironment::Priv
{
public:
    void setWatcher()
    {
        if (watcher.directories().isEmpty() && QFileInfo(directory).isDir())
            watcher.addPath(directory);
        if (watcher.files().isEmpty() && QFileInfo(file).isFile())
            watcher.addPath(file);
    }

    QProcessEnvironment createQProcessEnvironment(const EnvironmentList &envList)
    {
        QProcessEnvironment processEnv = QProcessEnvironment::systemEnvironment();
        for (auto it = envList.constBegin(), itEnd = envList.constEnd(); it != itEnd; ++it)
        {
            const QString &key = it.key();
            const QString &value = it.value();
            if (value.isNull())
                processEnv.remove(key);
            else
                processEnv.insert(key, value);
        }
        return  processEnv;
    }

    GObjectHolder<GAppLaunchContext> createGAppLaunchContext(const EnvironmentList &envList)
    {
        GObjectHolder<GAppLaunchContext> launchContext(g_app_launch_context_new());
        for (auto it = envList.constBegin(), itEnd = envList.constEnd(); it != itEnd; ++it)
        {
            const QString &key = it.key();
            const QString &value = it.value();
            if (value.isNull())
                g_app_launch_context_unsetenv(launchContext, key.toUtf8().constData());
            else
                g_app_launch_context_setenv(launchContext, key.toUtf8().constData(), value.toUtf8().constData());
        }
        return launchContext;
    }

public:
    QString directory;
    QString file;

    QTimer timer;
    QFileSystemWatcher watcher;

    EnvironmentList globalEnv;
    QHash<QString, EnvironmentList> envs;
    QDateTime fileTime;
};

/**/

ProcessEnvironment::ProcessEnvironment(QObject *parent)
    : QObject(parent)
    , d(*(new Priv))
{
    d.directory = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).value(0) + "/" + QCoreApplication::organizationName() + "/gio";
    d.file = d.directory + "/environment.json";

    d.timer.setSingleShot(true);
    d.timer.setInterval(500);

    connect(&d.timer, &QTimer::timeout,
            this, &ProcessEnvironment::loadFromJson);

    const auto watcherTriggered = [this] {
        d.setWatcher();
        d.timer.start();
    };

    connect(&d.watcher, &QFileSystemWatcher::directoryChanged,
            this, watcherTriggered);
    connect(&d.watcher, &QFileSystemWatcher::fileChanged,
            this, watcherTriggered);

    if (!QDir(d.directory).mkpath("."))
        qCCritical(processenv) << "Can't create path:" << d.directory;

    loadFromJson();
}
ProcessEnvironment::~ProcessEnvironment()
{
    delete &d;
}

bool ProcessEnvironment::hasApplicationsEnv()
{
    return !d.envs.isEmpty();
}

bool ProcessEnvironment::loadFromJson()
{
    d.setWatcher();

    const QDateTime fileTime = QFileInfo(d.file).metadataChangeTime();
    if (fileTime == d.fileTime)
        return false;

    d.globalEnv.clear();
    d.envs.clear();
    d.fileTime = fileTime;

    QFile jsonFile(d.file);
    if (!jsonFile.open(QFile::ReadOnly))
        return false;

    QJsonParseError e;
    const QJsonObject object = QJsonDocument::fromJson(jsonFile.readAll(), &e).object();
    if (e.error != QJsonParseError::NoError)
    {
        qCCritical(processenv) << "Can't load environment file:" << e.errorString();
        return false;
    }

    const auto parseEnv = [](EnvironmentList &envList, const QString &key, const QJsonValue &val) {
        if (val.isNull())
        {
            envList[key] = QString();
        }
        else if (val.isString())
        {
            // TODO: Allow "$key" syntax
            envList[key] = val.toString();
        }
    };

    {
        const QJsonObject globalEnvObj = object["environment"].toObject();
        const QStringList globalKeys = globalEnvObj.keys();
        for (const QString &key : globalKeys)
            parseEnv(d.globalEnv, key, globalEnvObj.value(key));
    }

    for (const QJsonValue &val : object["applications"].toArray())
    {
        const QJsonObject obj = val.toObject();
        if (obj.isEmpty())
            continue;

        const QString application = obj["name"].toString().trimmed();
        if (application.isEmpty())
            continue;

        const QJsonObject envObj = obj["environment"].toObject();
        if (envObj.isEmpty())
            continue;

        EnvironmentList processEnv = d.globalEnv;

        const QStringList keys = envObj.keys();
        for (const QString &key : keys)
            parseEnv(processEnv, key, envObj.value(key));

        d.envs[application] = std::move(processEnv);
    }

    return true;
}

std::optional<QProcessEnvironment> ProcessEnvironment::getQProcessEnvironment(const QString &key) const
{
    auto it = key.isEmpty() ? d.envs.constEnd() : d.envs.constFind(key);
    if (it == d.envs.constEnd())
    {
        if (d.globalEnv.isEmpty())
            return std::nullopt;
        return d.createQProcessEnvironment(d.globalEnv);
    }
    return d.createQProcessEnvironment(it.value());
}
GObjectHolder<GAppLaunchContext> ProcessEnvironment::getGAppLaunchContext(const QString &key) const
{
    auto it = key.isEmpty() ? d.envs.constEnd() : d.envs.constFind(key);
    if (it == d.envs.constEnd())
    {
        if (d.globalEnv.isEmpty())
            return nullptr;
        return d.createGAppLaunchContext(d.globalEnv);
    }
    return d.createGAppLaunchContext(it.value());
}

}
