/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <gio/gio.h>

#include <GSettings.hpp>
#include <GObjectHolder.hpp>

#include <QHash>

namespace QDRE {

class GSettingsPriv
{
public:
    GSettingsPriv(GSettings &settings, const QString &schema, const QString &path);
    ~GSettingsPriv();

    quint64 installSettingsChangedFunction(const QString &key, const GSettings::SettingsChangedFunction &func, QObject *parent);
    bool removeSettingsChangedFunction(const quint64 id);

    void triggerInstalledValues();

    bool getBool(const QString &key) const;
    qint32 getInt(const QString &key) const;
    qint32 getEnum(const QString &key) const;
    quint32 getUInt(const QString &key) const;
    double getDouble(const QString &key) const;
    QString getString(const QString &key, const QString &def) const;
    QByteArray getByteArray(const QString &key) const;

    QVariant getValue(const QString &key) const;
    QVariant getDefaultValue(const QString &key) const;
    QVariant getUserValue(const QString &key) const;

    bool setValue(const QString &key, const QVariant &value);

    bool setEnum(const QString &key, const qint32 &value);

    bool setTuple(const QString &key, const QVariantList &values);

    bool setArray(const QString &key, const qint32 *data, const int count);
    bool setArray(const QString &key, const quint32 *data, const int count);
    bool setArray(const QString &key, const double *data, const int count);
    bool setArray(const QString &key, const QStringList &arr);

    void resetValue(const QString &key);

public:
    void valueChanged(const QString &key);

private:
    static void valueChangedStatic(::GSettings *settings, gchar *key, GSettingsPriv &d);

private:
    GSettings &q;

public:
    GObjectHolder<::GSettings> m_settings;

    quint64 m_idSeq = 0;
    QHash<QString, QHash<quint64, GSettings::SettingsChangedFunction>> m_settingsChangedFunctions;
    QHash<quint64, QString> m_idToSettingsKey;
};

}
