/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <GSettingsPriv.hpp>

#include <QLoggingCategory>
#include <QProcess>

namespace QDRE {

GSettings::GSettings(QObject *parent, const QString &schema, const QString &path)
    : QObject(parent)
    , d(*(new GSettingsPriv(*this, schema, path)))
{}
GSettings::~GSettings()
{
    delete &d;
}

void GSettings::sync()
{
    g_settings_sync();
}

void GSettings::removeRecursively(const QString &schema, const QString &path)
{
    // FIXME: Use API
    QProcess::startDetached(QString("gsettings reset-recursively %1:%2")
                            .arg("org.qdre." + schema)
                            .arg("/org/qdre/" + path + "/"));
}

quint64 GSettings::installSettingsChangedFunction(const QString &key, const GSettings::SettingsChangedFunction &func, QObject *parent)
{
    return d.installSettingsChangedFunction(key, func, parent);
}
bool GSettings::removeSettingsChangedFunction(const quint64 id)
{
    return d.removeSettingsChangedFunction(id);
}

void GSettings::triggerValue(const QString &key)
{
    d.valueChanged(key);
}
void GSettings::triggerInstalledValues()
{
    d.triggerInstalledValues();
}

bool GSettings::hasUnapplied() const
{
    return g_settings_get_has_unapplied(d.m_settings);
}

void GSettings::delay()
{
    g_settings_delay(d.m_settings);
}
void GSettings::apply()
{
    g_settings_apply(d.m_settings);
}
void GSettings::revert()
{
    g_settings_revert(d.m_settings);
}

bool GSettings::getBool(const QString &key) const
{
    return d.getBool(key);
}
qint32 GSettings::getInt(const QString &key) const
{
    return d.getInt(key);
}
qint32 GSettings::getEnum(const QString &key) const
{
    return d.getEnum(key);
}
qint32 GSettings::getUInt(const QString &key) const
{
    return d.getUInt(key);
}
double GSettings::getDouble(const QString &key) const
{
    return d.getDouble(key);
}
QString GSettings::getString(const QString &key, const QString &def) const
{
    return d.getString(key, def);
}
QByteArray GSettings::getByteArray(const QString &key) const
{
    return d.getByteArray(key);
}
QStringList GSettings::getStringArray(const QString &key) const
{
    const QVariant value = getValue(key);
    if (value.typeId() == QMetaType::QStringList)
        return value.toStringList();
    qCritical() << "Not a string list type";
    return {};
}

QVariant GSettings::getValue(const QString &key) const
{
    return d.getValue(key);
}
QVariant GSettings::getDefaultValue(const QString &key) const
{
    return d.getDefaultValue(key);
}
QVariant GSettings::getUserValue(const QString &key) const
{
    return d.getUserValue(key);
}

bool GSettings::setValue(const QString &key, const QVariant &value)
{
    return d.setValue(key, value);
}

bool GSettings::setEnum(const QString &key, const qint32 value)
{
    return d.setEnum(key, value);
}

bool GSettings::setTuple(const QString &key, const QVariantList &values)
{
    return d.setTuple(key, values);
}

bool GSettings::setArray(const QString &key, const qint32 *data, const int count)
{
    return d.setArray(key, data, count);
}
bool GSettings::setArray(const QString &key, const quint32 *data, const int count)
{
    return d.setArray(key, data, count);
}
bool GSettings::setArray(const QString &key, const double *data, const int count)
{
    return d.setArray(key, data, count);
}
bool GSettings::setArray(const QString &key, const QStringList &arr)
{
    return d.setArray(key, arr);
}

void GSettings::resetValue(const QString &key)
{
    g_settings_reset(d.m_settings, key.toLatin1().constData());
}

}
