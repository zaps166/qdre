/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <FileOpError.hpp>

#include <QMutex>

namespace QDRE {

struct FileOpErrorPriv
{
    QMutex mutex;
    QString message;
    QString dstName;
    FileOpError::Type type = FileOpError::Type::AbortRetryIgnore;
    bool allowOverwrite = true;
    FileOpError::Response response = FileOpError::Response::Ignore;
    bool setToAll = false;
};

/**/

FileOpError::FileOpError(const Type type, const QString &message, const QString &dstName, const bool allowOverwrite)
    : d(*(new FileOpErrorPriv))
{
    d.mutex.lock();
    d.message = message;
    d.dstName = dstName;
    d.type = type;
    d.allowOverwrite = allowOverwrite;
}
FileOpError::~FileOpError()
{
    d.mutex.unlock();
    delete &d;
}

FileOpError::Response FileOpError::wait()
{
    d.mutex.lock();
    return d.response;
}

FileOpError::Type FileOpError::type() const
{
    return d.type;
}
QString FileOpError::message() const
{
    return d.message;
}
QString FileOpError::destinationName() const
{
    return d.dstName;
}
bool FileOpError::allowOverwrite() const
{
    return d.allowOverwrite;
}
bool FileOpError::setToAll() const
{
    return d.setToAll;
}

void FileOpError::response(const Response response, const bool setToAll, const QString &dstName)
{
    if (!dstName.isEmpty())
        d.dstName = dstName;
    d.response = response;
    d.setToAll = setToAll;
    d.mutex.unlock();
}

}
