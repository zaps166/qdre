/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <glib.h>

#include <GKeyFile.hpp>

#include <QLocale>

namespace QDRE {

class GKeyFile::Priv
{
public:
    inline void maybeAlloc()
    {
        if (!keyFile)
            keyFile = g_key_file_new();
    }
    inline void maybeLoadLocale()
    {
        if (locale.isNull())
            locale = QLocale::system().name().toLatin1();
    }
    inline void maybeFree()
    {
        if (keyFile)
            g_key_file_free(keyFile);
    }

public:
    ::GKeyFile *keyFile = nullptr;
    bool loaded = false;
    QByteArray groupName;
    QByteArray locale;
};

/**/

GKeyFile::GKeyFile()
    : d(std::make_shared<Priv>())
{}
GKeyFile::GKeyFile(const QString &filePath)
    : GKeyFile()
{
    load(filePath);
}
GKeyFile::~GKeyFile()
{
    d->maybeFree();
}

bool GKeyFile::isLoaded() const
{
    return d->loaded;
}
bool GKeyFile::load(const QString &filePath)
{
    d->maybeAlloc();
    d->loaded = g_key_file_load_from_file(
                d->keyFile,
                filePath.toUtf8().constData(),
                G_KEY_FILE_NONE,
                nullptr);
    return d->loaded;
}
void GKeyFile::unload()
{
    d->maybeFree();
    d->keyFile = nullptr;
    d->loaded = false;
}

QByteArray GKeyFile::groupName() const
{
    return d->groupName;
}
void GKeyFile::setGroupName(const QByteArray &groupName)
{
    d->groupName = groupName;
}

bool GKeyFile::getBool(const QByteArray &key, const bool def, const QByteArray &group) const
{
    GError *error = nullptr;
    const bool val = g_key_file_get_boolean(
                d->keyFile,
                (group.isNull() ? d->groupName : group).constData(),
                key.constData(),
                &error);
    if (error)
    {
        g_error_free(error);
        return def;
    }
    return val;
}

QString GKeyFile::getString(const QByteArray &key, const QByteArray &group) const
{
    gchar *stringRaw = g_key_file_get_string(
                d->keyFile,
                (group.isNull() ? d->groupName : group).constData(),
                key.constData(),
                nullptr);
    const QString string(stringRaw);
    g_free(stringRaw);
    return string;
}
QString GKeyFile::getLocaleString(const QByteArray &key, const QByteArray &group) const
{
    d->maybeLoadLocale();
    gchar *stringRaw = g_key_file_get_locale_string(
                d->keyFile,
                (group.isNull() ? d->groupName : group).constData(),
                key.constData(),
                d->locale.constData(),
                nullptr);
    const QString string(stringRaw);
    g_free(stringRaw);
    return string;
}

QStringList GKeyFile::getStringList(const QByteArray &key, const QByteArray &group) const
{
    QStringList stringList;

    gsize length = 0;
    gchar **stringListRaw = g_key_file_get_string_list(
                d->keyFile,
                (group.isNull() ? d->groupName : group).constData(),
                key.constData(),
                &length,
                nullptr);

    stringList.reserve(length);
    for (gsize i = 0; i != length; ++i)
        stringList.push_back(stringListRaw[i]);

    g_strfreev(stringListRaw);

    return stringList;
}

::GKeyFile *GKeyFile::keyFile()
{
    d->maybeAlloc();
    return d->keyFile;
}

}
