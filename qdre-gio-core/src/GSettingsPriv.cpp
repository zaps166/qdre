/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <GSettingsPriv.hpp>

#include <GVariantHelpers.hpp>

#include <QCoreApplication>
#include <QLoggingCategory>

namespace QDRE {

using namespace GVariantHelpers;

GSettingsPriv::GSettingsPriv(GSettings &settings, const QString &schema, const QString &path)
    : q(settings)
{
    const QByteArray schemaId = "org.qdre." + (schema.isEmpty()
                                               ? QCoreApplication::applicationName().toLower()
                                               : schema).toLatin1();
    if (path.isEmpty())
    {
        m_settings = g_settings_new(schemaId.constData());
    }
    else
    {
        const QByteArray fullPath = "/org/qdre/" + path.toLatin1().toLower() + "/";
        m_settings = g_settings_new_with_path(schemaId.constData(), fullPath.constData());
    }

    if (!m_settings)
        qFatal("Can't create GSettings instance...");

    g_signal_connect(
                m_settings,
                "changed",
                G_CALLBACK(valueChangedStatic),
                this);
}
GSettingsPriv::~GSettingsPriv()
{}

quint64 GSettingsPriv::installSettingsChangedFunction(const QString &key, const GSettings::SettingsChangedFunction &func, QObject *parent)
{
    if (!func)
        return 0;

    const auto id = ++m_idSeq;
    m_settingsChangedFunctions[key].insert(id, func);
    m_idToSettingsKey[id] = key;
    if (parent)
    {
        QObject::connect(parent, &QObject::destroyed,
                         &q, [=] {
            removeSettingsChangedFunction(id);
        });
    }
    return id;
}
bool GSettingsPriv::removeSettingsChangedFunction(const quint64 id)
{
    auto itKey = m_idToSettingsKey.find(id);
    if (itKey == m_idToSettingsKey.end())
        return false;

    auto it = m_settingsChangedFunctions.find(itKey.value());
    if (it == m_settingsChangedFunctions.end())
        return false;

    auto &functionsForKey = it.value();
    if (functionsForKey.remove(id) > 0)
    {
        if (functionsForKey.empty())
            m_settingsChangedFunctions.erase(it);
        m_idToSettingsKey.erase(itKey);
        return true;
    }

    return false;
}

void GSettingsPriv::triggerInstalledValues()
{
    for (auto it = m_settingsChangedFunctions.cbegin(), itEnd = m_settingsChangedFunctions.cend(); it != itEnd; ++it)
    {
        for (auto &&func : it.value())
            func(it.key());
    }
}

bool GSettingsPriv::getBool(const QString &key) const
{
    return g_settings_get_boolean(m_settings, key.toLatin1().constData());
}
qint32 GSettingsPriv::getInt(const QString &key) const
{
    return g_settings_get_int(m_settings, key.toLatin1().constData());
}
qint32 GSettingsPriv::getEnum(const QString &key) const
{
    return g_settings_get_enum(m_settings, key.toLatin1().constData());
}
quint32 GSettingsPriv::getUInt(const QString &key) const
{
    return g_settings_get_uint(m_settings, key.toLatin1().constData());
}
double GSettingsPriv::getDouble(const QString &key) const
{
    return g_settings_get_double(m_settings, key.toLatin1().constData());
}
QString GSettingsPriv::getString(const QString &key, const QString &def) const
{
    gchar *str = g_settings_get_string(m_settings, key.toLatin1().constData());
    const QString ret(str);
    g_free(str);
    return ret.isEmpty() ? def : ret;
}
QByteArray GSettingsPriv::getByteArray(const QString &key) const
{
    gchar *str = g_settings_get_string(m_settings, key.toLatin1().constData());
    const QByteArray data = QByteArray::fromBase64(QByteArray::fromRawData(str, strlen(str) + 1));
    g_free(str);
    return data;
}

QVariant GSettingsPriv::getValue(const QString &key) const
{
    GVariant *value = g_settings_get_value(m_settings, key.toLatin1().constData());
    Q_ASSERT(value);
    const QVariant qValue = gVariantToQVariant(value);
    g_variant_unref(value);
    return qValue;
}
QVariant GSettingsPriv::getDefaultValue(const QString &key) const
{
    GVariant *value = g_settings_get_default_value(m_settings, key.toLatin1().constData());
    Q_ASSERT(value);
    const QVariant qValue = gVariantToQVariant(value);
    g_variant_unref(value);
    return qValue;
}
QVariant GSettingsPriv::getUserValue(const QString &key) const
{
    GVariant *value = g_settings_get_user_value(m_settings, key.toLatin1().constData());
    Q_ASSERT(value);
    const QVariant qValue = gVariantToQVariant(value);
    g_variant_unref(value);
    return qValue;
}

bool GSettingsPriv::setValue(const QString &key, const QVariant &value)
{
    return g_settings_set_value(m_settings, key.toLatin1().constData(), qVariantToGVariant(value));
}

bool GSettingsPriv::setEnum(const QString &key, const qint32 &value)
{
    return g_settings_set_enum(m_settings, key.toLatin1().constData(), value);
}

bool GSettingsPriv::setTuple(const QString &key, const QVariantList &values)
{
    GVariant **children = new GVariant *[values.count()];
    for (int i = 0; i < values.count(); ++i)
        children[i] = qVariantToGVariant(values[i]);
    GVariant *gValue = g_variant_new_tuple(children, values.count());
    delete[] children;
    return g_settings_set_value(m_settings, key.toLatin1().constData(), gValue);
}

bool GSettingsPriv::setArray(const QString &key, const qint32 *data, const int count)
{
    GVariant **children = new GVariant *[count];
    for (int i = 0; i < count; ++i)
        children[i] = g_variant_new_int32(data[i]);
    GVariant *gValue = g_variant_new_array(G_VARIANT_TYPE_INT32, children, count);
    delete[] children;
    return g_settings_set_value(m_settings, key.toLatin1().constData(), gValue);
}
bool GSettingsPriv::setArray(const QString &key, const quint32 *data, const int count)
{
    GVariant **children = new GVariant *[count];
    for (int i = 0; i < count; ++i)
        children[i] = g_variant_new_uint32(data[i]);
    GVariant *gValue = g_variant_new_array(G_VARIANT_TYPE_UINT32, children, count);
    delete[] children;
    return g_settings_set_value(m_settings, key.toLatin1().constData(), gValue);
}
bool GSettingsPriv::setArray(const QString &key, const double *data, const int count)
{
    GVariant **children = new GVariant *[count];
    for (int i = 0; i < count; ++i)
        children[i] = g_variant_new_double(data[i]);
    GVariant *gValue = g_variant_new_array(G_VARIANT_TYPE_DOUBLE, children, count);
    delete[] children;
    return g_settings_set_value(m_settings, key.toLatin1().constData(), gValue);
}
bool GSettingsPriv::setArray(const QString &key, const QStringList &arr)
{
    GVariant **children = new GVariant *[arr.count()];
    for (int i = 0; i < arr.count(); ++i)
        children[i] = g_variant_new_string(arr[i].toUtf8());
    GVariant *gValue = g_variant_new_array(G_VARIANT_TYPE_STRING, children, arr.count());
    delete[] children;
    return g_settings_set_value(m_settings, key.toLatin1().constData(), gValue);
}

void GSettingsPriv::resetValue(const QString &key)
{
    g_settings_reset(m_settings, key.toLatin1().constData());
}

void GSettingsPriv::valueChanged(const QString &key)
{
    if (q.signalsBlocked())
        return;

    emit q.valueChanged(key);

    auto it = m_settingsChangedFunctions.constFind(key);
    if (it != m_settingsChangedFunctions.constEnd())
    {
        for (auto &&func : it.value())
            func(it.key());
    }
}

void GSettingsPriv::valueChangedStatic(::GSettings *settings, gchar *key, GSettingsPriv &d)
{
    Q_UNUSED(settings)
    d.valueChanged(key);
}

}
