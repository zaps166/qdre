/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <GObjectHolder.hpp>
#include <gio/gio.h>

#include <InitializeCore.hpp>
#include <GUri.hpp>

#include <QDebug>
#include <QUrl>

#include <utility>

namespace QDRE {

struct GUriPriv
{
    GObjectHolder<GFile> gfile;
};

/**/

QDRE_ADD_INIT_FUNCTION([] {
    qRegisterMetaType<GUri>("GUri");
})

static QByteArray gStringToQByteArray(char *strRaw)
{
    QByteArray str;
    if (strRaw)
    {
        str = strRaw;
        g_free(strRaw);
    }
    return str;
}
static QString maybeRemoveBaseName(const QString &str, const bool removeBaseName)
{
    if (!removeBaseName)
        return str;

    QString ret = str;
    const qint32 idx = ret.lastIndexOf('/');
    if (idx > -1 && idx + 1 < ret.size())
        ret.remove(idx, ret.size() - idx);
    return ret;
}

/**/

GUri GUri::trash()
{
    return GUri("trash:///");
}

GUri::GUri()
    : d(*(new GUriPriv))
{}
QDRE::GUri::GUri(GFile *gfile)
    : GUri()
{
    d.gfile.ref(gfile);
}
GUri::GUri(const QString &str)
    : GUri()
{
    if (!str.isEmpty())
        d.gfile = g_file_parse_name(str.toUtf8().constData());
}
GUri::GUri(const GUri &other)
    : GUri()
{
    *this = other;
}
GUri::GUri(GUri &&other)
    : GUri()
{
    *this = std::move(other);
}
GUri::~GUri()
{
    delete &d;
}

bool GUri::isNull() const
{
    return d.gfile.isNull();
}
bool GUri::isValid() const
{
    return !d.gfile.isNull();
}

bool GUri::isLocal() const
{
    if (isNull())
        return false;
    return g_file_has_uri_scheme(d.gfile, "file");
}
bool GUri::isNative() const
{
    if (isNull())
        return false;
    return g_file_is_native(d.gfile);
}
bool GUri::isTrash() const
{
    if (isNull())
        return false;
    return g_file_has_uri_scheme(d.gfile, "trash");
}

bool QDRE::GUri::hasParent() const
{
    if (isNull())
        return false;
    return g_file_has_parent(d.gfile, nullptr);
}
bool GUri::isParentOf(const GUri &other) const
{
    if (isNull())
        return false;
    return g_file_has_parent(other.d.gfile, d.gfile);
}

QString GUri::baseName() const
{
    if (isNull())
        return QString();
    return gStringToQByteArray(g_file_get_basename(d.gfile));
}
bool GUri::replaceBaseName(const QString &newBaseName)
{
    if (isNull() || newBaseName.isEmpty() || newBaseName.contains('/'))
        return false;

    const QString decodedUri = toDecoded();
    QString decodedUriWithoutBasename = toDecoded(true);
    if (decodedUri.size() == decodedUriWithoutBasename.size())
        return false;

    d.gfile = g_file_parse_name((decodedUriWithoutBasename + "/" + newBaseName).toUtf8().constData());

    return true;
}

GUri GUri::pathAppended(const QString &toAppend) const
{
    QString uri = toString();

    if (uri.isEmpty())
        return GUri(toAppend);

    if (!uri.endsWith('/') && !toAppend.startsWith('/'))
    {
        uri += '/';
    }
    else if (toAppend.startsWith('/'))
    {
        while (uri.endsWith('/'))
            uri.chop(1);
    }

    return GUri(uri + toAppend);
}

QString GUri::toLocalPath(const bool removeBaseName) const
{
    if (isNull())
        return QString();
    return maybeRemoveBaseName(gStringToQByteArray(g_file_get_path(d.gfile)), removeBaseName);
}
QString GUri::toString(const bool removeBaseName) const
{
    if (isNull())
        return QString();
    return maybeRemoveBaseName(gStringToQByteArray(g_file_get_uri(d.gfile)), removeBaseName);
}
QString GUri::toDecoded(const bool removeBaseName) const
{
    if (isNull())
        return QString();
    return maybeRemoveBaseName(QByteArray::fromPercentEncoding(gStringToQByteArray(g_file_get_uri(d.gfile))), removeBaseName);
}

QUrl GUri::toUrl() const
{
    const QString localPath = toLocalPath();
    if (localPath.isEmpty())
        return toString();
    return QUrl::fromLocalFile(localPath);
}

_GFile *GUri::gFile() const
{
    return d.gfile;
}

bool GUri::operator ==(_GFile *gfile) const
{
    return compare(d.gfile, gfile);
}
bool GUri::operator ==(const GUri &other) const
{
    return compare(d.gfile, other.d.gfile);
}

bool GUri::operator !=(_GFile *gfile) const
{
    return !compare(d.gfile, gfile);
}
bool GUri::operator !=(const GUri &other) const
{
    return !compare(d.gfile, other.d.gfile);
}

GUri &GUri::operator =(const GUri &other)
{
    d.gfile = other.d.gfile;
    return *this;
}
GUri &GUri::operator =(GUri &&other)
{
    std::swap(d.gfile, other.d.gfile);
    return *this;
}

bool GUri::compare(_GFile *first, _GFile *second) const
{
    if (!first && !second)
        return true;
    if (!first || !second)
        return false;
    return g_file_equal(first, second);
}

/**/

quint32 qHash(const GUri &key)
{
    return g_file_hash(key.gFile());
}

QDebug operator <<(QDebug dbg, const GUri &uri)
{
    dbg << ("GUri(" + uri.toString() + ")").toUtf8().constData();
    return dbg;
}

}
