/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <glib-object.h>

#include <utility>

namespace QDRE {

template<typename T>
class GObjectHolder
{
public:
    static inline GObjectHolder<T> makeRef(T *ptr);

    GObjectHolder() = default;
    inline GObjectHolder(T *&&ptr);
    inline GObjectHolder(const GObjectHolder &other);
    inline GObjectHolder(GObjectHolder &&other);
    inline ~GObjectHolder();

    inline bool isNull() const;

    inline void ref(T *ptr);

    inline T *ptr() const;

    inline GObjectHolder &operator =(const GObjectHolder &other);
    inline GObjectHolder &operator =(GObjectHolder &&other);

    inline operator T *() const;

private:
    inline void unref();

private:
    T *m_ptr = nullptr;
};

/* Inline implementation */

template<typename T>
GObjectHolder<T> GObjectHolder<T>::makeRef(T *ptr)
{
    GObjectHolder<T> gObjectHolder;
    gObjectHolder.ref(ptr);
    return gObjectHolder;
}

template<typename T>
GObjectHolder<T>::GObjectHolder(T *&&ptr)
    : m_ptr(ptr)
{
    ptr = nullptr;
}
template<typename T>
GObjectHolder<T>::GObjectHolder(const GObjectHolder &other)
{
    *this = other;
}
template<typename T>
GObjectHolder<T>::GObjectHolder(GObjectHolder &&other)
{
    *this = std::move(other);
}
template<typename T>
GObjectHolder<T>::~GObjectHolder()
{
    unref();
}

template<typename T>
bool GObjectHolder<T>::isNull() const
{
    return (m_ptr == nullptr);
}

template<typename T>
void GObjectHolder<T>::ref(T *ptr)
{
    unref();
    if (ptr)
        m_ptr = reinterpret_cast<T *>(g_object_ref(ptr));
    else
        m_ptr = nullptr;
}

template<typename T>
T *GObjectHolder<T>::ptr() const
{
    return m_ptr;
}

template<typename T>
GObjectHolder<T> &GObjectHolder<T>::operator =(const GObjectHolder &other)
{
    m_ptr = other.m_ptr
            ? reinterpret_cast<T *>(g_object_ref(other.m_ptr))
            : nullptr;
    return *this;
}
template<typename T>
GObjectHolder<T> &GObjectHolder<T>::operator =(GObjectHolder &&other)
{
    std::swap(m_ptr, other.m_ptr);
    return *this;
}

template<typename T>
void GObjectHolder<T>::unref()
{
    if (m_ptr)
        g_object_unref(m_ptr);
}

template<typename T>
GObjectHolder<T>::operator T *() const
{
    return m_ptr;
}

}
