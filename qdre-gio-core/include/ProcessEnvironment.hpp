#pragma once

#include <GIOCoreVisibility.hpp>
#include <GObjectHolder.hpp>

#include <QObject>

#include <optional>

class QProcessEnvironment;
struct _GAppLaunchContext;

namespace QDRE {

class QDRE_GIO_CORE_VISIBILITY ProcessEnvironment : public QObject
{
    Q_OBJECT

    class Priv;

public:
    ProcessEnvironment(QObject *parent = nullptr);
    ~ProcessEnvironment();

    bool hasApplicationsEnv();

    bool loadFromJson();

    std::optional<QProcessEnvironment> getQProcessEnvironment(const QString &key) const;
    GObjectHolder<_GAppLaunchContext> getGAppLaunchContext(const QString &key) const;

private:
    Priv &d;
};

}
