/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <GIOCoreVisibility.hpp>
#include <GUriList.hpp>

struct _GFile;
class QUrl;

namespace QDRE {

struct GUriPriv;

class QDRE_GIO_CORE_VISIBILITY GUri
{
public:
    static GUri trash();

public:
    GUri();
    explicit GUri(_GFile *gfile);
    explicit GUri(const QString &str);
    GUri(const GUri &other);
    GUri(GUri &&other);
    ~GUri();

    bool isNull() const;
    bool isValid() const;

    bool isLocal() const;
    bool isNative() const;
    bool isTrash() const;

    bool hasParent() const;
    bool isParentOf(const GUri &other) const;

    QString baseName() const;
    bool replaceBaseName(const QString &newBaseName);

    GUri pathAppended(const QString &toAppend) const;

    QString toLocalPath(const bool removeBaseName = false) const;
    QString toString(const bool removeBaseName = false) const;
    QString toDecoded(const bool removeBaseName = false) const;

    // Get local path with "file://" prefix if possible, otherwise get uri.
    // Use it for drag and copy.
    QUrl toUrl() const;

    _GFile *gFile() const;

    bool operator ==(_GFile *gfile) const;
    bool operator ==(const GUri &other) const;

    bool operator !=(_GFile *gfile) const;
    bool operator !=(const GUri &other) const;

    GUri &operator =(const GUri &other);
    GUri &operator =(GUri &&other);

private:
    bool compare(_GFile *first, _GFile *second) const;

private:
    GUriPriv &d;
};

QDRE_GIO_CORE_VISIBILITY quint32 qHash(const GUri &key);

QDRE_GIO_CORE_VISIBILITY QDebug operator <<(QDebug dbg, const GUri &uri);

}
