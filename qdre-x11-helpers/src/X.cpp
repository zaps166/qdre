/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <xcb/xcb.h>
#include <xcb/shape.h>

#include <X.hpp>

#include <ManagePtr.hpp>

#include <QtGui/private/qtx11extras_p.h>
#include <QLoggingCategory>
#include <QMetaEnum>
#include <QImage>

#include <climits>
#include <memory>
#include <vector>

Q_LOGGING_CATEGORY(xcb, "xcb")

namespace QDRE::X {

using namespace std;

static unique_ptr<xcb_atom_t[]> g_atoms;
static xcb_connection_t *g_conn;

/**/

bool xcbOk(const void *cookie, xcb_connection_t *conn)
{
    if (!conn)
        conn = g_conn;
    return (managePtr(xcb_request_check(conn, *reinterpret_cast<const xcb_void_cookie_t *>(cookie))) == nullptr);
}

void xcbSync(xcb_connection_t *conn)
{
    if (!conn)
        conn = g_conn;
    xcbOk(xcb_no_operation(conn), conn);
}

void initialize()
{
    if (g_atoms.get())
        return;

    g_conn = QX11Info::connection();
    if (!g_conn)
        return;

    const QMetaEnum atomNames = QMetaEnum::fromType<Atoms>();
    const qint32 count = atomNames.keyCount();

    g_atoms.reset(new xcb_atom_t[count]);

    unique_ptr<xcb_intern_atom_cookie_t[]> cookies(new xcb_intern_atom_cookie_t[count]);
    for (int i = 0; i < count; ++i)
    {
        const char *name = atomNames.key(i);
        cookies[i] = xcb_intern_atom(g_conn, false, qstrlen(name), name);
    }

    for (int i = 0; i < count; ++i)
    {
        if (auto reply = managePtr(xcb_intern_atom_reply(g_conn, cookies[i], nullptr)))
        {
            g_atoms[i] = reply->atom;
        }
    }
}

xcb_connection_t *getConnection()
{
    return g_conn;
}

QByteArray getAtomName(const quint32 atom)
{
    if (auto reply = XCB_CALL(xcb_get_atom_name, g_conn, atom))
        return QByteArray(xcb_get_atom_name_name(reply.get()), xcb_get_atom_name_name_length(reply.get()));
    return QByteArray();
}

quint32 getAtom(const Atoms atom)
{
    return g_atoms[atom];
}

xcb_screen_t *getXcbScreen(const qint32 screenNo, xcb_connection_t *conn)
{
    qint32 s = screenNo;
    if (!conn)
        conn = g_conn;
    for (xcb_screen_iterator_t it = xcb_setup_roots_iterator(xcb_get_setup(conn)); it.rem; --s, xcb_screen_next(&it))
    {
        if (s == 0)
            return it.data;
    }
    qCritical(xcb) << "Cannot get xcb screen";
    return nullptr;
}

void queryTreeIterate(quint32 winId, const std::function<bool(quint32)> &fn)
{
    if (auto reply = XCB_CALL_UNCHECKED(xcb_query_tree, g_conn, winId))
    {
        const auto children = reinterpret_cast<xcb_window_t *>(reply.get() + 1);
        for (uint16_t i = 0; i < reply->children_len; ++i)
        {
            if (fn(children[i]))
                break;
        }
    }
}

quint32 getXcbPixmapForAtom(const qint32 screenNo, const quint32 atom)
{
    xcb_screen_t *screen = getXcbScreen(screenNo);
    if (!screen)
        return 0;
    xcb_window_t xroot = screen->root;
    if (auto reply = XCB_CALL(xcb_get_property, g_conn, false, xroot, atom, XCB_ATOM_PIXMAP, 0, 1); reply && reply->length == 1)
        return *(xcb_pixmap_t *)xcb_get_property_value(reply.get());
    return 0;
}

void moveWindow(WId winId, const QPoint &pos)
{
    const quint32 values[] = {static_cast<quint32>(pos.x()),
                              static_cast<quint32>(pos.y())};
    xcb_configure_window(g_conn, winId, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, values);
}
void setWindowMask(WId winId, const QRegion &region, const QPoint &offset)
{
    if (region.isEmpty())
    {
        xcb_shape_mask(g_conn,
                       XCB_SHAPE_SO_SET,
                       XCB_SHAPE_SK_BOUNDING,
                       winId,
                       0,
                       0,
                       XCB_NONE);
    }
    else
    {
        vector<xcb_rectangle_t> rects;
        rects.reserve(region.rectCount());
        for (const QRect &rect : region)
        {
            rects.push_back({
                static_cast<qint16>(qMax<qint32>(SHRT_MIN, rect.x())),
                static_cast<qint16>(qMax<qint32>(SHRT_MIN, rect.y())),
                static_cast<quint16>(qMin<qint32>(USHRT_MAX, rect.width())),
                static_cast<quint16>(qMin<qint32>(USHRT_MAX, rect.height()))
            });
        }
        xcb_shape_rectangles(g_conn,
                             XCB_SHAPE_SO_SET,
                             XCB_SHAPE_SK_BOUNDING,
                             XCB_CLIP_ORDERING_UNSORTED,
                             winId,
                             offset.x(),
                             offset.y(),
                             rects.size(),
                             rects.data());
    }
}

}
