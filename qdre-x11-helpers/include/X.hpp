/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <X11CommonVisibility.hpp>
#include <ManagePtr.hpp>

#include <qobjectdefs.h>
#include <qwindowdefs.h>
#include <QRect>

#include <type_traits>
#include <functional>

struct xcb_connection_t;
struct xcb_screen_t;
class QImage;

#define XCB_CALL(xcbFunc, conn, ...) \
    managePtr(xcbFunc##_reply(conn, xcbFunc(conn, ##__VA_ARGS__), nullptr))
#define XCB_CALL_CHECKED(xcbFunc, conn, ...) \
    managePtr(xcbFunc##_reply(conn, xcbFunc##_checked(conn, ##__VA_ARGS__), nullptr))
#define XCB_CALL_UNCHECKED(xcbFunc, conn, ...) \
    managePtr(xcbFunc##_reply(conn, xcbFunc##_unchecked(conn, ##__VA_ARGS__), nullptr))

namespace QDRE::X {

Q_NAMESPACE

enum Atoms
{
    _NET_WM_STRUT_PARTIAL,

    _XROOTPMAP_ID,
    ESETROOT_PMAP_ID,

    _NET_SYSTEM_TRAY_OPCODE,
    _XEMBED,
    MANAGER,
    XEMBED_EMBEDDED_NOTIFY,
    _NET_SYSTEM_TRAY_S0,

    WM_NAME,

    _NET_WM_BYPASS_COMPOSITOR,
    _NET_WM_WINDOW_OPACITY,
};
Q_ENUM_NS(Atoms)

QDRE_X11_COMMON_VISIBILITY bool xcbOk(const void *cookie, xcb_connection_t *conn);
template<typename XcbVoidCookieT>
static inline bool xcbOk(const XcbVoidCookieT &cookie, xcb_connection_t *conn = nullptr)
{
    return xcbOk(reinterpret_cast<const void *>(&cookie), conn);
}

QDRE_X11_COMMON_VISIBILITY void xcbSync(xcb_connection_t *conn = nullptr);

QDRE_X11_COMMON_VISIBILITY void initialize();

QDRE_X11_COMMON_VISIBILITY xcb_connection_t *getConnection();

QDRE_X11_COMMON_VISIBILITY QByteArray getAtomName(const quint32 atom);

QDRE_X11_COMMON_VISIBILITY quint32 getAtom(const Atoms atom);

QDRE_X11_COMMON_VISIBILITY xcb_screen_t *getXcbScreen(const qint32 screenNo, xcb_connection_t *conn = nullptr);

QDRE_X11_COMMON_VISIBILITY void queryTreeIterate(quint32 winId, const std::function<bool(quint32)> &fn);

QDRE_X11_COMMON_VISIBILITY quint32 getXcbPixmapForAtom(const qint32 screenNo, const quint32 atom);

QDRE_X11_COMMON_VISIBILITY void moveWindow(WId winId, const QPoint &pos);
QDRE_X11_COMMON_VISIBILITY void setWindowMask(WId winId, const QRegion &region, const QPoint &offset = QPoint());

}
