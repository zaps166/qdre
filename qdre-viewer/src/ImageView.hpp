/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QScrollArea>

namespace QDRE {

class ImageContainer;

class ImageView final : public QScrollArea
{
    Q_OBJECT

public:
    enum class ZoomMode
    {
        Best,
        FitToWindow,
        Original,
        User
    };

    ImageView();
    ~ImageView();

    QSize realImageSize() const;

    inline bool isMousePressedForScrollOrIsDragActive() const;

    bool loadImage(const QByteArray &data, const bool reload, const QString &fileName);

    void smoothUpscaling(bool smooth);
    void useDpr(bool checked);
    void rotateLeft();
    void rotateRight();
    void hFlip();
    void vFlip();
    void zoomIn();
    void zoomOut();
    void originalSize();
    void fitToWindow();
    void scrollBarsVisibility(const bool checked);
    void fullScreenMode(const bool f);

Q_SIGNALS:
    void scaleChanged(qint32 scale);

private:
    void showEvent(QShowEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    void enterEvent(QEnterEvent *event) override;

    void mouseDoubleClickEvent(QMouseEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

private:
    inline qreal getDpr() const;

    ZoomMode getRealZoomMode() const;

    bool canScroll() const;
    void setReleasedMouseCursor(const bool force);

    void resizeImage(const QSizeF &size, const QPoint &mousePos = QPoint(-1, -1));
    void maybeFitImage(const QPoint &mousePos = QPoint(-1, -1));
    void zoomInOut(const qint32 zoomDirection, const QPoint &mousePos);

    qreal getZoom() const;
    qreal getScale() const;

    QPoint wrapMouse(const QPoint &mousePos);

    void onDprUpdated();

private:
    bool m_mousePressed = false;
    bool m_mousePressedForDrag = false;
    QPoint m_lastMousePosBeforeWrap;
    QPoint m_lastMousePos;
    QPoint m_newMousePos;
    int m_dragThreshold = 0;

    QTimer *m_cursorTimer;

    ImageContainer *m_imageContainer;
    const QSizeF &m_imageSize;

    ZoomMode m_zoomMode = ZoomMode::Best;

    bool m_useDpr = false;
    bool m_isFullScreen = false;

    bool m_dragActive = false;

    QString m_fileName;
};

/* Inline implementation */

bool ImageView::isMousePressedForScrollOrIsDragActive() const
{
    return m_mousePressed || m_dragActive;
}

}
