/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ImageView.hpp>

#include <ImageContainer.hpp>
#include <Utils.hpp>

#include <QLoggingCategory>
#include <QGuiApplication>
#include <QApplication>
#include <QScrollBar>
#include <QFileInfo>
#include <QMimeData>
#include <qevent.h>
#include <QWindow>
#include <QScreen>
#include <QLabel>
#include <QTimer>
#include <QDrag>
#include <QUrl>

namespace QDRE {

ImageView::ImageView()
    : m_cursorTimer(new QTimer(this))
    , m_imageContainer(new ImageContainer)
    , m_imageSize(m_imageContainer->imageSizeRef())
{
    setFrameShape(QFrame::NoFrame);
    setAlignment(Qt::AlignCenter);
    scrollBarsVisibility(false);
    setWidgetResizable(true);
    setWidget(m_imageContainer);

    m_imageContainer->setMouseTracking(true);
    viewport()->setMouseTracking(true);

    m_cursorTimer->setSingleShot(true);
    m_cursorTimer->setInterval(1250);
    connect(m_cursorTimer, &QTimer::timeout,
            [this] {
        viewport()->setCursor(Qt::BlankCursor);
    });
}
ImageView::~ImageView()
{}

QSize ImageView::realImageSize() const
{
    return m_imageContainer->realImageSize();
}

bool ImageView::loadImage(const QByteArray &data, const bool reload, const QString &fileName)
{
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    const bool ret = m_imageContainer->setImage(data, reload);
    QGuiApplication::restoreOverrideCursor();
    m_fileName = fileName;
    if (!reload)
    {
        m_zoomMode = ZoomMode::Best;
        maybeFitImage();
    }
    return ret;
}

void ImageView::smoothUpscaling(bool smooth)
{
    m_imageContainer->setSmoothUpscaling(smooth);
}
void ImageView::useDpr(bool checked)
{
    m_useDpr = checked;
    maybeFitImage();
}
void ImageView::rotateLeft()
{
    m_imageContainer->rotate90(-1);
    maybeFitImage();
}
void ImageView::rotateRight()
{
    m_imageContainer->rotate90(1);
    maybeFitImage();
}
void ImageView::hFlip()
{
    m_imageContainer->mirror(Qt::Horizontal);
}
void ImageView::vFlip()
{
    m_imageContainer->mirror(Qt::Vertical);
}
void ImageView::zoomIn()
{
    zoomInOut(1, mapFromGlobal(QCursor::pos()));
}
void ImageView::zoomOut()
{
    zoomInOut(-1, mapFromGlobal(QCursor::pos()));
}
void ImageView::originalSize()
{
    m_zoomMode = ZoomMode::Original;
    maybeFitImage();
}
void ImageView::fitToWindow()
{
    m_zoomMode = ZoomMode::FitToWindow;
    maybeFitImage();
}
void ImageView::scrollBarsVisibility(const bool checked)
{
    const Qt::ScrollBarPolicy sbp = checked ? Qt::ScrollBarAsNeeded : Qt::ScrollBarAlwaysOff;
    setHorizontalScrollBarPolicy(sbp);
    setVerticalScrollBarPolicy(sbp);
}
void ImageView::fullScreenMode(const bool f)
{
    // This method should be called before window size changes,
    // because it fits image on window resize.
    if (m_isFullScreen != f)
    {
        m_isFullScreen = f;
        m_zoomMode = ZoomMode::Best;
    }
}

void ImageView::showEvent(QShowEvent *event)
{
    connect(Utils::topLevelWindow(this), &QWindow::screenChanged,
            this, &ImageView::onDprUpdated);
    QWidget::showEvent(event);
}

void ImageView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && canScroll())
    {
        m_cursorTimer->stop();
        m_lastMousePos = event->pos();
        viewport()->setCursor(Qt::ClosedHandCursor);
        m_mousePressed = true;
    }
    else
    {
        setReleasedMouseCursor(true);

        m_mousePressedForDrag = (event->button() == Qt::LeftButton);
        m_dragThreshold = 0;
    }
    QScrollArea::mousePressEvent(event);
}
void ImageView::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mousePressed)
    {
        m_newMousePos = event->pos();
        QTimer::singleShot(1, [this] {
            if (m_mousePressed && m_lastMousePos != m_newMousePos && m_lastMousePosBeforeWrap != m_newMousePos)
            {
                const QPoint delta = m_lastMousePos - m_newMousePos;

                QScrollBar *hScrollBar = horizontalScrollBar();
                QScrollBar *vScrollBar = verticalScrollBar();
                hScrollBar->setValue(hScrollBar->value() + delta.x());
                vScrollBar->setValue(vScrollBar->value() + delta.y());

                m_lastMousePosBeforeWrap = m_newMousePos;
                m_lastMousePos = wrapMouse(m_newMousePos);
            }
        });
    }
    else
    {
        if (viewport()->cursor().shape() == Qt::BlankCursor)
            setReleasedMouseCursor(true);
        else
            m_cursorTimer->start();

        if (m_mousePressedForDrag && !m_fileName.isEmpty())
        {
            if (m_dragThreshold++ > QApplication::startDragDistance())
            {
                if (const QFileInfo fileNameInfo(m_fileName); fileNameInfo.exists())
                {
                    auto drag = new QDrag(this);

                    auto mimeData = new QMimeData;
                    mimeData->setUrls({QUrl::fromLocalFile(fileNameInfo.absoluteFilePath())});
                    drag->setMimeData(mimeData);

                    m_dragActive = true;
                    drag->exec(Qt::CopyAction | Qt::LinkAction, Qt::CopyAction);
                    m_dragActive = false;
                }
                m_mousePressedForDrag = false;
                m_dragThreshold = 0;
            }
        }
    }
    QScrollArea::mouseMoveEvent(event);
}
void ImageView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        if (m_mousePressed)
        {
            setReleasedMouseCursor(true);
            m_mousePressed = false;
        }
        if (m_mousePressedForDrag)
        {
            m_mousePressedForDrag = false;
            m_dragThreshold = 0;
        }
    }
    QScrollArea::mouseReleaseEvent(event);
}

void ImageView::enterEvent(QEnterEvent *event)
{
    if (!m_mousePressed)
        setReleasedMouseCursor(true);
    QScrollArea::enterEvent(event);
}

void ImageView::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        switch (getRealZoomMode())
        {
            case ZoomMode::FitToWindow:
                m_zoomMode = ZoomMode::Original;
                break;
            case ZoomMode::Original:
                m_zoomMode = ZoomMode::FitToWindow;
                break;
            default:
                m_zoomMode = ZoomMode::Best;
                break;
        }
        maybeFitImage(event->pos());
    }
    QScrollArea::mouseDoubleClickEvent(event);
}

void ImageView::wheelEvent(QWheelEvent *event)
{
    zoomInOut(qBound(-1, event->angleDelta().y(), 1), event->position().toPoint());
}

void ImageView::resizeEvent(QResizeEvent *event)
{
    maybeFitImage();
    QScrollArea::resizeEvent(event);
}

inline qreal ImageView::getDpr() const
{
    return m_useDpr ? m_imageContainer->dpr() : 1.0;
}

ImageView::ZoomMode ImageView::getRealZoomMode() const
{
    if (m_zoomMode == ZoomMode::Best)
    {
        if (!m_isFullScreen && m_imageSize.width() * getDpr() < viewport()->width() && m_imageSize.height() * getDpr() < viewport()->height())
            return ZoomMode::Original;
        return ZoomMode::FitToWindow;
    }
    return m_zoomMode;
}

bool ImageView::canScroll() const
{
    return (m_imageContainer->width() > viewport()->width()) || (m_imageContainer->height() > viewport()->height());
}
void ImageView::setReleasedMouseCursor(const bool force)
{
    if (force || viewport()->cursor().shape() != Qt::BlankCursor)
    {
        viewport()->setCursor(canScroll() ? Qt::OpenHandCursor : QCursor());
        m_cursorTimer->start();
    }
}

void ImageView::resizeImage(const QSizeF &size, const QPoint &mousePos)
{
    QPoint point = mousePos;
    if (!viewport()->rect().contains(point))
    {
        point = {viewport()->width()  / 2,
                 viewport()->height() / 2};
    }
    const bool doScroll = (point.x() >= 0 && point.y() >= 0) && (m_zoomMode == ZoomMode::User || m_zoomMode == ZoomMode::Original);
    QPointF p;
    if (doScroll)
    {
        QPoint imgOrigin = m_imageContainer->pos();
        if (imgOrigin.x() < 0)
            imgOrigin.setX(0);
        if (imgOrigin.y() < 0)
            imgOrigin.setY(0);
        p = m_imageContainer->visibleRegion().boundingRect().topLeft() + point - imgOrigin;
        p.rx() /= (qreal)m_imageContainer->width();
        p.ry() /= (qreal)m_imageContainer->height();
    }
    m_imageContainer->setSize(size);
    if (doScroll)
    {
        QScrollBar *hScrollBar = horizontalScrollBar();
        QScrollBar *vScrollBar = verticalScrollBar();
        hScrollBar->setValue(qRound(p.x() * m_imageContainer->width())  - point.x());
        vScrollBar->setValue(qRound(p.y() * m_imageContainer->height()) - point.y());
    }
    if (!m_mousePressed)
        setReleasedMouseCursor(false);
    emit scaleChanged(getScale());
}
void ImageView::maybeFitImage(const QPoint &pos)
{
    if (m_imageSize.isEmpty())
        return;
    switch (getRealZoomMode())
    {
        case ZoomMode::FitToWindow:
            resizeImage(viewport()->size());
            break;
        case ZoomMode::Original:
            resizeImage(m_imageSize * getDpr(), pos);
            break;
        default:
            emit scaleChanged(getScale());
    }
}
void ImageView::zoomInOut(const qint32 zoomDirection, const QPoint &mousePos)
{
    qreal newZoom = getZoom();
    if (zoomDirection == 1)
    {
        newZoom *= 1.1;
    }
    else if (zoomDirection == -1)
    {
        newZoom /= 1.1;
        if (newZoom < 0.01)
            newZoom = 0.01;
    }

    const QSizeF newSize = m_imageSize * newZoom;
    if (newSize.width() > 0 && newSize.width() <= 0xFFFFFF && newSize.height() > 0 && newSize.height() <= 0xFFFFFF)
    {
        m_zoomMode = ZoomMode::User;
        resizeImage(newSize, mousePos);
    }
}

qreal ImageView::getZoom() const
{
    const qreal sX = (qreal)m_imageContainer->width()  / (qreal)m_imageSize.width();
    const qreal sY = (qreal)m_imageContainer->height() / (qreal)m_imageSize.height();
    if (!qIsFinite(sX) || !qIsFinite(sY))
        return 0.0;
    return (sX + sY) / 2.0;
}
qreal ImageView::getScale() const
{
    return getZoom() * 100.0 / getDpr();
}

QPoint ImageView::wrapMouse(const QPoint &mousePos)
{
    const QSize winSize = size();

    QPoint newMousePos = mousePos;

    if (newMousePos.x() >= winSize.width() - 1)
        newMousePos.setX(2);
    else if (newMousePos.x() <= 0)
        newMousePos.setX(winSize.width() - 3);

    if (newMousePos.y() >= winSize.height() - 1)
        newMousePos.setY(2);
    else if (newMousePos.y() <= 0)
        newMousePos.setY(winSize.height() - 3);

    if (newMousePos != mousePos)
    {
        QCursor::setPos(Utils::screenForWidget(this), mapToGlobal(newMousePos));
        return newMousePos;
    }

    return mousePos;
}

void ImageView::onDprUpdated()
{
    if (m_imageContainer->updateDpr(false))
        maybeFitImage();
}

}
