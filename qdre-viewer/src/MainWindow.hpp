/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QMainWindow>

class QToolBar;
class QMenu;

namespace QDRE {

class MetadataWidget;
class KeySequence;
class DockWidget;
class ImageView;
class GSettings;
class GIOModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void installSettingsChangedFunctions();

private:
    void updateFileList();
    qint32 getCurrentFileIndex() const;

    void openImage();

    void loadImageFromDir(const qint32 step);
    bool loadImage(const QString &fileName, const bool reload = false);

    void loadFirstImage();
    void loadLastImage();

    void updateTitle();

    void toggleFullScreen();

    void keyPressEvent(QKeyEvent *event) override;

    void contextMenu(const QPoint &pos);

private:
    void closeEvent(QCloseEvent *event) override;

    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;

private:
    GSettings *const m_settings;
    GIOModel *const m_fileModel;
    KeySequence *const m_keySeq;

    bool m_maximized = false;

    qint32 m_currentScale = 0;

    QString m_currentFile;
    QVector<QString> m_fileList;

    DockWidget *m_metadataDock;

    MetadataWidget *m_metadataWidget;
    ImageView *m_imageView;
    QToolBar *m_toolbar;
    QMenu *m_popupMenu;
    QMenu *m_menu;

    QMetaObject::Connection m_nextAfterDeleteConn;

    union
    {
        struct
        {
            QAction *m_openAct;
            QAction *m_previousAct;
            QAction *m_nextAct;
            QAction *m_zoomInAct;
            QAction *m_zoomOutAct;
            QAction *m_originalSizeAct;
            QAction *m_fitToWindowAct;
            QAction *m_rotateLeftAct;
            QAction *m_rotateRightAct;
            QAction *m_hFlipAct;
            QAction *m_vFlipAct;
            QAction *m_reloadAct;
            QAction *m_moveToTrashAct;
            QAction *m_fullScreenAct;
            QAction *m_scrollBarsAct;
            QAction *m_smoothUpscalingAct;
            QAction *m_useDprAct;
            QAction *m_toggleMetadata;
        };
        QAction *m_allActions[18];
    };
};

}
