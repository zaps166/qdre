/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ImageContainer.hpp>

#include <QMimeDatabase>
#include <QApplication>
#include <QSvgRenderer>
#include <QPainter>
#include <qevent.h>
#include <QBuffer>
#include <QWindow>
#include <QMovie>
#include <QTimer>
#include <QDebug>

namespace QDRE {

ImageContainer::ImageContainer(QWidget *parent)
    : QWidget(parent)
    , m_mimeDB(new QMimeDatabase)
    , m_updateTimer(new QTimer(this))
{
    setAttribute(Qt::WA_OpaquePaintEvent);

    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    m_updateTimer->setSingleShot(true);
    m_updateTimer->setInterval(750);

    connect(m_updateTimer, &QTimer::timeout,
            this, qOverload<>(&QWidget::update));
}
ImageContainer::~ImageContainer()
{
    delete m_mimeDB;
}

bool ImageContainer::setImage(const QByteArray &data, const bool reload)
{
    const QByteArray suffix = m_mimeDB->mimeTypeForData(data).preferredSuffix().toLatin1();

    QSvgRenderer *svg = nullptr;
    QMovie *animation = nullptr;
    QImage image;

    if (QMovie::supportedFormats().contains(suffix))
    {
        animation = new QMovie(this);
        QBuffer *buffer = new QBuffer(animation);
        buffer->setData(data);
        buffer->open(QBuffer::ReadOnly);
        animation->setDevice(buffer);
        if (!animation->isValid())
        {
            delete animation;
            return false;
        }
    }
    else if (suffix == "svg" || suffix == "gz")
    {
        // For "gz" suffix try if it is "svgz".
        svg = new QSvgRenderer(this);
        if (!svg->load(data))
        {
            delete svg;
            return false;
        }
    }
    else
    {
        image = QImage::fromData(data);
        if (image.isNull())
            return false;
    }

    m_cache = QImage();

    delete m_animation;
    delete m_svg;
    m_image = image;

    if (animation)
    {
        m_animation = animation;
        connect(m_animation, &QMovie::frameChanged,
                [this] {
            m_image = m_animation->currentImage();
            update();
        });
        m_animation->start();
    }
    else if (svg)
    {
        m_svg = svg;
    }

    m_paintBackground = (m_image.isNull() || m_image.hasAlphaChannel());
    updateDpr(true);

    if (!reload)
        m_rotation = 0;
    calcImageSize();
    setSize(QSizeF(reload ? m_maybeRotatedSize : m_imageSize));

    return true;
}
void ImageContainer::setSize(const QSizeF &size)
{
    const bool isRotate90 = (m_rotation == 90 || m_rotation == 270);
    QSizeF defaultImageSize;

    if (m_svg)
    {
        defaultImageSize = m_svg->defaultSize() / m_dpr;
        const QSizeF newImageSize = defaultImageSize.scaled(size, Qt::KeepAspectRatio);
        if (m_imageSize != newImageSize)
        {
            m_imageSize = newImageSize;
            m_image = QImage();
        }
    }
    else
    {
        defaultImageSize = m_imageSize;
    }

    m_size = m_imageSize.scaled(isRotate90 ? size.transposed() : size, Qt::KeepAspectRatio);

    if (isRotate90)
    {
        m_maybeRotatedDefaultImageSize = defaultImageSize.transposed();
        m_maybeRotatedImageSize = m_imageSize.transposed();
        m_maybeRotatedSize = m_size.transposed();
    }
    else
    {
        m_maybeRotatedDefaultImageSize = defaultImageSize;
        m_maybeRotatedImageSize = m_imageSize;
        m_maybeRotatedSize = m_size;
    }

    if (minimumSize() != m_maybeRotatedSize)
        m_cache = QImage();
    setMinimumSize(m_maybeRotatedSize.toSize());
    setMaximumSize(m_maybeRotatedSize.toSize());
    m_zoomOut = (m_size.width() < m_imageSize.width() || m_size.height() < m_imageSize.height());
    m_updateTimer->stop();
    update();
}

void ImageContainer::rotate90(const qint32 direction)
{
    if (m_image.isNull())
        return;

    m_rotation += direction * 90;
    if (m_rotation < 0)
        m_rotation = 360 + m_rotation;
    else
        m_rotation = m_rotation % 360;

    const bool isRotate90 = (m_rotation == 90 || m_rotation == 270);
    setSize(isRotate90 ? m_size.transposed() : m_size);
}
void ImageContainer::mirror(const Qt::Orientation orientation)
{
    if (m_image.isNull())
        return;
    switch (orientation)
    {
        case Qt::Horizontal:
            m_image = m_image.mirrored(true, false);
            break;
        case Qt::Vertical:
            m_image = m_image.mirrored(false, true);
            break;
    }
    m_cache = QImage();
    update();
}

void ImageContainer::setSmoothUpscaling(bool smooth)
{
    if (m_smoothUpscaling == smooth)
        return;

    m_smoothUpscaling = smooth;
    update();
}

bool ImageContainer::updateDpr(const bool onlyDpr)
{
    const qreal oldDpr = m_dpr;
    if (QWindow *win = window()->windowHandle())
        m_dpr = win->devicePixelRatio();
    else
        m_dpr = qApp->devicePixelRatio();
    if (!onlyDpr && oldDpr != m_dpr)
    {
        calcImageSize();
        setSize(m_maybeRotatedSize);
        return true;
    }
    return onlyDpr;
}

void ImageContainer::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    if (m_paintBackground)
        p.fillRect(rect(), palette().window());

    if (m_svg && m_image.isNull())
    {
        m_image = QImage(m_size.toSize() * m_dpr, QImage::Format_ARGB32_Premultiplied);
        m_image.fill(Qt::transparent);

        QPainter p(&m_image);
        m_svg->render(&p, QRectF(QPointF(), m_image.size()));
    }

    m_updateTimer->stop();

    if (m_rotation > 0)
        rotateMatrix(p);

    if (m_zoomOut && !m_animation)
    {
        if (m_cache.isNull())
        {
            const int rectCount = event->region().rectCount();
            for (const QRect &rectToDisplay : event->region())
            {
                QRect rectToPaint;
                if (m_rotation > 0)
                {
                    QTransform mat;
                    rotateMatrix(mat);
                    rectToPaint = mat.inverted().mapRect(rectToDisplay);
                }
                else
                {
                    rectToPaint = rectToDisplay;
                }

                const QSize imgSize = rectToPaint.size() * m_dpr;
                if (rectCount == 1 && imgSize == m_size.toSize() * m_dpr)
                {
                    m_cache = m_image.scaled(imgSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
                }
                else
                {
                    QTransform mat;
                    mat.scale((qreal)m_imageSize.width()  * m_dpr / (qreal)m_size.width(),
                              (qreal)m_imageSize.height() * m_dpr / (qreal)m_size.height());
                    const QRect rectToCopy = mat.mapRect(rectToPaint);
                    const QImage img = m_image.copy(rectToCopy).scaled(imgSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
                    p.drawImage(rectToPaint, img);
                }
            }
            if (rectCount > 0 && m_cache.isNull() && (rectCount > 1 || event->region().cbegin()->size() != visibleRegion().boundingRect().size()))
            {
                // Start timer which will update the entire viewport. Partially
                // scaled image is corrupted because of problems with rounding.
                m_updateTimer->start();
            }
        }
        if (!m_cache.isNull())
            p.drawImage(QRect(QPoint(), m_size.toSize()), m_cache);
    }
    else
    {
        if (m_smoothUpscaling && (m_size.width() > m_imageSize.width() || m_size.height() > m_imageSize.height()))
            p.setRenderHint(QPainter::SmoothPixmapTransform);
        p.drawImage(QRect(QPoint(), m_size.toSize()), m_image);
    }
}

void ImageContainer::calcImageSize()
{
    m_imageSize = (m_svg ? m_svg->defaultSize() : m_image.size()) / m_dpr;
}

template<typename M>
void ImageContainer::rotateMatrix(M &mat) const
{
    mat.translate(m_maybeRotatedSize.width() / 2.0, m_maybeRotatedSize.height() / 2.0);
    mat.rotate(m_rotation);
    mat.translate(-m_size.width() / 2.0, -m_size.height() / 2.0);
}

}
