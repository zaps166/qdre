/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QPointer>
#include <QWidget>

class QMimeDatabase;
class QSvgRenderer;

namespace QDRE {

class ImageContainer : public QWidget
{
    Q_OBJECT

public:
    ImageContainer(QWidget *parent = nullptr);
    ~ImageContainer();

    inline QSize realImageSize() const;

    inline const QSizeF &imageSizeRef() const;
    inline qreal dpr() const;

    bool setImage(const QByteArray &data, const bool reload);
    void setSize(const QSizeF &size);

    void rotate90(const qint32 direction);
    void mirror(const Qt::Orientation orientation);

    void setSmoothUpscaling(bool smooth);

    bool updateDpr(const bool onlyDpr);

private:
    void paintEvent(QPaintEvent *event) override;

    void calcImageSize();

    template<typename M>
    void rotateMatrix(M &mat) const;

private:
    QMimeDatabase *m_mimeDB;

    QTimer *m_updateTimer;

    bool m_zoomOut = false;
    bool m_paintBackground = true;
    bool m_smoothUpscaling = true;

    QPointer<QMovie> m_animation;
    QPointer<QSvgRenderer> m_svg;

    QImage m_image;
    QImage m_cache;

    QSizeF m_imageSize;
    QSizeF m_size;

    QSizeF m_maybeRotatedDefaultImageSize;
    QSizeF m_maybeRotatedImageSize;
    QSizeF m_maybeRotatedSize;

    qint32 m_rotation = 0;
    qreal m_dpr = 0.0;
};

/* Inline implementation */

QSize ImageContainer::realImageSize() const
{
    return m_image.size();
}
const QSizeF &ImageContainer::imageSizeRef() const
{
    return m_maybeRotatedDefaultImageSize;
}
qreal ImageContainer::dpr() const
{
    return m_dpr;
}

}
