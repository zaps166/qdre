/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <MetadataWidget.hpp>

#include <ExifHelper.hpp>
#include <ImageView.hpp>

#include <QRegularExpression>
#include <QLoggingCategory>
#include <QFileInfo>

#include <exiv2/exiv2.hpp>

#include <iosfwd>

Q_LOGGING_CATEGORY(metadata, "qdre.viewer.metadata")

namespace QDRE {

MetadataWidget::MetadataWidget()
{
    viewport()->setBackgroundRole(QPalette::Window);
    viewport()->setCursor(QCursor());

    setTextInteractionFlags(Qt::NoTextInteraction);
    setContextMenuPolicy(Qt::PreventContextMenu);
    setFrameShape(NoFrame);
}
MetadataWidget::~MetadataWidget()
{}

void MetadataWidget::loadFromData(const QByteArray &data, const QString &fileName, ImageView *imageView)
{
    const QSize imageSize = imageView->realImageSize();
    QString text;

    clear();

    const auto appendTag = [&](const QString &tag, const QString &data) {
        text.append("<p><b>" + tag + "</b>:<br/>&nbsp;" + data + "</p>");
    };

    if (!fileName.isEmpty())
        appendTag(tr("File name"), QFileInfo(fileName).fileName());

    if (imageSize.isNull())
    {
        setHtml(text);
        return;
    }

    QString imageSizeStr = QString("%1 x %2")
            .arg(imageSize.width())
            .arg(imageSize.height());
    const double mPix = (imageSize.width() * imageSize.height()) / 1000000.0;
    if (mPix >= 0.1)
        imageSizeStr += QString(" (%1 MP)").arg(mPix, 0, 'f', 1);
    appendTag(tr("Image size"), imageSizeStr);

    Exiv2::Image::UniquePtr exiv2;
    try
    {
        exiv2 = Exiv2::ImageFactory::open((const Exiv2::byte *)data.constData(), data.size());
        Q_ASSERT(exiv2.get());
        exiv2->readMetadata();
    }
    catch (...)
    {
        setHtml(text);
        return;
    }

    const std::initializer_list<std::pair<QString, QString>> exifDataArr {
        {"Exif.Photo.DateTimeOriginal", tr("Original date")},
        {"Exif.Image.Make",             tr("Camera make")},
        {"Exif.Image.Model",            tr("Camera model")},
        {"Exif.Photo.ExposureTime",     tr("Exposure time")},
        {"Exif.Photo.ExposureMode",     tr("Exposure mode")},
        {"Exif.Photo.FNumber",          tr("Aperture")},
        {"Exif.*.Lens",                 tr("Lens")},
        {"Exif.Photo.ISOSpeedRatings",  tr("ISO")},
        {"Exif.Photo.Flash",            tr("Flash")},
        {"Exif.Photo.MeteringMode",     tr("Metering mode")},
        {"Exif.Photo.FocalLength",      tr("Focal length")},
        {"Exif.Photo.WhiteBalance",     tr("White balance")},
        {"Exif.Photo.DigitalZoomRatio", tr("Digital zoom ratio")},
        {"Exif.Photo.SceneCaptureType", tr("Scene capture type")},
    };

    const Exiv2::ExifData &tags = exiv2->exifData();
    for (const auto &exifData : exifDataArr)
    {
        QRegularExpression ex(QRegularExpression::wildcardToRegularExpression(exifData.first));
        for (auto it = tags.begin(), itEnd = tags.end(); it != itEnd; ++it)
        {
            if (ex.match(QString::fromLatin1(it->key().c_str())).hasMatch())
            {
                std::ostringstream oss;
                oss << *it;
                appendTag(exifData.second, QString::fromLocal8Bit(oss.str().c_str()).replace('\n', ' '));
                break;
            }
        }
    }

    auto orientationIt = tags.findKey(Exiv2::ExifKey("Exif.Image.Orientation"));
    if (orientationIt != tags.end())
    {
        switch (static_cast<ExifHelper::ImageOrientation>(orientationIt->value().toInt64()))
        {
            case ExifHelper::ImageOrientation::Unspecified:
            case ExifHelper::ImageOrientation::Normal:
                break;
            case ExifHelper::ImageOrientation::Hflip:
                imageView->hFlip();
                break;
            case ExifHelper::ImageOrientation::Rot180:
                imageView->rotateRight();
                imageView->rotateRight();
                break;
            case ExifHelper::ImageOrientation::Vflip:
                imageView->vFlip();
                break;
            case ExifHelper::ImageOrientation::Rot90:
                imageView->rotateRight();
                break;
            case ExifHelper::ImageOrientation::Rot270:
                imageView->rotateLeft();
                break;
            default:
            {
                std::ostringstream oss;
                oss << *orientationIt;
                qCWarning(metadata) << "Unimplemented image rotation:" << QString::fromLocal8Bit(oss.str().c_str());
                break;
            }
        }
    }

    setHtml(text);
}

}
