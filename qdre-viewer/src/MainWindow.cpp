/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <MainWindow.hpp>

#include <MetadataWidget.hpp>
#include <KeySequence.hpp>
#include <FileOpError.hpp>
#include <DockWidget.hpp>
#include <ImageView.hpp>
#include <GSettings.hpp>
#include <ThemeIcon.hpp>
#include <GIOFileOp.hpp>
#include <GIOModel.hpp>
#include <GUri.hpp>

#include <QGuiApplication>
#include <QImageReader>
#include <QMessageBox>
#include <QToolButton>
#include <QFileDialog>
#include <QMimeData>
#include <QToolBar>
#include <qevent.h>
#include <QAction>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QMenu>
#include <QDir>

namespace QDRE {

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_settings(new GSettings(this))
    , m_fileModel(new GIOModel(this))
    , m_keySeq(new KeySequence("MainWindow", this))
    , m_currentFile(QCoreApplication::arguments().value(1))
    , m_metadataDock(new DockWidget(tr("Metadata")))
    , m_metadataWidget(new MetadataWidget)
    , m_imageView(new ImageView)
    , m_popupMenu(new QMenu(this))
    , m_menu(new QMenu(this))
{
    m_fileModel->setSimpleReadMode();

    m_openAct = new QAction(ThemeIcon::get("document-open"), tr("Open"), this);
    m_keySeq->bind(m_openAct, "Open", "Ctrl+O");

    m_previousAct = new QAction(ThemeIcon::get("go-previous"), tr("Previous"), this);
    m_keySeq->bind(m_previousAct, "Previous", "Left");

    m_nextAct = new QAction(ThemeIcon::get("go-next"), tr("Next"), this);
    m_keySeq->bind(m_nextAct, "Next", "Right");

    m_zoomInAct = new QAction(ThemeIcon::get("zoom-in"), tr("Zoom in"), this);
    m_keySeq->bind(m_zoomInAct, "ZoomIn", "+");

    m_zoomOutAct = new QAction(ThemeIcon::get("zoom-out"), tr("Zoom out"), this);
    m_keySeq->bind(m_zoomOutAct, "ZoomOut", "-");

    m_originalSizeAct = new QAction(ThemeIcon::get("zoom-original"), tr("Original image size"), this);
    m_keySeq->bind(m_originalSizeAct, "OriginalSize", "1");
    m_originalSizeAct->setAutoRepeat(false);

    m_fitToWindowAct = new QAction(ThemeIcon::get("zoom-fit-best"), tr("Fit image to window"), this);
    m_keySeq->bind(m_fitToWindowAct, "FitToWindow", "F");
    m_fitToWindowAct->setAutoRepeat(false);

    m_rotateLeftAct = new QAction(ThemeIcon::get("object-rotate-left"), tr("Rotate left"), this);
    m_keySeq->bind(m_rotateLeftAct, "RotateLeft", "Ctrl+L");

    m_rotateRightAct = new QAction(ThemeIcon::get("object-rotate-right"), tr("Rotate right"), this);
    m_keySeq->bind(m_rotateRightAct, "RotateRight", "Ctrl+R");

    m_hFlipAct = new QAction(ThemeIcon::get("object-flip-horizontal"), tr("Horizontal flip"), this);
    m_keySeq->bind(m_hFlipAct, "HFlip", "Ctrl+H");

    m_vFlipAct = new QAction(ThemeIcon::get("object-flip-vertical"), tr("Vertical flip"), this);
    m_keySeq->bind(m_vFlipAct, "VFlip", "Ctrl+V");

    m_reloadAct = new QAction(ThemeIcon::get("view-refresh"), tr("Reload"), this);
    m_keySeq->bind(m_reloadAct, "Reload", "F5");
    m_reloadAct->setAutoRepeat(false);

    m_moveToTrashAct = new QAction(ThemeIcon::get("user-trash"), tr("Move to trash"), this);
    m_keySeq->bind(m_moveToTrashAct, "Trash", "Del");
    m_moveToTrashAct->setAutoRepeat(false);

    m_fullScreenAct = new QAction(ThemeIcon::get("view-fullscreen"), tr("Full screen"), this);
    m_keySeq->bind(m_fullScreenAct, "FullScreen", "F11");
    m_fullScreenAct->setCheckable(true);
    m_fullScreenAct->setAutoRepeat(false);

    m_scrollBarsAct = new QAction(QIcon(), tr("Scroll bars visible"), this);
    m_keySeq->bind(m_scrollBarsAct, "ScrollBars", "S");
    m_scrollBarsAct->setAutoRepeat(false);
    m_scrollBarsAct->setCheckable(true);
    m_scrollBarsAct->setChecked(false);

    m_smoothUpscalingAct = new QAction(QIcon(), tr("Smooth image upscaling"), this);
    m_keySeq->bind(m_smoothUpscalingAct, "SmoothUpscaling", "Alt+S");
    m_smoothUpscalingAct->setAutoRepeat(false);
    m_smoothUpscalingAct->setCheckable(true);

    m_useDprAct = new QAction(ThemeIcon::get("preferences-desktop-display"), tr("Use device pixel ratio"), this);
    m_keySeq->bind(m_useDprAct, "UseDPR", "D");
    m_useDprAct->setAutoRepeat(false);
    m_useDprAct->setCheckable(true);

    m_toggleMetadata = new QAction(ThemeIcon::get("view-list-details"), tr("Show metadata widget"), this);
    m_keySeq->bind(m_toggleMetadata, "ToggleMetadata", {"Alt+Return", "Alt+Enter"});
    m_toggleMetadata->setCheckable(true);

    for (QAction *act : m_allActions)
        addAction(act);


    m_toolbar = addToolBar(tr("Main toolbar"));
    m_toolbar->setObjectName("MainToolbar");
    m_toolbar->setIconSize({24, 24});
    m_toolbar->setMovable(false);

    m_toolbar->addAction(m_openAct);
    m_toolbar->addSeparator();
    m_toolbar->addAction(m_previousAct);
    m_toolbar->addAction(m_nextAct);
    m_toolbar->addSeparator();
    m_toolbar->addAction(m_zoomInAct);
    m_toolbar->addAction(m_zoomOutAct);
    m_toolbar->addAction(m_originalSizeAct);
    m_toolbar->addAction(m_fitToWindowAct);
    m_toolbar->addSeparator();
    m_toolbar->addAction(m_rotateLeftAct);
    m_toolbar->addAction(m_rotateRightAct);
    m_toolbar->addAction(m_hFlipAct);
    m_toolbar->addAction(m_vFlipAct);
    m_toolbar->addSeparator();
    m_toolbar->addAction(m_reloadAct);
    m_toolbar->addSeparator();
    m_toolbar->addAction(m_fullScreenAct);

    QWidget *spacer = new QWidget;
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_toolbar->addWidget(spacer);

    QAction *menuAct = m_toolbar->addAction(ThemeIcon::get("application-menu"), "Menu");
    menuAct->setMenu(m_menu);
    if (QToolButton *toolButton = qobject_cast<QToolButton *>(m_toolbar->widgetForAction(menuAct)))
        toolButton->setPopupMode(QToolButton::InstantPopup);


    m_menu->addActions(actions());
    m_popupMenu->addActions(actions());


    QAction *firstAct = new QAction(this);
    m_keySeq->bind(firstAct, "First", "Home");
    connect(firstAct, &QAction::triggered,
            this, &MainWindow::loadFirstImage);
    addAction(firstAct);

    QAction *lastAct = new QAction(this);
    m_keySeq->bind(lastAct, "Last", "End");
    connect(lastAct, &QAction::triggered,
            this, &MainWindow::loadLastImage);
    addAction(lastAct);


    m_metadataDock->setObjectName("MetadataDock");
    m_metadataDock->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable);
    m_metadataDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    m_metadataDock->setWidget(m_metadataWidget);
    addDockWidget(Qt::LeftDockWidgetArea, m_metadataDock);
    connect(m_metadataDock, &DockWidget::visibilityChanged,
            m_toggleMetadata, &QAction::setChecked);


    connect(m_toggleMetadata, &QAction::triggered,
            m_metadataDock, &DockWidget::setVisible);
    connect(m_smoothUpscalingAct, &QAction::triggered,
            this, [this](bool checked) {
        m_settings->setValue("smooth-upscaling", checked);
    });
    connect(m_useDprAct, &QAction::triggered,
            this, [this](bool checked) {
        m_settings->setValue("use-dpr", checked);
    });
    connect(m_reloadAct, &QAction::triggered,
            this, [this] {
        loadImage(m_currentFile, true);
    });
    connect(m_moveToTrashAct, &QAction::triggered,
            this, [this] {
        if (m_currentFile.isEmpty() || m_nextAfterDeleteConn)
            return;

        const QFileInfo currentFileInfo(m_currentFile);
        if (!currentFileInfo.exists())
            return;

        auto button = QMessageBox::question(
            this,
            tr("Delete file"),
            tr("Do you want to move <b>%1</b> to trash?").arg(currentFileInfo.fileName())
        );
        if (button != QMessageBox::Yes)
            return;

        if (auto ptr = GIOFileOp::trash({GUri(m_currentFile)}, false))
        {
            m_nextAfterDeleteConn = connect(
                ptr, &GIOFileOp::finishedSingle,
                this, [this](FileOpError *error) {
                    if (error)
                    {
                        error->response(FileOpError::Response::Ignore);
                        return;
                    }
                    m_nextAct->trigger();
            });
        }
    });
    connect(m_openAct, &QAction::triggered,
            this, &MainWindow::openImage);
    connect(m_previousAct, &QAction::triggered,
            this, [this] {
        loadImageFromDir(-1);
    });
    connect(m_nextAct, &QAction::triggered,
            this, [this] {
        loadImageFromDir(1);
    });
    connect(m_rotateLeftAct, &QAction::triggered,
            m_imageView, &ImageView::rotateLeft);
    connect(m_rotateRightAct, &QAction::triggered,
            m_imageView, &ImageView::rotateRight);
    connect(m_hFlipAct, &QAction::triggered,
            m_imageView, &ImageView::hFlip);
    connect(m_vFlipAct, &QAction::triggered,
            m_imageView, &ImageView::vFlip);
    connect(m_zoomInAct, &QAction::triggered,
            m_imageView, &ImageView::zoomIn);
    connect(m_zoomOutAct, &QAction::triggered,
            m_imageView, &ImageView::zoomOut);
    connect(m_originalSizeAct, &QAction::triggered,
            m_imageView, &ImageView::originalSize);
    connect(m_fitToWindowAct, &QAction::triggered,
            m_imageView, &ImageView::fitToWindow);
    connect(m_scrollBarsAct, &QAction::triggered,
            this, [this](bool checked) {
        m_settings->setValue("show-scrollbars", checked);
    });
    connect(m_fullScreenAct, &QAction::triggered,
            this, &MainWindow::toggleFullScreen);

    connect(this, &QWidget::customContextMenuRequested,
            this, &MainWindow::contextMenu);

    connect(m_imageView, &ImageView::scaleChanged,
            this, [this](qint32 scale) {
        m_currentScale = scale;
        updateTitle();
    }, Qt::QueuedConnection);

    connect(m_fileModel, &GIOModel::changed,
            this, &MainWindow::updateFileList);

    setContextMenuPolicy(Qt::CustomContextMenu);
    setAcceptDrops(true);

    setCentralWidget(m_imageView);

    QTimer::singleShot(0, this, [this] {
        loadImage(m_currentFile);
    });

    if (!restoreGeometry(m_settings->getByteArray("window-geometry")))
        showMaximized();
    restoreState(m_settings->getByteArray("dock-widget-state"));

    installSettingsChangedFunctions();
    m_settings->triggerInstalledValues();
}
MainWindow::~MainWindow()
{}

void MainWindow::installSettingsChangedFunctions()
{
    const auto setCheckedBlocked = [](QAction *act, const bool checked) {
        QSignalBlocker blocker(act);
        act->setChecked(checked);
    };

    m_settings->installSettingsChangedFunction("smooth-upscaling", [=](const QString &key) {
        const bool checked = m_settings->getBool(key);
        m_imageView->smoothUpscaling(checked);
        setCheckedBlocked(m_smoothUpscalingAct, checked);
    });

    m_settings->installSettingsChangedFunction("use-dpr", [=](const QString &key) {
        const bool checked = m_settings->getBool(key);
        m_imageView->useDpr(checked);
        setCheckedBlocked(m_useDprAct, checked);
    });

    m_settings->installSettingsChangedFunction("show-scrollbars", [=](const QString &key) {
        const bool checked = m_settings->getBool(key);
        m_imageView->scrollBarsVisibility(checked);
        setCheckedBlocked(m_scrollBarsAct, checked);
    });
}

void MainWindow::updateFileList()
{
    const QList<QByteArray> supportedMimeTypes = QImageReader::supportedMimeTypes();
    const int count = m_fileModel->rowCount(QModelIndex());
    QVector<QString> fileList;
    for (int i = 0; i < count; ++i)
    {
        const QModelIndex index = m_fileModel->index(i, 0, QModelIndex());
        if (!index.isValid() || m_fileModel->isDirectory(index) || !m_fileModel->isReadable(index))
            continue;

        const auto mimeType = m_fileModel->getMimeType(index).toLatin1();
        if (!supportedMimeTypes.contains(mimeType))
        {
            if (mimeType != "image/x-portable-anymap"
                    || (!supportedMimeTypes.contains("image/x-portable-bitmap")
                        && !supportedMimeTypes.contains("image/x-portable-graymap")
                        && !supportedMimeTypes.contains("image/x-portable-pixmap"))) {
                continue;
            }
        }
        fileList.append(m_fileModel->getGUri(index).toLocalPath());
    }
    m_fileList = std::move(fileList);
    updateTitle();

}
qint32 MainWindow::getCurrentFileIndex() const
{
    return m_fileList.indexOf(QFileInfo(m_currentFile).absoluteFilePath());
}

void MainWindow::openImage()
{
    loadImage(QFileDialog::getOpenFileName(this, tr("Choose image file"), m_currentFile));
}

void MainWindow::loadImageFromDir(const qint32 step)
{
    qint32 currentIdx = getCurrentFileIndex();
    for (;;)
    {
        currentIdx += step;
        const QString nextFile = m_fileList.value(currentIdx);
        if (nextFile.isNull())
            break;
        if (loadImage(nextFile))
            break;
    }
}

bool MainWindow::loadImage(const QString &fileName, const bool reload)
{
    QFile f(fileName);
    if (f.exists() && f.open(QFile::ReadOnly))
    {
        const QByteArray data = f.readAll();
        f.close();
        if (m_imageView->loadImage(data, reload, fileName))
        {
            disconnect(m_nextAfterDeleteConn);
            m_metadataWidget->loadFromData(data, fileName, m_imageView);
            if (!reload)
            {
                m_currentFile = fileName;
                m_fileModel->setRootPath(QFileInfo(m_currentFile).absolutePath());
            }
            return true;
        }
    }
    return false;
}

void MainWindow::loadFirstImage()
{
    if (getCurrentFileIndex() != 0)
        loadImage(m_fileList.value(0));
}
void MainWindow::loadLastImage()
{
    const qint32 last = m_fileList.count() - 1;
    if (getCurrentFileIndex() != last)
        loadImage(m_fileList.value(last));
}

void MainWindow::updateTitle()
{
    const QString fileName = QFileInfo(m_currentFile).fileName();
    const qint32 currentIdx = getCurrentFileIndex();
    if (fileName.isEmpty())
    {
        setWindowTitle(QCoreApplication::applicationName());
    }
    else if (m_fileList.isEmpty())
    {
        setWindowTitle(QString("%1 - %2%")
                       .arg(fileName)
                       .arg(m_currentScale));
    }
    else
    {
        setWindowTitle(QString("%1 - %2/%3 - %4%")
                       .arg(fileName)
                       .arg(currentIdx + 1)
                       .arg(m_fileList.count())
                       .arg(m_currentScale));
    }
}

void MainWindow::toggleFullScreen()
{
    if (isFullScreen())
    {
        setPalette(QGuiApplication::palette());
        m_imageView->fullScreenMode(false);
        showNormal();
        if (m_maximized)
            showMaximized();
        m_metadataDock->show();
        m_toolbar->show();
    }
    else
    {
        setPalette(Qt::black);
        m_imageView->fullScreenMode(true);
        m_maximized = isMaximized();
        m_metadataDock->hide();
        m_toolbar->hide();
        showFullScreen();
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape && m_fullScreenAct->isChecked())
        m_fullScreenAct->trigger();
    QMainWindow::keyPressEvent(event);
}

void MainWindow::contextMenu(const QPoint &pos)
{
    if (m_imageView->isMousePressedForScrollOrIsDragActive())
        return;
    m_popupMenu->popup(mapToGlobal(pos));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (!isFullScreen())
    {
        m_settings->setValue("window-geometry", saveGeometry());
        m_settings->setValue("dock-widget-state", saveState());
    }
    QMainWindow::closeEvent(event);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->ignore();
    if (const QMimeData *mimeData = event->mimeData())
    {
        if (mimeData->hasUrls() && mimeData->urls().count() == 1)
            event->accept();
    }
}
void MainWindow::dropEvent(QDropEvent *event)
{
    event->ignore();

    if (m_imageView->isMousePressedForScrollOrIsDragActive())
        return;

    if (const QMimeData *mimeData = event->mimeData())
    {
        const QUrl url = mimeData->urls().value(0);
        if (url.scheme() != "file")
            return;

        if (!loadImage(url.toLocalFile()))
            return;

        event->accept();
    }
}

}
