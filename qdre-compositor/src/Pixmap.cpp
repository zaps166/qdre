/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <Pixmap.hpp>
#include <CompositorInstance.hpp>
#include <X.hpp>

#include <QDebug>

namespace QDRE {

using namespace CompositorInstance;

Pixmap::Pixmap()
{}
Pixmap::~Pixmap()
{
    destroy();
}

void Pixmap::create(bool allowGLX)
{
    if (!m_pix)
    {
        m_pix = xcb_generate_id(g.conn);
        xcb_create_pixmap(g.conn, g.rootDepth, m_pix, g.root, g.rootWidth, g.rootHeight);
    }

    if (!m_pic)
    {
        m_pic = xcb_generate_id(g.conn);
        xcb_render_create_picture(g.conn, m_pic, m_pix, g.xcbRenderFindVisualFormat(g.rootVisual), 0, nullptr);
    }

    if (allowGLX && g.glxEnabled)
    {
        if (!m_glTex)
        {
            glGenTextures(1, &m_glTex);
            glBindTexture(GL_TEXTURE_RECTANGLE, m_glTex);
            glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, g.zoomBilinear ? GL_LINEAR : GL_NEAREST);
            glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, g.zoomBilinear ? GL_LINEAR : GL_NEAREST);
            glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }

        if (!m_glxPix)
        {
            const int pixmapAttribs[] = {
                GLX_TEXTURE_TARGET_EXT, GLX_TEXTURE_RECTANGLE_EXT,
                GLX_TEXTURE_FORMAT_EXT, GLX_TEXTURE_FORMAT_RGB_EXT,
                0
            };
            m_glxPix = glXCreatePixmap(g.dpy, g.fbConfig, m_pix, pixmapAttribs);
            g.glXBindTexImageEXT(g.dpy, m_glxPix, GLX_FRONT_EXT, nullptr);
        }

        if (!g.nvidiaProprietary && !m_fence)
        {
            m_fence = xcb_generate_id(g.conn);
            xcb_sync_create_fence(g.conn, m_pix, m_fence, false);
        }
    }
}

void Pixmap::setPictureZoomTransform(double zoom)
{
    if (!m_pic)
        return;

    xcb_render_transform_t renderTransform = {};
    renderTransform.matrix11 = 65536;
    renderTransform.matrix22 = 65536;
    renderTransform.matrix33 = zoom * 65536;
    xcb_render_set_picture_transform(g.conn, m_pic, renderTransform);
    if (g.zoomBilinear)
        xcb_render_set_picture_filter(g.conn, m_pic, 8, "bilinear", 0, nullptr);
}
void Pixmap::resetPictureZoomTransform()
{
    if (!m_pic)
        return;

    xcb_render_transform_t renderTransform = {};
    renderTransform.matrix11 = 65536;
    renderTransform.matrix22 = 65536;
    renderTransform.matrix33 = 65536;
    xcb_render_set_picture_transform(g.conn, m_pic, renderTransform);
    if (g.zoomBilinear)
        xcb_render_set_picture_filter(g.conn, m_pic, 7, "nearest", 0, nullptr);
}

void Pixmap::destroy()
{
    destroyGLX();

    if (m_pix)
    {
        xcb_free_pixmap(g.conn, m_pix);
        m_pix = 0;
    }
    if (m_pic)
    {
        xcb_render_free_picture(g.conn, m_pic);
        m_pic = 0;
    }
}
void Pixmap::destroyGLX()
{
    if (m_glxPix)
    {
        g.glXReleaseTexImageEXT(g.dpy, m_glxPix, GLX_FRONT_EXT);
        glXDestroyPixmap(g.dpy, m_glxPix);
        m_glxPix = 0;
    }
    if (m_glTex)
    {
        glBindTexture(GL_TEXTURE_RECTANGLE, 0);
        glDeleteTextures(1, &m_glTex);
        m_glTex = 0;
    }

    if (m_fence)
    {
        xcb_sync_destroy_fence(g.conn, m_fence);
        m_fence = 0;
    }
}

void Pixmap::sync()
{
    if (!m_fence)
    {
        if (g.nvidiaProprietary)
            glXWaitX();
        return;
    }

    xcb_sync_trigger_fence(g.conn, m_fence);
    X::xcbOk(xcb_sync_await_fence(g.conn, 1, &m_fence));
    xcb_sync_reset_fence(g.conn, m_fence);
}

}
