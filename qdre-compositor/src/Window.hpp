/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <xcb/xcb.h>
#include <xcb/render.h>
#include <xcb/damage.h>

#include <QRegion>

#include <functional>

namespace QDRE {

constexpr uint32_t g_opaque = 0xffffffff;

class Window
{
    Q_DISABLE_COPY(Window)

public:
    Window(xcb_window_t id, xcb_visualid_t visual, uint16_t winClass);
    ~Window();

    bool hasOpacity(bool real) const;

    void createPicture();
    void destroyPicture(bool resized = false);

    void updateBypassCompositorProp();
    bool updateOpacityProp();
    void updateTitleProp();

    void updateShapeInfo();

    void scheduleFetchWindowRegion();
    void scheduleRepair();

    void processCommands();

    xcb_window_t map();
    xcb_window_t unmap();

    void redirect(const char *logMsg);
    void unredirect(const char *logMsg);

    void doRxMatchTitle();

    bool isOffScreen() const;
    bool fillsEntireScreen() const;

private:
    void createDamage();
    void destroyDamage();

    void fetchWindowRegion();

    void destroyOpacityPicture();

private:
    static xcb_window_t findAppWindow(xcb_window_t id);

public:
    const xcb_window_t id;

    xcb_render_picture_t picture = 0;

    uint8_t overrideRedirect = 0; // Currently unused

    int16_t x = 0;
    int16_t y = 0;
    uint16_t w = 0;
    uint16_t h = 0;

    uint16_t borderWidth = 0;

    QRegion windowRegion;

    bool isMapped = false;

    int bypassCompositor = 0;

    QString title;

    bool hasAlphaChannel = false;
    uint32_t opacityValue = g_opaque;
    xcb_render_picture_t opacityPict = 0;

    bool isShaped = false;

    bool isRedirected = true; // All windows are automatically redirected

    bool forceExclusive = false;
    bool preventExclusive = false;
    bool preventWindowTransparency = false;

    struct
    {
        bool map = false;
        bool unmap = false;
        bool newRegion = false;
        bool repair = false;
        bool repaint = false;
    } commands;

    // Prepare paint helpers
    std::function<QRegion()> finishRegionCommand;
    QRegion firstRepaintRegion;

    // Paint helpers
    QRegion visibleRegion;
    QRegion opacityWindowPaintRegion;

private:
    const bool m_isIO;
    xcb_render_pictformat_t m_pictFormat = 0;
    xcb_window_t m_appWinId = 0;
#ifdef USE_NAME_WINDOW_PIXMAP
    xcb_pixmap_t m_pixmap = 0;
#endif
    xcb_damage_damage_t m_damage = 0;
};

}
