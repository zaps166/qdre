﻿/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <Compositor.hpp>

#include <InitializeCore.hpp>
#include <X.hpp>

#include <QGuiApplication>
#include <QDebug>

using namespace QDRE;

namespace QDRE::CompositorInstance {
    Compositor::Globals *g = nullptr;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QGuiApplication::setAttribute(Qt::AA_CompressHighFrequencyEvents, false);

    QGuiApplication::setApplicationName("compositor");

    initializeCore();

    X::initialize();

    static auto c = new Compositor;
    qAddPostRoutine([] {
        delete c;
        CompositorInstance::g = nullptr;
    });

    CompositorInstance::g = &c->g;

    if (!c->init())
        return -1;

    app.installNativeEventFilter(c);

    return app.exec();
}
