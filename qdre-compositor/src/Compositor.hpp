/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <xcb/xcb.h>
#include <xcb/render.h>
#include <xcb/xfixes.h>
#include <xcb/damage.h>
#include <xcb/present.h>
#include <xcb/xcb_ewmh.h>

#include <GLX.h>
#include <Pixmap.hpp>

#include <QAbstractNativeEventFilter>
#include <QRegularExpression>
#include <QRegion>

#include <unordered_map>
#include <functional>
#include <memory>
#include <vector>
#include <deque>

class QTimer;

namespace QDRE {

class GSettings;
class Window;
class Pixmap;

class Compositor final : public QObject, public QAbstractNativeEventFilter
{
    using WindowPtr = std::shared_ptr<Window>;
    using WindowList = std::deque<WindowPtr>;
    using WindowMap = std::unordered_map<xcb_window_t, WindowPtr>;

    enum class VSyncMode
    {
        Off,
        Auto,
        OpenGL,
        Present,
    };
    enum class ExclusiveFullScreenMode
    {
        DoNotAllow,
        AllowBypassCompositorWindows,
        AllowAllWindows,
    };
    enum class RepaintTimerMode
    {
        Off,
        Auto,
        On,
    };

public:
    Compositor();
    ~Compositor();

    bool init();

private:
    bool createGLXContext();
    void setGlViewport();
    void destroyGLXContext();

    bool enablePresent();
    void disablePresent();
    void makePresentUnusable();

    void getRefreshRateInfo(double &primaryRefreshRate, bool &refreshRateVaries);
    void getRefreshIntervalAndManageVSync();

    void updateRepaintTimerState();

    void updateRootWallpaperPicture();
    void destroyRootWallpaperPicture();

    void clearPrevRepaintRegions();
    void clearPixmaps();

    void scheduleRepaint();

    WindowList::iterator findWinIt(xcb_window_t id);
    WindowPtr findWin(xcb_window_t id);

    void preparePaintingWindows();
    bool paintWindows(const QRegion &inRegion, const QRegion &remainingRootRegion, Pixmap &pixmap);

    void grabZoomButtons(uint16_t zoomModMask);

    void maybeStartZoomRectTimer(bool onlyIfActive);
    void updateZoomRect();

    void updateCursorPicture();
    void freeCursorPicture();

    void doRxMatchTitle();

private:
    void restackWin(WindowList::iterator it, xcb_window_t newAbove);

    void handleButtonPress(xcb_button_press_event_t &e);
    void handleAddWin(xcb_window_t id);
    void handleConfigureWin(xcb_configure_notify_event_t &e);
    void handleDestroyWin(xcb_window_t id);
    void handleMapWin(xcb_window_t id);
    void handleUnmapWin(xcb_window_t id);
    void handleDamageWin(xcb_damage_notify_event_t &e);
    void handleShapeWin(xcb_shape_notify_event_t &e);
    void handleReparentWin(xcb_reparent_notify_event_t &e);
    void handleCirculateWin(xcb_circulate_notify_event_t &e);
    void handlePropertyNotify(xcb_property_notify_event_t &e);
    void handleGenericNotify(xcb_ge_generic_event_t &e);
    void handlePresentCompleteNotify(xcb_present_complete_notify_event_t &e);

    void finishPresentCompleteNotify();

    void handleExclusiveWindow(bool canUnredirectNow);

private:
    bool registerCM();

    bool initGLX();

    inline QRect getRootRect();
    inline QRect getCursorPixmapRect();

    std::pair<xcb_render_pictformat_t, uint8_t> findRenderFormat(const std::function<bool(const xcb_render_directformat_t &)> &callback);

    xcb_render_picture_t createSolidAlphaPixel(uint16_t alpha);

    xcb_xfixes_region_t qRegionToXcbRegion(xcb_xfixes_region_t region, const QRegion &qRegion);

private:
    bool nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result) override;

private:
    GSettings *const m_settings;

    bool m_initialized = false;
    bool m_glxInitialized = false;

    xcb_window_t m_selectionOwnerWin = 0;

    bool m_hasSyncExtension = false;
    bool m_hasRandrExtension = false;
    bool m_hasPresentExtension = false;

    VSyncMode m_vSyncMode = VSyncMode::Off;
    ExclusiveFullScreenMode m_exclusiveFullScreenMode = ExclusiveFullScreenMode::AllowBypassCompositorWindows;
    RepaintTimerMode m_repaintTimerMode = RepaintTimerMode::Off;
    bool m_canUnredirectInvisible = false;
    bool m_repaintTimerEnabled = false;
    bool m_repaintTimerWasSet = false;
    bool m_disableVSyncVariousRR = false;

    uint8_t m_damageFirstEvent = 0;
    uint8_t m_shapeFirstEvent = 0;
    uint8_t m_fixesFirstEvent = 0;
    uint8_t m_randrFirstEvent = 0;

    uint8_t m_renderFirstError = 0;
    uint8_t m_xfixesFirstError = 0;
    uint8_t m_damageFirstError = 0;

    uint8_t m_presentMajorOpcode = 0;
    xcb_present_event_t m_presentEventId = 0;

    QTimer *const m_exclusiveWindowTimer;
    xcb_window_t m_overlayWin = 0;
    xcb_window_t m_exclusiveWindow = 0;

    WindowList m_windowList; // All windows with correct stacking
    WindowMap m_windowHash; // All windows for quick lookup when window stacking doesn't matter
    WindowMap m_appWindowHash; // Mapped window for property notifies

    xcb_render_pictformat_t m_rootPictFmt = 0;
    xcb_render_picture_t m_rootPicture = 0;
    std::deque<Pixmap> m_pixmaps;
    size_t m_currPixmapIdx = 0;

    xcb_render_picture_t m_rootWallpaperPicture = 0;
    xcb_atom_t m_xRootPMapId = 0;

    QRegion m_globalRegionToRepaint;

    std::deque<QRegion> m_prevRepaintRegions;

    bool m_presentInProgress = false;
    bool m_presentEnabled = false;
    bool m_presentPending = false;
    bool m_presentUnusable = false;
    bool m_presentNoFlippingReported = false;
    uint32_t m_presentFlippedFrames = 0;
    uint32_t m_presentCopiedFrames = 0;
    uint32_t m_presentSerial = 0;
    QTimer *const m_presentTimeoutTimer;
    bool m_usePresentTimeout = false;

    void(*glxSwapInterval)(unsigned) = nullptr;
    GLXWindow m_glxWin = 0;
    unsigned m_bufferAge = 0;

    QTimer *const m_getRefreshIntervalTimer;
    double m_interval = 0.0;
    bool m_repaintScheduled = false;
    int m_timerFd = -1;
    double m_timePoint = 0.0;
    double m_newTimePoint = 0.0;
    double m_sleepTime = 0.0;

    std::deque<Pixmap> m_presentZoomedPixmaps;
    xcb_render_picture_t m_cursorPicture = 0;
    QTimer *const m_updateZoomRectTimer;
    uint16_t m_zoomInModMask = 0;
    uint16_t m_zoomOutModMask = 0;
    int16_t m_cursorX = 0;
    int16_t m_cursorY = 0;
    int16_t m_cursorHotX = 0;
    int16_t m_cursorHotY = 0;
    uint16_t m_cursorW = 0;
    uint16_t m_cursorH = 0;
    double m_zoomStep = 1.0;
    double m_zoom = 1.0;
    QRect m_zoomRect;

public:
    class Globals
    {
    public:
        void xcbDiscard(const xcb_void_cookie_t &cookie);

        QRegion qRegionFromXcbCookie(xcb_xfixes_fetch_region_cookie_t cookie);

        xcb_void_cookie_t xcbSelectInput(xcb_window_t id, uint32_t mask);
        xcb_void_cookie_t xcbSelectInputChecked(xcb_window_t id, uint32_t mask);

        xcb_render_picture_t createDrawablePicture(xcb_drawable_t id, xcb_render_pictformat_t format);

        xcb_render_pictformat_t xcbRenderFindVisualFormat(xcb_visualid_t visual, bool *hasAlpha = nullptr);

    public:
        xcb_connection_t *conn = nullptr;
        Display *dpy = nullptr;
        int screenNo = 0;

        xcb_ewmh_connection_t ewmh = {};

        const char *glxExtensions = nullptr;
        GLXFBConfig fbConfig = nullptr;
        PFNGLXBINDTEXIMAGEEXTPROC glXBindTexImageEXT = nullptr;
        PFNGLXRELEASETEXIMAGEEXTPROC glXReleaseTexImageEXT = nullptr;
        bool glxContextError = false;
        bool glxEnabled = false;
        bool nvidiaProprietary = false;

        bool zoomBilinear = false;

        xcb_window_t root = 0;
        xcb_visualid_t rootVisual = 0;
        uint8_t rootDepth = 0;
        uint16_t rootWidth = 0;
        uint16_t rootHeight = 0;

        xcb_atom_t wmName = 0;
        xcb_atom_t wmBypassCompositor = 0;
        xcb_atom_t wmWindowOpacity = 0;

        std::unordered_map<xcb_window_t, uint32_t> appWinRefs;

        std::vector<QRegularExpression> forceExclusiveFullScreen;
        std::vector<QRegularExpression> preventExclusiveFullScreen;
        std::vector<QRegularExpression> preventWindowTransparency;
    } g;
};

}
