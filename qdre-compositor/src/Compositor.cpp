/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <xcb/xcb_renderutil.h>
#include <xcb/composite.h>
#include <xcb/randr.h>
#include <xcb/sync.h>

#include <sys/timerfd.h>
#include <unistd.h>

#include <Compositor.hpp>
#include <Window.hpp>

#include <GSettings.hpp>
#include <X.hpp>

#include <QtGui/private/qtx11extras_p.h>
#include <QProcessEnvironment>
#include <QLoggingCategory>
#include <QCoreApplication>
#include <QSocketNotifier>
#include <QTransform>
#include <QTimer>

#include <type_traits>
#include <cstring>

namespace QDRE {

using namespace std;

constexpr auto g_exclusiveWindowTimerPresentCompleteInterval = 10;
constexpr auto g_presentTimeout = 100;

Compositor::Compositor()
    : m_settings(new GSettings(this))
    , m_exclusiveWindowTimer(new QTimer(this))
    , m_presentTimeoutTimer(new QTimer(this))
    , m_getRefreshIntervalTimer(new QTimer(this))
    , m_timerFd(timerfd_create(CLOCK_MONOTONIC, 0))
    , m_updateZoomRectTimer(new QTimer(this))
{
    g.conn = X::getConnection();
    g.dpy = QX11Info::display();
    g.screenNo = QX11Info::appScreen();
    m_pixmaps.resize(2);
}
Compositor::~Compositor()
{
    destroyGLXContext();
    disablePresent();

    destroyRootWallpaperPicture();

    m_appWindowHash.clear();
    m_windowList.clear();
    m_windowHash.clear();

    m_pixmaps.clear();
    m_presentZoomedPixmaps.clear();
    xcb_render_free_picture(g.conn, m_rootPicture);

    g.xcbSelectInput(g.root, 0);
    xcb_composite_unredirect_subwindows(g.conn, g.root, XCB_COMPOSITE_REDIRECT_MANUAL);

    if (m_overlayWin)
        xcb_composite_release_overlay_window(g.conn, m_overlayWin);

    if (m_selectionOwnerWin)
        xcb_destroy_window(g.conn, m_selectionOwnerWin);

    xcb_ewmh_connection_wipe(&g.ewmh);

    xcb_flush(g.conn);

    qDebug() << "Compositor finished";
}

bool Compositor::init()
{
    qDebug() << "Initializing compositor";

    if (!g.conn)
    {
        qCritical() << "No xcb connection";
        return false;
    }

    if (xcb_ewmh_init_atoms_replies(&g.ewmh, xcb_ewmh_init_atoms(g.conn, &g.ewmh), nullptr) != 1)
    {
        qCritical() << "Can't initialize EWMH";
        return false;
    }

    if (m_timerFd < 0)
    {
        qCritical() << "Can't create timer";
        return false;
    }

    const auto screen = X::getXcbScreen(g.screenNo);
    if (!screen)
    {
        qCritical() << "Can't get XCB screen info";
        return false;
    }

    const auto checkExtension = [this](xcb_extension_t &extension, uint8_t *firstError = nullptr, uint8_t *firstEvent = nullptr, uint8_t *majorOpcode = nullptr) {
        auto reply = xcb_get_extension_data(g.conn, &extension);
        if (reply && reply->present)
        {
            if (firstError)
                *firstError = reply->first_error;
            if (firstEvent)
                *firstEvent = reply->first_event;
            if (majorOpcode)
                *majorOpcode = reply->major_opcode;
            return true;
        }
        if (firstError)
            *firstError = 0;
        if (firstEvent)
            *firstEvent = 0;
        if (majorOpcode)
            *majorOpcode = 0;
        return false;
    };

    if (!checkExtension(xcb_composite_id))
    {
        qCritical() << "Composite extension is not available";
        return false;
    }
    if (!checkExtension(xcb_render_id, &m_renderFirstError))
    {
        qCritical() << "Render extension is not available";
        return false;
    }
    if (!checkExtension(xcb_xfixes_id, &m_xfixesFirstError, &m_fixesFirstEvent))
    {
        qCritical() << "XFixes extension is not available";
        return false;
    }
    if (!checkExtension(xcb_damage_id, &m_damageFirstError, &m_damageFirstEvent))
    {
        qCritical() << "Damage extension is not available";
        return false;
    }
    if (!checkExtension(xcb_shape_id, nullptr, &m_shapeFirstEvent))
    {
        qCritical() << "Shape extension is not available";
        return false;
    }
    m_hasSyncExtension = checkExtension(xcb_sync_id); // Sync is used only by OpenGL and it is required for it
    m_hasRandrExtension = checkExtension(xcb_randr_id, nullptr, &m_randrFirstEvent); // RandR is required for OpenGL
    m_hasPresentExtension = checkExtension(xcb_present_id, nullptr, nullptr, &m_presentMajorOpcode);

    if (auto reply = XCB_CALL_UNCHECKED(xcb_composite_query_version, g.conn, XCB_COMPOSITE_MAJOR_VERSION, XCB_COMPOSITE_MINOR_VERSION))
    {
        if (reply->major_version == 0 && reply->minor_version < 3)
        {
            qCritical() << "Too old composite extension";
            return false;
        }
    }

    xcb_render_query_version_unchecked(g.conn, XCB_RENDER_MAJOR_VERSION, XCB_RENDER_MINOR_VERSION);
    xcb_xfixes_query_version_unchecked(g.conn, XCB_XFIXES_MAJOR_VERSION, XCB_XFIXES_MINOR_VERSION);
    xcb_damage_query_version_unchecked(g.conn, XCB_DAMAGE_MAJOR_VERSION, XCB_DAMAGE_MINOR_VERSION);
    xcb_shape_query_version_unchecked(g.conn);
    if (m_hasSyncExtension)
        xcb_sync_initialize(g.conn, XCB_SYNC_MAJOR_VERSION, XCB_SYNC_MINOR_VERSION);
    if (m_hasRandrExtension)
        xcb_randr_query_version_unchecked(g.conn, XCB_RANDR_MAJOR_VERSION, XCB_RANDR_MINOR_VERSION);
    else
        qWarning() << "RandR extension not available";
    if (m_hasPresentExtension)
        xcb_present_query_version_unchecked(g.conn, XCB_PRESENT_MAJOR_VERSION, XCB_PRESENT_MINOR_VERSION);

    g.root = screen->root;
    g.rootVisual = screen->root_visual;
    g.rootDepth = screen->root_depth;
    g.rootWidth = screen->width_in_pixels;
    g.rootHeight = screen->height_in_pixels;

    m_xRootPMapId = X::getAtom(X::_XROOTPMAP_ID);
    g.wmName = X::getAtom(X::WM_NAME);
    g.wmBypassCompositor = getAtom(X::_NET_WM_BYPASS_COMPOSITOR);
    g.wmWindowOpacity = getAtom(X::_NET_WM_WINDOW_OPACITY);

    if (auto reply = XCB_CALL_UNCHECKED(xcb_composite_get_overlay_window, g.conn, g.root); reply && reply->overlay_win)
    {
        m_overlayWin = reply->overlay_win;

        auto region = xcb_generate_id(g.conn);
        xcb_xfixes_create_region(g.conn, region, 0, nullptr);
        xcb_xfixes_set_window_shape_region(g.conn, m_overlayWin, XCB_SHAPE_SK_BOUNDING, 0, 0, 0);
        xcb_xfixes_set_window_shape_region(g.conn, m_overlayWin, XCB_SHAPE_SK_INPUT, 0, 0, region);
        xcb_xfixes_destroy_region(g.conn, region);
    }
    else
    {
        qCritical() << "Can't get overlay window";
        return false;
    }

    if (!registerCM())
        return false;

    qDebug() << "BadPictureFormat error code:" << m_renderFirstError + XCB_RENDER_PICT_FORMAT;
    qDebug() << "BadPicture error code:" << m_renderFirstError + XCB_RENDER_PICTURE;
    qDebug() << "BadPictureOp error code:" << m_renderFirstError + XCB_RENDER_PICT_OP;
    qDebug() << "BadRegion error code:" << m_xfixesFirstError + XCB_XFIXES_BAD_REGION;
    qDebug() << "BadDamage error code:" << m_damageFirstError + XCB_DAMAGE_BAD_DAMAGE;

    m_rootPictFmt = g.xcbRenderFindVisualFormat(g.rootVisual);
    m_rootPicture = g.createDrawablePicture(g.root, m_rootPictFmt);

    xcb_composite_redirect_subwindows(g.conn, g.root, XCB_COMPOSITE_REDIRECT_MANUAL);
    g.xcbSelectInput(g.root, XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_PROPERTY_CHANGE);

    xcb_xfixes_select_cursor_input(g.conn, g.root, XCB_XFIXES_CURSOR_NOTIFY_MASK_DISPLAY_CURSOR);

    if (m_hasPresentExtension)
    {
        m_presentEventId = xcb_generate_id(g.conn);
        xcb_present_select_input(g.conn, m_presentEventId, m_overlayWin, XCB_PRESENT_EVENT_MASK_COMPLETE_NOTIFY);
    }

    m_exclusiveWindowTimer->setSingleShot(true);
    connect(m_exclusiveWindowTimer, &QTimer::timeout,
            this, [this] {
        Q_ASSERT(!m_exclusiveWindowTimer->isActive());
        handleExclusiveWindow(true);
    });

    m_presentTimeoutTimer->setSingleShot(true);
    m_presentTimeoutTimer->setInterval(g_presentTimeout);
    connect(m_presentTimeoutTimer, &QTimer::timeout,
            this, [this] {
        Q_ASSERT(!m_presentTimeoutTimer->isActive());
        Q_ASSERT(m_presentEnabled);
        qWarning() << "Present timeout";
        m_presentInProgress = false;
        finishPresentCompleteNotify();
    });

    m_getRefreshIntervalTimer->setSingleShot(true);
    connect(m_getRefreshIntervalTimer, &QTimer::timeout,
            this, &Compositor::getRefreshIntervalAndManageVSync);

    connect(m_updateZoomRectTimer, &QTimer::timeout,
            this, &Compositor::updateZoomRect);

    m_settings->installSettingsChangedFunction("vsync", [this](const QString &key) {
        m_vSyncMode = static_cast<VSyncMode>(m_settings->getEnum(key));
        m_presentUnusable = false; // Let give another chance for Present
        if (m_vSyncMode == VSyncMode::OpenGL && !initGLX())
        {
            qCritical() << "OpenGL is not available";
        }
        else if (m_vSyncMode == VSyncMode::Present && !m_hasPresentExtension)
        {
            qCritical() << "Present extension is not available";
        }
        getRefreshIntervalAndManageVSync();
    });
    m_settings->installSettingsChangedFunction("exclusive-full-screen", [this](const QString &key) {
        m_exclusiveFullScreenMode = static_cast<ExclusiveFullScreenMode>(m_settings->getEnum(key));
    });
    m_settings->installSettingsChangedFunction("unredirect-invisible", [this](const QString &key) {
        m_canUnredirectInvisible = m_settings->getBool(key);
    });
    m_settings->installSettingsChangedFunction("enable-repaint-timer", [this](const QString &key) {
        m_repaintTimerMode = static_cast<RepaintTimerMode>(m_settings->getEnum(key));
        updateRepaintTimerState();
    });
    m_settings->installSettingsChangedFunction("disable-vsync-various-rr", [this](const QString &key) {
        m_disableVSyncVariousRR = m_settings->getBool(key);
        getRefreshIntervalAndManageVSync();
    });
    m_settings->installSettingsChangedFunction("zoom-step-size", [this](const QString &key) {
        m_zoomStep = 1.0 + qBound(1, m_settings->getUInt(key), 100) / 200.0;
    });
    m_settings->installSettingsChangedFunction("zoom-key", [this](const QString &key) {
        grabZoomButtons(m_settings->getEnum(key));
    });
    m_settings->installSettingsChangedFunction("zoom-bilinear", [this](const QString &key) {
        const bool zoomBilinear = m_settings->getBool(key);
        if (g.zoomBilinear != zoomBilinear)
        {
            g.zoomBilinear = zoomBilinear;
            if (m_initialized)
                clearPixmaps();
        }
    });
    m_settings->installSettingsChangedFunction("exclusive-full-screen-force-exceptions", [this](const QString &key) {
        const auto list = m_settings->getStringArray(key);
        g.forceExclusiveFullScreen.clear();
        for (auto &&t : list)
        {
            if (!t.trimmed().isEmpty())
                g.forceExclusiveFullScreen.push_back(QRegularExpression(t));
        }
        doRxMatchTitle();
    });
    m_settings->installSettingsChangedFunction("exclusive-full-screen-prevent-exceptions", [this](const QString &key) {
        const auto list = m_settings->getStringArray(key);
        g.preventExclusiveFullScreen.clear();
        for (auto &&t : list)
        {
            if (!t.trimmed().isEmpty())
                g.preventExclusiveFullScreen.push_back(QRegularExpression(t));
        }
        doRxMatchTitle();
    });
    m_settings->installSettingsChangedFunction("prevent-window-transparency-exceptions", [this](const QString &key) {
        const auto list = m_settings->getStringArray(key);
        g.preventWindowTransparency.clear();
        for (auto &&t : list)
        {
            if (!t.trimmed().isEmpty())
                g.preventWindowTransparency.push_back(QRegularExpression(t));
        }
        doRxMatchTitle();
    });
    m_settings->triggerInstalledValues();

    m_initialized = true;
    getRefreshIntervalAndManageVSync();

    X::queryTreeIterate(g.root, [this](xcb_window_t id) {
        handleAddWin(id);
        return false;
    });
    handleExclusiveWindow(true);

    const auto notifier = new QSocketNotifier(m_timerFd, QSocketNotifier::Read, this);
    connect(notifier, &QSocketNotifier::activated,
            this, [this] {
        uint64_t val;
        read(m_timerFd, &val, sizeof(val));
        preparePaintingWindows();
    });

    return true;
}

bool Compositor::createGLXContext()
{
    if (!g.fbConfig || g.glxContextError)
        return false;

    g.glxEnabled = true;

    auto glxFailed = [this] {
        g.glxContextError = true;
        destroyGLXContext();
        qWarning() << "OpenGL disabled";
        return false;
    };

    m_glxWin = glXCreateWindow(g.dpy, g.fbConfig, m_overlayWin, nullptr);
    if (!m_glxWin)
    {
        qWarning() << "Can't create GLX window";
        return glxFailed();
    }

    const auto context = glXCreateNewContext(g.dpy, g.fbConfig, GLX_RGBA_TYPE, nullptr, true);
    if (!context)
    {
        qWarning() << "Can't create GLX context";
        return glxFailed();
    }

    if (!glXMakeContextCurrent(g.dpy, m_glxWin, m_glxWin, context))
    {
        qWarning() << "Can't make GLX context current";
        return glxFailed();
    }

    const auto glRenderer = reinterpret_cast<const char *>(glGetString(GL_RENDERER));
    if (!glRenderer)
    {
        qWarning() << "Can't get OpenGL renderer";
        return glxFailed();
    }

    const char *const blacklistedRenderers[] = {
        "llvmpipe",
        "Software Rasterizer",
        "swrast",
        "Chromium",
        "Mesa X11",
    };
    for (auto &&blacklistedRenderer : blacklistedRenderers)
    {
        if (strcasestr(glRenderer, blacklistedRenderer))
        {
            qWarning() << "Found blacklisted OpenGL renderer:" << glRenderer;
            return glxFailed();
        }
    }

    if (const auto glVendor = reinterpret_cast<const char *>(glGetString(GL_VENDOR)))
    {
        if (strcasestr(glVendor, "NVIDIA Corporation"))
        {
            g.nvidiaProprietary = true;
            qInfo() << "NVIDIA proprietary driver detected";
        }
    }

    glxSwapInterval(1);

    glDisable(GL_DITHER);
    glDepthMask(GL_FALSE);
    glEnable(GL_TEXTURE_RECTANGLE);

    setGlViewport();

    m_globalRegionToRepaint = getRootRect();
    scheduleRepaint();

    return true;
}
void Compositor::setGlViewport()
{
    glViewport(0, 0, g.rootWidth, g.rootHeight);

    glLoadIdentity();
    glTranslatef(-1.0f, 1.0f, 0.0f);
    glScalef(2.0f / g.rootWidth, -2.0f / g.rootHeight, 1.0f);
}
void Compositor::destroyGLXContext()
{
    if (!g.glxEnabled)
        return;

    clearPrevRepaintRegions();

    for (auto &&pixmap : m_pixmaps)
        pixmap.destroyGLX();

    if (auto context = glXGetCurrentContext())
    {
        glXMakeContextCurrent(g.dpy, 0, 0, 0);
        glXDestroyContext(g.dpy, context);
    }

    if (m_glxWin)
    {
        glXDestroyWindow(g.dpy, m_glxWin);
        m_glxWin = 0;
    }

    g.glxEnabled = false;
    g.nvidiaProprietary = false;
    m_globalRegionToRepaint = getRootRect();
    scheduleRepaint();
}

bool Compositor::enablePresent()
{
    if (!m_hasPresentExtension)
        return false;

    m_presentFlippedFrames = 0;
    m_presentCopiedFrames = 0;
    m_presentEnabled = true;
    m_globalRegionToRepaint = getRootRect();
    scheduleRepaint();

    return true;
}
void Compositor::disablePresent()
{
    if (!m_presentEnabled)
        return;

    clearPrevRepaintRegions();

    for (size_t i = 0; i < m_pixmaps.size(); ++i)
    {
        if (i != m_currPixmapIdx)
            m_pixmaps[i].destroy();
    }
    m_presentZoomedPixmaps.clear();

    m_presentEnabled = false;
    m_presentNoFlippingReported = false;
    m_presentTimeoutTimer->stop();
    m_usePresentTimeout = false;
    m_globalRegionToRepaint = getRootRect();
    scheduleRepaint();
}
void Compositor::makePresentUnusable()
{
    disablePresent();
    m_presentUnusable = true;
    m_getRefreshIntervalTimer->start(0); // Maybe OpenGL can be used instead, so check it
}

void Compositor::getRefreshRateInfo(double &primaryRefreshRate, bool &refreshRateVaries)
{
    primaryRefreshRate = 0.0;
    refreshRateVaries = false;

    if (!m_hasRandrExtension)
        return;

    const auto screenResourcesReply = XCB_CALL_UNCHECKED(xcb_randr_get_screen_resources, g.conn, g.root);
    if (!screenResourcesReply)
        return;

    const auto modes = xcb_randr_get_screen_resources_modes(screenResourcesReply.get());
    const auto modesEnd = modes + xcb_randr_get_screen_resources_modes_length(screenResourcesReply.get());
    const auto getModeRefreshRate = [&](xcb_randr_mode_t mode) {
        const auto it = find_if(modes, modesEnd, [&](const xcb_randr_mode_info_t &modeInfo) {
            return (modeInfo.id == mode);
        });
        if (it == modesEnd)
            return 0.0;
        return static_cast<double>(it->dot_clock) / static_cast<double>(it->htotal * it->vtotal);
    };

    const int numCrtcs = xcb_randr_get_screen_resources_crtcs_length(screenResourcesReply.get());
    const auto crtcs = xcb_randr_get_screen_resources_crtcs(screenResourcesReply.get());

    vector<xcb_randr_get_crtc_info_cookie_t> crtcInfoCookies;
    crtcInfoCookies.reserve(numCrtcs);

    for (int i = 0; i < numCrtcs; ++i)
        crtcInfoCookies.push_back(xcb_randr_get_crtc_info_unchecked(g.conn, crtcs[i], 0));

    vector<pair<uint16_t, double>> crtcInfoArr;
    crtcInfoArr.reserve(numCrtcs);

    for (auto &&crtcInfoCookie : crtcInfoCookies)
    {
        const auto crtcInfoReply = managePtr(xcb_randr_get_crtc_info_reply(g.conn, crtcInfoCookie, nullptr));
        crtcInfoArr.emplace_back(
            crtcInfoReply ? crtcInfoReply->num_outputs : 0,
            crtcInfoReply ? getModeRefreshRate(crtcInfoReply->mode) : 0.0
        );
    }

    int primaryCrtcIdx = -1;

    const auto primaryOutputReply = XCB_CALL_UNCHECKED(xcb_randr_get_output_primary, g.conn, g.root);
    if (primaryOutputReply && primaryOutputReply->output != 0)
    {
        const auto primaryOutputInfoReply = XCB_CALL_UNCHECKED(xcb_randr_get_output_info, g.conn, primaryOutputReply->output, 0);
        if (primaryOutputInfoReply && primaryOutputInfoReply->crtc != 0)
        {
            for (int i = 0; i < numCrtcs; ++i)
            {
                if (primaryOutputInfoReply->crtc == crtcs[i])
                {
                    primaryCrtcIdx = i;
                    break;
                }
            }
        }
    }

    if (primaryCrtcIdx < 0)
    {
        qWarning() << "Can't get primary output/crtc";
        for (int i = 0; i < numCrtcs; ++i)
        {
            if (crtcInfoArr[i].first > 0) // Has at least one output
            {
                primaryCrtcIdx = i;
                break;
            }
        }
        if (primaryCrtcIdx < 0)
        {
            qWarning() << "Can't get crtc with outputs";
            return;
        }
    }

    primaryRefreshRate = crtcInfoArr[primaryCrtcIdx].second;

    for (int i = 0; i < numCrtcs; ++i)
    {
        if (i == primaryCrtcIdx || crtcInfoArr[i].first == 0)
            continue;

        const auto refreshRateDiff = abs(primaryRefreshRate - crtcInfoArr[i].second);
        if (refreshRateDiff >= 0.1)
        {
            refreshRateVaries = true;
            break;
        }
    }
}
void Compositor::getRefreshIntervalAndManageVSync()
{
    if (!m_initialized)
        return;

    double primaryRefreshRate = 0.0;
    bool refreshRateVaries = false;
    getRefreshRateInfo(primaryRefreshRate, refreshRateVaries);

    if (primaryRefreshRate > 0.0)
    {
        const double newInterval = 1.0 / primaryRefreshRate;
        if (!qFuzzyCompare(m_interval, newInterval))
        {
            m_interval = 1.0 / primaryRefreshRate;
            qDebug() << "Refresh rate:" << primaryRefreshRate << "Hz";
            maybeStartZoomRectTimer(true);
        }
    }
    else
    {
        m_interval = 0.0;
        qWarning() << "Can't get screen refresh rate";
        maybeStartZoomRectTimer(true);
    }

    const bool vSyncEnabled = (m_vSyncMode != VSyncMode::Off);
    const bool canUseVSync = (vSyncEnabled && (!m_disableVSyncVariousRR || !refreshRateVaries));
    if (canUseVSync)
    {
        const bool autoDetect = (m_vSyncMode == VSyncMode::Auto);

        if ((m_vSyncMode == VSyncMode::Present || autoDetect) && !m_presentUnusable)
        {
            destroyGLXContext();
            if (!m_presentEnabled && enablePresent())
                qDebug() << "Enabled Present for VSync";
        }

        if (m_vSyncMode == VSyncMode::OpenGL || (autoDetect && !m_presentEnabled && initGLX()))
        {
            disablePresent();
            if (!g.glxEnabled && createGLXContext())
                qDebug() << "Enabled OpenGL for VSync";
        }
    }
    else
    {
        if (g.glxEnabled)
        {
            destroyGLXContext();
            qWarning() << "OpenGL disabled";
        }
        if (m_presentEnabled)
        {
            disablePresent();
            qWarning() << "Present disabled";
        }
        if (vSyncEnabled)
            qWarning() << "V-Sync disabled due to various refresh rates";
    }

    updateRepaintTimerState();
}

void Compositor::updateRepaintTimerState()
{
    if (!m_initialized)
        return;

    bool repaintTimerEnabled = m_repaintTimerEnabled;
    switch (m_repaintTimerMode)
    {
        case RepaintTimerMode::Off:
            repaintTimerEnabled = false;
            break;
        case RepaintTimerMode::Auto:
            repaintTimerEnabled = !m_presentEnabled;
            break;
        case RepaintTimerMode::On:
            repaintTimerEnabled = true;
            break;
    }

    if (m_repaintTimerEnabled == repaintTimerEnabled && m_repaintTimerWasSet)
        return;

    m_repaintTimerEnabled = repaintTimerEnabled;
    m_repaintTimerWasSet = true;

    qDebug() << "Repaint timer is" << (m_repaintTimerEnabled ? "enabled" : "disabled");
}

void Compositor::updateRootWallpaperPicture()
{
    if (m_rootWallpaperPicture)
        return;

    auto pixmap = X::getXcbPixmapForAtom(g.screenNo, m_xRootPMapId);
    bool fill = false;

    if (!pixmap)
    {
        pixmap = xcb_generate_id(g.conn);
        xcb_create_pixmap(g.conn, g.rootDepth, pixmap, g.root, 1, 1);
        fill = true;
    }

    const uint32_t value = true;
    m_rootWallpaperPicture = xcb_generate_id(g.conn);
    xcb_render_create_picture(g.conn, m_rootWallpaperPicture, pixmap, m_rootPictFmt, XCB_RENDER_CP_REPEAT, &value);

    if (fill)
    {
        xcb_render_color_t color = {};
        color.alpha = 0xffff;

        xcb_rectangle_t rect {0, 0, 1, 1};
        xcb_render_fill_rectangles(g.conn, XCB_RENDER_PICT_OP_SRC, m_rootWallpaperPicture, color, 1, &rect);

        xcb_free_pixmap(g.conn, pixmap);
    }
}
void Compositor::destroyRootWallpaperPicture()
{
    if (!m_rootWallpaperPicture)
        return;

    xcb_render_free_picture(g.conn, m_rootWallpaperPicture);
    m_rootWallpaperPicture = 0;
}

void Compositor::clearPrevRepaintRegions()
{
    m_prevRepaintRegions.clear();
    m_bufferAge = 0;
}
void Compositor::clearPixmaps()
{
    clearPrevRepaintRegions();
    for (auto &&pixmap : m_pixmaps)
        pixmap.destroy();
    m_presentZoomedPixmaps.clear();
    m_globalRegionToRepaint = getRootRect();
    scheduleRepaint();
}

void Compositor::scheduleRepaint()
{
    if (m_repaintScheduled || m_exclusiveWindow != 0)
        return;

    if (m_presentEnabled && m_presentInProgress)
    {
        m_presentPending = true;
        return;
    }
    m_presentPending = false;

    timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    m_newTimePoint = now.tv_sec + (now.tv_nsec / 1e9);

    if (m_repaintTimerEnabled)
    {
        m_sleepTime = m_interval - m_newTimePoint + m_timePoint;
        if (m_sleepTime > m_interval)
        {
            m_sleepTime = 0.0;
            qWarning() << "Sleep time > refresh interval";
        }
    }
    else
    {
        m_sleepTime = 0.0;
    }

    m_repaintScheduled = true;

    itimerspec its = {};
    if (m_sleepTime >= 2e-9)
    {
        its.it_value.tv_sec = static_cast<decltype(timespec::tv_sec)>(m_sleepTime);
        its.it_value.tv_nsec = static_cast<decltype(timespec::tv_nsec)>((m_sleepTime - its.it_value.tv_sec) * 1e9);
    }
    else
    {
        its.it_value.tv_nsec = 1;
    }
    timerfd_settime(m_timerFd, 0, &its, nullptr);
}

Compositor::WindowList::iterator Compositor::findWinIt(xcb_window_t id)
{
    if (id == 0)
        return m_windowList.end();
    return find_if(m_windowList.begin(), m_windowList.end(), [&](const WindowPtr &w) {
        return (w->id == id);
    });
}
Compositor::WindowPtr Compositor::findWin(xcb_window_t id)
{
    if (auto it = m_windowHash.find(id); it != m_windowHash.end())
        return it->second;
    return nullptr;
}

void Compositor::preparePaintingWindows()
{
    Q_ASSERT(!m_presentEnabled || !m_presentInProgress);

    m_repaintScheduled = false;

    if (m_exclusiveWindow != 0)
        return;

    for (auto &&w : m_windowList)
        w->processCommands();

    if (m_zoomRect.isValid())
        updateCursorPicture();

    auto regionToPaint = qMove(m_globalRegionToRepaint);

    QRegion remainingRootRegion = getRootRect();
    for (auto &&w : m_windowList)
    {
        QRegion secondRepaintRegion;
        if (w->finishRegionCommand)
        {
            secondRepaintRegion = w->finishRegionCommand();
            w->finishRegionCommand = nullptr;
        }

        if (w->isMapped)
        {
            w->visibleRegion = (w->windowRegion & remainingRootRegion);

            if (!w->hasOpacity(false))
                remainingRootRegion -= w->windowRegion;

            if (m_canUnredirectInvisible && w->visibleRegion.isNull())
                w->unredirect("invisible");
            else
                w->redirect("visible");
        }
        else if (!w->visibleRegion.isNull())
        {
            w->visibleRegion = QRegion();
        }

        regionToPaint |= w->firstRepaintRegion | (secondRepaintRegion & w->visibleRegion);

        if (!w->firstRepaintRegion.isNull())
            w->firstRepaintRegion = QRegion();
    }

    regionToPaint |= remainingRootRegion; // For root wallpaper
    if (m_zoomRect.isValid())
        regionToPaint &= m_zoomRect;

    if (!paintWindows(regionToPaint, remainingRootRegion, m_pixmaps[m_currPixmapIdx]))
        return;

    if (m_presentEnabled && ++m_currPixmapIdx >= m_pixmaps.size())
        m_currPixmapIdx = 0;

    m_timePoint = m_newTimePoint + max(0.0, m_sleepTime);
}
bool Compositor::paintWindows(const QRegion &inRegion, const QRegion &remainingRootRegion, Pixmap &pixmap)
{
    if (inRegion.isNull())
        return false;

    QRegion regionToPaint;

    if (m_presentEnabled)
    {
        if (m_prevRepaintRegions.size() >= m_pixmaps.size())
            m_prevRepaintRegions.erase(m_prevRepaintRegions.end() - 1);
        m_prevRepaintRegions.push_front(inRegion);

        if (m_prevRepaintRegions.size() == m_pixmaps.size())
        {
            for (auto &&prevDamage : m_prevRepaintRegions)
                regionToPaint |= prevDamage;
        }
        else
        {
            regionToPaint = getRootRect();
        }
    }
    else
    {
        regionToPaint = inRegion;
    }

    pixmap.create();

    auto xfixesRegion = xcb_generate_id(g.conn);
    xcb_xfixes_create_region(g.conn, xfixesRegion, 0, nullptr);

    bool painted = false;
    for (auto &&w : m_windowList)
    {
        if (!w->isRedirected || w->visibleRegion.isNull())
            continue;

        const auto windowPaintRegion = (w->visibleRegion & regionToPaint);
        if (windowPaintRegion.isNull())
            continue;

        w->createPicture();

        if (!w->hasOpacity(false))
        {
            xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, windowPaintRegion), 0, 0);
            xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_SRC, w->picture, 0, pixmap.picture(),
                                 0, 0, 0, 0,
                                 w->x, w->y, w->w + w->borderWidth * 2, w->h + w->borderWidth * 2);
        }
        else
        {
            w->opacityWindowPaintRegion = windowPaintRegion;
        }

        painted = true;
    }

    if (!remainingRootRegion.isNull())
    {
        updateRootWallpaperPicture();

        xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, remainingRootRegion), 0, 0);
        xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_SRC, m_rootWallpaperPicture, 0, pixmap.picture(),
                             0, 0, 0, 0,
                             0, 0, g.rootWidth, g.rootHeight);

        painted = true;
    }

    if (!painted)
    {
        xcb_xfixes_destroy_region(g.conn, xfixesRegion);
        return false;
    }

    for (auto it = m_windowList.rbegin(), itEnd = m_windowList.rend(); it != itEnd; ++it)
    {
        auto &w = *it;
        if (w->opacityWindowPaintRegion.isNull())
            continue;

        if (w->opacityValue != g_opaque && !w->opacityPict)
            w->opacityPict = createSolidAlphaPixel(w->opacityValue >> 16);

        xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, w->opacityWindowPaintRegion), 0, 0);
        xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_OVER, w->picture, w->opacityPict, pixmap.picture(),
                             0, 0, 0, 0,
                             w->x, w->y, w->w + w->borderWidth * 2, w->h + w->borderWidth * 2);

        w->opacityWindowPaintRegion = QRegion();
    }

    const bool zoomed = m_zoomRect.isValid();
    if (zoomed && m_cursorPicture)
    {
        const auto cursorPixmapRect = getCursorPixmapRect();
        xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, cursorPixmapRect), 0, 0);
        xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_OVER, m_cursorPicture, 0, pixmap.picture(),
                             0, 0, 0, 0,
                             cursorPixmapRect.x(), cursorPixmapRect.y(), cursorPixmapRect.width(), cursorPixmapRect.height());
    }

    if (g.glxEnabled)
    {
        if (m_prevRepaintRegions.size() >= 10)
            m_prevRepaintRegions.erase(m_prevRepaintRegions.end() - 1);
        m_prevRepaintRegions.push_front(inRegion);

        QRegion regionToDisplay;
        if (m_bufferAge > 0 && m_bufferAge <= m_prevRepaintRegions.size())
        {
            for (unsigned i = 0; i < m_bufferAge; ++i)
                regionToDisplay |= m_prevRepaintRegions[i];
        }
        else
        {
            regionToDisplay = getRootRect();
        }

        if (zoomed)
        {
            glPushMatrix();
            glScaled(m_zoom, m_zoom, 1.0);
            glTranslated(-m_zoomRect.left(), -m_zoomRect.top(), 0.0);

            if (g.zoomBilinear)
            {
                QRegion tmpRegionToDisplay;
                for (auto &&rect : regionToDisplay)
                    tmpRegionToDisplay |= rect.marginsAdded(QMargins(1, 1, 1, 1));
                regionToDisplay = qMove(tmpRegionToDisplay);
            }
        }

        pixmap.sync();

        for (auto &&rect : regionToDisplay)
        {
            glBegin(GL_QUADS);

            glTexCoord2i(rect.x(), rect.y());
            glVertex2i(rect.x(), rect.y());

            glTexCoord2i(rect.x() + rect.width(), rect.y());
            glVertex2i(rect.x() + rect.width(), rect.y());

            glTexCoord2i(rect.x() + rect.width(), rect.y() + rect.height());
            glVertex2i(rect.x() + rect.width(), rect.y() + rect.height());

            glTexCoord2i(rect.x(), rect.y() + rect.height());
            glVertex2i(rect.x(), rect.y() + rect.height());

            glEnd();
        }

        if (zoomed)
            glPopMatrix();

        glXSwapBuffers(g.dpy, m_glxWin);
        glXQueryDrawable(g.dpy, m_glxWin, GLX_BACK_BUFFER_AGE_EXT, &m_bufferAge);

        glXWaitGL();
    }
    else if (m_presentEnabled)
    {
        xcb_pixmap_t pixmapToPresent = 0;
        QRegion regionToDisplay;

        if (!zoomed)
        {
            pixmapToPresent = pixmap.pixmap();
            regionToDisplay = inRegion;
        }
        else
        {
            if (m_currPixmapIdx >= m_presentZoomedPixmaps.size())
                m_presentZoomedPixmaps.resize(m_currPixmapIdx + 1);
            auto &zoomedPixmap = m_presentZoomedPixmaps[m_currPixmapIdx];

            zoomedPixmap.create(false);
            pixmapToPresent = zoomedPixmap.pixmap();

            const int16_t srcX = qRound(m_zoomRect.left() * m_zoom);
            const int16_t srcY = qRound(m_zoomRect.top()  * m_zoom);

            auto transformMatrix = QTransform().scale(m_zoom, m_zoom);

            QRegion tmpRegion;
            for (auto &&rect : regionToPaint)
                tmpRegion |= transformMatrix.mapRect(rect.marginsAdded(QMargins(1, 1, 1, 1)));

            pixmap.setPictureZoomTransform(m_zoom);
            xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, tmpRegion), 0, 0);
            xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_SRC, pixmap.picture(), 0, zoomedPixmap.picture(),
                                 srcX, srcY, 0, 0,
                                 0, 0, g.rootWidth, g.rootHeight);
            pixmap.resetPictureZoomTransform();

            transformMatrix.translate(-m_zoomRect.left(), -m_zoomRect.top());
            for (auto &&rect : inRegion)
                regionToDisplay |= transformMatrix.mapRect(rect.marginsAdded(QMargins(1, 1, 1, 1)));
        }

        const bool ok = X::xcbOk(xcb_present_pixmap_checked(
            g.conn,
            m_overlayWin,
            pixmapToPresent,
            m_presentSerial++,
            0,
            qRegionToXcbRegion(xfixesRegion, regionToDisplay),
            0, 0,
            0,
            0,
            0,
            0,
            0,
            1,
            0,
            0,
            nullptr
        ));
        if (ok)
        {
            m_presentInProgress = true;
            if (m_usePresentTimeout)
            {
                m_usePresentTimeout = false;
                m_presentTimeoutTimer->start();
            }
        }
        else
        {
            // Something is wrong - disable Present and fallback to Render
            makePresentUnusable();
            qDebug() << "Present error, disabling";
        }
    }

    if (!g.glxEnabled && !m_presentEnabled)
    {
        int16_t srcX = 0;
        int16_t srcY = 0;

        if (!zoomed)
        {
            xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, inRegion), 0, 0);
        }
        else
        {
            srcX = qRound(m_zoomRect.left() * m_zoom);
            srcY = qRound(m_zoomRect.top()  * m_zoom);

            pixmap.setPictureZoomTransform(m_zoom);

            const auto transformMatrix = QTransform().scale(m_zoom, m_zoom);
            QRegion tmpInRegion;
            for (auto &&rect : inRegion)
                tmpInRegion |= transformMatrix.mapRect(rect.marginsAdded(QMargins(1, 1, 1, 1)));
            xcb_xfixes_set_picture_clip_region(g.conn, pixmap.picture(), qRegionToXcbRegion(xfixesRegion, tmpInRegion), 0, 0);
        }

        xcb_render_composite(g.conn, XCB_RENDER_PICT_OP_SRC, pixmap.picture(), 0, m_rootPicture,
                             srcX, srcY, 0, 0,
                             0, 0, g.rootWidth, g.rootHeight);

        if (zoomed)
            pixmap.resetPictureZoomTransform();

        xcb_flush(g.conn);
    }

    xcb_xfixes_destroy_region(g.conn, xfixesRegion);

    return true;
}

void Compositor::grabZoomButtons(uint16_t zoomModMask)
{
    auto callWithAdditionalModifiers = [](uint16_t modMask, auto &&fn) {
        fn(modMask);
        fn(modMask | XCB_MOD_MASK_LOCK);
        fn(modMask | XCB_MOD_MASK_2);
        fn(modMask | XCB_MOD_MASK_LOCK | XCB_MOD_MASK_2);
    };

    auto ungrabButton = [&](uint8_t button, uint16_t &modMask) {
        if (modMask != 0)
        {
            callWithAdditionalModifiers(modMask, [&](uint16_t modMask) {
                xcb_ungrab_button(g.conn, button, g.root, modMask);
            });
            modMask = 0;
        }
    };

    ungrabButton(XCB_BUTTON_INDEX_4, m_zoomInModMask);
    ungrabButton(XCB_BUTTON_INDEX_5, m_zoomOutModMask);

    if (zoomModMask == 0)
        return;

    auto grabButton = [&](uint8_t button, uint16_t &modMask) {
        bool ok = true;
        callWithAdditionalModifiers(zoomModMask, [&](uint16_t modMask) {
            ok &= X::xcbOk(xcb_grab_button_checked(g.conn, false, g.root, 0, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC, XCB_NONE, XCB_NONE, button, modMask));
        });
        if (!ok)
        {
            qWarning() << "Can't grab zoom button:" << button << "with a key modifier:" << modMask;
            ungrabButton(button, modMask);
            Q_ASSERT(modMask == 0);
        }
    };

    m_zoomInModMask = m_zoomOutModMask = zoomModMask;

    grabButton(XCB_BUTTON_INDEX_4, m_zoomInModMask);
    grabButton(XCB_BUTTON_INDEX_5, m_zoomOutModMask);
}

void Compositor::maybeStartZoomRectTimer(bool onlyIfActive)
{
    if (onlyIfActive == m_updateZoomRectTimer->isActive())
        m_updateZoomRectTimer->start(qFuzzyIsNull(m_interval) ? 10 :  qRound(m_interval * 1000));
}
void Compositor::updateZoomRect()
{
    const auto prevZoomRect = m_zoomRect;

    if (qFuzzyCompare(m_zoom, 1.0))
    {
        m_zoomRect = QRect();
        m_updateZoomRectTimer->stop();
        m_presentZoomedPixmaps.clear();
    }
    else
    {
        const double maxZoom = min({10.0, 32767.0 / g.rootWidth, 32767.0 / g.rootHeight});
        if (m_zoom > maxZoom)
        {
            m_zoom = maxZoom;
            qDebug() << "Limit zoom to:" << m_zoom;
        }

        QRect rect(0, 0, qRound(g.rootWidth / m_zoom), qRound(g.rootHeight / m_zoom));
        if (auto reply = XCB_CALL(xcb_query_pointer, g.conn, g.root))
        {
            if (m_cursorX != reply->root_x || m_cursorY != reply->root_y)
            {
                m_globalRegionToRepaint |= getCursorPixmapRect();
                scheduleRepaint();

                m_cursorX = reply->root_x;
                m_cursorY = reply->root_y;
            }
            rect.translate(
                qBound<int>(0, m_cursorX - rect.width()  / 2, g.rootWidth  - rect.width()),
                qBound<int>(0, m_cursorY - rect.height() / 2, g.rootHeight - rect.height())
            );
        }
        m_zoomRect = rect;

        maybeStartZoomRectTimer(false);
    }

    if (m_zoomRect != prevZoomRect)
    {
        if (m_zoomRect.isValid() != prevZoomRect.isValid())
        {
            if (m_zoomRect.isValid())
            {
                xcb_xfixes_hide_cursor(g.conn, g.root);
            }
            else
            {
                xcb_xfixes_show_cursor(g.conn, g.root);
                freeCursorPicture();
            }
        }
        m_globalRegionToRepaint = getRootRect();
        scheduleRepaint();
    }
}

void Compositor::updateCursorPicture()
{
    if (m_cursorPicture)
    {
        m_globalRegionToRepaint |= getCursorPixmapRect(); // Force update cursor to fix transparency
        return;
    }

    const auto format = findRenderFormat([](const xcb_render_directformat_t &direct) {
        return (direct.red_shift == 16
                && direct.green_shift == 8
                && direct.blue_shift == 0
                && direct.alpha_shift == 24
                && direct.red_mask == 0x00ff
                && direct.green_mask == 0x00ff
                && direct.blue_mask == 0x00ff
                && direct.alpha_mask == 0x00ff);
    }).first;
    if (format == 0)
        return;

    if (auto reply = XCB_CALL(xcb_xfixes_get_cursor_image, g.conn))
    {
        m_cursorHotX = reply->xhot;
        m_cursorHotY = reply->yhot;
        m_cursorW = reply->width;
        m_cursorH = reply->height;

        const auto pixmap = xcb_generate_id(g.conn);
        xcb_create_pixmap(g.conn, 32, pixmap, g.root, m_cursorW, m_cursorH);

        const auto gc = xcb_generate_id(g.conn);
        xcb_create_gc(g.conn, gc, pixmap, 0, nullptr);

        xcb_put_image(
            g.conn,
            XCB_IMAGE_FORMAT_Z_PIXMAP,
            pixmap,
            gc,
            m_cursorW,
            m_cursorH,
            0,
            0,
            0,
            32,
            reply->length * 4,
            reinterpret_cast<const uint8_t *>(reply.get() + 1)
        );

        xcb_free_gc(g.conn, gc);

        m_cursorPicture = xcb_generate_id(g.conn);
        xcb_render_create_picture(g.conn, m_cursorPicture, pixmap, format, 0, 0);

        xcb_free_pixmap(g.conn, pixmap);

        m_globalRegionToRepaint |= getCursorPixmapRect();
    }
}
void Compositor::freeCursorPicture()
{
    if (!m_cursorPicture)
        return;

    xcb_render_free_picture(g.conn, m_cursorPicture);
    m_cursorPicture = 0;

    m_globalRegionToRepaint |= getCursorPixmapRect();

    m_cursorHotX = m_cursorHotY = m_cursorW = m_cursorH = 0;
}

void Compositor::doRxMatchTitle()
{
    for (auto &&w : m_windowList)
        w->doRxMatchTitle();
}

void Compositor::restackWin(WindowList::iterator it, xcb_window_t newAbove)
{
    xcb_window_t oldAbove = 0;

    if (auto itNext = it + 1; itNext != m_windowList.end())
        oldAbove = (*itNext)->id;

    if (oldAbove == newAbove)
        return;

    auto winToInsert = qMove(*it);
    m_windowList.erase(it);
    m_windowList.insert(findWinIt(newAbove), qMove(winToInsert));
}

void Compositor::handleButtonPress(xcb_button_press_event_t &e)
{
    if (e.event != g.root || (e.detail != 4 && e.detail != 5))
        return;

    if (e.detail == 4)
    {
        m_zoom *= m_zoomStep;
        // Maximum zoom is limited in "updateZoomRect()"
    }
    else if (e.detail == 5)
    {
        m_zoom /= m_zoomStep;
        if (m_zoom < 1.0)
            m_zoom = 1.0;
    }

    qDebug().nospace() << "Set zoom to: " << m_zoom << ", step: " << m_zoomStep;

    updateZoomRect();
}
void Compositor::handleAddWin(xcb_window_t id)
{
    if (m_windowHash.count(id) > 0)
        return;

    auto winAttribsCookie = xcb_get_window_attributes(g.conn, id);
    auto winGeoCookie = xcb_get_geometry(g.conn, id);

    auto winAttribsReply = managePtr(xcb_get_window_attributes_reply(g.conn,winAttribsCookie, nullptr));
    auto winGeoReply = managePtr(xcb_get_geometry_reply(g.conn, winGeoCookie, nullptr));

    if (!winAttribsReply || !winGeoReply)
    {
        // This window probably is no longer available and will be destroyed soon.
        // Add it for proper window stacking.
        const auto w = make_shared<Window>(
            id,
            0,
            XCB_WINDOW_CLASS_INPUT_ONLY
        );
        m_windowList.push_front(w);
        m_windowHash[id] = w;
        return;
    }

    const auto w = make_shared<Window>(
        id,
        winAttribsReply->visual,
        winAttribsReply->_class
    );

    w->overrideRedirect = winAttribsReply->override_redirect;

    w->x = winGeoReply->x;
    w->y = winGeoReply->y;
    w->w = winGeoReply->width;
    w->h = winGeoReply->height;

    w->borderWidth = winGeoReply->border_width;

    if (winAttribsReply->map_state == XCB_MAP_STATE_VIEWABLE)
    {
        if (auto appWin = w->map())
        {
            m_appWindowHash[appWin] = w;
            if (m_exclusiveWindow)
                w->unredirect("behind fullscreen");
        }
        scheduleRepaint();
    }

    m_windowList.push_front(w);
    m_windowHash[id] = w;
}
void Compositor::handleConfigureWin(xcb_configure_notify_event_t &e)
{
    if (e.window == g.root)
    {
        if (g.rootWidth != e.width || g. rootHeight != e.height)
        {
            g.rootWidth = e.width;
            g.rootHeight = e.height;

            clearPixmaps();

            if (g.glxEnabled)
                setGlViewport();

            updateZoomRect();
        }
        return;
    }

    auto it = findWinIt(e.window);
    if (it == m_windowList.end())
        return;

    auto &w = *it;

    w->overrideRedirect = e.override_redirect;

    w->x = e.x;
    w->y = e.y;
    if (w->w != e.width || w->h != e.height)
    {
        w->destroyPicture(true);
        w->w = e.width;
        w->h = e.height;
    }

    w->borderWidth = e.border_width;

    w->scheduleFetchWindowRegion();
    scheduleRepaint();

    restackWin(it, e.above_sibling);
}
void Compositor::handleDestroyWin(xcb_window_t id)
{
    auto it = findWinIt(id);
    if (it == m_windowList.end())
        return;

    auto &w = *it;
    if (!w->windowRegion.isNull())
    {
        m_globalRegionToRepaint |= w->windowRegion;
        scheduleRepaint();
    }
    m_windowList.erase(it);
    m_windowHash.erase(id);
}
void Compositor::handleMapWin(xcb_window_t id)
{
    if (auto w = findWin(id))
    {
        if (auto appWin = w->map())
        {
            m_appWindowHash[appWin] = w;
            if (m_exclusiveWindow)
                w->unredirect("behind fullscreen");
        }
        scheduleRepaint();
    }
}
void Compositor::handleUnmapWin(xcb_window_t id)
{
    if (auto w = findWin(id))
    {
        if (auto appWin = w->unmap())
        {
            auto it = m_appWindowHash.find(appWin);
            if (it != m_appWindowHash.end() && it->second == w)
                m_appWindowHash.erase(it);
        }
        scheduleRepaint();
    }
}
void Compositor::handleDamageWin(xcb_damage_notify_event_t &e)
{
    if (auto w = findWin(e.drawable))
    {
        w->scheduleRepair();
        scheduleRepaint();
    }
}
void Compositor::handleShapeWin(xcb_shape_notify_event_t &e)
{
    auto w = findWin(e.affected_window);
    if (!w || (e.shape_kind != XCB_SHAPE_SK_CLIP && e.shape_kind != XCB_SHAPE_SK_BOUNDING))
        return;

    w->updateShapeInfo();
    w->scheduleFetchWindowRegion();
    scheduleRepaint();
}
void Compositor::handleReparentWin(xcb_reparent_notify_event_t &e)
{
    if (e.parent == g.root)
        handleAddWin(e.window);
    else
        handleDestroyWin(e.window);
}
void Compositor::handleCirculateWin(xcb_circulate_notify_event_t &e)
{
    auto it = findWinIt(e.window);
    if (it == m_windowList.end())
        return;

    xcb_window_t newAbove = 0;
    if (e.place == XCB_PLACE_ON_TOP)
        newAbove = m_windowList[0]->id;
    restackWin(it, newAbove);
}
void Compositor::handlePropertyNotify(xcb_property_notify_event_t &e)
{
    if (e.window == g.root)
    {
        if (e.atom == m_xRootPMapId)
            destroyRootWallpaperPicture();
        return;
    }

    const bool bypassCompositorProp = (e.atom == g.wmBypassCompositor);
    const bool opacityProp = (e.atom == g.wmWindowOpacity);
    const bool titleProp = (e.atom == g.ewmh._NET_WM_NAME);
    const bool titlePropOld = (e.atom == g.wmName);

    if (!bypassCompositorProp && !opacityProp && !titleProp && !titlePropOld)
        return;

    auto it = m_appWindowHash.find(e.window);
    if (it == m_appWindowHash.end())
        return;
    const auto &w = it->second;

    if (bypassCompositorProp)
        w->updateBypassCompositorProp();
    else if (opacityProp && w->updateOpacityProp())
        scheduleRepaint();
    else if (titleProp || titlePropOld)
        w->updateTitleProp();
}
void Compositor::handleGenericNotify(xcb_ge_generic_event_t &e)
{
    if (e.extension == m_presentMajorOpcode && e.event_type == XCB_PRESENT_EVENT_COMPLETE_NOTIFY)
        handlePresentCompleteNotify(reinterpret_cast<xcb_present_complete_notify_event_t &>(e));
}
void Compositor::handlePresentCompleteNotify(xcb_present_complete_notify_event_t &e)
{
    if (e.kind != XCB_PRESENT_COMPLETE_KIND_PIXMAP || e.event != m_presentEventId)
        return;

    m_presentInProgress = false;

    if (!m_presentEnabled)
        return;

    if (m_presentTimeoutTimer->isActive())
        m_presentTimeoutTimer->stop();

    const bool presentFlippingUnknown = (m_presentFlippedFrames < 3);
    if (e.mode == XCB_PRESENT_COMPLETE_MODE_FLIP)
    {
        if (presentFlippingUnknown)
        {
            ++m_presentFlippedFrames;
            m_presentCopiedFrames = 0;
        }
    }
    else if (presentFlippingUnknown && ++m_presentCopiedFrames > 3)
    {
        if (m_vSyncMode == VSyncMode::Auto)
        {
            makePresentUnusable();
            qDebug() << "Present flipping doesn't work, disabling";
        }
        else if (!m_presentNoFlippingReported)
        {
            qDebug() << "Present flipping doesn't work, expect screen tearing";
            m_presentNoFlippingReported = true;
        }
    }

    finishPresentCompleteNotify();
}

void Compositor::finishPresentCompleteNotify()
{
    if (m_presentPending)
        scheduleRepaint();

    if (m_exclusiveWindowTimer->isActive() && m_exclusiveWindowTimer->interval() == g_exclusiveWindowTimerPresentCompleteInterval)
    {
        m_exclusiveWindowTimer->stop();
        handleExclusiveWindow(true);
    }
}

void Compositor::handleExclusiveWindow(bool canUnredirectNow)
{
    // TODO: Check for "overrideRedirect"

    if (m_exclusiveWindowTimer->isActive())
        return;

    WindowPtr exclusiveWindowPtr;

    const bool modeDontAllow = (m_exclusiveFullScreenMode == ExclusiveFullScreenMode::DoNotAllow);
    const bool modeAllowAll = (m_exclusiveFullScreenMode == ExclusiveFullScreenMode::AllowAllWindows);
    if (!m_zoomRect.isValid() && (!modeDontAllow || !g.forceExclusiveFullScreen.empty()))
    {
        for (auto &&w : m_windowList)
        {
            if (w->isOffScreen())
                continue;

            if (w->bypassCompositor == 2 || w->isShaped || w->hasOpacity(true) || !w->fillsEntireScreen())
            {
                if (w->isShaped && w->w == 1 && w->h == 1)
                    continue;
                break;
            }

            if (!w->preventExclusive && (w->forceExclusive || modeAllowAll || (w->bypassCompositor == 1 && !modeDontAllow)))
                exclusiveWindowPtr = w;

            break;
        }
    }

    if (exclusiveWindowPtr && m_exclusiveWindow == exclusiveWindowPtr->id)
        return;

    if (m_exclusiveWindow)
    {
        xcb_map_window(g.conn, m_overlayWin);
        if (auto oldExclusive = findWin(m_exclusiveWindow))
        {
            oldExclusive->redirect("fullscreen");
            m_globalRegionToRepaint = getRootRect();
        }
        m_exclusiveWindow = 0;
        m_usePresentTimeout = true; // After mapping "m_overlayWin" the present might not complete
        scheduleRepaint();
    }

    if (exclusiveWindowPtr)
    {
        const bool presentInProgress = (m_presentEnabled && m_presentInProgress);
        if (canUnredirectNow && !presentInProgress)
        {
            xcb_unmap_window(g.conn, m_overlayWin);
            exclusiveWindowPtr->unredirect("fullscreen");
            m_exclusiveWindow = exclusiveWindowPtr->id;

            // Unredirect all other mapped windows
            for (auto &&w : m_windowList)
                w->unredirect("behind fullscreen");
        }
        else
        {
            m_exclusiveWindowTimer->start((canUnredirectNow && presentInProgress)
                ? g_exclusiveWindowTimerPresentCompleteInterval
                : 50
            );
        }
    }
}

bool Compositor::registerCM()
{
    if (m_selectionOwnerWin)
        return true;

    xcb_atom_t atom = 0;

    const QByteArray atomName("_NET_WM_CM_S" + QByteArray::number(g.screenNo));
    if (auto reply = XCB_CALL(xcb_intern_atom, g.conn, false, atomName.size(), atomName.constData()))
        atom = reply->atom;

    if (!atom)
    {
        qCritical() << "Can't get atom:" << atomName;
        return false;
    }

    if (auto reply = XCB_CALL_UNCHECKED(xcb_get_selection_owner, g.conn, atom))
    {
        if (auto w = reply->owner)
        {
            if (auto reply = XCB_CALL_UNCHECKED(xcb_get_property, g.conn, false, w, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 0, ~0))
            {
                auto data = reinterpret_cast<const char *>(xcb_get_property_value(reply.get()));
                qDebug().noquote() << "Another composite manager is already running (" + QByteArray::fromRawData(data, reply->value_len) + ")";
            }
            else
            {
                qDebug("Another composite manager is already running (0x%x)", w);
            }
            return false;
        }
    }

    m_selectionOwnerWin = xcb_generate_id(g.conn);
    xcb_create_window(
        g.conn,
        g.rootDepth,
        m_selectionOwnerWin,
        g.root,
        0, 0,
        1, 1,
        0,
        XCB_WINDOW_CLASS_INPUT_OUTPUT,
        g.rootVisual,
        0,
        nullptr
    );

    const char title[] = "qdre-compositor";
    xcb_change_property(
        g.conn,
        XCB_PROP_MODE_REPLACE,
        m_selectionOwnerWin,
        XCB_ATOM_WM_NAME,
        XCB_ATOM_STRING,
        8,
        sizeof(title) - 1,
        title
    );

    xcb_set_selection_owner(g.conn, m_selectionOwnerWin, atom, 0);

    return true;
}

bool Compositor::initGLX()
{
    if (m_glxInitialized)
        return static_cast<bool>(g.fbConfig);
    m_glxInitialized = true;

    if (!m_hasRandrExtension)
        qWarning() << "Missing RandR extension required for GLX";
    if (!m_hasSyncExtension)
        qWarning() << "Missing Sync extension required for GLX";
    if (!m_hasSyncExtension || !m_hasSyncExtension)
        return false;

    // For NVIDIA proprietary drivers
    const auto envKeys = QProcessEnvironment::systemEnvironment().keys();
    for (auto &&envKey : envKeys)
    {
        if (envKey.startsWith(QStringLiteral("__GL_")) && envKey != QStringLiteral("__GL_SYNC_DISPLAY_DEVICE"))
        {
            qDebug() << "Unset environment variable:" << envKey;
            qunsetenv(envKey.toLocal8Bit());
        }
    }
    qputenv("__GL_YIELD", "usleep");

    int error_base = 0, event_base = 0;
    if (!glXQueryExtension(g.dpy, &error_base, &event_base))
    {
        qWarning() << "GLX extension is missing";
        return false;
    }

    int maj = 0, min = 0;
    if (!glXQueryVersion(g.dpy, &maj, &min) || maj < 1 || (maj == 1 && min < 4))
    {
        qWarning() << "GLX extension is too old";
        return false;
    }

    g.glxExtensions = glXGetClientString(g.dpy, GLX_EXTENSIONS);
    if (!g.glxExtensions)
    {
        qWarning() << "Can't get GLX extensions string";
        return false;
    }

    if (!strstr(g.glxExtensions, "GLX_EXT_texture_from_pixmap"))
    {
        qWarning() << "GLX_EXT_texture_from_pixmap extension is missing";
        return false;
    }
    if (!strstr(g.glxExtensions, "GLX_EXT_buffer_age"))
    {
        qWarning() << "GLX_EXT_buffer_age extension is missing";
        return false;
    }

    const auto getGlProcAddress = [](auto &proc, const char *procName) {
        proc = reinterpret_cast<remove_reference_t<decltype(proc)>>(glXGetProcAddress(reinterpret_cast<const GLubyte *>(procName)));
    };

    getGlProcAddress(g.glXBindTexImageEXT, "glXBindTexImageEXT");
    getGlProcAddress(g.glXReleaseTexImageEXT, "glXReleaseTexImageEXT");
    if (strstr(g.glxExtensions, "GLX_MESA_swap_control"))
        getGlProcAddress(glxSwapInterval, "glXSwapIntervalMESA");
    else if (strstr(g.glxExtensions, "GLX_SGI_swap_control"))
        getGlProcAddress(glxSwapInterval, "glXSwapIntervalSGI");

    if (!g.glXBindTexImageEXT || !g.glXReleaseTexImageEXT)
    {
        qWarning() << "Can't get GLX functions";
        return false;
    }
    if (!glxSwapInterval)
    {
        qWarning() << "Can't get GLX swap interval function";
        return false;
    }

    int numReturned = 0;
    const auto fbConfigs = glXGetFBConfigs(g.dpy, g.screenNo, &numReturned);
    if (!fbConfigs || numReturned < 1)
    {
        qWarning() << "Can't get GLX FBConfigs";
        return false;
    }

    const auto xVisualId = XVisualIDFromVisual(DefaultVisual(g.dpy, g.screenNo));
    for (int i = 0; i < numReturned; ++i)
    {
        const auto visualInfo = glXGetVisualFromFBConfig(g.dpy, fbConfigs[i]);
        if (!visualInfo)
            continue;

        const auto xVisualIdCurr = visualInfo->visualid;
        XFree(visualInfo);

        if (xVisualIdCurr != xVisualId)
            continue;

        int value;

        value = 0;
        glXGetFBConfigAttrib(g.dpy, fbConfigs[i], GLX_DOUBLEBUFFER, &value);
        if (value != 1)
            continue;

        value = 0;
        glXGetFBConfigAttrib(g.dpy, fbConfigs[i], GLX_DRAWABLE_TYPE, &value);
        if (!(value & GLX_PIXMAP_BIT))
            continue;

        value = 0;
        glXGetFBConfigAttrib(g.dpy, fbConfigs[i], GLX_BIND_TO_TEXTURE_TARGETS_EXT, &value);
        if (!(value & GLX_TEXTURE_RECTANGLE_BIT_EXT))
            continue;

        value = 0;
        glXGetFBConfigAttrib (g.dpy, fbConfigs[i], GLX_BIND_TO_TEXTURE_RGBA_EXT, &value);
        if (value != 1)
        {
            value = 0;
            glXGetFBConfigAttrib (g.dpy, fbConfigs[i], GLX_BIND_TO_TEXTURE_RGB_EXT, &value);
            if (value != 1)
                continue;
        }

        value = 0;
        glXGetFBConfigAttrib(g.dpy, fbConfigs[i], GLX_Y_INVERTED_EXT, &value);
        if (value == 0)
            continue;

        g.fbConfig = fbConfigs[i];

        break;
    }

    XFree(fbConfigs);

    if (!g.fbConfig)
        qWarning() << "Can't find GLX FBConfig";

    return static_cast<bool>(g.fbConfig);
}

inline QRect Compositor::getRootRect()
{
    return QRect(0, 0, g.rootWidth, g.rootHeight);
}
inline QRect Compositor::getCursorPixmapRect()
{
    return QRect(
        m_cursorX - m_cursorHotX,
        m_cursorY - m_cursorHotY,
        m_cursorW,
        m_cursorH
    );
}

pair<xcb_render_pictformat_t, uint8_t> Compositor::findRenderFormat(const function<bool(const xcb_render_directformat_t &)> &callback)
{
    const auto reply = XCB_CALL_UNCHECKED(xcb_render_query_pict_formats, g.conn);
    if (!reply)
        return {};

    const int nFormats = xcb_render_query_pict_formats_formats_length(reply.get());
    const auto formats = xcb_render_query_pict_formats_formats(reply.get());
    for (int i = 0; i < nFormats; ++i)
    {
        if (formats[i].type != XCB_RENDER_PICT_TYPE_DIRECT)
            continue;
        if (callback(formats[i].direct))
            return {formats[i].id, formats[i].depth};
    }

    return {};
}

xcb_render_picture_t Compositor::createSolidAlphaPixel(uint16_t alpha)
{
    const auto [format, depth] = findRenderFormat([](const xcb_render_directformat_t &direct) {
        return (direct.red_mask == 0x0000
                && direct.green_mask == 0x0000
                && direct.blue_mask == 0x0000
                && direct.alpha_mask >= 0x00ff);
    });
    if (format == 0 || depth == 0)
        return 0;

    const auto pixmap = xcb_generate_id(g.conn);
    xcb_create_pixmap(g.conn, depth, pixmap, g.root, 1, 1);

    const uint32_t value = true;
    auto picture = xcb_generate_id(g.conn);
    xcb_render_create_picture(g.conn, picture, pixmap, format, XCB_RENDER_CP_REPEAT, &value);

    xcb_render_color_t color = {};
    color.alpha = alpha;

    xcb_rectangle_t rect {0, 0, 1, 1};
    xcb_render_fill_rectangles(g.conn, XCB_RENDER_PICT_OP_SRC, picture, color, 1, &rect);

    xcb_free_pixmap(g.conn, pixmap);

    return picture;
}

xcb_xfixes_region_t Compositor::qRegionToXcbRegion(xcb_xfixes_region_t region, const QRegion &qRegion)
{
    vector<xcb_rectangle_t> rects;
    for (auto &&qRect : qRegion)
    {
        xcb_rectangle_t rect;
        rect.x = qRect.x();
        rect.y = qRect.y();
        rect.width = qRect.width();
        rect.height = qRect.height();
        rects.push_back(rect);
    }
    xcb_xfixes_set_region(g.conn, region, rects.size(), rects.data());
    return region;
}

bool Compositor::nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result)
{
    Q_UNUSED(result)
    Q_ASSERT(eventType == "xcb_generic_event_t");

    const auto e = static_cast<xcb_generic_event_t *>(message);
    const auto type = (e->response_type & ~0x80);

    if (type == 0)
        return false; // Let Qt handle errors

    bool checkForExclusiveWindow = true;

    switch (type)
    {
        case XCB_BUTTON_PRESS:
            handleButtonPress(reinterpret_cast<xcb_button_press_event_t &>(*e));
            break;
        case XCB_CREATE_NOTIFY:
            handleAddWin(reinterpret_cast<xcb_create_notify_event_t &>(*e).window);
            break;
        case XCB_CONFIGURE_NOTIFY:
            handleConfigureWin(reinterpret_cast<xcb_configure_notify_event_t &>(*e));
            break;
        case XCB_DESTROY_NOTIFY:
            handleDestroyWin(reinterpret_cast<xcb_destroy_notify_event_t &>(*e).window);
            break;
        case XCB_MAP_NOTIFY:
            handleMapWin(reinterpret_cast<xcb_map_notify_event_t &>(*e).window);
            break;
        case XCB_UNMAP_NOTIFY:
            handleUnmapWin(reinterpret_cast<xcb_map_notify_event_t &>(*e).window);
            break;
        case XCB_REPARENT_NOTIFY:
            handleReparentWin(reinterpret_cast<xcb_reparent_notify_event_t &>(*e));
            break;
        case XCB_CIRCULATE_NOTIFY:
            handleCirculateWin(reinterpret_cast<xcb_circulate_notify_event_t &>(*e));
            break;
        case XCB_PROPERTY_NOTIFY:
            handlePropertyNotify(reinterpret_cast<xcb_property_notify_event_t &>(*e));
            break;
        case XCB_GE_GENERIC:
            handleGenericNotify(reinterpret_cast<xcb_ge_generic_event_t &>(*e));
            checkForExclusiveWindow = false;
            break;
        default:
            if (type == m_damageFirstEvent + XCB_DAMAGE_NOTIFY)
            {
                handleDamageWin(reinterpret_cast<xcb_damage_notify_event_t &>(*e));
                checkForExclusiveWindow = false;
            }
            else if (type == m_shapeFirstEvent + XCB_SHAPE_NOTIFY)
            {
                handleShapeWin(reinterpret_cast<xcb_shape_notify_event_t &>(*e));
            }
            else if (type == m_fixesFirstEvent + XCB_XFIXES_CURSOR_NOTIFY)
            {
                freeCursorPicture();
                checkForExclusiveWindow = false;
            }
            else if (m_hasRandrExtension && type == m_randrFirstEvent + XCB_RANDR_NOTIFY)
            {
                m_getRefreshIntervalTimer->start(100);
                checkForExclusiveWindow = false;
            }
            else
            {
                checkForExclusiveWindow = false;
            }
            break;
    }

    if (checkForExclusiveWindow)
        handleExclusiveWindow(false);

    return true;
}

// Globals

void Compositor::Globals::xcbDiscard(const xcb_void_cookie_t &cookie)
{
    xcb_discard_reply(conn, cookie.sequence);
}

QRegion Compositor::Globals::qRegionFromXcbCookie(xcb_xfixes_fetch_region_cookie_t cookie)
{
    QRegion qRegion;
    if (auto reply = managePtr(xcb_xfixes_fetch_region_reply(conn, cookie, nullptr)))
    {
        const int numRects = xcb_xfixes_fetch_region_rectangles_length(reply.get());
        const auto rects = xcb_xfixes_fetch_region_rectangles(reply.get());
        for (int i = 0; i < numRects; ++i)
            qRegion |= QRect(rects[i].x, rects[i].y, rects[i].width, rects[i].height);
    }
    return qRegion;
}

xcb_void_cookie_t Compositor::Globals::xcbSelectInput(xcb_window_t id, uint32_t mask)
{
    return xcb_change_window_attributes(conn, id, XCB_CW_EVENT_MASK, &mask);
}
xcb_void_cookie_t Compositor::Globals::xcbSelectInputChecked(xcb_window_t id, uint32_t mask)
{
    return xcb_change_window_attributes_checked(conn, id, XCB_CW_EVENT_MASK, &mask);
}

xcb_render_picture_t Compositor::Globals::createDrawablePicture(xcb_drawable_t id, xcb_render_pictformat_t format)
{
    const uint32_t value = XCB_SUBWINDOW_MODE_INCLUDE_INFERIORS;

    auto picture = xcb_generate_id(conn);
    xcb_render_create_picture(conn, picture, id, format, XCB_RENDER_CP_SUBWINDOW_MODE, &value);

    return picture;
}

xcb_render_pictformat_t Compositor::Globals::xcbRenderFindVisualFormat(xcb_visualid_t visual, bool *hasAlpha)
{
    if (hasAlpha)
        *hasAlpha = false;

    auto reply = XCB_CALL_UNCHECKED(xcb_render_query_pict_formats, conn);
    if (!reply)
        return 0;

    auto pictVisual = xcb_render_util_find_visual_format(reply.get(), visual);
    if (!pictVisual)
        return 0;

    if (hasAlpha)
    {
        const int nFormats = xcb_render_query_pict_formats_formats_length(reply.get());
        const auto formats = xcb_render_query_pict_formats_formats(reply.get());
        for (int i = 0; i < nFormats; ++i)
        {
            if (formats[i].id == pictVisual->format)
            {
                if (formats[i].type == XCB_RENDER_PICT_TYPE_DIRECT && formats[i].direct.alpha_mask)
                    *hasAlpha = true;
                break;
            }
        }
    }

    return pictVisual->format;
}

}
