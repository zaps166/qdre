/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <xcb/composite.h>

#include <Window.hpp>
#include <CompositorInstance.hpp>

#include <X.hpp>

#include <QDebug>

namespace QDRE {

using namespace CompositorInstance;
using namespace std;

Window::Window(xcb_window_t id, xcb_visualid_t visual, uint16_t winClass)
    : id(id)
    , m_isIO(winClass == XCB_WINDOW_CLASS_INPUT_OUTPUT)
{
    if (m_isIO)
        m_pictFormat = g.xcbRenderFindVisualFormat(visual, &hasAlphaChannel);
}
Window::~Window()
{
    unmap();
}

bool QDRE::Window::hasOpacity(bool real) const
{
    return (opacityValue != g_opaque || (hasAlphaChannel && (real || !preventWindowTransparency)));
}

void Window::createPicture()
{
    if (!isMapped)
        return;

#ifdef USE_NAME_WINDOW_PIXMAP
    if (!m_pixmap)
    {
        m_pixmap = xcb_generate_id(g.conn);
        xcb_composite_name_window_pixmap(g.conn, id, m_pixmap);
    }

    if (!picture)
    {
        picture = g.createDrawablePicture(m_pixmap, m_pictFormat);
    }
#else
    if (!picture)
    {
        picture = g.createDrawablePicture(id, m_pictFormat);
    }
#endif
}
void Window::destroyPicture(bool resized)
{
#ifdef USE_NAME_WINDOW_PIXMAP
    Q_UNUSED(resized)

    if (m_pixmap)
    {
        xcb_free_pixmap(g.conn, m_pixmap);
        m_pixmap = 0;
    }
#else
    if (resized)
        return;
#endif

    if (picture)
    {
        xcb_render_free_picture(g.conn, picture);
        picture = 0;
    }
}

void Window::updateBypassCompositorProp()
{
    if (!isMapped)
        return;

    if (auto reply = XCB_CALL(xcb_get_property, g.conn, false, m_appWinId, g.wmBypassCompositor, XCB_ATOM_CARDINAL, 0, 1); reply && reply->value_len == 1)
        bypassCompositor = reinterpret_cast<int *>(xcb_get_property_value(reply.get()))[0];
    else
        bypassCompositor = 0;
}
bool Window::updateOpacityProp()
{
    if (!isMapped)
        return false;

    uint32_t newOpacityValue = g_opaque;

    if (auto reply = XCB_CALL(xcb_get_property, g.conn, false, m_appWinId, g.wmWindowOpacity, XCB_ATOM_CARDINAL, 0, 1); reply && reply->value_len == 1)
        newOpacityValue = reinterpret_cast<uint32_t *>(xcb_get_property_value(reply.get()))[0];

    if (newOpacityValue == opacityValue)
        return false;

    destroyOpacityPicture();

    opacityValue = newOpacityValue;
    commands.repaint = true;

    return true;
}
void Window::updateTitleProp()
{
    xcb_ewmh_get_utf8_strings_reply_t t = {};
    auto cookie = xcb_ewmh_get_wm_name(&g.ewmh, m_appWinId);
    if (xcb_ewmh_get_wm_name_reply(&g.ewmh, cookie, &t, nullptr) == 1)
    {
        title = QString::fromUtf8(t.strings, t.strings_len);
        xcb_ewmh_get_utf8_strings_reply_wipe(&t);
        doRxMatchTitle();
        return;
    }

    if (auto reply = XCB_CALL(xcb_get_property, g.conn, false, m_appWinId, g.wmName, XCB_ATOM_STRING, 0, ~0); reply)
    {
        const auto str = reinterpret_cast<char *>(xcb_get_property_value(reply.get()));
        const auto len = xcb_get_property_value_length(reply.get());
        title = QString::fromLocal8Bit(str, len);
        doRxMatchTitle();
        return;
    }

    title.clear();
    doRxMatchTitle();
}

void Window::updateShapeInfo()
{
    if (!isMapped)
        return;

    if (auto reply = XCB_CALL(xcb_shape_query_extents, g.conn, m_appWinId))
        isShaped = (reply->bounding_shaped != 0);
    else
        isShaped = false;
}

void Window::scheduleFetchWindowRegion()
{
    if (isMapped && !commands.map && !commands.unmap)
    {
        commands.newRegion = true;
        commands.repaint = false;
    }
}
void Window::scheduleRepair()
{
    if (!m_damage)
        return;

    commands.repair = true;
}

void Window::processCommands()
{
    Q_ASSERT(!finishRegionCommand);
    Q_ASSERT(firstRepaintRegion.isNull());

    if (commands.map)
    {
        if (commands.unmap)
            firstRepaintRegion |= windowRegion;
        fetchWindowRegion();
    }
    else if (commands.unmap)
    {
        firstRepaintRegion = qMove(windowRegion);
    }
    else if (commands.newRegion)
    {
        firstRepaintRegion = qMove(windowRegion);
        fetchWindowRegion();
    }

    if (commands.repaint)
    {
        firstRepaintRegion |= windowRegion;
    }

    if (commands.repair)
    {
        if (commands.map || commands.newRegion || commands.repaint)
        {
            g.xcbDiscard(xcb_damage_subtract_checked(g.conn, m_damage, 0, 0));
        }
        else
        {
            auto region = xcb_generate_id(g.conn);
            xcb_xfixes_create_region(g.conn, region, 0, nullptr);
            g.xcbDiscard(xcb_damage_subtract_checked(g.conn, m_damage, 0, region));
            auto cookie = xcb_xfixes_fetch_region_unchecked(g.conn, region);
            xcb_xfixes_destroy_region(g.conn, region);
            finishRegionCommand = [=] {
                return g.qRegionFromXcbCookie(cookie).translated(x + borderWidth, y + borderWidth);
            };
        }
    }

    commands = {};
}

xcb_window_t Window::map()
{
    if (!m_isIO || isMapped)
        return 0;

    isMapped = true;

    createDamage();

    m_appWinId = findAppWindow(id);

    const bool firstRef = (++g.appWinRefs[m_appWinId] == 1);

    if (firstRef)
        g.xcbSelectInput(m_appWinId, XCB_EVENT_MASK_PROPERTY_CHANGE);
    xcb_shape_select_input(g.conn, id, true);

    updateBypassCompositorProp();
    updateOpacityProp();
    updateTitleProp();

    updateShapeInfo();

    commands.map = true;
    commands.newRegion = false;
    commands.repair = false;
    commands.repaint = false;

    return m_appWinId;
}
xcb_window_t Window::unmap()
{
    if (!isMapped)
        return 0;

    const auto prevAppWinId = m_appWinId;
    const bool lastRef = (--g.appWinRefs[m_appWinId] == 0);

    isMapped = false;

    destroyOpacityPicture();
    destroyPicture();
    destroyDamage();

    if (lastRef)
        g.xcbDiscard(g.xcbSelectInputChecked(m_appWinId, XCB_EVENT_MASK_NO_EVENT));
    g.xcbDiscard(xcb_shape_select_input_checked(g.conn, id, false));

    m_appWinId = 0;
    opacityValue = 0;
    bypassCompositor = 0;
    isShaped = false;

    commands.map = false;
    commands.unmap = true;
    commands.newRegion = false;
    commands.repair = false;
    commands.repaint = false;

    return prevAppWinId;
}

void Window::redirect(const char *logMsg)
{
    if (!isMapped || isRedirected)
        return;

    if (title.isEmpty())
        qDebug().nospace() << "Redirect window: " << id << " (" << logMsg << ")";
    else
        qDebug().nospace() << "Redirect window: " << id << " " << title << " (" << logMsg << ")";

    createDamage();
    xcb_composite_redirect_window(g.conn, id, XCB_COMPOSITE_REDIRECT_MANUAL);
    isRedirected = true;
}
void Window::unredirect(const char *logMsg)
{
    if (!isMapped || !isRedirected)
        return;

    if (title.isEmpty())
        qDebug().nospace() << "Unredirect window: " << id << " (" << logMsg << ")";
    else
        qDebug().nospace() << "Unredirect window: " << id << " " << title << " (" << logMsg << ")";

    xcb_composite_unredirect_window(g.conn, id, XCB_COMPOSITE_REDIRECT_MANUAL);
    destroyPicture();
    destroyDamage();
    isRedirected = false;
}

void Window::doRxMatchTitle()
{
    if (!isMapped)
        return;

    auto rxMatchTitle = [this](auto &&list) {
        if (title.isEmpty())
            return false;
        return (find_if(list.begin(), list.end(), [&](auto &&rx) {
            return rx.match(title).hasMatch();
        }) != list.end());
    };

    forceExclusive = rxMatchTitle(g.forceExclusiveFullScreen);
    preventExclusive = rxMatchTitle(g.preventExclusiveFullScreen);
    if (forceExclusive && preventExclusive)
        forceExclusive = preventExclusive = false;

    preventWindowTransparency = rxMatchTitle(g.preventWindowTransparency);
}

bool Window::isOffScreen() const
{
    return (!isMapped || x + w <= 0 || y + h <= 0 || x >= g.rootWidth || y >= g.rootHeight);
}
bool Window::fillsEntireScreen() const
{
    return (x == 0 && y == 0 && w == g.rootWidth && h == g.rootHeight);
}

void Window::createDamage()
{
    if (m_damage)
        return;

    m_damage = xcb_generate_id(g.conn);
    xcb_damage_create(g.conn, m_damage, id, XCB_DAMAGE_REPORT_LEVEL_NON_EMPTY);
}
void Window::destroyDamage()
{
    if (!m_damage)
        return;

    g.xcbDiscard(xcb_damage_destroy_checked(g.conn, m_damage));
    m_damage = 0;
}

void Window::fetchWindowRegion()
{
    Q_ASSERT(!finishRegionCommand);

    auto region = xcb_generate_id(g.conn);
    xcb_xfixes_create_region_from_window(g.conn, region, id, XCB_SHAPE_SK_BOUNDING);
    auto cookie = xcb_xfixes_fetch_region_unchecked(g.conn, region);
    xcb_xfixes_destroy_region(g.conn, region);
    finishRegionCommand = [=] {
        windowRegion = g.qRegionFromXcbCookie(cookie).intersected(QRect(0, 0, w, h)).translated(x + borderWidth, y + borderWidth);
        return windowRegion;
    };
}

void Window::destroyOpacityPicture()
{
    if (!opacityPict)
        return;

    xcb_render_free_picture(g.conn, opacityPict);
    opacityPict = 0;
}

xcb_window_t Window::findAppWindow(xcb_window_t id)
{
    const auto hasWmStateProperty = [](xcb_window_t w) {
        const auto reply = XCB_CALL(xcb_get_property, g.conn, false, w, g.ewmh._NET_WM_STATE, XCB_GET_PROPERTY_TYPE_ANY, 0, 0);
        return (reply && reply->type != 0);
    };

    xcb_window_t ret = id;
    if (!hasWmStateProperty(id))
    {
        X::queryTreeIterate(id, [&](xcb_window_t it) {
            if (hasWmStateProperty(it))
            {
                ret = it;
                return true;
            }
            return false;
        });
    }
    return ret;
}

}
