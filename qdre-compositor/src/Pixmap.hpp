/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <QtGlobal>

#include <xcb/xcb.h>
#include <xcb/render.h>
#include <xcb/sync.h>

#include <GLX.h>

namespace QDRE {

class Pixmap
{
    Q_DISABLE_COPY(Pixmap)

public:
    Pixmap();
    ~Pixmap();

    void create(bool allowGLX = true);

    void setPictureZoomTransform(double zoom);
    void resetPictureZoomTransform();

    void destroy();
    void destroyGLX();

    void sync();

    inline xcb_pixmap_t pixmap() const;
    inline xcb_render_picture_t picture() const;

private:
    xcb_pixmap_t m_pix = 0;
    xcb_render_picture_t m_pic = 0;

    GLXPixmap m_glxPix = 0;
    uint m_glTex = 0;

    xcb_sync_fence_t m_fence = 0;
};

/* Inline implementation */

xcb_pixmap_t Pixmap::pixmap() const
{
    return m_pix;
}
xcb_render_picture_t Pixmap::picture() const
{
    return m_pic;
}

}
