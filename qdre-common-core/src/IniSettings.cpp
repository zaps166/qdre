/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <IniSettings.hpp>

#include <UtilsCore.hpp>

#include <QCoreApplication>
#include <QLoggingCategory>
#include <QStandardPaths>
#include <QWaitCondition>
#include <QSettings>
#include <QMutex>
#include <QTimer>

#include <memory>
#include <thread>

namespace QDRE {

struct IniSettings::Priv
{
    std::thread thread;

    QWaitCondition cond;
    QMutex mutex;

    QTimer timer;

    std::unique_ptr<QSettings> sets;

    bool br = false;
};

/**/

IniSettings::IniSettings(const Location location,
                         const QString &fileName,
                         QObject *parent,
                         bool saveTimeout)
    : QObject(parent)
    , d(*(new Priv))
{
    d.timer.setSingleShot(true);
    d.timer.setInterval(saveTimeout ? UtilsCore::dataSaveTimeout() : 0);
    connect(&d.timer, &QTimer::timeout,
            this, [this] {
        d.cond.wakeOne();
    });

    const auto mutexRun = std::make_shared<QMutex>();
    QMutexLocker locker(mutexRun.get());

    d.thread = std::thread([=] {
        const QStandardPaths::StandardLocation standardLocation = (location == Location::Local)
                ? QStandardPaths::GenericDataLocation
                : QStandardPaths::GenericConfigLocation;

        QString path = QStandardPaths::standardLocations(standardLocation).value(0)
                + "/" + QCoreApplication::organizationName()
                + "/" + QCoreApplication::applicationName().toLower();
        if (!fileName.isEmpty())
            path += "/" + fileName;
        path += ".ini";

        d.sets = std::make_unique<QSettings>(path, QSettings::IniFormat);
        path.clear();

        mutexRun->unlock(); // Notify started

        for (;;)
        {
            QMutexLocker locker(&d.mutex);
            d.cond.wait(&d.mutex);
            if (d.br)
                break;
            d.sets->sync();
        }

        d.sets.reset();
    });

    mutexRun->lock(); // Wait for thread to be started
}
IniSettings::~IniSettings()
{
    {
        QMutexLocker locker(&d.mutex);
        d.br = true;
        d.cond.wakeOne();
    }
    d.thread.join();
    delete &d;
}

void IniSettings::beginGroup(const QString &prefix)
{
    QMutexLocker locker(&d.mutex);
    d.sets->beginGroup(prefix);
}
QString IniSettings::group() const
{
    QMutexLocker locker(&d.mutex);
    return d.sets->group();
}
void IniSettings::endGroup()
{
    QMutexLocker locker(&d.mutex);
    d.sets->endGroup();
}

QStringList IniSettings::allKeys() const
{
    QMutexLocker locker(&d.mutex);
    return d.sets->allKeys();
}
QStringList IniSettings::childKeys() const
{
    QMutexLocker locker(&d.mutex);
    return d.sets->childKeys();
}

void IniSettings::setValue(const QString &key, const QVariant &value)
{
    QMutexLocker locker(&d.mutex);
    d.sets->setValue(key, value);
    d.timer.start();
}
QVariant IniSettings::value(const QString &key, const QVariant &defaultValue) const
{
    QMutexLocker locker(&d.mutex);
    return d.sets->value(key, defaultValue);
}

void IniSettings::remove(const QString &key)
{
    QMutexLocker locker(&d.mutex);
    d.sets->remove(key);
    d.timer.start();
}
bool IniSettings::contains(const QString &key) const
{
    QMutexLocker locker(&d.mutex);
    return d.sets->contains(key);
}

}
