/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <InitializeCore.hpp>

#include <QCoreApplication>
#include <QMutex>
#include <QTime>

#include <csignal>

namespace QDRE {

static QMutex g_mutex;

static void signalHandler(int sig)
{
    Q_UNUSED(sig)

    static qint32 i;
    if (++i > 1)
        raise(SIGKILL);
    else
        QCoreApplication::quit();
}

static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    const auto timeStr = QTime::currentTime().toString("hh:mm:ss.zzz");
    QMutexLocker locker(&g_mutex);
    fprintf(
        stderr,
        "%s\n",
        QString("[%1] %2")
            .arg(timeStr)
            .arg((qstrcmp(context.category, "default") == 0)
                ? msg
                : qFormatLogMessage(type, context, msg)
        ).toUtf8().constData()
    );
    fflush(stderr);
}

/**/

namespace InitializePriv {

QDRE_COMMON_CORE_VISIBILITY bool otherInitialization = false;
QDRE_COMMON_CORE_VISIBILITY bool initialized = false;

constexpr int maxInitFunctionsCount = 8;
static void(*initFunctionsArr[maxInitFunctionsCount])();
static int initFunctionsCount;

QDRE_COMMON_CORE_VISIBILITY void finalizeInit()
{
    for (int i = 0; i < initFunctionsCount; ++i)
    {
        initFunctionsArr[i]();
        initFunctionsArr[i] = nullptr;
    }
    initFunctionsCount = 0;
    initialized = true;
}

}

/**/

void addInitFunction(void(*fn)())
{
    if (fn)
    {
        if (InitializePriv::initialized)
        {
            fn();
        }
        else
        {
            if (InitializePriv::initFunctionsCount < InitializePriv::maxInitFunctionsCount)
                InitializePriv::initFunctionsArr[InitializePriv::initFunctionsCount++] = fn;
            else
                qFatal("Maximum number of init functions exceeded");
        }
    }
}

void initializeCore()
{
    if (InitializePriv::initialized)
        return;

    QCoreApplication::setOrganizationName("qdre");

    if (!qEnvironmentVariableIsSet("QT_MESSAGE_PATTERN"))
        qSetMessagePattern("[%{category} %{type}] %{line}: %{message}");
    qInstallMessageHandler(messageHandler);

    std::signal(SIGINT,  signalHandler);
    std::signal(SIGTERM, signalHandler);

    if (!InitializePriv::otherInitialization)
        InitializePriv::finalizeInit();
}

}
