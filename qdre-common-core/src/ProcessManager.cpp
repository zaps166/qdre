/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ProcessManager.hpp>

#include <QLoggingCategory>
#include <QProcess>
#include <QVariant>
#include <QThread>

#include <functional>
#include <map>

namespace QDRE {

constexpr qint32 g_waitTimeout = 1000;

class ProcessManager::Priv
{
public:
    Priv(ProcessManager &processManager);

    quint64 startOrEnqueue(const std::function<void(QProcess &)> &setProcessParamsFn);
    void startProcess(QProcess &process);
    void maybeTerminateProcess(QProcess &process);

public:
    ProcessManager &q;

    std::map<quint64, QProcess> processes;

    bool disableProcessStop = false;

    quint32 runningProcesses = 0;
    quint32 maxProcesses = 0;
    quint64 currId = 0;
};

/**/

ProcessManager::ProcessManager(QObject *parent)
    : QObject(parent)
    , d(*(new Priv(*this)))
{
    setMaximumProcesses();
}
ProcessManager::~ProcessManager()
{
    stopAll();
    delete &d;
}

void ProcessManager::setMaximumProcesses(const quint8 num)
{
    d.maxProcesses = (num > 0) ? num : QThread::idealThreadCount();
}

bool ProcessManager::isRunning() const
{
    return !d.processes.empty();
}

quint64 ProcessManager::startOrEnqueue(const QString &program, const QStringList &arguments)
{
    return d.startOrEnqueue([=](QProcess &process) {
        process.setProgram(program);
        process.setArguments(arguments);
    });
}
quint64 ProcessManager::startOrEnqueue(const QString &command)
{
    return d.startOrEnqueue([=](QProcess &process) {
        process.setProperty("command", command);
    });
}
void ProcessManager::stop(const quint64 id)
{
    if (!d.disableProcessStop)
    {
        d.disableProcessStop = true;
        auto it = d.processes.find(id);
        if (it != d.processes.end())
        {
            d.maybeTerminateProcess(it->second);
            d.processes.erase(it);

            for (auto &&item : d.processes)
            {
                QProcess &process = item.second;
                if (process.property("new").toBool() == true)
                {
                    d.startProcess(process);
                    break;
                }
            }
            if (--d.runningProcesses == 0)
                emit allFinished();
        }
        d.disableProcessStop = false;
    }
}

void ProcessManager::stopAll()
{
    d.disableProcessStop = true;
    for (auto &&item : d.processes)
        d.maybeTerminateProcess(item.second);
    d.processes.clear();
    d.disableProcessStop = false;
    if (d.runningProcesses > 0)
    {
        d.runningProcesses = 0;
        emit allFinished();
    }
}

/**/

ProcessManager::Priv::Priv(ProcessManager &processManager)
    : q(processManager)
{}

quint64 ProcessManager::Priv::startOrEnqueue(const std::function<void(QProcess &)> &setProcessParamsFn)
{
    const quint64 id = ++currId;

    QProcess &process = processes[id]; // Create and get reference to new QProcess instance
    process.setProperty("new", true);

    connect(&process, &QProcess::started,
            &q, [=] {
        emit q.stateChanged(id, Started);
    });
    connect(&process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished),
            &q, [=](int exitCode, QProcess::ExitStatus exitStatus) {
        if (QProcess *process = qobject_cast<QProcess *>(q.sender()); process->property("cancelled").toBool())
        {
            emit q.stateChanged(id, Cancelled);
        }
        else
        {
            const State state = (exitStatus == QProcess::NormalExit) ? Finished : Crashed;
            emit q.stateChanged(id, state, exitCode);
        }
        q.stop(id);
    });
    connect(&process, &QProcess::errorOccurred,
            &q, [=](QProcess::ProcessError error) {
        if (error == QProcess::FailedToStart)
        {
            emit q.stateChanged(id, CannotStart);
            q.stop(id);
        }
    }, Qt::QueuedConnection);

    const auto readProcessOutput = [=, &process](const Channel channel) {
        QByteArray line;
        char c = 0;

        while (process.getChar(&c))
        {
            if (c == '\r' || c == '\n')
            {
                if (!line.isEmpty())
                {
                    emit q.newLine(id, line, channel);
                    line.clear();
                }
                continue;
            }
            line += c;
        }

        for (int i = line.length() - 1; i >= 0; --i)
            process.ungetChar(line.at(i));
    };
    connect(&process, &QProcess::readyReadStandardOutput,
            &q, [=, &process] {
        process.setReadChannel(QProcess::StandardOutput);
        readProcessOutput(StdOut);
    });
    connect(&process, &QProcess::readyReadStandardError,
            &q, [=, &process] {
        process.setReadChannel(QProcess::StandardError);
        readProcessOutput(StdErr);
    });

    setProcessParamsFn(process);

    if (runningProcesses < maxProcesses)
        startProcess(processes[id]);

    return id;
}
void ProcessManager::Priv::startProcess(QProcess &process)
{
    const QString command = process.property("command").toString();
    process.setProperty("new", false);
    if (command.isEmpty())
    {
        process.start(QProcess::ReadOnly);
    }
    else
    {
        auto splittedCommand = QProcess::splitCommand(command);
        auto program = splittedCommand.takeFirst();
        process.start(program, splittedCommand, QProcess::ReadOnly);
    }
    ++runningProcesses;
}
void ProcessManager::Priv::maybeTerminateProcess(QProcess &process)
{
    switch (process.state())
    {
        case QProcess::Starting:
            process.waitForStarted();
            [[fallthrough]];
        case QProcess::Running:
            process.setProperty("cancelled", true);
            process.terminate();
            if (!process.waitForFinished(g_waitTimeout))
            {
                process.kill();
                process.waitForFinished(g_waitTimeout);
            }
            break;
        default:
            break;
    }
}

}
