/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <Allocator.hpp>

#include <sys/mman.h>
#include <unistd.h>

namespace QDRE {
namespace Allocator {

constexpr quintptr g_alignment = 32;
constexpr quintptr g_offset = g_alignment / sizeof(quintptr);

static inline quintptr aligned(const quintptr val, const quintptr alignment)
{
    constexpr auto one = static_cast<quintptr>(1);
    return (val + alignment - one) & ~(alignment - one);
}

/**/

void *alloc(const quintptr size)
{
    const quintptr requiredSize = g_alignment + size;
    const quintptr pagesize = getpagesize();
    quintptr *data = static_cast<quintptr *>(mmap(nullptr, aligned(requiredSize, pagesize), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, 0, 0));
    if (reinterpret_cast<quintptr>(data) != static_cast<quintptr>(-1))
    {
        data[0] = size;
        return data + g_offset;
    }
    return nullptr;
}

void free(void *ptr)
{
    if (quintptr *data = static_cast<quintptr *>(ptr))
    {
        data -= g_offset;
        munmap(data, data[0]);
    }
}

}
}
