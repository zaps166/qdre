/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <ParallelRun.hpp>

#include <QtConcurrent/QtConcurrentRun>
#include <QVariant>
#include <QMutex>

namespace QDRE {
namespace ParallelRun {

using namespace std;

void run(const Function &func, const size_t wantedJobs)
{
    const size_t nCpuThreads = QThread::idealThreadCount();
    const size_t nJobs = (wantedJobs == 0)
            ? nCpuThreads
            : qMin(wantedJobs, nCpuThreads);
    vector<QFuture<void>> threads(nJobs - 1);
    QThreadPool pool;
    pool.setMaxThreadCount(threads.size());
    for (size_t jobId = 0; jobId < threads.size(); ++jobId)
        threads[jobId] = QtConcurrent::run(&pool, func, jobId, nJobs);
    func(threads.size(), nJobs);
    for (auto &&thr : threads)
        thr.waitForFinished();
}
void runFor(const size_t n, const ForFunction &func)
{
    run([&](const size_t jobId, const size_t nJobs) {
        const size_t begin = n * (jobId + 0) / nJobs;
        const size_t   end = n * (jobId + 1) / nJobs;
        for (size_t i = begin; i < end; ++i)
            func(i);
    }, n);
}
vector<any> runForWithValue(const size_t n, const ForFunctionWithValue &func)
{
    vector<any> anyArr;
    QMutex mutex;
    run([&](const size_t jobId, const size_t nJobs) {
        const size_t begin = n * (jobId + 0) / nJobs;
        const size_t   end = n * (jobId + 1) / nJobs;
        for (size_t i = begin; i < end; ++i)
        {
            if (any val = func(i); val.has_value())
            {
                QMutexLocker locker(&mutex);
                anyArr.push_back(qMove(val));
                Q_ASSERT(!val.has_value());
            }
        }
    }, n);
    return anyArr;
}

}
}
