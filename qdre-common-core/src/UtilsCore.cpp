/*
    MIT License

    Copyright (c) 2017-2025 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>

#include <UtilsCore.hpp>

#include <QLoggingCategory>
#include <QString>

#include <array>

namespace QDRE {
namespace UtilsCore {

QString bytesToString(const qint64 bytes)
{
    if (bytes < 1024ll)
        return QString::number(bytes) + " B";
    else if (bytes < 1048576ll)
        return QString::number(bytes / 1024.0, 'f', 2) + " KiB";
    else if (bytes < 1073741824ll)
        return QString::number(bytes / 1048576.0, 'f', 2) + " MiB";
    else if (bytes  < 1099511627776ll)
        return QString::number(bytes / 1073741824.0, 'f', 2) + " GiB";
    return QString::number(bytes / 1099511627776.0, 'f', 2) + " TiB";
}

QString timeToString(const double seconds)
{
    if (seconds < 0.0)
        return QString();

    const qint32 intT = seconds;
    const qint32 h = intT / 3600;
    const qint32 m = intT % 3600 / 60;
    const qint32 s = intT % 60;

    QString timStr;
    if (h > 0)
        timStr = QString("%1:").arg(h, 2, 10, QChar('0'));
    timStr += QString("%1:%2").arg(m, 2, 10, QChar('0')).arg(s, 2, 10, QChar('0'));

    return timStr;
}

int dataSaveTimeout()
{
    return 5000;
}

quint32 crc32(const quint8 *data, const int length)
{
    static const auto crcTable = [] {
        std::array<quint32, 256> crcTable;
        for (size_t i = 0; i < crcTable.size(); ++i)
        {
            quint32 c = i;
            for (int k = 0; k < 8; ++k)
                c = (c & 1) ? (0xedb88320L ^ (c >> 1)) : (c >> 1);
            crcTable[i] = c;
        }
        return crcTable;
    }();

    quint32 crc = 0xffffffff;
    for (int i = 0; i < length; ++i)
        crc = crcTable[(crc ^ data[i]) & 0xff] ^ (crc >> 8);
    return crc ^ 0xffffffff;
}

bool checkProcessExists(const QString &process)
{
    auto dir = opendir("/proc");
    if (!dir)
        return false;

    const QByteArray processLocal8Bit = process.toLocal8Bit();
    while (auto dirent = readdir(dir))
    {
        const QByteArray name(dirent->d_name);
        if (name.toInt() <= 0) // Not PID
            continue;

        const int fd = open(("/proc/" + name + "/comm").constData(), O_RDONLY);
        if (fd <= 0)
            continue;

        char comm[256];
        const int bread = read(fd, comm, sizeof(comm) - 1);

        close(fd);

        if (bread > 1)
        {
            comm[bread - 1] = '\0';
            if (qstrcmp(comm, processLocal8Bit) == 0)
                return true;
        }
    }

    closedir(dir);

    return false;
}

}
}
