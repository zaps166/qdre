/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <CommonCoreVisibility.hpp>

#include <QList>
#include <QSet>

namespace QDRE {
namespace UtilsCore {

QDRE_COMMON_CORE_VISIBILITY QString bytesToString(const qint64 bytes);
QDRE_COMMON_CORE_VISIBILITY QString timeToString(const double seconds);

QDRE_COMMON_CORE_VISIBILITY int dataSaveTimeout();

QDRE_COMMON_CORE_VISIBILITY quint32 crc32(const quint8 *data, const int length);

QDRE_COMMON_CORE_VISIBILITY bool checkProcessExists(const QString &process);

template<typename T>
QSet<T> listToSet(const QList<T> &l)
{
    return QSet<T>(l.constBegin(), l.constEnd());
}

}
}
