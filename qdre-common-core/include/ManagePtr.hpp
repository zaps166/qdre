#pragma once

#include <cstdlib>
#include <memory>

namespace QDRE {

template<typename Ptr>
static inline auto managePtr(Ptr *reply)
{
    return std::unique_ptr<Ptr, void(*)(void *)>(reply, free);
}

}
