/*
    MIT License

    Copyright (c) 2017-2019 Błażej Szczygieł

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#pragma once

#include <CommonCoreVisibility.hpp>

#include <QObject>
#include <QVariant>

namespace QDRE {

class QDRE_COMMON_CORE_VISIBILITY IniSettings final : public QObject
{
    Q_OBJECT

    struct Priv;

public:
    enum class Location
    {
        Local,
        Config,
    };

public:
    IniSettings(const Location location,
                const QString &fileName = QString(),
                QObject *parent = nullptr,
                bool saveTimeout = true);
    ~IniSettings();

    void beginGroup(const QString &prefix);
    QString group() const;
    void endGroup();

    QStringList allKeys() const;
    QStringList childKeys() const;

    void setValue(const QString &key, const QVariant &value);
    QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;

    void remove(const QString &key);
    bool contains(const QString &key) const;

private:
    Priv &d;
};

}
